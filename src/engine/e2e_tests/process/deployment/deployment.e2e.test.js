const fs = require('fs');
const path = require('path');
const request = require('supertest')('localhost');
const { startMockEngineProcesses } = require('./startEngines.js');
const { getExpectedEndMatchers, getAllExpectedOutputMatchers } = require('./matchers.js');

jest.setTimeout(60000);

function killEngineProcess(engineProcess) {
  return new Promise((resolve) => {
    engineProcess.kill();
    engineProcess.on('close', () => {
      resolve();
    });
  });
}

// send process in the file with the given definitionId to the given engines
async function deployProcess(definitionId, engines) {
  let enginesList = [];

  if (Array.isArray(engines)) {
    enginesList = engines;
  } else {
    enginesList.push(engines);
  }

  const bpmn = fs.readFileSync(
    path.resolve(__dirname, 'testProcesses', definitionId, `${definitionId}.xml`),
    'utf-8'
  );
  const requests = enginesList.map(async (engine) => {
    await request.put(`:${engine.port}/process/${definitionId}`).send({ bpmn });
  });
  await Promise.all(requests);
}

async function deleteProcess(definitionId, engine) {
  await request.delete(`:${engine.port}/process/${definitionId}`);
}

let engineProcesses = [];
describe('Test deploying a process', () => {
  beforeAll(async () => {
    engineProcesses = await startMockEngineProcesses(6);
  });

  afterAll(async () => {
    // kills all processes and their subprocesses
    const killCommands = engineProcesses.map((engineProcess) =>
      killEngineProcess(engineProcess.process)
    );

    await Promise.all(killCommands);
  });

  beforeEach(() => {
    // allows each test to get only the output that occured while running the test
    engineProcesses.forEach((engineProcess) => engineProcess.resetTestOutputStream());
  });

  test('every machine is reachable', async () => {
    const requests = engineProcesses.map(async (engineProcess) => {
      const response = await request.get(`:${engineProcess.port}/machine`);
      return response;
    });

    const answers = await Promise.all(requests);

    answers.forEach((answer) => {
      expect(answer.status).toBe(200);
    });
  });

  describe('deploying and starting a process on a machine', () => {
    describe('checks log messages for process execution', () => {
      const processes = [
        require('./testProcesses/basicStaticProcess/basicStaticProcess.json'),
        require('./testProcesses/twoEngineStatic/twoEngineStatic.json'),
        require('./testProcesses/threeEngineDynamicHC/threeEngineDynamicHC.json'),
        require('./testProcesses/threeEngineDynamicSC/threeEngineDynamicSC.json'),
      ];

      afterEach(async () => {
        // delete all deployments from all engines
        await Promise.all(
          processes.map(async (process) => {
            await Promise.all(
              engineProcesses.map((engine) => deleteProcess(process.definitionId, engine))
            );
          })
        );
      });

      test.each(processes.map((process) => [process.definitionId, process]))(
        'Execution test for %s',
        async (_, process) => {
          const deployTo = process.deployTo.map((name) =>
            engineProcesses.find((engine) => engine.name === name)
          );

          await deployProcess(process.definitionId, deployTo);

          // make the end of the instance execution awaitable, by creating promises that resolve on certain engine outputs
          const instanceExecutionEndMatchers = getExpectedEndMatchers(process.expectedExecution);
          const instanceEndPromises = Object.entries(instanceExecutionEndMatchers).flatMap(
            ([engineName, matchers]) => {
              const engine = engineProcesses.find((process) => process.name === engineName);
              return engine.setOutputCheckers(matchers);
            }
          );

          const response = await request.post(
            `:${engineProcesses[0].port}/process/${process.definitionId}/instance`
          );

          const { instanceId } = response.body;

          expect(response.status).toBe(201);
          await Promise.all(instanceEndPromises);

          const engineMatchers = getAllExpectedOutputMatchers(
            instanceId,
            process.expectedExecution
          );

          Object.entries(engineMatchers).forEach(([engineName, matchers]) => {
            const engineLogs = engineProcesses
              .find((engine) => engine.name === engineName)
              .getTestOutputStream();

            matchers.forEach((matcher) => {
              expect(engineLogs).toMatch(matcher);
            });
          });
        }
      );
    });

    describe('checks instance-information after process execution', () => {
      describe('dynamic deployment', () => {
        test('three engines - dynamic HC', async () => {
          // execution order: machine 1 -> machine 3 -> machine 2
          const definitionId = 'threeEngineDynamicHC';

          // filter for engines to be used
          const usedEngines = engineProcesses.filter(
            (engine) =>
              engine.name === 'machine1' || engine.name === 'machine2' || engine.name === 'machine3'
          );

          const startingEngine = usedEngines.find((engine) => engine.name === 'machine1');

          await deployProcess(definitionId, startingEngine);

          const response = await request.post(
            `:${startingEngine.port}/process/${definitionId}/instance`
          );

          const { instanceId } = response.body;

          expect(response.status).toBe(201);

          // after starting the process, wait 3 seconds before requesting the state of the instance
          await new Promise((resolve) => setTimeout(() => resolve(), 3000));

          const engineResponses = await Promise.all(
            usedEngines.map((engine) =>
              request
                .get(`:${engine.port}/process/${definitionId}/instance/${instanceId}`)
                .then((response) => ({ ...engine, response }))
            )
          );

          // check for instance information
          engineResponses.forEach((engine) => {
            const instanceInfo = engine.response.body;

            if (engine.name === 'machine1') {
              expect(engine.response.status).toEqual(200);
              expect(instanceInfo.instanceState).toEqual(['FORWARDED']);
              expect(instanceInfo.tokens).toEqual([
                {
                  tokenId: expect.any(String),
                  state: 'FORWARDED',
                  currentFlowElementId: 'Flow_1rsci5r',
                  currentFlowElementStartTime: expect.any(Number),
                  currentFlowNodeProgress: 0,
                  localStartTime: expect.any(Number),
                  localExecutionTime: expect.any(Number),
                  machineHops: 0,
                  deciderStorageRounds: 0,
                  deciderStorageTime: 0,
                  nextMachine: {
                    id: 'machineId3',
                    ip: expect.any(String),
                    port: 33022,
                    name: 'machine3',
                    hostname: expect.any(String),
                    currentlyConnectedEnvironments: expect.any(Array),
                  },
                },
              ]);
              expect(instanceInfo.log).toEqual([
                {
                  tokenId: expect.any(String),
                  flowElementId: 'StartEvent_1',
                  executionState: 'COMPLETED',
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                  machine: {
                    id: 'machineId1',
                    ip: expect.any(String),
                    name: 'machine1',
                    port: 33020,
                  },
                },
                {
                  tokenId: expect.any(String),
                  flowElementId: 'Activity_1y3m10b',
                  executionState: 'COMPLETED',
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                  machine: {
                    id: 'machineId1',
                    ip: expect.any(String),
                    name: 'machine1',
                    port: 33020,
                  },
                },
                {
                  tokenId: expect.any(String),
                  flowElementId: 'Flow_1rsci5r',
                  executionState: 'FORWARDED',
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                  machine: {
                    id: 'machineId1',
                    ip: expect.any(String),
                    name: 'machine1',
                    port: 33020,
                  },
                  nextMachine: {
                    id: 'machineId3',
                    ip: expect.any(String),
                    name: 'machine3',
                    hostname: expect.any(String),
                    currentlyConnectedEnvironments: expect.any(Array),
                    port: 33022,
                  },
                },
              ]);
            }

            if (engine.name === 'machine2') {
              expect(engine.response.status).toEqual(200);
              expect(instanceInfo.instanceState).toEqual(['ENDED']);
              expect(instanceInfo.tokens).toEqual([
                {
                  tokenId: expect.any(String),
                  state: 'ENDED',
                  currentFlowElementId: 'Event_01l350g',
                  currentFlowNodeProgress: 100,
                  currentFlowElementStartTime: expect.any(Number),
                  machineHops: 2,
                  localStartTime: expect.any(Number),
                  localExecutionTime: expect.any(Number),
                  deciderStorageRounds: 0,
                  deciderStorageTime: 0,
                },
              ]);
              expect(instanceInfo.log).toEqual([
                {
                  tokenId: expect.any(String),
                  flowElementId: 'StartEvent_1',
                  executionState: 'COMPLETED',
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                  machine: {
                    id: 'machineId1',
                    ip: expect.any(String),
                    name: 'machine1',
                    port: 33020,
                  },
                },
                {
                  tokenId: expect.any(String),
                  flowElementId: 'Activity_1y3m10b',
                  executionState: 'COMPLETED',
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                  machine: {
                    id: 'machineId1',
                    ip: expect.any(String),
                    name: 'machine1',
                    port: 33020,
                  },
                },
                {
                  tokenId: expect.any(String),
                  flowElementId: 'Activity_1l0spa8',
                  executionState: 'COMPLETED',
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                  machine: {
                    id: 'machineId3',
                    ip: expect.any(String),
                    name: 'machine3',
                    port: 33022,
                  },
                },
                {
                  tokenId: expect.any(String),
                  flowElementId: 'Activity_1hulyn1',
                  executionState: 'COMPLETED',
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                  machine: {
                    id: 'machineId2',
                    ip: expect.any(String),
                    name: 'machine2',
                    port: 33021,
                  },
                },
                {
                  tokenId: expect.any(String),
                  flowElementId: 'Event_01l350g',
                  executionState: 'COMPLETED',
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                  machine: {
                    id: 'machineId2',
                    ip: expect.any(String),
                    name: 'machine2',
                    port: 33021,
                  },
                },
              ]);
            }

            if (engine.name === 'machine3') {
              expect(engine.response.status).toEqual(200);
              expect(instanceInfo.instanceState).toEqual(['FORWARDED']);
              expect(instanceInfo.tokens).toEqual([
                {
                  tokenId: expect.any(String),
                  state: 'FORWARDED',
                  currentFlowElementId: 'Flow_0v67ohp',
                  currentFlowElementStartTime: expect.any(Number),
                  currentFlowNodeProgress: 0,
                  machineHops: 1,
                  localStartTime: expect.any(Number),
                  localExecutionTime: expect.any(Number),
                  deciderStorageRounds: 0,
                  deciderStorageTime: 0,
                  nextMachine: {
                    id: 'machineId2',
                    ip: expect.any(String),
                    port: 33021,
                    name: 'machine2',
                    hostname: expect.any(String),
                    currentlyConnectedEnvironments: expect.any(Array),
                  },
                },
              ]);
              expect(instanceInfo.log).toEqual([
                {
                  tokenId: expect.any(String),
                  flowElementId: 'StartEvent_1',
                  executionState: 'COMPLETED',
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                  machine: {
                    id: 'machineId1',
                    ip: expect.any(String),
                    name: 'machine1',
                    port: 33020,
                  },
                },
                {
                  tokenId: expect.any(String),
                  flowElementId: 'Activity_1y3m10b',
                  executionState: 'COMPLETED',
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                  machine: {
                    id: 'machineId1',
                    ip: expect.any(String),
                    name: 'machine1',
                    port: 33020,
                  },
                },
                {
                  tokenId: expect.any(String),
                  flowElementId: 'Activity_1l0spa8',
                  executionState: 'COMPLETED',
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                  machine: {
                    id: 'machineId3',
                    ip: expect.any(String),
                    name: 'machine3',
                    port: 33022,
                  },
                },
                {
                  tokenId: expect.any(String),
                  flowElementId: 'Flow_0v67ohp',
                  executionState: 'FORWARDED',
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                  machine: {
                    id: 'machineId3',
                    ip: expect.any(String),
                    name: 'machine3',
                    port: 33022,
                  },
                  nextMachine: {
                    hostname: expect.any(String),
                    currentlyConnectedEnvironments: expect.any(Array),
                    id: 'machineId2',
                    ip: expect.any(String),
                    name: 'machine2',
                    port: 33021,
                  },
                },
              ]);
            }
          });
        });

        test('three engines - dynamic SC', async () => {
          // execution order: machine 1 -> machine 3 -> machine 2
          const definitionId = 'threeEngineDynamicSC';

          // filter for engines to be used
          const usedEngines = engineProcesses.filter(
            (engine) =>
              engine.name === 'machine1' || engine.name === 'machine2' || engine.name === 'machine3'
          );

          const startingEngine = usedEngines.find((engine) => engine.name === 'machine1');

          await deployProcess(definitionId, startingEngine);

          const response = await request.post(
            `:${startingEngine.port}/process/${definitionId}/instance`
          );

          const { instanceId } = response.body;

          expect(response.status).toBe(201);

          // after starting the process, wait 10 seconds before requesting the state of the instance
          await new Promise((resolve) => setTimeout(() => resolve(), 10000));

          const engineResponses = await Promise.all(
            usedEngines.map((engine) =>
              request
                .get(`:${engine.port}/process/${definitionId}/instance/${instanceId}`)
                .then((response) => ({ ...engine, response }))
            )
          );

          // check for instance information
          engineResponses.forEach((engine) => {
            const instanceInfo = engine.response.body;

            if (engine.name === 'machine1') {
              expect(engine.response.status).toEqual(200);
              expect(instanceInfo.instanceState).toEqual(['FORWARDED']);
              expect(instanceInfo.tokens).toEqual([
                {
                  tokenId: expect.any(String),
                  state: 'FORWARDED',
                  currentFlowElementId: 'Flow_1o4ewqd',
                  currentFlowElementStartTime: expect.any(Number),
                  currentFlowNodeProgress: 0,
                  localStartTime: expect.any(Number),
                  localExecutionTime: expect.any(Number),
                  machineHops: 0,
                  deciderStorageRounds: 0,
                  deciderStorageTime: 0,
                  nextMachine: {
                    id: 'machineId3',
                    ip: expect.any(String),
                    port: 33022,
                    name: 'machine3',
                    hostname: expect.any(String),
                    currentlyConnectedEnvironments: expect.any(Array),
                  },
                },
              ]);
              expect(instanceInfo.log).toEqual([
                {
                  tokenId: expect.any(String),
                  flowElementId: 'StartEvent_1',
                  executionState: 'COMPLETED',
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                  machine: {
                    id: 'machineId1',
                    ip: expect.any(String),
                    name: 'machine1',
                    port: 33020,
                  },
                },
                {
                  tokenId: expect.any(String),
                  flowElementId: 'Activity_00ovd1m',
                  executionState: 'COMPLETED',
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                  machine: {
                    id: 'machineId1',
                    ip: expect.any(String),
                    name: 'machine1',
                    port: 33020,
                  },
                },
                {
                  tokenId: expect.any(String),
                  flowElementId: 'Flow_1o4ewqd',
                  executionState: 'FORWARDED',
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                  machine: {
                    id: 'machineId1',
                    ip: expect.any(String),
                    name: 'machine1',
                    port: 33020,
                  },
                  nextMachine: {
                    id: 'machineId3',
                    ip: expect.any(String),
                    name: 'machine3',
                    hostname: expect.any(String),
                    currentlyConnectedEnvironments: expect.any(Array),
                    port: 33022,
                  },
                },
              ]);
            }

            if (engine.name === 'machine2') {
              expect(engine.response.status).toEqual(200);
              expect(instanceInfo.instanceState).toEqual(['ENDED']);
              expect(instanceInfo.tokens).toEqual([
                {
                  tokenId: expect.any(String),
                  state: 'ENDED',
                  currentFlowElementId: 'Event_18gxr62',
                  machineHops: 2,
                  currentFlowNodeProgress: 100,
                  currentFlowElementStartTime: expect.any(Number),
                  localStartTime: expect.any(Number),
                  localExecutionTime: expect.any(Number),
                  deciderStorageRounds: 0,
                  deciderStorageTime: 0,
                },
              ]);
              expect(instanceInfo.log).toEqual([
                {
                  tokenId: expect.any(String),
                  flowElementId: 'StartEvent_1',
                  executionState: 'COMPLETED',
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                  machine: {
                    id: 'machineId1',
                    ip: expect.any(String),
                    name: 'machine1',
                    port: 33020,
                  },
                },
                {
                  tokenId: expect.any(String),
                  flowElementId: 'Activity_00ovd1m',
                  executionState: 'COMPLETED',
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                  machine: {
                    id: 'machineId1',
                    ip: expect.any(String),
                    name: 'machine1',
                    port: 33020,
                  },
                },
                {
                  tokenId: expect.any(String),
                  flowElementId: 'Activity_1j4puwu',
                  executionState: 'COMPLETED',
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                  machine: {
                    id: 'machineId3',
                    ip: expect.any(String),
                    name: 'machine3',
                    port: 33022,
                  },
                },
                {
                  tokenId: expect.any(String),
                  flowElementId: 'Activity_0nkqieq',
                  executionState: 'COMPLETED',
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                  machine: {
                    id: 'machineId2',
                    ip: expect.any(String),
                    name: 'machine2',
                    port: 33021,
                  },
                },
                {
                  tokenId: expect.any(String),
                  flowElementId: 'Event_18gxr62',
                  executionState: 'COMPLETED',
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                  machine: {
                    id: 'machineId2',
                    ip: expect.any(String),
                    name: 'machine2',
                    port: 33021,
                  },
                },
              ]);
            }

            if (engine.name === 'machine3') {
              expect(engine.response.status).toEqual(200);
              expect(instanceInfo.instanceState).toEqual(['FORWARDED']);
              expect(instanceInfo.tokens).toEqual([
                {
                  tokenId: expect.any(String),
                  state: 'FORWARDED',
                  currentFlowElementId: 'Flow_0cmy65u',
                  currentFlowElementStartTime: expect.any(Number),
                  currentFlowNodeProgress: 0,
                  localStartTime: expect.any(Number),
                  localExecutionTime: expect.any(Number),
                  machineHops: 1,
                  deciderStorageRounds: 0,
                  deciderStorageTime: 0,
                  nextMachine: {
                    id: 'machineId2',
                    ip: expect.any(String),
                    port: 33021,
                    name: 'machine2',
                    hostname: expect.any(String),
                    currentlyConnectedEnvironments: expect.any(Array),
                  },
                },
              ]);
              expect(instanceInfo.log).toEqual([
                {
                  tokenId: expect.any(String),
                  flowElementId: 'StartEvent_1',
                  executionState: 'COMPLETED',
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                  machine: {
                    id: 'machineId1',
                    ip: expect.any(String),
                    name: 'machine1',
                    port: 33020,
                  },
                },
                {
                  tokenId: expect.any(String),
                  flowElementId: 'Activity_00ovd1m',
                  executionState: 'COMPLETED',
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                  machine: {
                    id: 'machineId1',
                    ip: expect.any(String),
                    name: 'machine1',
                    port: 33020,
                  },
                },
                {
                  tokenId: expect.any(String),
                  flowElementId: 'Activity_1j4puwu',
                  executionState: 'COMPLETED',
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                  machine: {
                    id: 'machineId3',
                    ip: expect.any(String),
                    name: 'machine3',
                    port: 33022,
                  },
                },
                {
                  tokenId: expect.any(String),
                  flowElementId: 'Flow_0cmy65u',
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                  executionState: 'FORWARDED',
                  machine: {
                    id: 'machineId3',
                    ip: expect.any(String),
                    name: 'machine3',
                    port: 33022,
                  },
                  nextMachine: {
                    hostname: expect.any(String),
                    currentlyConnectedEnvironments: expect.any(Array),
                    id: 'machineId2',
                    ip: expect.any(String),
                    name: 'machine2',
                    port: 33021,
                  },
                },
              ]);
            }
          });
        });

        test('stop Instance due to maxTokenStorageRounds constraint for process', async () => {
          /**
           * Instance is executed by machine 1
           * -> at task Activity_0uqog8n, the decider will stop the instance due to unfulfilled constraints
           * -> other token running in parallel will also be stopped
           */

          const definitionId = 'deciderStopInstanceProcess';

          // filter for engines to deploy this process to
          const usedEngine = engineProcesses.find((engine) => engine.name === 'machine1');

          await deployProcess(definitionId, usedEngine);

          const response = await request.post(
            `:${usedEngine.port}/process/${definitionId}/instance`
          );

          const { instanceId } = response.body;

          expect(response.status).toBe(201);

          // after starting the process, wait 3 seconds before requesting the state of the instance
          await new Promise((resolve) => setTimeout(() => resolve(), 4000));

          const engineResponse = await request.get(
            `:${usedEngine.port}/process/${definitionId}/instance/${instanceId}`
          );

          const instanceInfo = engineResponse.body;

          expect(engineResponse.status).toEqual(200);
          expect(instanceInfo.instanceState).toEqual(['ERROR-CONSTRAINT-UNFULFILLED']);
          expect(instanceInfo.tokens).toEqual([
            {
              tokenId: expect.any(String),
              state: 'ERROR-CONSTRAINT-UNFULFILLED',
              currentFlowElementId: 'Flow_1m9r868',
              currentFlowElementStartTime: expect.any(Number),
              currentFlowNodeProgress: 0,
              localStartTime: expect.any(Number),
              localExecutionTime: expect.any(Number),
              machineHops: 0,
              deciderStorageRounds: 2,
              deciderStorageTime: expect.any(Number),
            },
            {
              tokenId: expect.any(String),
              state: 'ERROR-CONSTRAINT-UNFULFILLED',
              currentFlowElementId: 'Activity_066db2t',
              currentFlowElementStartTime: expect.any(Number),
              currentFlowNodeProgress: 0,
              localStartTime: expect.any(Number),
              localExecutionTime: expect.any(Number),
              machineHops: 0,
              deciderStorageRounds: 0,
              deciderStorageTime: 0,
            },
          ]);
          expect(instanceInfo.log).toEqual([
            {
              tokenId: expect.any(String),
              flowElementId: 'StartEvent_1',
              executionState: 'COMPLETED',
              startTime: expect.any(Number),
              endTime: expect.any(Number),
              machine: {
                id: 'machineId1',
                ip: expect.any(String),
                name: 'machine1',
                port: 33020,
              },
            },
            {
              tokenId: expect.any(String),
              flowElementId: 'Gateway_1rj4z55',
              executionState: 'COMPLETED',
              startTime: expect.any(Number),
              endTime: expect.any(Number),
              machine: {
                id: 'machineId1',
                ip: expect.any(String),
                name: 'machine1',
                port: 33020,
              },
            },
            {
              tokenId: expect.any(String),
              flowElementId: 'Flow_1m9r868',
              executionState: 'ERROR-CONSTRAINT-UNFULFILLED',
              errorMessage: 'Instance stopped execution because of: maxTokenStorageRounds',
              startTime: expect.any(Number),
              endTime: expect.any(Number),
              machine: {
                id: 'machineId1',
                ip: expect.any(String),
                name: 'machine1',
                port: 33020,
              },
            },
            {
              tokenId: expect.any(String),
              flowElementId: 'Activity_066db2t',
              executionState: 'ERROR-CONSTRAINT-UNFULFILLED',
              errorMessage: 'Instance stopped execution because of: maxTokenStorageRounds',
              startTime: expect.any(Number),
              endTime: expect.any(Number),
              machine: {
                id: 'machineId1',
                ip: expect.any(String),
                name: 'machine1',
                port: 33020,
              },
            },
          ]);
        });

        test('stop token due to maxTokenStorageRounds constraint for flowNode', async () => {
          /**
           * Instance is executed by 2 engines
           * -> at sequenceFlow Flow_1qga0qa on machine 1, the decider will stop the token due to unfulfilled constraints
           * -> other token running in parallel on machine 2 will not be affected by this
           */

          const definitionId = 'deciderStopTokenProcess';

          // filter for engines used in this process
          const usedEngines = engineProcesses.filter(
            (engine) => engine.name === 'machine1' || engine.name === 'machine2'
          );

          const startingEngine = usedEngines.find((engine) => engine.name === 'machine1');

          await deployProcess(definitionId, startingEngine);

          const response = await request.post(
            `:${startingEngine.port}/process/${definitionId}/instance`
          );

          const { instanceId } = response.body;

          expect(response.status).toBe(201);

          // after starting the process, wait 3 seconds before requesting the state of the instance
          await new Promise((resolve) => setTimeout(() => resolve(), 3000));

          const engineResponses = await Promise.all(
            usedEngines.map((engine) =>
              request
                .get(`:${engine.port}/process/${definitionId}/instance/${instanceId}`)
                .then((response) => ({ ...engine, response }))
            )
          );

          engineResponses.forEach((engine) => {
            const instanceInfo = engine.response.body;

            if (engine.name === 'machine1') {
              expect(engine.response.status).toEqual(200);
              expect(instanceInfo.instanceState).toEqual(['ERROR-CONSTRAINT-UNFULFILLED', 'ENDED']);
              expect(instanceInfo.tokens).toEqual([
                {
                  tokenId: expect.any(String),
                  state: 'ERROR-CONSTRAINT-UNFULFILLED',
                  currentFlowElementId: 'Flow_1qga0qa',
                  currentFlowElementStartTime: expect.any(Number),
                  currentFlowNodeProgress: 0,
                  localStartTime: expect.any(Number),
                  localExecutionTime: expect.any(Number),
                  machineHops: 0,
                  deciderStorageRounds: 2,
                  deciderStorageTime: 2000,
                },
                {
                  tokenId: expect.any(String),
                  state: 'ENDED',
                  currentFlowElementId: 'Event_125goxg',
                  currentFlowNodeProgress: 100,
                  currentFlowElementStartTime: expect.any(Number),
                  machineHops: 0,
                  localStartTime: expect.any(Number),
                  localExecutionTime: expect.any(Number),
                  deciderStorageRounds: 0,
                  deciderStorageTime: 0,
                },
              ]);
              expect(instanceInfo.log).toEqual([
                {
                  tokenId: expect.any(String),
                  flowElementId: 'StartEvent_1',
                  executionState: 'COMPLETED',
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                  machine: {
                    id: 'machineId1',
                    ip: expect.any(String),
                    name: 'machine1',
                    port: 33020,
                  },
                },
                {
                  tokenId: expect.any(String),
                  flowElementId: 'Gateway_0t67gjs',
                  executionState: 'COMPLETED',
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                  machine: {
                    id: 'machineId1',
                    ip: expect.any(String),
                    name: 'machine1',
                    port: 33020,
                  },
                },
                {
                  tokenId: expect.any(String),
                  flowElementId: 'Activity_0fxx8a4',
                  executionState: 'COMPLETED',
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                  machine: {
                    id: 'machineId1',
                    ip: expect.any(String),
                    name: 'machine1',
                    port: 33020,
                  },
                },
                {
                  tokenId: expect.any(String),
                  flowElementId: 'Event_125goxg',
                  executionState: 'COMPLETED',
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                  machine: {
                    id: 'machineId1',
                    ip: expect.any(String),
                    name: 'machine1',
                    port: 33020,
                  },
                },
                {
                  tokenId: expect.any(String),
                  flowElementId: 'Flow_1qga0qa',
                  executionState: 'ERROR-CONSTRAINT-UNFULFILLED',
                  errorMessage: 'Token stopped execution because of: maxTokenStorageRounds',
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                  machine: {
                    id: 'machineId1',
                    ip: expect.any(String),
                    name: 'machine1',
                    port: 33020,
                  },
                },
              ]);
            }
          });
        });

        test('deployment waiting for unfulfilled constraints', async () => {
          /**
           *  Instance will be executed on machine 1 until sequenceFlow Flow_1k0g6oo
           * -> at this sequenceFlow, constraints will be unfulfilled resulting in "reEvaluation rounds"
           * -> instance state is requested while instance is still trying to deploy the next task
           * -> token with state DEPLOYMENT_WAITING and attributes deciderStorageRounds, deciderStorageTime are increased respectively
           */
          const definitionId = 'deploymentWaitingProcess';

          // filter for engines to deploy this process to
          const usedEngine = engineProcesses.find((engine) => engine.name === 'machine1');

          await deployProcess(definitionId, usedEngine);

          const response = await request.post(
            `:${usedEngine.port}/process/${definitionId}/instance`
          );

          const { instanceId } = response.body;

          expect(response.status).toBe(201);

          // after starting the process, wait 3 seconds before requesting the state of the instance
          await new Promise((resolve) => setTimeout(() => resolve(), 3000));

          const engineResponse = await request.get(
            `:${usedEngine.port}/process/${definitionId}/instance/${instanceId}`
          );

          const instanceInfo = engineResponse.body;

          expect(engineResponse.status).toEqual(200);
          expect(instanceInfo.instanceState).toEqual(['DEPLOYMENT-WAITING']);
          expect(instanceInfo.tokens).toHaveLength(1);
          const [token] = instanceInfo.tokens;
          expect(token).toStrictEqual({
            tokenId: expect.any(String),
            state: 'DEPLOYMENT-WAITING',
            currentFlowElementId: 'Flow_1k0g6oo',
            currentFlowElementStartTime: expect.any(Number),
            currentFlowNodeProgress: 0,
            localStartTime: expect.any(Number),
            localExecutionTime: expect.any(Number),
            machineHops: 0,
            deciderStorageRounds: expect.any(Number),
            deciderStorageTime: expect.any(Number),
          });
          expect(token.deciderStorageRounds).toBeGreaterThan(0); // decider is re-evaluating due to unfulfilled constraints
          expect(token.deciderStorageTime).toBeGreaterThan(0); // decider is re-evaluating due to unfulfilled constraints

          expect(instanceInfo.log).toEqual([
            {
              tokenId: expect.any(String),
              flowElementId: 'StartEvent_1',
              executionState: 'COMPLETED',
              startTime: expect.any(Number),
              endTime: expect.any(Number),
              machine: {
                id: 'machineId1',
                ip: expect.any(String),
                name: 'machine1',
                port: 33020,
              },
            },
            {
              tokenId: expect.any(String),
              flowElementId: 'Activity_0ngpal8',
              executionState: 'COMPLETED',
              startTime: expect.any(Number),
              endTime: expect.any(Number),
              machine: {
                id: 'machineId1',
                ip: expect.any(String),
                name: 'machine1',
                port: 33020,
              },
            },
          ]);
        });

        test('LocalMachineOnly profile config - deployment waiting due to unfulfilled constraints on local engine', async () => {
          /**
           *  Instance will be executed only on machine 4 due to profile config LocalMachineOnly
           * -> at flowNode Event_0uhm40l, constraints are unfulfilled locally
           * -> no further execution, neither locally nor on other engine
           * -> instance state is requested while instance is still trying to deploy the next flowNode
           * -> token with state DEPLOYMENT_WAITING and attributes deciderStorageRounds, deciderStorageTime are increased respectively
           */
          const definitionId = 'twoEngineDynamicHC';

          // filter for engines to deploy this process to
          const usedEngine = engineProcesses.find((engine) => engine.name === 'machine4');

          // deploy and start process on machine 4
          await deployProcess(definitionId, usedEngine);

          const response = await request.post(
            `:${usedEngine.port}/process/${definitionId}/instance`
          );

          const { instanceId } = response.body;

          expect(response.status).toBe(201);

          // after starting the process, wait 3 seconds before requesting the state of the instance
          await new Promise((resolve) => setTimeout(() => resolve(), 2000));

          const engineResponse = await request.get(
            `:${usedEngine.port}/process/${definitionId}/instance/${instanceId}`
          );

          const instanceInfo = engineResponse.body;

          expect(engineResponse.status).toEqual(200);
          expect(instanceInfo.instanceState).toEqual(['DEPLOYMENT-WAITING']);
          expect(instanceInfo.tokens).toHaveLength(1);
          const [token] = instanceInfo.tokens;
          expect(token).toStrictEqual({
            tokenId: expect.any(String),
            state: 'DEPLOYMENT-WAITING',
            currentFlowElementId: 'Flow_1i2xfx7',
            currentFlowElementStartTime: expect.any(Number),
            currentFlowNodeProgress: 0,
            localStartTime: expect.any(Number),
            localExecutionTime: expect.any(Number),
            machineHops: 0,
            deciderStorageRounds: expect.any(Number),
            deciderStorageTime: expect.any(Number),
          });
          expect(token.deciderStorageRounds).toBeGreaterThan(0); // decider is re-evaluating due to unfulfilled constraints
          expect(token.deciderStorageTime).toBeGreaterThan(0); // decider is re-evaluating due to unfulfilled constraints

          expect(instanceInfo.log).toEqual([
            {
              tokenId: expect.any(String),
              flowElementId: 'StartEvent_1',
              executionState: 'COMPLETED',
              startTime: expect.any(Number),
              endTime: expect.any(Number),
              machine: {
                id: 'machineId4',
                ip: expect.any(String),
                name: 'machine4',
                port: 33023,
              },
            },
            {
              tokenId: expect.any(String),
              flowElementId: 'Activity_0r3k12t',
              executionState: 'COMPLETED',
              startTime: expect.any(Number),
              endTime: expect.any(Number),
              machine: {
                id: 'machineId4',
                ip: expect.any(String),
                name: 'machine4',
                port: 33023,
              },
            },
            {
              tokenId: expect.any(String),
              flowElementId: 'Activity_0ferk46',
              executionState: 'COMPLETED',
              startTime: expect.any(Number),
              endTime: expect.any(Number),
              machine: {
                id: 'machineId4',
                ip: expect.any(String),
                name: 'machine4',
                port: 33023,
              },
            },
          ]);
        });

        test('PreferLocalMachine profile config - execution only locally until constraints unfulfilled on local engine', async () => {
          /**
           *  Instance will be executed on machine 5 until task Event_0uhm40l
           * -> at this flowNode, constraints are unfulfilled locally
           * -> token will be shifted to machine 1
           */
          const definitionId = 'twoEngineDynamicHC';

          // filter for engines which will be used in this process
          const usedEngines = engineProcesses.filter(
            (engine) => engine.name === 'machine1' || engine.name === 'machine5'
          );

          const startingEngine = engineProcesses.find((engine) => engine.name === 'machine5');

          // deploy and start process on machine 5
          await deployProcess(definitionId, startingEngine);

          const response = await request.post(
            `:${startingEngine.port}/process/${definitionId}/instance`
          );

          const { instanceId } = response.body;

          expect(response.status).toBe(201);

          // after starting the process, wait 2 seconds before requesting the state of the instance
          await new Promise((resolve) => setTimeout(() => resolve(), 2000));

          const engineResponses = await Promise.all(
            usedEngines.map((engine) =>
              request
                .get(`:${engine.port}/process/${definitionId}/instance/${instanceId}`)
                .then((response) => ({ ...engine, response }))
            )
          );

          engineResponses.forEach((engine) => {
            const instanceInfo = engine.response.body;

            if (engine.name === 'machine1') {
              expect(engine.response.status).toEqual(200);
              expect(instanceInfo.instanceState).toEqual(['ENDED']);
              expect(instanceInfo.tokens).toHaveLength(1);
              const [token] = instanceInfo.tokens;
              expect(token).toStrictEqual({
                tokenId: expect.any(String),
                state: 'ENDED',
                currentFlowElementId: 'Event_0uhm40l',
                currentFlowNodeProgress: 100,
                currentFlowElementStartTime: expect.any(Number),
                localStartTime: expect.any(Number),
                localExecutionTime: expect.any(Number),
                machineHops: 1,
                deciderStorageRounds: 0,
                deciderStorageTime: 0,
              });

              expect(instanceInfo.log).toEqual([
                {
                  tokenId: expect.any(String),
                  flowElementId: 'StartEvent_1',
                  executionState: 'COMPLETED',
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                  machine: {
                    id: 'machineId5',
                    ip: expect.any(String),
                    name: 'machine5',
                    port: 33024,
                  },
                },
                {
                  tokenId: expect.any(String),
                  flowElementId: 'Activity_0r3k12t',
                  executionState: 'COMPLETED',
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                  machine: {
                    id: 'machineId5',
                    ip: expect.any(String),
                    name: 'machine5',
                    port: 33024,
                  },
                },
                {
                  tokenId: expect.any(String),
                  flowElementId: 'Activity_0ferk46',
                  executionState: 'COMPLETED',
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                  machine: {
                    id: 'machineId5',
                    ip: expect.any(String),
                    name: 'machine5',
                    port: 33024,
                  },
                },
                {
                  tokenId: expect.any(String),
                  flowElementId: 'Event_0uhm40l',
                  executionState: 'COMPLETED',
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                  machine: {
                    id: 'machineId1',
                    ip: expect.any(String),
                    port: 33020,
                    name: 'machine1',
                  },
                },
              ]);
            }

            if (engine.name === 'machine5') {
              expect(engine.response.status).toEqual(200);
              expect(instanceInfo.instanceState).toEqual(['FORWARDED']);
              expect(instanceInfo.tokens).toHaveLength(1);
              const [token] = instanceInfo.tokens;
              expect(token).toStrictEqual({
                tokenId: expect.any(String),
                state: 'FORWARDED',
                currentFlowElementId: 'Flow_1i2xfx7',
                currentFlowElementStartTime: expect.any(Number),
                currentFlowNodeProgress: 0,
                localStartTime: expect.any(Number),
                localExecutionTime: expect.any(Number),
                machineHops: 0,
                deciderStorageRounds: 0,
                deciderStorageTime: 0,
                nextMachine: {
                  id: 'machineId1',
                  ip: expect.any(String),
                  port: 33020,
                  name: 'machine1',
                  hostname: expect.any(String),
                  currentlyConnectedEnvironments: [],
                },
              });

              expect(instanceInfo.log).toEqual([
                {
                  tokenId: expect.any(String),
                  flowElementId: 'StartEvent_1',
                  executionState: 'COMPLETED',
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                  machine: {
                    id: 'machineId5',
                    ip: expect.any(String),
                    name: 'machine5',
                    port: 33024,
                  },
                },
                {
                  tokenId: expect.any(String),
                  flowElementId: 'Activity_0r3k12t',
                  executionState: 'COMPLETED',
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                  machine: {
                    id: 'machineId5',
                    ip: expect.any(String),
                    name: 'machine5',
                    port: 33024,
                  },
                },
                {
                  tokenId: expect.any(String),
                  flowElementId: 'Activity_0ferk46',
                  executionState: 'COMPLETED',
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                  machine: {
                    id: 'machineId5',
                    ip: expect.any(String),
                    name: 'machine5',
                    port: 33024,
                  },
                },
                {
                  tokenId: expect.any(String),
                  flowElementId: 'Flow_1i2xfx7',
                  executionState: 'FORWARDED',
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                  machine: {
                    id: 'machineId5',
                    ip: expect.any(String),
                    name: 'machine5',
                    port: 33024,
                  },
                  nextMachine: {
                    id: 'machineId1',
                    ip: expect.any(String),
                    port: 33020,
                    name: 'machine1',
                    hostname: expect.any(String),
                    currentlyConnectedEnvironments: [],
                  },
                },
              ]);
            }
          });
        });

        test('maxTimeFlowNode profile config - stop token if time exceed for flowNode', async () => {
          /**
           * on machine 4 the profile config has a maxTimeFlowNode of 1 sec
           * -> task Activity_024hiuu takes above 2 seconds to execute -> profile config unfulfilled
           * -> no further execution for this token, other tokens remain untouched
           */
          const definitionId = 'parallelScriptTaskDynamicProcess';

          const usedEngine = engineProcesses.find((engine) => engine.name === 'machine4');

          await deployProcess(definitionId, usedEngine);

          const response = await request.post(
            `:${usedEngine.port}/process/${definitionId}/instance`
          );

          const { instanceId } = response.body;

          expect(response.status).toBe(201);

          // after starting the process, wait 5 seconds before requesting the state of the instance
          await new Promise((resolve) => setTimeout(() => resolve(), 5000));

          const engineResponse = await request.get(
            `:${usedEngine.port}/process/${definitionId}/instance/${instanceId}`
          );

          expect(engineResponse.status).toEqual(200);
          const instanceInfo = engineResponse.body;
          expect(instanceInfo.instanceState).toEqual(['READY', 'ERROR-CONSTRAINT-UNFULFILLED']);
          expect(instanceInfo.tokens).toEqual([
            {
              tokenId: expect.any(String),
              state: 'READY',
              currentFlowElementId: 'Gateway_1br4fav',
              currentFlowElementStartTime: expect.any(Number),
              currentFlowNodeProgress: 0,
              localStartTime: expect.any(Number),
              localExecutionTime: expect.any(Number),
              machineHops: 0,
              deciderStorageRounds: expect.any(Number),
              deciderStorageTime: expect.any(Number),
            },
            {
              tokenId: expect.any(String),
              state: 'ERROR-CONSTRAINT-UNFULFILLED',
              currentFlowElementId: 'Flow_0ijy7a4',
              currentFlowElementStartTime: expect.any(Number),
              currentFlowNodeProgress: 0,
              localStartTime: expect.any(Number),
              localExecutionTime: expect.any(Number),
              machineHops: 0,
              deciderStorageRounds: expect.any(Number),
              deciderStorageTime: expect.any(Number),
            },
          ]);

          expect(instanceInfo.log).toEqual([
            {
              tokenId: expect.any(String),
              flowElementId: 'StartEvent_1',
              executionState: 'COMPLETED',
              startTime: expect.any(Number),
              endTime: expect.any(Number),
              machine: {
                id: 'machineId4',
                ip: expect.any(String),
                name: 'machine4',
                port: 33023,
              },
            },
            {
              tokenId: expect.any(String),
              flowElementId: 'Gateway_1qgcro8',
              executionState: 'COMPLETED',
              startTime: expect.any(Number),
              endTime: expect.any(Number),
              machine: {
                id: 'machineId4',
                ip: expect.any(String),
                name: 'machine4',
                port: 33023,
              },
            },
            {
              tokenId: expect.any(String),
              flowElementId: 'Activity_13f2plc',
              executionState: 'COMPLETED',
              startTime: expect.any(Number),
              endTime: expect.any(Number),
              machine: {
                id: 'machineId4',
                ip: expect.any(String),
                name: 'machine4',
                port: 33023,
              },
            },
            {
              tokenId: expect.any(String),
              flowElementId: 'Activity_024hiuu',
              executionState: 'COMPLETED',
              startTime: expect.any(Number),
              endTime: expect.any(Number),
              machine: {
                id: 'machineId4',
                ip: expect.any(String),
                name: 'machine4',
                port: 33023,
              },
            },
            {
              tokenId: expect.any(String),
              flowElementId: 'Flow_0ijy7a4',
              executionState: 'ERROR-CONSTRAINT-UNFULFILLED',
              errorMessage: 'Token stopped execution because of: maxTimeFlowNode',
              startTime: expect.any(Number),
              endTime: expect.any(Number),
              machine: {
                id: 'machineId4',
                ip: expect.any(String),
                name: 'machine4',
                port: 33023,
              },
            },
          ]);
        });

        test('maxTimeProcessLocal profile config - stop Instance if time exceed for token', async () => {
          /**
           * on machine 5 the profile config has a maxTimeProcessLocal of 1 sec
           * -> after flowNode Activity_024hiuu the token took above 2 seconds to execute -> profile config unfulfilled
           * -> no further execution of every token on machine 5
           */
          const definitionId = 'parallelScriptTaskDynamicProcess';

          const usedEngine = engineProcesses.find((engine) => engine.name === 'machine5');

          await deployProcess(definitionId, usedEngine);

          const response = await request.post(
            `:${usedEngine.port}/process/${definitionId}/instance`
          );

          const { instanceId } = response.body;

          expect(response.status).toBe(201);

          // after starting the process, wait 5 seconds before requesting the state of the instance
          await new Promise((resolve) => setTimeout(() => resolve(), 5000));

          const engineResponse = await request.get(
            `:${usedEngine.port}/process/${definitionId}/instance/${instanceId}`
          );

          const instanceInfo = engineResponse.body;

          expect(engineResponse.status).toEqual(200);
          expect(instanceInfo.instanceState).toEqual(['ERROR-CONSTRAINT-UNFULFILLED']);
          expect(instanceInfo.tokens).toEqual([
            {
              tokenId: expect.any(String),
              state: 'ERROR-CONSTRAINT-UNFULFILLED',
              currentFlowElementId: 'Gateway_1br4fav',
              currentFlowElementStartTime: expect.any(Number),
              currentFlowNodeProgress: 0,
              localStartTime: expect.any(Number),
              localExecutionTime: expect.any(Number),
              machineHops: 0,
              deciderStorageRounds: expect.any(Number),
              deciderStorageTime: expect.any(Number),
            },
            {
              tokenId: expect.any(String),
              state: 'ERROR-CONSTRAINT-UNFULFILLED',
              currentFlowElementId: 'Flow_0ijy7a4',
              currentFlowElementStartTime: expect.any(Number),
              currentFlowNodeProgress: 0,
              localStartTime: expect.any(Number),
              localExecutionTime: expect.any(Number),
              machineHops: 0,
              deciderStorageRounds: expect.any(Number),
              deciderStorageTime: expect.any(Number),
            },
          ]);

          expect(instanceInfo.log).toEqual([
            {
              tokenId: expect.any(String),
              flowElementId: 'StartEvent_1',
              executionState: 'COMPLETED',
              startTime: expect.any(Number),
              endTime: expect.any(Number),
              machine: {
                id: 'machineId5',
                ip: expect.any(String),
                name: 'machine5',
                port: 33024,
              },
            },
            {
              tokenId: expect.any(String),
              flowElementId: 'Gateway_1qgcro8',
              executionState: 'COMPLETED',
              startTime: expect.any(Number),
              endTime: expect.any(Number),
              machine: {
                id: 'machineId5',
                ip: expect.any(String),
                name: 'machine5',
                port: 33024,
              },
            },
            {
              tokenId: expect.any(String),
              flowElementId: 'Activity_13f2plc',
              executionState: 'COMPLETED',
              startTime: expect.any(Number),
              endTime: expect.any(Number),
              machine: {
                id: 'machineId5',
                ip: expect.any(String),
                name: 'machine5',
                port: 33024,
              },
            },
            {
              tokenId: expect.any(String),
              flowElementId: 'Activity_024hiuu',
              executionState: 'COMPLETED',
              startTime: expect.any(Number),
              endTime: expect.any(Number),
              machine: {
                id: 'machineId5',
                ip: expect.any(String),
                name: 'machine5',
                port: 33024,
              },
            },
            {
              tokenId: expect.any(String),
              flowElementId: 'Gateway_1br4fav',
              executionState: 'ERROR-CONSTRAINT-UNFULFILLED',
              errorMessage: 'Instance stopped execution because of: maxTimeProcessLocal',
              startTime: expect.any(Number),
              endTime: expect.any(Number),
              machine: {
                id: 'machineId5',
                ip: expect.any(String),
                name: 'machine5',
                port: 33024,
              },
            },
            {
              tokenId: expect.any(String),
              flowElementId: 'Flow_0ijy7a4',
              executionState: 'ERROR-CONSTRAINT-UNFULFILLED',
              errorMessage: 'Instance stopped execution because of: maxTimeProcessLocal',
              startTime: expect.any(Number),
              endTime: expect.any(Number),
              machine: {
                id: 'machineId5',
                ip: expect.any(String),
                name: 'machine5',
                port: 33024,
              },
            },
          ]);
        });

        test('maxTimeProcessGlobal profile config - stop Instance if time exceed for global process', async () => {
          /**
           * on machine 6 the profile config has a maxTimeProcessGlobal of 1 sec
           * -> after task Activity_024hiuu the process in sum took 2 seconds to execute -> profile config unfulfilled
           * -> no further execution of every token
           */
          const definitionId = 'parallelScriptTaskDynamicProcess';

          const usedEngine = engineProcesses.find((engine) => engine.name === 'machine6');

          await deployProcess(definitionId, usedEngine);

          const response = await request.post(
            `:${usedEngine.port}/process/${definitionId}/instance`
          );

          const { instanceId } = response.body;

          expect(response.status).toBe(201);

          // after starting the process, wait 5 seconds before requesting the state of the instance
          await new Promise((resolve) => setTimeout(() => resolve(), 5000));

          const engineResponse = await request.get(
            `:${usedEngine.port}/process/${definitionId}/instance/${instanceId}`
          );

          const instanceInfo = engineResponse.body;

          expect(engineResponse.status).toEqual(200);
          expect(instanceInfo.instanceState).toEqual(['ERROR-CONSTRAINT-UNFULFILLED']);
          expect(instanceInfo.tokens).toEqual([
            {
              tokenId: expect.any(String),
              state: 'ERROR-CONSTRAINT-UNFULFILLED',
              currentFlowElementId: 'Gateway_1br4fav',
              currentFlowElementStartTime: expect.any(Number),
              currentFlowNodeProgress: 0,
              localStartTime: expect.any(Number),
              localExecutionTime: expect.any(Number),
              machineHops: 0,
              deciderStorageRounds: expect.any(Number),
              deciderStorageTime: expect.any(Number),
            },
            {
              tokenId: expect.any(String),
              state: 'ERROR-CONSTRAINT-UNFULFILLED',
              currentFlowElementId: 'Flow_0ijy7a4',
              currentFlowElementStartTime: expect.any(Number),
              currentFlowNodeProgress: 0,
              localStartTime: expect.any(Number),
              localExecutionTime: expect.any(Number),
              machineHops: 0,
              deciderStorageRounds: expect.any(Number),
              deciderStorageTime: expect.any(Number),
            },
          ]);

          expect(instanceInfo.log).toEqual([
            {
              tokenId: expect.any(String),
              flowElementId: 'StartEvent_1',
              executionState: 'COMPLETED',
              startTime: expect.any(Number),
              endTime: expect.any(Number),
              machine: {
                id: 'machineId6',
                ip: expect.any(String),
                name: 'machine6',
                port: 33025,
              },
            },
            {
              tokenId: expect.any(String),
              flowElementId: 'Gateway_1qgcro8',
              executionState: 'COMPLETED',
              startTime: expect.any(Number),
              endTime: expect.any(Number),
              machine: {
                id: 'machineId6',
                ip: expect.any(String),
                name: 'machine6',
                port: 33025,
              },
            },
            {
              tokenId: expect.any(String),
              flowElementId: 'Activity_13f2plc',
              executionState: 'COMPLETED',
              startTime: expect.any(Number),
              endTime: expect.any(Number),
              machine: {
                id: 'machineId6',
                ip: expect.any(String),
                name: 'machine6',
                port: 33025,
              },
            },
            {
              tokenId: expect.any(String),
              flowElementId: 'Activity_024hiuu',
              executionState: 'COMPLETED',
              startTime: expect.any(Number),
              endTime: expect.any(Number),
              machine: {
                id: 'machineId6',
                ip: expect.any(String),
                name: 'machine6',
                port: 33025,
              },
            },
            {
              tokenId: expect.any(String),
              flowElementId: 'Gateway_1br4fav',
              executionState: 'ERROR-CONSTRAINT-UNFULFILLED',
              errorMessage: 'Instance stopped execution because of: maxTimeProcessGlobal',
              startTime: expect.any(Number),
              endTime: expect.any(Number),
              machine: {
                id: 'machineId6',
                ip: expect.any(String),
                name: 'machine6',
                port: 33025,
              },
            },
            {
              tokenId: expect.any(String),
              flowElementId: 'Flow_0ijy7a4',
              executionState: 'ERROR-CONSTRAINT-UNFULFILLED',
              errorMessage: 'Instance stopped execution because of: maxTimeProcessGlobal',
              startTime: expect.any(Number),
              endTime: expect.any(Number),
              machine: {
                id: 'machineId6',
                ip: expect.any(String),
                name: 'machine6',
                port: 33025,
              },
            },
          ]);
        });
      });

      describe('static deployment', () => {
        test('basic process on single engine', async () => {
          const definitionId = 'basicStaticProcess';

          const usedEngine = engineProcesses.find((engine) => engine.name === 'machine1');

          await deployProcess(definitionId, usedEngine);

          const response = await request.post(
            `:${usedEngine.port}/process/${definitionId}/instance`
          );

          const { instanceId } = response.body;

          expect(response.status).toBe(201);

          // after starting the process, wait 2 seconds before requesting the state of the instance
          // maybe think about a better solution: when to request the state after instance was started?
          await new Promise((resolve) => setTimeout(() => resolve(), 2000));

          const engineResponse = await request.get(
            `:${usedEngine.port}/process/${definitionId}/instance/${instanceId}`
          );

          const instanceInfo = engineResponse.body;

          expect(engineResponse.status).toEqual(200);
          expect(instanceInfo.instanceState).toEqual(['ENDED']);
          expect(instanceInfo.tokens).toEqual([
            {
              tokenId: expect.any(String),
              state: 'ENDED',
              currentFlowElementId: 'Event_0maz5yf',
              currentFlowNodeProgress: 100,
              currentFlowElementStartTime: expect.any(Number),
              localStartTime: expect.any(Number),
              localExecutionTime: expect.any(Number),
              machineHops: 0,
              deciderStorageRounds: 0,
              deciderStorageTime: 0,
            },
          ]);
          expect(instanceInfo.log).toEqual([
            {
              tokenId: expect.any(String),
              flowElementId: 'StartEvent_1',
              executionState: 'COMPLETED',
              startTime: expect.any(Number),
              endTime: expect.any(Number),
              machine: {
                id: 'machineId1',
                ip: expect.any(String),
                name: 'machine1',
                port: 33020,
              },
            },
            {
              tokenId: expect.any(String),
              flowElementId: 'Activity_0xfbs3r',
              executionState: 'COMPLETED',
              startTime: expect.any(Number),
              endTime: expect.any(Number),
              machine: {
                id: 'machineId1',
                ip: expect.any(String),
                name: 'machine1',
                port: 33020,
              },
            },
            {
              tokenId: expect.any(String),
              flowElementId: 'Event_0maz5yf',
              executionState: 'COMPLETED',
              startTime: expect.any(Number),
              endTime: expect.any(Number),
              machine: {
                id: 'machineId1',
                ip: expect.any(String),
                name: 'machine1',
                port: 33020,
              },
            },
          ]);
        });

        test('two engines', async () => {
          // execution order: machine 1 -> machine 2
          const definitionId = 'twoEngineStatic';

          const usedEngines = engineProcesses.filter(
            (engine) => engine.name === 'machine1' || engine.name === 'machine2'
          );

          const startingEngine = usedEngines.find((engine) => engine.name === 'machine1');

          await deployProcess(definitionId, usedEngines);

          const response = await request.post(
            `:${startingEngine.port}/process/${definitionId}/instance`
          );

          const { instanceId } = response.body;

          expect(response.status).toBe(201);

          // after starting the process, wait 2 seconds before requesting the state of the instance
          await new Promise((resolve) => setTimeout(() => resolve(), 2000));

          const engineResponses = await Promise.all(
            usedEngines.map((engine) =>
              request
                .get(`:${engine.port}/process/${definitionId}/instance/${instanceId}`)
                .then((response) => ({ ...engine, response }))
            )
          );

          // check for instance information
          engineResponses.forEach((engine) => {
            const instanceInfo = engine.response.body;

            if (engine.name === 'machine1') {
              expect(engine.response.status).toEqual(200);
              expect(instanceInfo.instanceState).toEqual(['FORWARDED']);
              expect(instanceInfo.tokens).toEqual([
                {
                  tokenId: expect.any(String),
                  state: 'FORWARDED',
                  currentFlowElementId: 'Flow_0ioijmn',
                  currentFlowElementStartTime: expect.any(Number),
                  currentFlowNodeProgress: 0,
                  localStartTime: expect.any(Number),
                  localExecutionTime: expect.any(Number),
                  machineHops: 0,
                  deciderStorageRounds: 0,
                  deciderStorageTime: 0,
                  nextMachine: {
                    id: 'machineId2',
                    ip: expect.any(String),
                    port: 33021,
                    name: 'machine2',
                  },
                },
              ]);
              expect(instanceInfo.log).toEqual([
                {
                  tokenId: expect.any(String),
                  flowElementId: 'StartEvent_1',
                  executionState: 'COMPLETED',
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                  machine: {
                    id: 'machineId1',
                    ip: expect.any(String),
                    name: 'machine1',
                    port: 33020,
                  },
                },
                {
                  tokenId: expect.any(String),
                  flowElementId: 'Activity_0j1fp4s',
                  executionState: 'COMPLETED',
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                  machine: {
                    id: 'machineId1',
                    ip: expect.any(String),
                    name: 'machine1',
                    port: 33020,
                  },
                },
                {
                  tokenId: expect.any(String),
                  flowElementId: 'Flow_0ioijmn',
                  executionState: 'FORWARDED',
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                  machine: {
                    id: 'machineId1',
                    ip: expect.any(String),
                    name: 'machine1',
                    port: 33020,
                  },
                  nextMachine: {
                    id: 'machineId2',
                    ip: expect.any(String),
                    name: 'machine2',
                    port: 33021,
                  },
                },
              ]);
            }

            if (engine.name === 'machine2') {
              expect(engine.response.status).toEqual(200);
              expect(instanceInfo.instanceState).toEqual(['ENDED']);
              expect(instanceInfo.tokens).toEqual([
                {
                  tokenId: expect.any(String),
                  state: 'ENDED',
                  currentFlowElementId: 'Event_17thomc',
                  currentFlowNodeProgress: 100,
                  currentFlowElementStartTime: expect.any(Number),
                  localStartTime: expect.any(Number),
                  localExecutionTime: expect.any(Number),
                  machineHops: 1,
                  deciderStorageRounds: 0,
                  deciderStorageTime: 0,
                },
              ]);
              expect(instanceInfo.log).toEqual([
                {
                  tokenId: expect.any(String),
                  flowElementId: 'StartEvent_1',
                  executionState: 'COMPLETED',
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                  machine: {
                    id: 'machineId1',
                    ip: expect.any(String),
                    name: 'machine1',
                    port: 33020,
                  },
                },
                {
                  tokenId: expect.any(String),
                  flowElementId: 'Activity_0j1fp4s',
                  executionState: 'COMPLETED',
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                  machine: {
                    id: 'machineId1',
                    ip: expect.any(String),
                    name: 'machine1',
                    port: 33020,
                  },
                },
                {
                  tokenId: expect.any(String),
                  flowElementId: 'Activity_06wlm0q',
                  executionState: 'COMPLETED',
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                  machine: {
                    id: 'machineId2',
                    ip: expect.any(String),
                    name: 'machine2',
                    port: 33021,
                  },
                },
                {
                  tokenId: expect.any(String),
                  flowElementId: 'Event_17thomc',
                  executionState: 'COMPLETED',
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                  machine: {
                    id: 'machineId2',
                    ip: expect.any(String),
                    name: 'machine2',
                    port: 33021,
                  },
                },
              ]);
            }

            if (engine.name === 'machine3') {
              expect(engine.response.status).toEqual(404);
            }
          });
        });

        test('two engines with variables', async () => {
          /**
           * Instance is executed by 2 engines which change variables
           * -> when passing token to different machine, previous updated variables will also be transmitted
           * -> variable "a", updated at script task Activity_018ajzu on machine 2 is updated respectively
           * on machine 1 when passing token
           */

          // execution order: machine 1 -> machine 2
          const definitionId = 'twoEngineVariablesStatic';

          const usedEngines = engineProcesses.filter(
            (engine) => engine.name === 'machine1' || engine.name === 'machine2'
          );

          const startingEngine = usedEngines.find((engine) => engine.name === 'machine1');

          await deployProcess(definitionId, usedEngines);

          const response = await request.post(
            `:${startingEngine.port}/process/${definitionId}/instance`
          );

          const { instanceId } = response.body;

          expect(response.status).toBe(201);

          // after starting the process, wait 2 seconds before requesting the state of the instance
          await new Promise((resolve) => setTimeout(() => resolve(), 3000));

          const engineResponses = await Promise.all(
            engineProcesses.map((engine) =>
              request
                .get(`:${engine.port}/process/${definitionId}/instance/${instanceId}`)
                .then((response) => ({ ...engine, response }))
            )
          );

          // check for instance information
          engineResponses.forEach((engine) => {
            const instanceInfo = engine.response.body;

            if (engine.name === 'machine1') {
              expect(engine.response.status).toEqual(200);
              expect(instanceInfo.instanceState).toEqual(['ENDED']);
              expect(instanceInfo.tokens).toEqual([
                {
                  tokenId: expect.any(String),
                  state: 'ENDED',
                  currentFlowElementId: 'Event_02s5s5e',
                  currentFlowNodeProgress: 100,
                  currentFlowElementStartTime: expect.any(Number),
                  localStartTime: expect.any(Number),
                  localExecutionTime: expect.any(Number),
                  machineHops: 2,
                  deciderStorageRounds: 0,
                  deciderStorageTime: 0,
                },
              ]);
              expect(instanceInfo.variables).toEqual({
                a: {
                  log: [
                    {
                      changedBy: 'Activity_1q040zy',
                      changedTime: expect.any(Number),
                    },
                    {
                      changedBy: 'Activity_018ajzu', // changed on machine 2
                      changedTime: expect.any(Number),
                      oldValue: 5,
                    },
                  ],
                  value: 50,
                },
                b: {
                  log: [
                    {
                      changedBy: 'Activity_1q040zy',
                      changedTime: expect.any(Number),
                    },
                    {
                      changedBy: 'Activity_1okjejr',
                      changedTime: expect.any(Number),
                      oldValue: 10,
                    },
                  ],
                  value: 100,
                },
              });
              expect(instanceInfo.log).toEqual([
                {
                  tokenId: expect.any(String),
                  flowElementId: 'StartEvent_1',
                  executionState: 'COMPLETED',
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                  machine: {
                    id: 'machineId1',
                    ip: expect.any(String),
                    name: 'machine1',
                    port: 33020,
                  },
                },
                {
                  tokenId: expect.any(String),
                  flowElementId: 'Activity_1q040zy',
                  executionState: 'COMPLETED',
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                  machine: {
                    id: 'machineId1',
                    ip: expect.any(String),
                    name: 'machine1',
                    port: 33020,
                  },
                },
                {
                  tokenId: expect.any(String),
                  flowElementId: 'Gateway_01qoyez',
                  executionState: 'COMPLETED',
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                  machine: {
                    id: 'machineId1',
                    ip: expect.any(String),
                    name: 'machine1',
                    port: 33020,
                  },
                },
                {
                  tokenId: expect.any(String),
                  flowElementId: 'Activity_1okjejr',
                  executionState: 'COMPLETED',
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                  machine: {
                    id: 'machineId1',
                    ip: expect.any(String),
                    name: 'machine1',
                    port: 33020,
                  },
                },
                {
                  tokenId: expect.any(String),
                  flowElementId: 'Flow_09ovpq4',
                  executionState: 'FORWARDED',
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                  machine: {
                    id: 'machineId1',
                    ip: expect.any(String),
                    name: 'machine1',
                    port: 33020,
                  },
                  nextMachine: {
                    id: 'machineId2',
                    ip: expect.any(String),
                    name: 'machine2',
                    port: 33021,
                  },
                },
                {
                  tokenId: expect.any(String),
                  flowElementId: 'Activity_018ajzu',
                  executionState: 'COMPLETED',
                  machine: {
                    id: 'machineId2',
                    ip: expect.any(String),
                    name: 'machine2',
                    port: 33021,
                  },
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                },
                {
                  tokenId: expect.any(String),
                  flowElementId: 'Gateway_1yoqb3e',
                  executionState: 'COMPLETED',
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                  machine: {
                    id: 'machineId1',
                    ip: expect.any(String),
                    name: 'machine1',
                    port: 33020,
                  },
                },
                {
                  tokenId: expect.any(String),
                  flowElementId: 'Event_02s5s5e',
                  executionState: 'COMPLETED',
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                  machine: {
                    id: 'machineId1',
                    ip: expect.any(String),
                    name: 'machine1',
                    port: 33020,
                  },
                },
              ]);
            }

            if (engine.name === 'machine2') {
              expect(engine.response.status).toEqual(200);
              expect(instanceInfo.instanceState).toEqual(['FORWARDED']);
              expect(instanceInfo.tokens).toEqual([
                {
                  tokenId: expect.any(String),
                  state: 'FORWARDED',
                  currentFlowElementId: 'Flow_1u0p01n',
                  currentFlowElementStartTime: expect.any(Number),
                  currentFlowNodeProgress: 0,
                  localStartTime: expect.any(Number),
                  localExecutionTime: expect.any(Number),
                  machineHops: 1,
                  deciderStorageRounds: 0,
                  deciderStorageTime: 0,
                  nextMachine: {
                    id: 'machineId1',
                    ip: expect.any(String),
                    name: 'machine1',
                    port: 33020,
                  },
                },
              ]);
              expect(instanceInfo.variables).toEqual({
                a: {
                  log: [
                    {
                      changedBy: 'Activity_1q040zy',
                      changedTime: expect.any(Number),
                    },
                    {
                      changedBy: 'Activity_018ajzu',
                      changedTime: expect.any(Number),
                      oldValue: 5,
                    },
                  ],
                  value: 50,
                },
                b: {
                  log: [
                    {
                      changedBy: 'Activity_1q040zy',
                      changedTime: expect.any(Number),
                    },
                  ],
                  value: 10,
                },
              });
              expect(instanceInfo.log).toEqual([
                {
                  tokenId: expect.any(String),
                  flowElementId: 'StartEvent_1',
                  executionState: 'COMPLETED',
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                  machine: {
                    id: 'machineId1',
                    ip: expect.any(String),
                    name: 'machine1',
                    port: 33020,
                  },
                },
                {
                  tokenId: expect.any(String),
                  flowElementId: 'Activity_1q040zy',
                  executionState: 'COMPLETED',
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                  machine: {
                    id: 'machineId1',
                    ip: expect.any(String),
                    name: 'machine1',
                    port: 33020,
                  },
                },
                {
                  tokenId: expect.any(String),
                  flowElementId: 'Gateway_01qoyez',
                  executionState: 'COMPLETED',
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                  machine: {
                    id: 'machineId1',
                    ip: expect.any(String),
                    name: 'machine1',
                    port: 33020,
                  },
                },
                {
                  tokenId: expect.any(String),
                  flowElementId: 'Activity_018ajzu',
                  executionState: 'COMPLETED',
                  machine: {
                    id: 'machineId2',
                    ip: expect.any(String),
                    name: 'machine2',
                    port: 33021,
                  },
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                },
                {
                  tokenId: expect.any(String),
                  flowElementId: 'Flow_1u0p01n',
                  executionState: 'FORWARDED',
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                  machine: {
                    id: 'machineId2',
                    ip: expect.any(String),
                    name: 'machine2',
                    port: 33021,
                  },
                  nextMachine: {
                    id: 'machineId1',
                    ip: expect.any(String),
                    name: 'machine1',
                    port: 33020,
                  },
                },
              ]);
            }

            if (engine.name === 'machine3') {
              expect(engine.response.status).toEqual(404);
            }
          });
        });

        test('two engines with termination endevent', async () => {
          /**
           * Running Instance will be aborted due to termination-event Event_04sczc5 triggered on machine 2
           * -> running token on machine 1 will be aborted immediately
           */

          // execution order: machine 1 -> machine 2
          const definitionId = 'twoEngineTerminationStatic';

          const usedEngines = engineProcesses.filter(
            (engine) => engine.name === 'machine1' || engine.name === 'machine2'
          );

          const startingEngine = usedEngines.find((engine) => engine.name === 'machine1');

          await deployProcess(definitionId, usedEngines);

          const response = await request.post(
            `:${startingEngine.port}/process/${definitionId}/instance`
          );

          const { instanceId } = response.body;

          expect(response.status).toBe(201);

          // after starting the process, wait 2 seconds before requesting the state of the instance
          await new Promise((resolve) => setTimeout(() => resolve(), 2000));

          const engineResponses = await Promise.all(
            usedEngines.map((engine) =>
              request
                .get(`:${engine.port}/process/${definitionId}/instance/${instanceId}`)
                .then((response) => ({ ...engine, response }))
            )
          );

          // check for instance information
          engineResponses.forEach((engine) => {
            const instanceInfo = engine.response.body;

            if (engine.name === 'machine1') {
              expect(engine.response.status).toEqual(200);
              expect(instanceInfo.instanceState).toEqual(['FORWARDED', 'ABORTED']);
              expect(instanceInfo.tokens).toEqual([
                {
                  tokenId: expect.any(String),
                  state: 'FORWARDED',
                  currentFlowElementId: 'Flow_03y5vg1',
                  currentFlowElementStartTime: expect.any(Number),
                  currentFlowNodeProgress: 0,
                  localStartTime: expect.any(Number),
                  localExecutionTime: expect.any(Number),
                  machineHops: 0,
                  deciderStorageRounds: 0,
                  deciderStorageTime: 0,
                  nextMachine: {
                    id: 'machineId2',
                    ip: expect.any(String),
                    name: 'machine2',
                    port: 33021,
                  },
                },
                {
                  tokenId: expect.any(String),
                  state: 'ABORTED',
                  currentFlowElementId: 'Activity_119hhzf',
                  currentFlowNodeProgress: 0,
                  currentFlowElementStartTime: expect.any(Number),
                  localStartTime: expect.any(Number),
                  localExecutionTime: expect.any(Number),
                  machineHops: 0,
                  deciderStorageRounds: 0,
                  deciderStorageTime: 0,
                },
              ]);
              expect(instanceInfo.log).toEqual([
                {
                  tokenId: expect.any(String),
                  flowElementId: 'StartEvent_1',
                  executionState: 'COMPLETED',
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                  machine: {
                    id: 'machineId1',
                    ip: expect.any(String),
                    name: 'machine1',
                    port: 33020,
                  },
                },
                {
                  tokenId: expect.any(String),
                  flowElementId: 'Gateway_0ewmf7i',
                  executionState: 'COMPLETED',
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                  machine: {
                    id: 'machineId1',
                    ip: expect.any(String),
                    name: 'machine1',
                    port: 33020,
                  },
                },
                {
                  tokenId: expect.any(String),
                  flowElementId: 'Activity_0ddq1cb',
                  executionState: 'COMPLETED',
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                  machine: {
                    id: 'machineId1',
                    ip: expect.any(String),
                    name: 'machine1',
                    port: 33020,
                  },
                },
                {
                  tokenId: expect.any(String),
                  flowElementId: 'Flow_03y5vg1',
                  executionState: 'FORWARDED',
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                  machine: {
                    id: 'machineId1',
                    ip: expect.any(String),
                    name: 'machine1',
                    port: 33020,
                  },
                  nextMachine: {
                    id: 'machineId2',
                    ip: expect.any(String),
                    name: 'machine2',
                    port: 33021,
                  },
                },
                {
                  tokenId: expect.any(String),
                  flowElementId: 'Activity_119hhzf',
                  executionState: 'ABORTED',
                  machine: {
                    id: 'machineId1',
                    ip: expect.any(String),
                    name: 'machine1',
                    port: 33020,
                  },
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                },
              ]);
            }

            if (engine.name === 'machine2') {
              expect(engine.response.status).toEqual(200);
              expect(instanceInfo.instanceState).toEqual(['ENDED']);
              expect(instanceInfo.tokens).toEqual([
                {
                  tokenId: expect.any(String),
                  state: 'ENDED',
                  currentFlowElementId: 'Event_04sczc5',
                  currentFlowNodeProgress: 100,
                  currentFlowElementStartTime: expect.any(Number),
                  localStartTime: expect.any(Number),
                  localExecutionTime: expect.any(Number),
                  machineHops: 1,
                  deciderStorageRounds: 0,
                  deciderStorageTime: 0,
                },
              ]);
              expect(instanceInfo.log).toEqual([
                {
                  tokenId: expect.any(String),
                  flowElementId: 'StartEvent_1',
                  executionState: 'COMPLETED',
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                  machine: {
                    id: 'machineId1',
                    ip: expect.any(String),
                    name: 'machine1',
                    port: 33020,
                  },
                },
                {
                  tokenId: expect.any(String),
                  flowElementId: 'Gateway_0ewmf7i',
                  executionState: 'COMPLETED',
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                  machine: {
                    id: 'machineId1',
                    ip: expect.any(String),
                    name: 'machine1',
                    port: 33020,
                  },
                },
                {
                  tokenId: expect.any(String),
                  flowElementId: 'Activity_1b1xntr',
                  executionState: 'COMPLETED',
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                  machine: {
                    id: 'machineId2',
                    ip: expect.any(String),
                    name: 'machine2',
                    port: 33021,
                  },
                },
                {
                  tokenId: expect.any(String),
                  flowElementId: 'Event_04sczc5', //termination end event
                  executionState: 'COMPLETED',
                  startTime: expect.any(Number),
                  endTime: expect.any(Number),
                  machine: {
                    id: 'machineId2',
                    ip: expect.any(String),
                    name: 'machine2',
                    port: 33021,
                  },
                },
              ]);
            }
          });
        });

        test('pause process at task', async () => {
          /**
           * Running Instance will be paused (via provided interface) while executing script task Activity_1m0u15u
           * -> instance state is supposed to be PAUSED
           * -> executed task will be finished, but next element won't
           */

          const definitionId = 'scriptTaskStaticProcess';

          const usedEngine = engineProcesses.find((engine) => engine.name === 'machine1');

          await deployProcess(definitionId, usedEngine);

          const response = await request.post(
            `:${usedEngine.port}/process/${definitionId}/instance`
          );

          const { instanceId } = response.body;

          expect(response.status).toBe(201);

          await new Promise((resolve) => setTimeout(() => resolve(), 1000));

          const pauseResponse = await request
            .put(`:${usedEngine.port}/process/${definitionId}/instance/${instanceId}/instanceState`)
            .send({ instanceState: 'paused' });

          expect(pauseResponse.status).toBe(200);

          // after pausing the process, wait 2 seconds before requesting the state of the instance
          await new Promise((resolve) => setTimeout(() => resolve(), 2000));

          const engineResponse = await request.get(
            `:${usedEngine.port}/process/${definitionId}/instance/${instanceId}`
          );

          const instanceInfo = engineResponse.body;

          expect(engineResponse.status).toEqual(200);
          expect(instanceInfo.instanceState).toEqual(['PAUSED']);
          expect(instanceInfo.tokens).toEqual([
            {
              tokenId: expect.any(String),
              state: 'READY',
              currentFlowElementId: 'Flow_1t4tcmh',
              currentFlowElementStartTime: expect.any(Number),
              currentFlowNodeProgress: 0,
              localStartTime: expect.any(Number),
              localExecutionTime: expect.any(Number),
              machineHops: 0,
              deciderStorageRounds: 0,
              deciderStorageTime: 0,
            },
          ]);
          expect(instanceInfo.log).toEqual([
            {
              tokenId: expect.any(String),
              flowElementId: 'StartEvent_1',
              executionState: 'COMPLETED',
              startTime: expect.any(Number),
              endTime: expect.any(Number),
              machine: {
                id: 'machineId1',
                ip: expect.any(String),
                name: 'machine1',
                port: 33020,
              },
            },
            {
              tokenId: expect.any(String),
              flowElementId: 'Activity_1m0u15u',
              executionState: 'COMPLETED',
              startTime: expect.any(Number),
              endTime: expect.any(Number),
              machine: {
                id: 'machineId1',
                ip: expect.any(String),
                name: 'machine1',
                port: 33020,
              },
            },
          ]);
        });

        test('RESUME process at task', async () => {
          /**
           * Running Instance will be paused (via provided interface) while executing script task Activity_1m0u15u
           * and resumed again
           */

          const definitionId = 'scriptTaskStaticProcess';

          const usedEngine = engineProcesses.find((engine) => engine.name === 'machine1');

          await deployProcess(definitionId, usedEngine);

          const response = await request.post(
            `:${usedEngine.port}/process/${definitionId}/instance`
          );

          const { instanceId } = response.body;

          expect(response.status).toBe(201);

          await new Promise((resolve) => setTimeout(() => resolve(), 1000));

          const pauseResponse = await request
            .put(`:${usedEngine.port}/process/${definitionId}/instance/${instanceId}/instanceState`)
            .send({ instanceState: 'paused' });

          expect(pauseResponse.status).toBe(200);

          // after pausing the process, wait 2 seconds before resuming again
          await new Promise((resolve) => setTimeout(() => resolve(), 2000));

          const resumeResponse = await request
            .put(`:${usedEngine.port}/process/${definitionId}/instance/${instanceId}/instanceState`)
            .send({ instanceState: 'resume' });

          expect(resumeResponse.status).toBe(200);

          // after resuming the process, wait 2 seconds before requesting the state
          await new Promise((resolve) => setTimeout(() => resolve(), 2000));

          const engineResponse = await request.get(
            `:${usedEngine.port}/process/${definitionId}/instance/${instanceId}`
          );

          const instanceInfo = engineResponse.body;

          expect(engineResponse.status).toEqual(200);
          expect(instanceInfo.instanceState).toEqual(['ENDED']);
          expect(instanceInfo.tokens).toEqual([
            {
              tokenId: expect.any(String),
              state: 'ENDED',
              currentFlowElementId: 'Event_1yprmoh',
              currentFlowElementStartTime: expect.any(Number),
              currentFlowNodeProgress: 100,
              localStartTime: expect.any(Number),
              localExecutionTime: expect.any(Number),
              machineHops: 0,
              deciderStorageRounds: 0,
              deciderStorageTime: 0,
            },
          ]);
          expect(instanceInfo.log).toEqual([
            {
              tokenId: expect.any(String),
              flowElementId: 'StartEvent_1',
              executionState: 'COMPLETED',
              startTime: expect.any(Number),
              endTime: expect.any(Number),
              machine: {
                id: 'machineId1',
                ip: expect.any(String),
                name: 'machine1',
                port: 33020,
              },
            },
            {
              tokenId: expect.any(String),
              flowElementId: 'Activity_1m0u15u',
              executionState: 'COMPLETED',
              startTime: expect.any(Number),
              endTime: expect.any(Number),
              machine: {
                id: 'machineId1',
                ip: expect.any(String),
                name: 'machine1',
                port: 33020,
              },
            },
            {
              tokenId: expect.any(String),
              flowElementId: 'Activity_1h9twnk',
              executionState: 'COMPLETED',
              startTime: expect.any(Number),
              endTime: expect.any(Number),
              machine: {
                id: 'machineId1',
                ip: expect.any(String),
                name: 'machine1',
                port: 33020,
              },
            },
            {
              tokenId: expect.any(String),
              flowElementId: 'Event_1yprmoh',
              executionState: 'COMPLETED',
              startTime: expect.any(Number),
              endTime: expect.any(Number),
              machine: {
                id: 'machineId1',
                ip: expect.any(String),
                name: 'machine1',
                port: 33020,
              },
            },
          ]);
        });

        test('pause process at catching event', async () => {
          /**
           * Running Instance will be paused (via provided interface) while executing catching event Event_0l2tx82
           * -> instance state is supposed to be PAUSED, token state remain untouched
           * -> executed task will be paused immediately
           */

          const definitionId = 'timerCatchingStaticProcess';

          const usedEngine = engineProcesses.find((engine) => engine.name === 'machine1');

          await deployProcess(definitionId, usedEngine);

          const response = await request.post(
            `:${usedEngine.port}/process/${definitionId}/instance`
          );

          const { instanceId } = response.body;

          expect(response.status).toBe(201);

          await new Promise((resolve) => setTimeout(() => resolve(), 1000));

          const pauseResponse = await request
            .put(`:${usedEngine.port}/process/${definitionId}/instance/${instanceId}/instanceState`)
            .send({ instanceState: 'paused' });

          expect(pauseResponse.status).toBe(200);

          // after pausing the process, wait 2 seconds before requesting the state of the instance
          await new Promise((resolve) => setTimeout(() => resolve(), 2000));

          const engineResponse = await request.get(
            `:${usedEngine.port}/process/${definitionId}/instance/${instanceId}`
          );

          const instanceInfo = engineResponse.body;

          expect(engineResponse.status).toEqual(200);
          expect(instanceInfo.instanceState).toEqual(['PAUSED']);
          expect(instanceInfo.tokens).toEqual([
            {
              tokenId: expect.any(String),
              state: 'READY',
              currentFlowElementId: 'Event_0l2tx82',
              currentFlowNodeProgress: 0,
              currentFlowElementStartTime: expect.any(Number),
              localStartTime: expect.any(Number),
              localExecutionTime: expect.any(Number),
              machineHops: 0,
              deciderStorageRounds: 0,
              deciderStorageTime: 0,
            },
          ]);
          expect(instanceInfo.log).toEqual([
            {
              tokenId: expect.any(String),
              flowElementId: 'StartEvent_1',
              executionState: 'COMPLETED',
              startTime: expect.any(Number),
              endTime: expect.any(Number),
              machine: {
                id: 'machineId1',
                ip: expect.any(String),
                name: 'machine1',
                port: 33020,
              },
            },
          ]);
        });

        test('resume process at catching event', async () => {
          /**
           * Running Instance will be paused (via provided interface) while executing catching event Event_0l2tx82
           * and resumed again
           */

          const definitionId = 'timerCatchingStaticProcess';

          const usedEngine = engineProcesses.find((engine) => engine.name === 'machine1');

          await deployProcess(definitionId, usedEngine);

          const response = await request.post(
            `:${usedEngine.port}/process/${definitionId}/instance`
          );

          const { instanceId } = response.body;

          expect(response.status).toBe(201);

          await new Promise((resolve) => setTimeout(() => resolve(), 1000));

          const pauseResponse = await request
            .put(`:${usedEngine.port}/process/${definitionId}/instance/${instanceId}/instanceState`)
            .send({ instanceState: 'paused' });

          expect(pauseResponse.status).toBe(200);

          // after pausing the process, wait 2 seconds before resuming again
          await new Promise((resolve) => setTimeout(() => resolve(), 2000));

          const resumeResponse = await request
            .put(`:${usedEngine.port}/process/${definitionId}/instance/${instanceId}/instanceState`)
            .send({ instanceState: 'resume' });

          expect(resumeResponse.status).toBe(200);

          // after resuming the process, wait 4 seconds before requesting the state
          await new Promise((resolve) => setTimeout(() => resolve(), 4000));

          const engineResponse = await request.get(
            `:${usedEngine.port}/process/${definitionId}/instance/${instanceId}`
          );

          const instanceInfo = engineResponse.body;

          expect(engineResponse.status).toEqual(200);
          expect(instanceInfo.instanceState).toEqual(['ENDED']);
          expect(instanceInfo.tokens).toEqual([
            {
              tokenId: expect.any(String),
              state: 'ENDED',
              currentFlowElementId: 'Event_1kos1cn',
              currentFlowNodeProgress: 100,
              currentFlowElementStartTime: expect.any(Number),
              localStartTime: expect.any(Number),
              localExecutionTime: expect.any(Number),
              machineHops: 0,
              deciderStorageRounds: 0,
              deciderStorageTime: 0,
            },
          ]);
          expect(instanceInfo.log).toEqual([
            {
              tokenId: expect.any(String),
              flowElementId: 'StartEvent_1',
              executionState: 'COMPLETED',
              startTime: expect.any(Number),
              endTime: expect.any(Number),
              machine: {
                id: 'machineId1',
                ip: expect.any(String),
                name: 'machine1',
                port: 33020,
              },
            },
            {
              tokenId: expect.any(String),
              flowElementId: 'Event_0l2tx82',
              executionState: 'COMPLETED',
              startTime: expect.any(Number),
              endTime: expect.any(Number),
              machine: {
                id: 'machineId1',
                ip: expect.any(String),
                name: 'machine1',
                port: 33020,
              },
            },
            {
              tokenId: expect.any(String),
              flowElementId: 'Activity_042ziaw',
              executionState: 'COMPLETED',
              startTime: expect.any(Number),
              endTime: expect.any(Number),
              machine: {
                id: 'machineId1',
                ip: expect.any(String),
                name: 'machine1',
                port: 33020,
              },
            },
            {
              tokenId: expect.any(String),
              flowElementId: 'Event_1kos1cn',
              executionState: 'COMPLETED',
              startTime: expect.any(Number),
              endTime: expect.any(Number),
              machine: {
                id: 'machineId1',
                ip: expect.any(String),
                name: 'machine1',
                port: 33020,
              },
            },
          ]);
        });
        test('stop process at task', async () => {
          /**
           * Running Instance will be stopped (via provided interface) while executing script-task Activity_1m0u15u
           * -> instance state is supposed to be STOPPED, token state remain untouched
           * -> executed task will be stopped and logged
           */

          const definitionId = 'scriptTaskStaticProcess';

          const usedEngine = engineProcesses.find((engine) => engine.name === 'machine1');

          await deployProcess(definitionId, usedEngine);

          const response = await request.post(
            `:${usedEngine.port}/process/${definitionId}/instance`
          );

          const { instanceId } = response.body;

          expect(response.status).toBe(201);

          await new Promise((resolve) => setTimeout(() => resolve(), 1000));

          const stopResponse = await request
            .put(`:${usedEngine.port}/process/${definitionId}/instance/${instanceId}/instanceState`)
            .send({ instanceState: 'stopped' });

          expect(stopResponse.status).toBe(200);

          // after stopping the process, wait 2 seconds before requesting the state of the instance
          await new Promise((resolve) => setTimeout(() => resolve(), 2000));

          const engineResponse = await request.get(
            `:${usedEngine.port}/process/${definitionId}/instance/${instanceId}`
          );

          const instanceInfo = engineResponse.body;

          expect(engineResponse.status).toEqual(200);
          expect(instanceInfo.instanceState).toEqual(['STOPPED']);
          expect(instanceInfo.tokens).toEqual([
            {
              tokenId: expect.any(String),
              state: 'RUNNING',
              currentFlowElementId: 'Activity_1m0u15u',
              currentFlowNodeProgress: 0,
              currentFlowElementStartTime: expect.any(Number),
              localStartTime: expect.any(Number),
              localExecutionTime: expect.any(Number),
              machineHops: 0,
              deciderStorageRounds: 0,
              deciderStorageTime: 0,
            },
          ]);
          expect(instanceInfo.log).toEqual([
            {
              tokenId: expect.any(String),
              flowElementId: 'StartEvent_1',
              executionState: 'COMPLETED',
              startTime: expect.any(Number),
              endTime: expect.any(Number),
              machine: {
                id: 'machineId1',
                ip: expect.any(String),
                name: 'machine1',
                port: 33020,
              },
            },
            {
              tokenId: expect.any(String),
              flowElementId: 'Activity_1m0u15u',
              executionState: 'STOPPED',
              startTime: expect.any(Number),
              endTime: expect.any(Number),
              machine: {
                id: 'machineId1',
                ip: expect.any(String),
                name: 'machine1',
                port: 33020,
              },
            },
          ]);
        });
      });
    });
  });
});
