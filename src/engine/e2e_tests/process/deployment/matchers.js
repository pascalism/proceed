function getInstanceStartMatcher(instanceId) {
  return new RegExp(`A new process instance was created. Id = ${instanceId}`, 'g');
}
function getInstanceEndMatcher(instanceId) {
  if (instanceId) {
    return new RegExp(`Process instance ended. Id = ${instanceId}`);
  }

  return new RegExp(/Process instance ended. Id = _\w{4,12}(-\w{4,12}){9}/, 'g');
}

function getTokenForwardingMatcher(machineName) {
  return new RegExp(`Forwarding token execution to another machine ${machineName}.`, 'g');
}
function getTokenEndedMatcher() {
  return new RegExp(`Ending execution for current token`, 'g');
}
function getContinueInstanceMatcher(instanceId) {
  return new RegExp(`Continuing process instance. Id = ${instanceId}.`, 'g');
}
function getFinishedFlowNodeMatchers(flowNodeIds, instanceId) {
  return flowNodeIds.map((id) => {
    return new RegExp(`Finished execution of flowNode ${id}. InstanceId = ${instanceId}`);
  });
}
function getExpectedEndMatchers(expectedExecution) {
  const machines = {};

  expectedExecution.forEach((flowNode) => {
    // if a machine has at least one expected endEvent we need to wait until the instance finished on that machine
    if (flowNode.type === 'EndEvent') {
      machines[flowNode.machine.name] = [getInstanceEndMatcher()];
    }
  });

  return machines;
}
function getAllExpectedOutputMatchers(instanceId, expectedExecution) {
  const machines = {};

  expectedExecution.forEach((flowNode) => {
    // make sure we have an array for matchers for the executing machine
    if (flowNode.machine) {
      const machineName = flowNode.machine.name;

      if (!machines[machineName]) {
        machines[machineName] = [];
      }

      if (flowNode.type === 'StartEvent') {
        machines[machineName].push(getInstanceStartMatcher(instanceId));
      }

      if (flowNode.oldMachine) {
        machines[machineName].push(getContinueInstanceMatcher(instanceId));
      }

      machines[machineName].push(getFinishedFlowNodeMatchers([flowNode.id], instanceId)[0]);

      if (flowNode.type === 'EndEvent') {
        machines[machineName].push(getInstanceEndMatcher(instanceId));
      }
    }

    // make sure we have an array for matchers for the forwarding machine
    if (flowNode.oldMachine) {
      const machineName = flowNode.oldMachine.name;

      if (!machines[machineName]) {
        machines[machineName] = [];
      }

      machines[machineName].push(getTokenForwardingMatcher(flowNode.machine.name));
    }
  });

  return machines;
}

module.exports = {
  getExpectedEndMatchers,
  getAllExpectedOutputMatchers,
};
