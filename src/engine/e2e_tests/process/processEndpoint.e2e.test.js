const { spawn } = require('child_process');
const fs = require('fs');
const path = require('path');
const request = require('supertest')('localhost:33019');

const bpmn = fs.readFileSync(
  path.resolve(__dirname, './data/processBPMN/basicUserTaskProcess.xml'),
  'utf8'
);

jest.setTimeout(15000);

describe('Test process endpoints', () => {
  describe('/process', () => {
    it('is an accessible endpoint', async () => {
      const response = await request.get('/process');
      expect(response.status).toBe(200);
      expect(response.headers['content-type']).toMatch(/json/);
    });
  });
  describe('GET PUT DELETE /process/{definitionId}', () => {
    beforeAll(async () => {
      const getResponse = await request.get('/process/definitionId');
      expect(getResponse.status).toBe(404);
      const putResponse = await request.put('/process/definitionId').send({ bpmn });
      expect(putResponse.status).toBe(200);
    });
    afterAll(async () => {
      const response = await request.delete('/process/definitionId');
      expect(response.status).toBe(200);
      const getResponse = await request.get('/process/definitionId');
      expect(getResponse.status).toBe(404);
    });
    it('returns process bpmn on GET on existing process', async () => {
      const response = await request.get('/process/definitionId');
      expect(response.status).toBe(200);
      expect(response.body).toStrictEqual({
        bpmn,
        definitionId: '_32eda263-b76f-47ff-be8a-77b7410d7589',
        definitionName: 'basicStatic',
        deploymentMethod: 'dynamic',
        deploymentDate: expect.any(Number),
      });
    });
    describe('/process/{definitionId}/user-tasks/{userTaskFileName}', () => {
      beforeAll(async () => {
        const response = await request.get('/process/definitionId/user-tasks/userTaskFileName');
        expect(response.status).toBe(404);
      });
      it('saves the user task html on PUT request', async () => {
        const putResponse = await request
          .put('/process/definitionId/user-tasks/userTaskFileName')
          .send({ html: 'HTML' });
        expect(putResponse.status).toBe(200);
        const getResponse = await request.get('/process/definitionId/user-tasks/userTaskFileName');
        expect(getResponse.status).toBe(200);
        expect(getResponse.body).toStrictEqual({ html: 'HTML' });
      });
    });
    describe('/process/{definitionId}/instance', () => {
      let instanceId;
      beforeAll(async () => {
        const getResponse = await request.get('/process/definitionId/instance');
        expect(getResponse.status).toBe(200);
        expect(getResponse.body).toStrictEqual([]);
        const postResponse = await request.post('/process/definitionId/instance');
        expect(postResponse.status).toBe(201);
        ({ instanceId } = postResponse.body);
      });
      it('responds with statuscode 404 on nonexisting definitionId', async () => {
        const response = await request.get('/process/unknownDefinitionId/instance');
        expect(response.status).toBe(404);
      });
      it('returns array containing information about all instances of a process', async () => {
        const getResponse = await request.get('/process/definitionId/instance');
        expect(getResponse.status).toBe(200);
        expect(getResponse.body).toStrictEqual([instanceId]);
      });
      describe('/process/{definitionId}/instance/{instanceId}/instanceState', () => {
        it('allows remotely stopping an instance with a PUT request', async () => {
          const putResponse = await request
            .put(`/process/definitionId/instance/${instanceId}/instanceState`)
            .send({ instanceState: 'stopped' });
          expect(putResponse.status).toBe(200);
          const getResponse = await request.get(`/process/definitionId/instance/${instanceId}`);
          expect(getResponse.body.instanceState).toStrictEqual(['STOPPED']);
        });
      });
      describe('/process/{definitionId}/instance/{instanceId}', () => {
        let tokenId;
        it('responds with an information object for the instance with the requested id', async () => {
          const response = await request.get(`/process/definitionId/instance/${instanceId}`);
          expect(response.status).toBe(200);
          expect(response.body).toHaveProperty('instanceState');
          expect(response.body).toHaveProperty('processId');
          expect(response.body).toHaveProperty('processInstanceId');
          expect(response.body).toHaveProperty('tokens');
          response.body.tokens.forEach((token) => {
            // TODO: expect(token).toHaveProperty('state');
            expect(token).toHaveProperty('tokenId');
            expect(token).toHaveProperty('state');
            expect(token).toHaveProperty('currentFlowElementId');
            //expect(token).toHaveProperty('localStartTime');
            //expect(token).toHaveProperty('localExecutionTime');
            //expect(token).toHaveProperty('machineHops');
            // TODO: expect(token).toHaveProperty('machineHops');
          });
        });
      });
    });
  });
});
