const NativeModule = require('@proceed/native-module');
const fs = require('fs');
const { default: exitHook } = require('@darkobits/adeiu');

class NativeFS extends NativeModule {
  constructor(options) {
    super();

    this.commands = ['read', 'write'];
    this._mutex = new Set();
    this._lastInQueue = new Map();
    /**
     * Stop the whole process (cleanup).
     */
    this._stop = false;

    // Create the data files folder if it doesn't exist yet
    this.path = (options && options.dir) || __dirname;
    try {
      fs.statSync(`${this.path}/data_files/`);
    } catch (err) {
      if (err.code === 'ENOENT') {
        return fs.mkdirSync(`${this.path}/data_files/`);
      }
      throw err;
    }

    // Unpublish on exit
    exitHook(async () => {
      this._stop = true;
      // Await all ongoing writes but don't schedule new ones.
      await Promise.all(Array.from(this._lastInQueue.values()));
    });
  }

  executeCommand(command, args, send) {
    if (this._stop) {
      return undefined;
    }

    if (command === 'read') {
      return this.read(args);
    }
    if (command === 'write') {
      return this.write(args, send);
    }
    return undefined;
  }

  async read(args) {
    let succeed;
    let fail;
    const promisedResponse = new Promise((resolve, reject) => {
      succeed = resolve;
      fail = reject;
    }).finally(() => {
      this._mutex.delete(table);
      if (this._lastInQueue.get(table) === promisedResponse) {
        this._lastInQueue.delete(table);
      }
    });

    const [key] = args;

    let table = key;
    if (key.indexOf('/') !== -1) {
      table = key.substr(0, key.indexOf('/'));
    }

    // Check mutex
    if (this._mutex.has(table)) {
      // Wait till previous operations on this key finished.
      await this._freeQueue(promisedResponse, table);
    }

    // Block resource
    // TODO: this is a temporary fix
    // PROBLEM: we want to block writes from happening while one or more reads are happening but this also blocks more than one read from happening
    // SOLUTION 1: Have Objects that get updated here which get occasionally written to the file system => reads are atomic reads of Object attributes
    // SOLUTION 2: Read-Write-Lock, Reads block only writes and writes block everything
    if (!this._lastInQueue.has(table)) {
      this._lastInQueue.set(table, promisedResponse);
    }
    this._mutex.add(table);

    if (key.indexOf('/') === -1) {
      // No id, so send the whole file
      fs.readFile(`${this.path}/data_files/${key}.json`, 'utf8', (err, data) => {
        if (err) {
          if (err.code === 'ENOENT') {
            succeed([null]);
          } else {
            fail(err);
          }
          return;
        }
        // File is JSON with key-value-objects as elements
        const file = JSON.parse(data);
        succeed([file]);
      });
    } else {
      const filename = key.substr(0, key.indexOf('/'));
      const id = key.substr(key.indexOf('/') + 1);
      fs.readFile(`${this.path}/data_files/${filename}.json`, (err, data) => {
        if (err) {
          if (err.code === 'ENOENT') {
            succeed([null]);
          } else {
            fail(err);
          }
          return;
        }
        // File is JSON with key-value-objects as elements
        const file = JSON.parse(data);
        // Send null if no value
        const value = file[id] !== undefined ? file[id] : null;
        succeed([value]);
      });
    }
    return promisedResponse;
  }

  async write(args) {
    const [key, value] = args;

    let table = key;
    if (key.indexOf('/') !== -1) {
      table = key.substr(0, key.indexOf('/'));
    }

    let succeed;
    let fail;
    const promisedResponse = new Promise((resolve, reject) => {
      succeed = resolve;
      fail = reject;
    }).finally(() => {
      this._mutex.delete(table);
      if (this._lastInQueue.get(table) === promisedResponse) {
        this._lastInQueue.delete(table);
      }
    });

    // Check mutex
    if (this._mutex.has(table)) {
      // Wait till previous operations on this key finished.
      await this._freeQueue(promisedResponse, table);
    }

    // Block resource
    if (!this._lastInQueue.has(table)) {
      this._lastInQueue.set(table, promisedResponse);
    }
    this._mutex.add(table);

    // Delete
    if (value === null) {
      // delete file
      if (key.indexOf('/') === -1) {
        fs.unlink(`${this.path}/data_files/${key}.json`, (err) => {
          if (err && err.code !== 'ENOENT') {
            fail(err);
            return;
          }
          succeed();
        });
        // delete single val
      } else {
        const filename = key.substr(0, key.indexOf('/'));
        const id = key.substr(key.indexOf('/') + 1);
        const path = `${this.path}/data_files/${filename}.json`;

        fs.readFile(path, (err, data) => {
          let newFile = false;
          if (err && err.code === 'ENOENT') {
            newFile = true;
          } else if (err) {
            fail(err);
            return;
          }
          // File is JSON with key-value-objects as elements
          const file = newFile ? {} : JSON.parse(data);
          delete file[id];
          fs.writeFile(path, JSON.stringify(file), (err2) => {
            if (err2) {
              fail(err2);
              return;
            }
            succeed();
          });
        });
      }
      // New val
    } else {
      const filename = key.substr(0, key.indexOf('/'));
      const id = key.substr(key.indexOf('/') + 1);
      const path = `${this.path}/data_files/${filename}.json`;

      fs.readFile(path, (err, data) => {
        let newFile = false;
        if (err && err.code === 'ENOENT') {
          newFile = true;
        } else if (err) {
          fail(err);
          return;
        }

        // File is JSON with key-value-objects as elements
        const file = newFile ? {} : JSON.parse(data);
        file[id] = value;
        fs.writeFile(path, JSON.stringify(file), (err2) => {
          if (err2) {
            fail(err2);
            return;
          }
          succeed();
        });
      });
    }

    return promisedResponse;
  }

  async _freeQueue(operation, table) {
    const last = this._lastInQueue.get(table);
    this._lastInQueue.set(table, operation);

    const wait = new Promise((resolve) => {
      last.then(() => {
        if (!this._stop) {
          resolve();
        }
      });
    });
    return wait;
  }
}

module.exports = NativeFS;
