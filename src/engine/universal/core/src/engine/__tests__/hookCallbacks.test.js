jest.mock('neo-bpmn-engine', () => ({
  LogLevel: {
    info: 'info',
  },
}));
jest.mock('@proceed/distribution');
jest.mock('@proceed/bpmn-helper');
jest.mock('../processForwarding.js');

const { getNewInstanceHandler } = require('../hookCallbacks.js');
const { db } = require('@proceed/distribution');
const { getProcessIds, getCallActivityDefinitionIdMapping } = require('@proceed/bpmn-helper');
const { abortInstanceOnNetwork } = require('../processForwarding.js');

let mockEngine;

// mock for the engine class in the engine.js file
const mockEngineClass = function () {
  return {
    deployProcess: jest.fn(),
    startProcess: jest.fn(),
  };
};

const onStarted = jest.fn();
const onEnded = jest.fn();
const onTokenEnded = jest.fn();

let mockNewInstance;

let instanceHandler;
describe('Test for the function that sets up callbacks for the different lifecycle hooks exposed by the neo engine', () => {
  beforeEach(async () => {
    jest.resetAllMocks();

    mockEngine = {
      definitionId: 'processFile',
      processID: 'processId',
      _bpmn: 'BPMN',
      _log: {
        log: jest.fn(),
        info: jest.fn(),
      },
      instanceIDs: [],
      userTasks: [],
      callActivityExecutors: {},
    };

    mockNewInstance = {
      id: 'newInstanceId',
      getState: jest.fn().mockImplementation(() => ({})),
      logExecution: jest.fn(),
      updateLog: jest.fn(),
      completeCallActivity: jest.fn(),
      getLog$: jest.fn().mockImplementation(function () {
        return { subscribe: (cb) => (this.logCallback = cb) };
      }),
      getUserTask$: jest.fn().mockImplementation(function () {
        return { subscribe: (cb) => (this.userTaskCallback = cb) };
      }),
      getCallActivity$: jest.fn().mockImplementation(function () {
        return { subscribe: (cb) => (this.callActivityCallback = cb) };
      }),
      onEnded: jest.fn().mockImplementation(function (cb) {
        this.endedCallback = cb;
      }),
      onAborted: jest.fn().mockImplementation(function (cb) {
        this.abortedCallback = cb;
      }),
      onUserTaskInterrupted: jest.fn().mockImplementation(function (cb) {
        this.userTaskInterruptedCallback = cb;
      }),
      onCallActivityInterrupted: jest.fn().mockImplementation(function (cb) {
        this.callActivityInterruptedCallback = cb;
      }),
      onScriptTaskError: jest.fn().mockImplementation(function (cb) {
        this.scriptTaskErrorCallback = cb;
      }),
      onTokenEnded: jest.fn().mockImplementation(function (cb) {
        this.tokenEndedCallback = cb;
      }),
      onFlowNodeExecuted: jest.fn().mockImplementation(function (cb) {
        this.flowNodeExecutedCallback = cb;
      }),
      onInstanceStateChange: jest.fn().mockImplementation(function (cb) {
        this.instanceStateChangeCallback = cb;
      }),
      logCallback: null,
      userTaskCallback: null,
      callActivityCallback: null,
      endedCallback: null,
      abortedCallback: null,
      scriptTaskErrorCallback: null,
      userTaskInterruptedCallback: null,
      callActivityInterruptedCallback: null,
      tokenEndedCallback: null,
      flowNodeExecutedCallback: null,
      instanceStateChangeCallback: null,

      log: jest.fn().mockImplementation(function (log) {
        this.logCallback(log);
      }),
      userTask: jest.fn().mockImplementation(function (userTask) {
        this.userTaskCallback(userTask);
      }),
      callActivity: jest.fn().mockImplementation(async function (callActivity) {
        await this.callActivityCallback(callActivity);
      }),
      ended: jest.fn().mockImplementation(function () {
        this.endedCallback();
      }),
      aborted: jest.fn().mockImplementation(function () {
        this.abortedCallback();
      }),
      scriptTaskError: jest.fn().mockImplementation(function (token) {
        this.scriptTaskErrorCallback(token);
      }),
      userTaskInterrupted: jest.fn().mockImplementation(function (token) {
        this.userTaskInterruptedCallback(token);
      }),
      callActivityInterrupted: jest.fn().mockImplementation(function (token) {
        this.callActivityInterruptedCallback(token);
      }),
      tokenEnded: jest.fn().mockImplementation(function (token) {
        this.tokenEndedCallback(token);
      }),
      flowNodeExecuted: jest.fn().mockImplementation(function (execution) {
        this.flowNodeExecutedCallback(execution);
      }),
      instanceStateChange: jest.fn().mockImplementation(function (state) {
        this.instanceStateChangeCallback(state);
      }),
    };

    instanceHandler = getNewInstanceHandler(
      mockEngine,
      mockEngineClass,
      undefined,
      onStarted,
      onEnded,
      onTokenEnded
    );

    await instanceHandler(mockNewInstance);
    mockEngine._log.info.mockReset();
  });

  it('saves the id of a new instance in the given engine and calls onStarted function', async () => {
    expect(mockEngine.instanceIDs).toStrictEqual(['newInstanceId']);
    expect(onStarted).toHaveBeenCalled();
  });

  describe('different events for which hooks are set up', () => {
    describe('new log message', () => {
      it('gives new logs originating from the instance to the engine', async () => {
        mockNewInstance.log({ level: 'info', message: 'test123' });

        expect(mockEngine._log.log).toHaveBeenCalledWith({
          level: 'info',
          moduleName: 'BPMN-ENGINE',
          msg: 'test123',
          instanceId: 'newInstanceId',
        });
      });
    });

    describe('instance ended', () => {
      it('logs end of instance, calls optional external onEnded, archives instance', async () => {
        mockNewInstance.ended();

        expect(mockEngine._log.info).toHaveBeenCalled();
        expect(onEnded).toHaveBeenCalled();
        expect(db.archiveInstance).toHaveBeenCalledWith('processFile', 'newInstanceId', {});
      });
    });

    describe('instance aborted', () => {
      it('logs failure of instance and sends abort command to other engines', async () => {
        mockNewInstance.aborted();

        expect(mockEngine._log.info).toHaveBeenCalled();
        expect(abortInstanceOnNetwork).toHaveBeenCalledWith('processFile', 'newInstanceId');
      });
    });

    describe('token ended', () => {
      it('logs the end of the specific token, calls optional external onTokenEnded callback', async () => {
        mockNewInstance.tokenEnded({
          id: 'tokenId',
        });

        expect(mockEngine._log.info).toHaveBeenCalled();
        expect(onTokenEnded).toHaveBeenCalled();
      });
    });

    describe('flowNode executed', () => {
      it('logs the execution of a flow node', async () => {
        mockEngine.machineInformation = { id: 'machineId' };

        mockNewInstance.flowNodeExecuted({
          flowElementId: 'executionId',
          tokenId: 'tokenId',
        });

        expect(mockNewInstance.updateLog).toHaveBeenCalledWith('executionId', 'tokenId', {
          machine: { id: 'machineId' },
        });
        expect(mockEngine._log.info).toHaveBeenCalled();
      });
    });

    describe('new user task encountered', () => {
      it('logs that a new user task was encountered and stores the user task in the engine instance', async () => {
        mockNewInstance.userTask({ userTaskID: 'task1', info: 'some info' });

        expect(mockEngine._log.info).toHaveBeenCalled();
        expect(mockEngine.userTasks).toStrictEqual([
          {
            processInstance: mockNewInstance,
            processChain: mockEngine.processID,
            userTaskID: 'task1',
            info: 'some info',
          },
        ]);
      });
    });

    describe('new call activity encountered', () => {
      beforeEach(async () => {
        // mock the variables of the curret instance
        mockNewInstance.getState.mockReturnValueOnce({ variables: { varA: { value: 1 } } });

        // mock the return values of the function analyzing the bpmn or returning the import bpmn
        getCallActivityDefinitionIdMapping.mockResolvedValueOnce({
          callActivityId: { definitionId: 'callActivityDefinitionId' },
          otherCallActivityId: { definitionId: 'otherCallActivityDefinitionId' },
        });
        db.getImportedProcess.mockResolvedValueOnce('importedBpmn');
        getProcessIds.mockResolvedValueOnce(['importProcessId']);

        // call the callActivity hook
        await mockNewInstance.callActivity({ id: 'callActivityId', tokenId: 'tokenId1' });
      });
      it('creates a new instance of the engine class and makes it the one responsible for running all future instances of the call activity process', async () => {
        // check if the bpmn analysis and database functions were called correctly
        expect(getCallActivityDefinitionIdMapping).toHaveBeenCalledWith(mockEngine._bpmn);
        expect(db.getImportedProcess).toHaveBeenCalledWith(
          mockEngine.definitionId,
          'callActivityDefinitionId'
        );
        expect(getProcessIds).toHaveBeenCalledWith('importedBpmn');

        // check if the execution engine for the imported process was created and registered for the correct process
        expect(mockEngine.callActivityExecutors).toStrictEqual({
          importProcessId: {
            deployProcess: expect.any(Function),
            startProcess: expect.any(Function),
            _log: mockEngine._log,
          },
        });
      });

      it('deploys and starts a new instance of the imported process', () => {
        // check if the execution of the call activity process was done correctly
        const importExecutionEngine = mockEngine.callActivityExecutors['importProcessId'];
        expect(importExecutionEngine.deployProcess).toHaveBeenCalledWith(
          mockEngine.definitionId,
          'callActivityDefinitionId'
        );
        expect(importExecutionEngine.startProcess).toHaveBeenCalledWith(
          { varA: { value: 1 } }, //variables
          undefined, // existing instance => not needed here
          expect.any(Function), // onStarted for the new instance of the imported process
          expect.any(Function) // onEnded for the new instance of the imported process
        );

        // trigger function used as onEnded callBack to simulate what happens when the imported process ends
        const importOnEnded = importExecutionEngine.startProcess.mock.calls[0][3];
        importOnEnded({ getState: () => ({ variables: { var1: { value: 5 } } }) });
        expect(mockNewInstance.completeCallActivity).toHaveBeenCalledWith(
          'callActivityId',
          'tokenId1',
          { var1: 5 }
        );
      });
    });
  });
});
