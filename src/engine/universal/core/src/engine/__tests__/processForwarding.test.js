jest.mock('@proceed/system');
jest.mock('@proceed/distribution');

const {
  forwardProcess,
  forwardInstance,
  forwardHTML,
  forwardImports,
  getMachineInfo,
  abortInstanceOnNetwork,
} = require('../processForwarding.js');
const system = require('@proceed/system');
const { db, communication } = require('@proceed/distribution');

describe('Test for functions used to send process/instance information to another engine', () => {
  beforeEach(() => {
    jest.resetAllMocks();
    communication.getAvailableMachines.mockReturnValue([]);
  });

  describe('forwardProcess', () => {
    it('sends the definition of a process to another machine', async () => {
      await forwardProcess('123.456.78.9', 12345, 'definitionId', 'BPMN');

      expect(system.network.sendData).toHaveBeenCalledWith(
        '123.456.78.9',
        12345,
        '/process/definitionId',
        'PUT',
        'application/json',
        { bpmn: 'BPMN' }
      );
    });
  });

  describe('forwardInstance', () => {
    it('sends an instance object to another machine', async () => {
      await forwardInstance('123.456.78.9', 12345, 'definitionId', 'instanceId', {
        useful: 'information',
      });

      expect(system.network.sendData).toHaveBeenCalledWith(
        '123.456.78.9',
        12345,
        '/process/definitionId/instance/instanceId',
        'PUT',
        'application/json',
        { useful: 'information' }
      );
    });
  });

  describe('forwardHTML', () => {
    it('sends the html for all user task of a process to another machine', async () => {
      db.getAllUserTasks.mockResolvedValueOnce(['userTask1', 'userTask2']);
      db.getHTML.mockResolvedValueOnce('HTML1');
      db.getHTML.mockResolvedValueOnce('HTML2');
      await forwardHTML('123.456.78.9', 12345, 'definitionId');

      expect(db.getAllUserTasks).toHaveBeenCalledTimes(1);
      expect(db.getAllUserTasks).toHaveBeenCalledWith('definitionId', undefined);

      expect(db.getHTML).toHaveBeenCalledTimes(2);
      expect(db.getHTML).toHaveBeenCalledWith('definitionId', 'userTask1', undefined);
      expect(db.getHTML).toHaveBeenCalledWith('definitionId', 'userTask2', undefined);

      expect(system.network.sendData).toHaveBeenCalledTimes(2);
      expect(system.network.sendData).toHaveBeenCalledWith(
        '123.456.78.9',
        12345,
        '/process/definitionId/user-tasks/userTask1',
        'PUT',
        'application/json',
        { html: 'HTML1' }
      );
      expect(system.network.sendData).toHaveBeenCalledWith(
        '123.456.78.9',
        12345,
        '/process/definitionId/user-tasks/userTask2',
        'PUT',
        'application/json',
        { html: 'HTML2' }
      );
    });
  });

  describe('forwardImports', () => {
    it('sends the definitions and the user task data of all imported processes to another machine', async () => {
      db.getImportedProcesses.mockResolvedValueOnce({
        importDefinitionId1: 'ImportBpmn1',
        importDefinitionId2: 'ImportBpmn2',
      });
      db.getAllUserTasks.mockResolvedValueOnce(['importUserTask']);
      db.getHTML.mockResolvedValueOnce('ImportHtml');

      await forwardImports('123.456.78.9', 12345, 'definitionId');

      expect(db.getImportedProcesses).toBeCalledTimes(1);
      expect(db.getImportedProcesses).toHaveBeenCalledWith('definitionId');

      expect(db.getAllUserTasks).toHaveBeenCalledTimes(1);
      expect(db.getAllUserTasks).toHaveBeenCalledWith('definitionId', true);

      expect(db.getHTML).toHaveBeenCalledTimes(1);
      expect(db.getHTML).toHaveBeenCalledWith('definitionId', 'importUserTask', true);

      expect(system.network.sendData).toHaveBeenCalledTimes(3);
      expect(system.network.sendData).toHaveBeenCalledWith(
        '123.456.78.9',
        12345,
        '/process/definitionId/imported/importDefinitionId1',
        'PUT',
        'application/json',
        { bpmn: 'ImportBpmn1' }
      );
      expect(system.network.sendData).toHaveBeenCalledWith(
        '123.456.78.9',
        12345,
        '/process/definitionId/imported/importDefinitionId2',
        'PUT',
        'application/json',
        { bpmn: 'ImportBpmn2' }
      );
      expect(system.network.sendData).toHaveBeenCalledWith(
        '123.456.78.9',
        12345,
        '/process/definitionId/imported/user-tasks/importUserTask',
        'PUT',
        'application/json',
        { html: 'ImportHtml' }
      );
    });
  });

  describe('getMachineInfo', () => {
    it('requests id, name and hostname information from another machine', async () => {
      system.network.sendRequest.mockResolvedValueOnce({
        body: '{ "name": "someName", "hostname": "someHostname", "id": "someId" }',
      });
      const info = await getMachineInfo('123.456.78.9', 12345);
      expect(info).toStrictEqual({
        name: 'someName',
        hostname: 'someHostname',
        id: 'someId',
      });
      expect(system.network.sendRequest).toHaveBeenCalledTimes(1);
      expect(system.network.sendRequest).toHaveBeenCalledWith(
        '123.456.78.9',
        12345,
        '/machine/id,name,hostname'
      );
    });
  });

  describe('abortInstanceOnNetwork', () => {
    beforeEach(() => {
      communication.getAvailableMachines.mockReturnValue([
        {
          ip: '123.456.0.1',
          port: 12345,
        },
        {
          ip: '123.456.0.2',
          port: 54321,
        },
      ]);
    });
    it('sends an abort signal for a specific instance to all machines in the network', async () => {
      await abortInstanceOnNetwork('definitionId', 'instanceId');

      expect(system.network.sendData).toHaveBeenCalledTimes(2);

      expect(system.network.sendData).toBeCalledWith(
        '123.456.0.1',
        12345,
        '/process/definitionId/instance/instanceId/instanceState',
        'PUT',
        'application/json',
        { instanceState: 'aborted' }
      );

      expect(system.network.sendData).toBeCalledWith(
        '123.456.0.2',
        54321,
        '/process/definitionId/instance/instanceId/instanceState',
        'PUT',
        'application/json',
        { instanceState: 'aborted' }
      );
    });
    it("doesn't throw when an engine that doesn't have the instance responds with a 404 error", async () => {
      system.network.sendData.mockResolvedValueOnce({});
      system.network.sendData.mockRejectedValueOnce(
        new Error("404 Error, engine doesn't know instance")
      );

      await expect(abortInstanceOnNetwork('definitionId', 'instanceId')).resolves.not.toThrow();
    });
  });
});
