const NeoEngine = require('neo-bpmn-engine');
const distribution = require('@proceed/distribution');
const { getProcessIds, getCallActivityDefinitionIdMapping } = require('@proceed/bpmn-helper');
const { abortInstanceOnNetwork } = require('./processForwarding.js');

/**
 * Creates a callback function that can be used to handle calls from the log stream of the neo engine
 *
 * @param {Object} engine proceed engine instance that contains the process information
 */
function getLogHandler(engine, instance) {
  return (bpmnLog) => {
    engine._log.log({
      level: NeoEngine.LogLevel[bpmnLog.level].toLowerCase(),
      msg: bpmnLog.message,
      moduleName: 'BPMN-ENGINE',
      instanceId: instance.id,
    });
  };
}

/**
 * Creates a callback function that can be used to handle calls from the onEnded hook of the neo engine
 *
 * @param {Object} engine proceed engine instance that contains the process information
 * @param {Function} onEnded function that is supposed to be called when instance ends
 * @param {Object} instance the process instance that ended
 */
function getOnEndedHandler(engine, onEnded, instance) {
  return () => {
    engine._log.info({
      msg: `Process instance ended. Id = ${instance.id}`,
      instanceId: instance.id,
    });

    // archive the information for the finalized instance
    if (!engine.importDefinitionId) {
      distribution.db.archiveInstance(engine.definitionId, instance.id, instance.getState());
    }

    if (typeof onEnded === 'function') {
      onEnded(instance);
    }
  };
}

/**
 * Creates a callback function that can be used to handle calls from the onAborted hook of the neo engine
 *
 * @param {Object} engine proceed engine instance that contains the process information
 * @param {Object} instance the process instance that ended
 */
function getOnAbortedHandler(engine, instance) {
  return () => {
    engine._log.info({
      msg: `Process instance aborted. Id = ${instance.id}`,
      instanceId: instance.id,
    });
    engine._log.info({
      msg: `Broadcasting instance abort signal into network. Id = ${instance.id}`,
      instanceId: instance.id,
    });
    abortInstanceOnNetwork(engine.definitionId, instance.id);
  };
}

/**
 * Creates a callback function that can be used to handle calls from the onTokenEnded hook of the neo engine
 *
 * @param {Object} engine proceed engine instance that contains the process information
 * @param {Function} onTokenEnded function that is supposed to be called when the execution of a token ends
 * @param {Object} instance the process instance the token is in
 */
function getOnTokenEndedHandler(engine, onTokenEnded, instance) {
  return (token) => {
    engine._log.info({
      msg: `Token with id ${token.tokenId} ended. InstanceId = ${instance.id} `,
      instanceId: instance.id,
    });

    if (typeof onTokenEnded === 'function') {
      onTokenEnded(token);
    }
  };
}

/**
 * Creates a callback function that can be used to handle calls from the onScriptTaskError hook of the neo engine
 *
 * @param {Object} engine proceed engine instance that contains the process information
 * @param {Object} instance the process instance the token is in
 */
function getOnScriptTaskErrorHandler(engine, instance) {
  return (token) => {
    // engine._log.info({
    //   msg: `Technical Error in Script Task with id ${token.currentFlowElementId} on token ${token.tokenId}. InstanceId = ${instance.id} `,
    //   instanceId: instance.id,
    // });
  };
}

/**
 * Creates a callback function that can be used to handle calls from the onUserTaskInterrupted hook of the neo engine
 *
 * @param {Object} engine proceed engine instance that contains the process information
 * @param {Object} instance the process instance the token is in
 */
function getOnUserTaskInterruptedHandler(engine, instance) {
  return (token) => {
    engine._log.info({
      msg: `User Task with id ${token.currentFlowElementId} on token ${token.tokenId} ended. InstanceId = ${instance.id} `,
      instanceId: instance.id,
    });

    const endedUserTask = engine.userTasks.find((u) => u.tokenId === token.tokenId);
    // if usertask is terminated / failed, remove from list
    if (endedUserTask) {
      engine.userTasks.splice(engine.userTasks.indexOf(endedUserTask), 1);
    }
  };
}

/**
 * Creates a callback function that can be used to handle calls from the onCallActivityInterrupted hook of the neo engine
 * -> stop execution of call activity processs
 * @param {Object} engine proceed engine instance that contains the process information
 * @param {Object} instance the process instance the token is in
 */
function getOnCallActivityInterruptedHandler(engine, instance) {
  return async (token) => {
    const callActivityId = token.currentFlowElementId;
    const callActivityDefinitionIdMapping = await getCallActivityDefinitionIdMapping(engine._bpmn);
    const callActivityDefinitionId = callActivityDefinitionIdMapping[callActivityId].definitionId;
    const importBPMN = await distribution.db.getImportedProcess(
      engine.definitionId,
      callActivityDefinitionId
    );
    const [importProcessId] = await getProcessIds(importBPMN);

    const CAExecutionEngine = engine.callActivityExecutors[importProcessId];
    if (CAExecutionEngine) {
      CAExecutionEngine.instanceIDs.forEach((instanceID) => {
        CAExecutionEngine.stopInstance(instanceID);
      });
    }
  };
}

/**
 * Creates a callback function that can be used to register to the userTask stream of the neo engine
 *
 * @param {Object} engine proceed engine instance that contains the process information
 * @param {Object} instance the process instance the user task was encountered in
 */
function getUserTaskHandler(engine, instance) {
  return (userTask) => {
    engine._log.info({
      msg: `A new User Task was encountered, InstanceId = ${instance.id}`,
      instanceId: instance.id,
    });
    engine.userTasks.push({
      processInstance: instance,
      processChain: engine.processID,
      ...userTask,
    });
  };
}

/**
 * Creates a callback that handles the execution of callActivities when one becomes active
 *
 * @param {Object} engine proceed engine instance that contains the process information
 * @param {Class} Engine the process Execution class that we want to create a new Instance of to execute the callActivity process
 * @param {Object} instance the process instance the call activity was encountered in
 */
function getCallActivityHandler(engine, Engine, instance) {
  return async (callActivity) => {
    // get necessary process information about the process referenced by the callActivity
    const callActivityDefinitionIdMapping = await getCallActivityDefinitionIdMapping(engine._bpmn);
    const callActivityDefinitionId = callActivityDefinitionIdMapping[callActivity.id].definitionId;
    const importBPMN = await distribution.db.getImportedProcess(
      engine.definitionId,
      callActivityDefinitionId
    );
    const [importProcessId] = await getProcessIds(importBPMN);

    // if there is no execution engine registered for the process to execute create a new one
    if (!Object.keys(engine.callActivityExecutors).includes(importProcessId)) {
      const CAExecutionEngine = new Engine();
      engine.callActivityExecutors[importProcessId] = CAExecutionEngine;
      CAExecutionEngine._log = engine._log;

      await CAExecutionEngine.deployProcess(engine.definitionId, callActivityDefinitionId);
    }

    // get current variable state to pass on to the instance of the callActivity process
    const variables = Object.entries(instance.getState().variables).reduce(
      (acc, [varName, varInfo]) => {
        return { ...acc, [varName]: { value: varInfo.value } };
      },
      {}
    );

    // start execution of callActivity process with variables ffrom the current instance
    engine._log.info({
      msg: `Starting callActivity with id ${callActivity.id}. Imported process definitionId: ${callActivityDefinitionId}. CallingInstanceId = ${instance.id}`,
      instanceId: instance.id,
    });
    engine.callActivityExecutors[importProcessId].startProcess(
      variables,
      undefined,
      // onStarted callBack: log that we started an instance of a callActivity process
      (callActivityInstance) => {
        callActivityInstance.callingInstance = instance;
      },
      // onEnded callBack: return possibly changed variables from the callActivity instance back to the calling instance
      (callActivityInstance) => {
        const variables = Object.entries(callActivityInstance.getState().variables).reduce(
          (acc, [varName, varInfo]) => {
            return { ...acc, [varName]: varInfo.value };
          },
          {}
        );

        instance.completeCallActivity(callActivity.id, callActivity.tokenId, variables);
      }
    );
  };
}

module.exports = {
  /**
   * Returns a callBack function that is used for the instance stream of the neo engine
   * this callBack registers callBack functions for the different lifecycle hooks of a newly created process
   *
   * @param {Object} engine proceed engine instance that contains the process information
   * @param {Class} Engine the class we use to store information about a specific process and its instances
   * @param {Object} preexistingInstance an optional object containing information about an instance we want to continue on this machine
   * @param {Function} onStarted a callback function that is supposed to be called when a new instance starts
   * @param {Function} onEnded a callback function that is supposed to be called when an instance ends
   * @param {Function} onTokenEnded a callback function that is supposed to be called a token inside an instance reaches a finished state
   */
  getNewInstanceHandler(engine, Engine, preexistingInstance, onStarted, onEnded, onTokenEnded) {
    return (newInstance) => {
      if (!preexistingInstance) {
        // we are starting a new instance
        engine._log.info({
          msg: `A new process instance was created. Id = ${newInstance.id}`,
          instanceId: newInstance.id,
        });
        engine.instanceIDs.push(newInstance.id);
      } else {
        engine._log.info({
          msg: `Process instance started. Id = ${newInstance.id}`,
          instanceId: newInstance.id,
        });
        // we are starting a new local instance to continue an instance started on another machine
        engine.instanceIDs.push(preexistingInstance.processInstanceId);
      }

      newInstance.getLog$().subscribe(getLogHandler(engine, newInstance)); // subscribe to log-stream of bpmn processinstance

      // Set up lifecycle listeners
      if (typeof onStarted === 'function') {
        onStarted(newInstance);
      }

      newInstance.onEnded(getOnEndedHandler(engine, onEnded, newInstance));

      newInstance.onScriptTaskError(getOnScriptTaskErrorHandler(engine, newInstance));

      newInstance.onAborted(getOnAbortedHandler(engine, newInstance));

      newInstance.onUserTaskInterrupted(getOnUserTaskInterruptedHandler(engine, newInstance));

      newInstance.onCallActivityInterrupted(
        getOnCallActivityInterruptedHandler(engine, newInstance)
      );

      newInstance.onTokenEnded(getOnTokenEndedHandler(engine, onTokenEnded, newInstance));

      newInstance.onFlowNodeExecuted((execution) => {
        if (!execution.machine) {
          newInstance.updateLog(execution.flowElementId, execution.tokenId, {
            machine: engine.machineInformation,
          });
          engine._log.info({
            msg: `Finished execution of flowNode ${execution.flowElementId}. InstanceId = ${newInstance.id}`,
            instanceId: newInstance.id,
          });
        }
      });

      newInstance.onInstanceStateChange((instanceState) => {
        //instanceState = array of token states
        const instanceEnded = instanceState.every(
          (s) =>
            s === 'ENDED' ||
            s === 'FAILED' ||
            s === 'TERMINATED' ||
            s === 'ABORTED' ||
            s === 'ERROR-TECHNICAL' ||
            s === 'ERROR-SEMANTIC' ||
            s === 'FORWARDED' ||
            s === 'ERROR-CONSTRAINT-UNFULFILLED'
        );

        if (instanceEnded) {
          // TODO: save instance data, delete instance
        }
      });

      // Subscribe to user tasks stream, will invoke when a user task becomes active
      newInstance.getUserTask$().subscribe(getUserTaskHandler(engine, newInstance));

      // Subscribe to call-activity stream, will invoke when a call-activity becomes active
      newInstance.getCallActivity$().subscribe(getCallActivityHandler(engine, Engine, newInstance));
    };
  },
};
