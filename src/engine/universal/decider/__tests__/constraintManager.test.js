/* eslint-disable import/no-dynamic-require */
const { information, config } = require('@proceed/machine');
const { network } = require('@proceed/system');
const { communication } = require('@proceed/distribution');
const constraintManager = require('../constraintManager');

const path = './../../../../helper-modules/constraint-parser-xml-json/__tests__/ConstraintsJSON/';
const processConstraints1 = require(`${path}1-ProcessJSON.json`).processConstraints;

const flowNodeConstraints1 = require(`${path}1-ConstraintsJSON.json`).processConstraints;

const concatenatedConstraints2 = require(`${path}2-ConcatenationJSON.json`).processConstraints;
const processConstraints2 = require(`${path}2-ProcessJSON.json`).processConstraints;
const flowNodeConstraints2 = require(`${path}1-ConstraintsJSON.json`).processConstraints;

const concatenatedConstraints3 = require(`${path}3-ConcatenationJSON.json`).processConstraints;
const processConstraints3 = require(`${path}AND-ConstraintGroupJSON.json`).processConstraints;
const flowNodeConstraints3 = require(`${path}OR-ConstraintGroupJSON.json`).processConstraints;

const sameMachineConstraint1 = require(`${path}sameMachineConstraint1JSON.json`).processConstraints;
const sameMachineConstraint2 = require(`${path}sameMachineConstraint2JSON.json`).processConstraints;

const exampleConstraints = require(`${path}1-ConstraintsJSON.json`).processConstraints;

jest.mock('@proceed/distribution', () => ({
  communication: {
    getAvailableMachines: jest.fn(),
  },
}));

jest.mock('@proceed/machine', () => ({
  information: {
    getMachineInformation: jest.fn(),
  },
  config: {
    readConfig: jest.fn(),
  },
}));

beforeAll(() => {
  network.sendRequest = jest.fn();
  communication.getAvailableMachines = jest.fn();
  config.readConfig = jest.fn();
});

afterEach(() => {
  jest.clearAllMocks();
});

describe('#filterDuplicateProcessConstraints', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });
  test('no flowNode and/or process Constraints are given', () => {
    expect(
      constraintManager.filterDuplicateProcessConstraints(
        flowNodeConstraints1.hardConstraints,
        undefined
      )
    ).toStrictEqual([]);

    expect(
      constraintManager.filterDuplicateProcessConstraints(
        undefined,
        processConstraints1.hardConstraints
      )
    ).toStrictEqual(processConstraints1.hardConstraints);

    expect(constraintManager.filterDuplicateProcessConstraints(undefined, undefined)).toStrictEqual(
      []
    );
  });

  test('hardconstraint (same key)', () => {
    expect(
      constraintManager.filterDuplicateProcessConstraints(
        flowNodeConstraints1.hardConstraints,
        processConstraints1.hardConstraints
      )
    ).toStrictEqual([]);
  });

  test('hardconstraint (different key)', () => {
    expect(
      constraintManager.filterDuplicateProcessConstraints(
        flowNodeConstraints2.hardConstraints,
        processConstraints2.hardConstraints
      )
    ).toStrictEqual(processConstraints2.hardConstraints);
  });

  test('softconstraint (same key)', () => {
    expect(
      constraintManager.filterDuplicateProcessConstraints(
        flowNodeConstraints1.softConstraints,
        processConstraints1.softConstraints
      )
    ).toStrictEqual([]);
  });

  test('softconstraint (different key)', () => {
    expect(
      constraintManager.filterDuplicateProcessConstraints(
        flowNodeConstraints2.softConstraints,
        processConstraints2.softConstraints
      )
    ).toStrictEqual(processConstraints2.softConstraints);
  });

  test('constraintGroup', () => {
    expect(
      constraintManager.filterDuplicateProcessConstraints(
        flowNodeConstraints3.hardConstraints,
        processConstraints3.hardConstraints
      )
    ).toStrictEqual(processConstraints3.hardConstraints);
  });
});

describe('#concatAllConstraints', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });
  test('no flowNode and/or process Constraints are given', () => {
    expect(
      constraintManager.concatAllConstraints(flowNodeConstraints1.hardConstraints, undefined)
    ).toStrictEqual(flowNodeConstraints1.hardConstraints);

    expect(
      constraintManager.concatAllConstraints(undefined, processConstraints1.hardConstraints)
    ).toStrictEqual(processConstraints1.hardConstraints);

    expect(constraintManager.concatAllConstraints(undefined, undefined)).toStrictEqual([]);
  });

  test('hardconstraint (same key)', () => {
    expect(
      constraintManager.concatAllConstraints(
        flowNodeConstraints1.hardConstraints,
        processConstraints1.hardConstraints
      )
    ).toStrictEqual(flowNodeConstraints1.hardConstraints);
  });

  test('hardconstraint (different key)', () => {
    expect(
      constraintManager.concatAllConstraints(
        flowNodeConstraints2.hardConstraints,
        processConstraints2.hardConstraints
      )
    ).toStrictEqual(concatenatedConstraints2.hardConstraints);
  });

  test('softconstraint (same key)', () => {
    expect(
      constraintManager.concatAllConstraints(
        flowNodeConstraints1.softConstraints,
        processConstraints1.softConstraints
      )
    ).toStrictEqual(flowNodeConstraints1.softConstraints);
  });

  test('softconstraint (different key)', () => {
    expect(
      constraintManager.concatAllConstraints(
        flowNodeConstraints2.softConstraints,
        processConstraints2.softConstraints
      )
    ).toStrictEqual(concatenatedConstraints2.softConstraints);
  });

  test('constraintGroup', () => {
    expect(
      constraintManager.concatAllConstraints(
        flowNodeConstraints3.hardConstraints,
        processConstraints3.hardConstraints
      )
    ).toStrictEqual(concatenatedConstraints3.hardConstraints);
  });
});

describe('#preCheckLocalExec', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });
  test('constraint sameMachine as true', async () => {
    expect(
      await constraintManager.preCheckLocalExec(sameMachineConstraint1.hardConstraints)
    ).toEqual(true);
  });

  test('constraint sameMachine as false', async () => {
    expect(
      await constraintManager.preCheckLocalExec(sameMachineConstraint2.hardConstraints)
    ).toEqual(false);
  });

  test('no constraint sameMachine given', async () => {
    expect(await constraintManager.preCheckLocalExec(exampleConstraints.hardConstraints)).toEqual(
      false
    );
  });

  test('softConstraintPolicy is LocalMachineOnly', async () => {
    config.readConfig.mockResolvedValue('LocalMachineOnly');
    expect(await constraintManager.preCheckLocalExec(exampleConstraints.hardConstraints)).toEqual(
      true
    );
  });
});

describe('#sendHardConstraints', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });
  test('no allowed machines available', (done) => {
    communication.getAvailableMachines.mockReturnValueOnce([
      { ip: '222.222.222.222', id: 'machine-4' },
    ]);

    const callback = jest.fn();

    constraintManager.sendHardConstraints(
      exampleConstraints.hardConstraints,
      exampleConstraints.softConstraints,
      {},
      callback
    );

    setTimeout(() => {
      // no allowed machines available -> no request to be sent
      expect(network.sendRequest).toHaveBeenCalledTimes(0);
      expect(callback).toHaveBeenCalledTimes(1);
      done();
    }, 0);
  });

  test('requests every allowed machine', (done) => {
    communication.getAvailableMachines.mockReturnValueOnce([
      { ip: '111.111.111.111', id: 'machine-1' },
      { ip: '123.123.123.123', id: 'machine-2' },
      { ip: '101.101.101.101', id: 'machine-3' },
      { ip: '222.222.222.222', id: 'machine-4' },
    ]);
    network.sendRequest.mockResolvedValue({ body: JSON.stringify(false) });

    const callback = jest.fn();

    constraintManager.sendHardConstraints(
      exampleConstraints.hardConstraints,
      exampleConstraints.softConstraints,
      {},
      callback
    );

    setTimeout(() => {
      // only 3 out of 4 available machines get requested due to adress-constraints in exampleConstraints
      expect(network.sendRequest).toHaveBeenCalledTimes(3);
      expect(callback).toBeCalledTimes(3);
      done();
    }, 0);
  });

  test('callback called with no values if machine gives false as response', (done) => {
    communication.getAvailableMachines.mockReturnValueOnce([
      { ip: '111.111.111.111', id: 'machine-1' },
    ]);
    network.sendRequest.mockResolvedValue({
      body: JSON.stringify(false),
    });

    const callback = jest.fn();

    constraintManager.sendHardConstraints(
      exampleConstraints.hardConstraints,
      exampleConstraints.softConstraints,
      {},
      callback
    );

    setTimeout(() => {
      expect(network.sendRequest).toHaveBeenCalledTimes(1);
      expect(callback).toBeCalledWith(undefined, false);
      done();
    }, 10);
  });

  test('callback called with softConstraint values if hardConstraints are fulfilled for machine', (done) => {
    communication.getAvailableMachines.mockReturnValueOnce([
      { ip: '111.111.111.111', id: 'machine-1' },
    ]);
    const machineMemFree = 'machine.mem.free';
    network.sendRequest.mockResolvedValue({
      body: JSON.stringify({ [machineMemFree]: 500 }),
    });

    const callback = jest.fn();

    constraintManager.sendHardConstraints(
      exampleConstraints.hardConstraints,
      exampleConstraints.softConstraints,
      {},
      callback
    );

    setTimeout(() => {
      expect(network.sendRequest).toHaveBeenCalledTimes(1);
      expect(callback).toHaveBeenCalledWith(
        {
          machineId: 'machine-1',
          softConstraintValues: {
            [machineMemFree]: 500,
          },
        },
        false
      );
      done();
    }, 0);
  });
});

describe('#getExternalSoftConstraintValues', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });
  test('valueList contains machines with softConstraintValues', async () => {
    config.readConfig.mockResolvedValue({
      router: {
        waitTimeExternalEvaluations: 1000,
      },
      engine: {
        networkRequestTimeout: 1,
      },
    });

    communication.getAvailableMachines.mockReturnValueOnce([
      { ip: '111.111.111.111', id: 'machine-1' },
      { ip: '123.123.123.123', id: 'machine-2' },
      { ip: '101.101.101.101', id: 'machine-3' },
      { ip: '222.222.222.222', id: 'machine-4' },
    ]);

    const machineMemFree = 'machine.mem.free';
    network.sendRequest
      .mockResolvedValueOnce({
        body: JSON.stringify({ [machineMemFree]: 500 }),
      })
      .mockResolvedValueOnce({
        body: JSON.stringify({ [machineMemFree]: 1000 }),
      })
      .mockResolvedValueOnce({
        body: JSON.stringify({ [machineMemFree]: 2000 }),
      });

    expect(
      await constraintManager.getExternalSoftConstraintValues(
        exampleConstraints.hardConstraints,
        exampleConstraints.softConstraints,
        {}
      )
    ).toEqual([
      { machineId: 'machine-1', softConstraintValues: { [machineMemFree]: 500 } },
      { machineId: 'machine-2', softConstraintValues: { [machineMemFree]: 1000 } },
      { machineId: 'machine-3', softConstraintValues: { [machineMemFree]: 2000 } },
    ]);
  });

  test('do not listen for further answers after networkRequestTimeout expired', async () => {
    config.readConfig.mockResolvedValue({
      router: {
        waitTimeExternalEvaluations: 1000,
      },
      engine: {
        networkRequestTimeout: 1,
      },
    });
    communication.getAvailableMachines.mockReturnValueOnce([
      { ip: '111.111.111.111', id: 'machine-1' },
      { ip: '123.123.123.123', id: 'machine-2' },
      { ip: '101.101.101.101', id: 'machine-3' },
      { ip: '222.222.222.222', id: 'machine-4' },
    ]);

    const machineMemFree = 'machine.mem.free';
    network.sendRequest
      .mockImplementationOnce(
        () =>
          new Promise((resolve) =>
            setTimeout(
              () =>
                resolve({
                  body: JSON.stringify({ [machineMemFree]: 500 }),
                }),
              100
            )
          )
      )
      .mockImplementationOnce(
        () =>
          new Promise((resolve) =>
            setTimeout(
              () =>
                resolve({
                  body: JSON.stringify({ [machineMemFree]: 1000 }),
                }),
              200
            )
          )
      )
      .mockImplementationOnce(
        () =>
          new Promise((resolve) =>
            setTimeout(
              () =>
                resolve({
                  body: JSON.stringify({ [machineMemFree]: 2000 }),
                }),
              2000 // return after 2 seconds, after networkRequestTimeout already expired
            )
          )
      );
    expect(
      await constraintManager.getExternalSoftConstraintValues(
        exampleConstraints.hardConstraints,
        exampleConstraints.softConstraints,
        {}
      )
    ).toEqual([
      { machineId: 'machine-1', softConstraintValues: { [machineMemFree]: 500 } },
      { machineId: 'machine-2', softConstraintValues: { [machineMemFree]: 1000 } },
    ]);
  });

  test('do not listen for further answers after finding firsting fitting machine if softConstraintPolicy is OnFirstFittingMachine', async () => {
    config.readConfig.mockResolvedValue({
      router: {
        waitTimeExternalEvaluations: 1000,
        softConstraintPolicy: 'OnFirstFittingMachine',
      },
      engine: {
        networkRequestTimeout: 1,
      },
    });

    communication.getAvailableMachines.mockReturnValueOnce([
      { ip: '111.111.111.111', id: 'machine-1' },
      { ip: '123.123.123.123', id: 'machine-2' },
      { ip: '101.101.101.101', id: 'machine-3' },
      { ip: '222.222.222.222', id: 'machine-4' },
    ]);

    network.sendRequest
      .mockImplementationOnce(
        () =>
          new Promise((resolve) =>
            setTimeout(
              () =>
                resolve({
                  body: JSON.stringify({}),
                }),
              100
            )
          )
      )
      .mockImplementationOnce(
        () =>
          new Promise((resolve) =>
            setTimeout(
              () =>
                resolve({
                  body: JSON.stringify({}),
                }),
              200
            )
          )
      )
      .mockImplementationOnce(
        () =>
          new Promise((resolve) =>
            setTimeout(
              () =>
                resolve({
                  body: JSON.stringify({}),
                }),
              300
            )
          )
      );
    expect(
      await constraintManager.getExternalSoftConstraintValues(
        exampleConstraints.hardConstraints,
        exampleConstraints.softConstraints,
        {}
      )
    ).toEqual([{ machineId: 'machine-1', softConstraintValues: {} }]);
  });
});

describe('#getLocalSoftConstraintValues', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });
  test('no hardConstraints are given', async () => {
    config.readConfig.mockResolvedValue({
      router: {
        waitTimeExternalEvaluations: 1000,
      },
      engine: {
        networkRequestTimeout: 1,
      },
      processes: {
        acceptUserTasks: true,
      },
    });

    expect(await constraintManager.getLocalSoftConstraintValues([], [], {})).toEqual({});
  });

  test('hardConstraints not fulfilled locally', async () => {
    config.readConfig.mockResolvedValue({
      router: {
        waitTimeExternalEvaluations: 1000,
      },
      engine: {
        networkRequestTimeout: 1,
      },
      processes: {
        acceptUserTasks: true,
      },
    });
    const infos = {
      network: {
        ip4: '222.222.222.222',
      },
    };
    information.getMachineInformation.mockResolvedValue(infos); // infos for local machine
    expect(
      await constraintManager.getLocalSoftConstraintValues(
        exampleConstraints.hardConstraints,
        [],
        {}
      )
    ).toEqual(null);
  });

  test('userTasks not accepted locally', async () => {
    config.readConfig.mockResolvedValue({
      router: {
        waitTimeExternalEvaluations: 1000,
      },
      engine: {
        networkRequestTimeout: 1,
      },
      processes: {
        acceptUserTasks: false,
      },
    });

    expect(
      await constraintManager.getLocalSoftConstraintValues(exampleConstraints.hardConstraints, [], {
        isUserTask: true,
      })
    ).toEqual(null);
  });

  test('process execution deactivated locally', async () => {
    config.readConfig.mockResolvedValue({
      router: {
        waitTimeExternalEvaluations: 1000,
      },
      engine: {
        networkRequestTimeout: 1,
      },
      processes: {
        acceptUserTasks: true,
        deactivateProcessExecution: true,
      },
    });

    expect(
      await constraintManager.getLocalSoftConstraintValues(
        exampleConstraints.hardConstraints,
        [],
        {}
      )
    ).toEqual(null);
  });

  test('hardConstraints fulfilled locally', async () => {
    config.readConfig.mockResolvedValue({
      router: {
        waitTimeExternalEvaluations: 1000,
      },
      engine: {
        networkRequestTimeout: 1,
      },
      processes: {
        acceptUserTasks: true,
      },
    });

    const machineMemFree = 'machine.mem.free';

    const infos = {
      network: {
        ip4: '111.111.111.111',
      },
      mem: {
        free: 1000,
      },
    };

    information.getMachineInformation.mockResolvedValue(infos); // infos for local machine

    expect(
      await constraintManager.getLocalSoftConstraintValues(
        exampleConstraints.hardConstraints,
        exampleConstraints.softConstraints,
        {}
      )
    ).toEqual({ [machineMemFree]: 1000 });
  });
});

describe('#getSoftConstraintValues', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });
  test('no hardConstraints are given', async () => {
    config.readConfig.mockResolvedValue({
      router: {
        waitTimeExternalEvaluations: 1000,
      },
      engine: {
        networkRequestTimeout: 1,
      },
      processes: {
        acceptUserTasks: true,
      },
    });

    communication.getAvailableMachines.mockReturnValueOnce([
      { ip: '111.111.111.111', id: 'machine-1' },
    ]);

    network.sendRequest.mockResolvedValueOnce({
      body: JSON.stringify({}),
    });

    expect(await constraintManager.getSoftConstraintValues([], [], {})).toEqual([
      { machineId: 'local-engine', softConstraintValues: {} },
      { machineId: 'machine-1', softConstraintValues: {} },
    ]);
  });

  test('execution only locally because softConstraintPolicy is PreferLocalMachine', async () => {
    config.readConfig.mockResolvedValue({
      router: {
        waitTimeExternalEvaluations: 1000,
        softConstraintPolicy: 'PreferLocalMachine',
      },
      engine: {
        networkRequestTimeout: 1,
      },
      processes: {
        acceptUserTasks: true,
      },
    });

    communication.getAvailableMachines.mockReturnValueOnce([
      { ip: '111.111.111.111', id: 'machine-1' },
    ]);
    const infos = {
      network: {
        ip4: '110.110.110.110',
      },
    };
    information.getMachineInformation.mockResolvedValue(infos); // infos for local machine

    network.sendRequest.mockResolvedValueOnce({
      body: JSON.stringify({}),
    });

    expect(
      await constraintManager.getSoftConstraintValues(exampleConstraints.hardConstraints, [], {})
    ).toEqual([{ machineId: 'local-engine', softConstraintValues: {} }]);
  });

  test('execution only locally because softConstraintPolicy is OnFirstFittingMachine', async () => {
    config.readConfig.mockResolvedValue({
      router: {
        waitTimeExternalEvaluations: 1000,
        softConstraintPolicy: 'OnFirstFittingMachine',
      },
      engine: {
        networkRequestTimeout: 1,
      },
      processes: {
        acceptUserTasks: true,
      },
    });

    communication.getAvailableMachines.mockReturnValueOnce([
      { ip: '111.111.111.111', id: 'machine-1' },
    ]);
    const infos = {
      network: {
        ip4: '110.110.110.110',
      },
    };
    information.getMachineInformation.mockResolvedValue(infos); // infos for local machine

    network.sendRequest.mockResolvedValueOnce({
      body: JSON.stringify({}),
    });

    expect(
      await constraintManager.getSoftConstraintValues(exampleConstraints.hardConstraints, [], {})
    ).toEqual([{ machineId: 'local-engine', softConstraintValues: {} }]);
  });
});
