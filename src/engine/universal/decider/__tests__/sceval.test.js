/* eslint-disable import/no-dynamic-require */
const Sceval = require('../soft_constraint_evaluation/sc-eval.js');

const path = './../../../../helper-modules/constraint-parser-xml-json/__tests__/ConstraintsJSON/';

const test1Input = require(`${path}1-ConstraintsJSON.json`).processConstraints;
const test2Input = require(`${path}2-ProcessJSON.json`).processConstraints;
const test3Input = require(`${path}2-ConcatenationJSON.json`).processConstraints;
const test4Input = require(`${path}CompleteEvaluationConstraintsJSON.json`).processConstraints;

jest.mock('@proceed/machine', () => ({
  information: {
    getMachineInformation: jest.fn(),
  },
}));

describe('#evaluateEveryMachine', () => {
  test('no softconstraints given', () => {
    const valueList = [
      {
        machineId: 'machine1',
        softConstraintValues: {},
      },
      {
        machineId: 'machine2',
        softConstraintValues: {},
      },
      {
        machineId: 'machine3',
        softConstraintValues: {},
      },
    ];
    expect(Sceval.evaluateEveryMachine([], valueList)).toEqual([
      'machine1',
      'machine2',
      'machine3',
    ]);
  });

  test('single softconstraint (max)', () => {
    const machineMemFree = 'machine.mem.free';
    const valueList = [
      {
        machineId: 'machine1',
        softConstraintValues: {
          [machineMemFree]: 20,
        },
      },
      {
        machineId: 'machine2',
        softConstraintValues: {
          [machineMemFree]: 50,
        },
      },
      {
        machineId: 'machine3',
        softConstraintValues: {
          [machineMemFree]: 60,
        },
      },
    ];
    expect(Sceval.evaluateEveryMachine(test1Input.softConstraints, valueList)).toEqual([
      'machine3',
      'machine2',
      'machine1',
    ]);
  });

  test('single softconstraint (min)', () => {
    const machineCpuCurrentLoad = 'machine.cpu.currentLoad';
    const valueList = [
      {
        machineId: 'machine1',
        softConstraintValues: {
          [machineCpuCurrentLoad]: 20,
        },
      },
      {
        machineId: 'machine2',
        softConstraintValues: {
          [machineCpuCurrentLoad]: 30,
        },
      },
      {
        machineId: 'machine3',
        softConstraintValues: {
          [machineCpuCurrentLoad]: 60,
        },
      },
    ];
    expect(Sceval.evaluateEveryMachine(test2Input.softConstraints, valueList)).toEqual([
      'machine1',
      'machine2',
      'machine3',
    ]);
  });

  test('multiple softconstraints', () => {
    const machineMemFree = 'machine.mem.free';
    const machineCpuCurrentLoad = 'machine.cpu.currentLoad';
    const valueList = [
      {
        machineId: 'machine1',
        softConstraintValues: {
          [machineMemFree]: 20,
          [machineCpuCurrentLoad]: 20,
        },
      },
      {
        machineId: 'machine2',
        softConstraintValues: {
          [machineMemFree]: 50,
          [machineCpuCurrentLoad]: 30,
        },
      },
      {
        machineId: 'machine3',
        softConstraintValues: {
          [machineMemFree]: 60,
          [machineCpuCurrentLoad]: 60,
        },
      },
    ];
    expect(Sceval.evaluateEveryMachine(test3Input.softConstraints, valueList)).toEqual([
      'machine2',
      'machine1',
      'machine3',
    ]);
  });

  test('multiple softconstraints with weights', () => {
    const machineMemFree = 'machine.mem.free';
    const machineCpuCurrentLoad = 'machine.cpu.currentLoad';
    const valueList = [
      {
        machineId: 'machine1',
        softConstraintValues: {
          [machineMemFree]: 20,
          [machineCpuCurrentLoad]: 20,
        },
      },
      {
        machineId: 'machine2',
        softConstraintValues: {
          [machineMemFree]: 50,
          [machineCpuCurrentLoad]: 30,
        },
      },
      {
        machineId: 'machine3',
        softConstraintValues: {
          [machineMemFree]: 60,
          [machineCpuCurrentLoad]: 60,
        },
      },
    ];
    expect(Sceval.evaluateEveryMachine(test4Input.softConstraints, valueList)).toEqual([
      'machine1',
      'machine2',
      'machine3',
    ]);
  });
});
