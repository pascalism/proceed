/* eslint-disable guard-for-in */
const { communication } = require('@proceed/distribution');
const { information, config } = require('@proceed/machine');
const { network, timer } = require('@proceed/system');
const Hceval = require('./hard_constraint_evaluation/hc-eval.js');

module.exports = {
  /**
   * Retrieves values for every softconstraint for local and external machines which satisfies the hard constraints
   *
   * @param {Array} hardConstraints Every hardconstraint to be fullfiled by the machine
   * @param {Array} softConstraints - Softconstraints to retrieve value for
   * @param {Object} flowNodeinformation - information about the next flowNode to be executed, e.g. whether it is a user task
   * @returns {Array} valuesList - List with machines and corresponding softConstraintValues
   */
  async getSoftConstraintValues(hardConstraints, softConstraints, flowNodeInformation) {
    const { router } = await config.readConfig();
    let valuesList = [];
    const localSoftConstraintValues = await this.getLocalSoftConstraintValues(
      hardConstraints,
      softConstraints,
      flowNodeInformation
    );

    if (localSoftConstraintValues) {
      valuesList.push({
        machineId: 'local-engine',
        softConstraintValues: localSoftConstraintValues,
      });

      if (
        router.softConstraintPolicy === 'PreferLocalMachine' ||
        router.softConstraintPolicy === 'OnFirstFittingMachine'
      ) {
        return valuesList;
      }
    }

    const valuesListExternalMachines = await this.getExternalSoftConstraintValues(
      hardConstraints,
      softConstraints,
      flowNodeInformation
    ); // get valuesList for external machines
    valuesList = [...valuesList, ...valuesListExternalMachines];

    return valuesList;
  },

  /**
   * Retrieves values for every softconstraint for every external machine which satisfies the hardconstraints
   * External machines have to respect the adress-data constraints
   * @param {Array} hardConstraints Every hardconstraint to be fullfiled by external machines
   * @param {Array} softConstraints - Softconstraints to retrieve value for from external machines
   * @param {Object} flowNodeinformation - information about the next flowNode to be executed, e.g. whether it is a user task
   * @returns {Array} valuesList - List with externalmachines and corresponding softConstraintValues
   */
  async getExternalSoftConstraintValues(hardConstraints, softConstraints, flowNodeInformation) {
    // get softconstraint values for external machines

    const { router, engine } = await config.readConfig();

    const timeout = Math.min(
      router.waitTimeExternalEvaluations,
      engine.networkRequestTimeout * 1000
    );

    return new Promise((resolve) => {
      const valuesList = [];

      // stop listening to network requests after timer expired
      timer.setTimeout(() => {
        resolve(valuesList);
      }, timeout);

      const callback = (result, moreAreComing) => {
        if (result) {
          valuesList.push(result);
          if (router.softConstraintPolicy === 'OnFirstFittingMachine') {
            resolve(valuesList);
          }
        }

        if (!moreAreComing) {
          resolve(valuesList);
        }
      };
      if (
        router.softConstraintPolicy === 'AsFastAsPossible' ||
        router.softConstraintPolicy === 'OnFirstFittingMachine'
      ) {
        this.sendHardConstraints(hardConstraints, [], flowNodeInformation, callback);
      } else {
        this.sendHardConstraints(hardConstraints, softConstraints, flowNodeInformation, callback);
      }
    });
  },

  /**
   * Retrieves values for every softconstraint for local machine if it satisfies the hardconstraints
   * @param {Array} hardConstraints Every hardconstraint to be fullfiled by local machine
   * @param {Array} softConstraints - Softconstraints to retrieve value for
   * @param {Object} flowNodeinformation - information about the next flowNode to be executed, e.g. whether it is a user task
   * @returns {object|null} Returns softConstraintValues if hardConstraints fulfilled or null if not
   */
  async getLocalSoftConstraintValues(hardConstraints, softConstraints, flowNodeInformation) {
    const { processes } = await config.readConfig();

    if (
      !processes.deactivateProcessExecution &&
      !(flowNodeInformation && flowNodeInformation.isUserTask && !processes.acceptUserTasks) &&
      (hardConstraints.length === 0 ||
        (await Hceval.machineSatisfiesAllHardConstraints(hardConstraints)))
    ) {
      let localSoftConstraintValues = {}; // get softconstraint values for local machine

      if (softConstraints.length > 0) {
        const informationCategories = softConstraints.reduce((categories, softConstraint) => {
          const [newCategory] = softConstraint.name.replace('machine.', '').split('.');

          if (!categories.includes(newCategory)) {
            categories.push(newCategory);
          }

          return categories;
        }, []);

        const machineInformation = await information.getMachineInformation(informationCategories);

        // map nested values to softconstraints
        // e.g: SC: machine.cpu.cores, IO { cpu: { cores: 12, ... }, ...} => { "machine.cpu.cores": 12 }
        localSoftConstraintValues = softConstraints.reduce((values, softConstraint) => {
          let name = softConstraint.name.replace('machine.', '');

          let deepValue;

          while (name) {
            const [subscript, nextName] = name.split('.');
            deepValue = deepValue ? deepValue[subscript] : machineInformation[subscript];
            name = nextName;
          }

          return { ...values, [softConstraint.name]: deepValue };
        }, {});
      }
      return localSoftConstraintValues;
    }
    return null;
  },

  /**
   * Send HardConstraints to every allowed Machine and call callback with softConstraint values for every succesful request
   * which satisfies hard constraints
   * @param {Array} hardConstraints Every hardconstraint to be fullfiled by external machines
   * @param {Array} softConstraints - Softconstraints to retrieve value for from external machines
   * @param {Object} flowNodeInformation - information about the next flowNode to be executed, e.g. whether it is a user task
   */
  sendHardConstraints(hardConstraints, softConstraints, flowNodeInformation, callback) {
    const adressConstraintNames = [
      'machine.id',
      'machine.name',
      'machine.hostname',
      'machine.network.ip4',
      'machine.network.ip6',
      'machine.network.mac',
      'machine.network.netmaskv4',
      'machine.network.netmaskv6',
    ];

    const adressConstraints = hardConstraints.filter((hardConstraint) =>
      adressConstraintNames.includes(hardConstraint.name)
    );
    const availableMachines = communication.getAvailableMachines();
    // filter for allowed machines using adressData
    const allowedMachines = availableMachines.filter((availableMachine) => {
      const { ip, id, name, hostname } = availableMachine;
      // true if machine satisfies all adress-constrains
      return Hceval.evaluateAllConstraints(adressConstraints, {
        'machine.network.ip4': ip,
        'machine.id': id,
        'machine.name': name,
        'machine.hostname': hostname,
      });
    });

    if (allowedMachines.length === 0) {
      callback(undefined, false);
    }

    let remainingRequestsCounter = allowedMachines.length;

    allowedMachines.forEach((allowedMachine) => {
      const { ip, port, id } = allowedMachine;
      const data = {};
      data.machineId = id;
      data.softConstraintValues = {};

      // request hardConstraint evaluation and softConstraint values
      network
        .sendData(ip, port, '/evaluation', 'POST', 'application/json', {
          formData: { hardConstraints, softConstraints, flowNodeInformation },
        })
        .then(
          (result) => {
            // Handle request success or failures
            const softConstraintValues = JSON.parse(result.body);

            if (softConstraintValues) {
              data.softConstraintValues = softConstraintValues;
              --remainingRequestsCounter > 0 ? callback(data, true) : callback(data, false);
            } else {
              --remainingRequestsCounter > 0
                ? callback(undefined, true)
                : callback(undefined, false);
            }
          },
          (error) => {
            --remainingRequestsCounter > 0 ? callback(undefined, true) : callback(undefined, false);
          }
        );
    });
  },

  /**
   * Concatenates flowNodeConstraints and processConstraint
   * For equal keys the flowNodeConstraint is prefered
   * @param {Array} flowNodeConstraints
   * @param {Array} processConstraints
   * @returns {Array} concatenatedConstraints - all flowNodeConstraints, and processConstraints without duplicates
   */
  concatAllConstraints(flowNodeConstraints, processConstraints) {
    if (!processConstraints && !flowNodeConstraints) {
      return [];
    }
    if (!processConstraints) {
      return [...flowNodeConstraints];
    }
    if (!flowNodeConstraints) {
      return [...processConstraints];
    }

    const filteredProcessConstraints = this.filterDuplicateProcessConstraints(
      flowNodeConstraints,
      processConstraints
    );

    const concatenatedConstraints = [...filteredProcessConstraints, ...flowNodeConstraints];
    return concatenatedConstraints;
  },

  /**
   * Filters out constraints from processConstraints which are already present in flowNodeConstraints
   * @param {Array} flowNodeConstraints
   * @param {Array} processConstraints
   * @returns {Array} processConstraints without duplicates
   */
  filterDuplicateProcessConstraints(flowNodeConstraints, processConstraints) {
    if (!processConstraints) {
      return [];
    }
    if (!flowNodeConstraints) {
      return processConstraints;
    }

    const filteredProcessConstraints = processConstraints.filter((processConstraint) => {
      if (processConstraint._type !== 'constraintGroup') {
        const duplicateConstraint = flowNodeConstraints.find((flowNodeConstraint) => {
          return flowNodeConstraint.name === processConstraint.name;
        });
        return !duplicateConstraint;
      }
      return true;
    });

    return filteredProcessConstraints;
  },

  /**
   *  Checks if local engine has to be used
   * @param {Array} constraints
   * @returns boolean - true if execution has to be on local engine, false if not
   */
  async preCheckLocalExec(constraints) {
    // last occurence of sameMachine-constraint
    const softConstraintPolicy = await config.readConfig('router.softConstraintPolicy');
    if (softConstraintPolicy === 'LocalMachineOnly') {
      return true;
    }

    const sameMachineConstraint = constraints
      .reverse()
      .find((constraint) => constraint.name === 'sameMachine');
    if (!sameMachineConstraint) {
      return false;
    }
    // should always be only one value in values-array
    return sameMachineConstraint.values[sameMachineConstraint.values.length - 1].value;
  },
};
