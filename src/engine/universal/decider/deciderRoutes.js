/* eslint-disable no-plusplus */
const { network } = require('@proceed/system');
const { information, config } = require('@proceed/machine');
const Hceval = require('./hard_constraint_evaluation/hc-eval');
const constraintManager = require('./constraintManager');

const route = '/evaluation';

module.exports = () => {
  network.post(`${route}/`, { cors: true }, async (req) => {
    const { hardConstraints, softConstraints, flowNodeInformation } = req.body.formData;

    const localSoftConstraintValues = await constraintManager.getLocalSoftConstraintValues(
      hardConstraints,
      softConstraints,
      flowNodeInformation
    );

    return JSON.stringify(localSoftConstraintValues);
  });
};
