const db = require('../db.js');
const { data } = require('@proceed/system');
const fs = require('fs');
const path = require('path');

jest.mock('@proceed/system', () => ({
  data: {
    read: jest.fn().mockResolvedValue(null),
    write: jest.fn(),
    delete: jest.fn(),
  },
}));

jest.mock('@proceed/machine', () => ({
  information: {
    getMachineInformation: jest.fn().mockResolvedValue({ id: 'mockId', ip: 'mockIp' }),
  },
}));

const OneProcessDefinition = fs.readFileSync(
  path.resolve(__dirname, 'data/OneProcess.xml'),
  'utf-8'
);
const TwoProcessesDefinition = fs.readFileSync(
  path.resolve(__dirname, 'data/TwoProcesses.xml'),
  'utf-8'
);
const OneUserTaskDefinition = fs.readFileSync(
  path.resolve(__dirname, 'data/OneUserTask.xml'),
  'utf-8'
);
const OneImportDefinition = fs.readFileSync(path.resolve(__dirname, 'data/OneImport.xml'), 'utf-8');
const OneImportWrongProcessRefDefinition = fs.readFileSync(
  path.resolve(__dirname, 'data/OneImportWrongProcessRef.xml'),
  'utf-8'
);
const TwoUserTasksStaticDefinition = fs.readFileSync(
  path.resolve(__dirname, 'data/TwoUserTasksStatic.xml'),
  'utf-8'
);

describe('Tests for the functions in the database module', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  describe('isProcessExisting', () => {
    it("returns false if the process doesn't exist", async () => {
      const result = await db.isProcessExisting('testFile');

      expect(result).toBe(false);
      expect(data.read).toHaveBeenCalledWith('processes/testFile');
    });
    it('returns true if the process exists', async () => {
      data.read.mockResolvedValueOnce('Test');

      const result = await db.isProcessExisting('testFile');

      expect(result).toBe(true);
      expect(data.read).toHaveBeenCalledWith('processes/testFile');
    });
  });
  describe('saveProcessDefinition', () => {
    it('saves the process definition if it contains only one process, also stores deployment date', async () => {
      await db.saveProcessDefinition('OneProcess', OneProcessDefinition);

      expect(data.write).toHaveBeenCalledTimes(4);
      expect(data.write).toHaveBeenCalledWith('OneProcess/bpmn', OneProcessDefinition);
      expect(data.write).toHaveBeenCalledWith('OneProcess/deploymentDate', expect.any(String));
      expect(data.write).toHaveBeenCalledWith('OneProcess/validated', 'false');
      expect(data.write).toHaveBeenCalledWith('processes/OneProcess', expect.any(String));
    });
    it('rejects process descriptions that contain more than one process', async () => {
      await expect(
        db.saveProcessDefinition('twoProcesses', TwoProcessesDefinition)
      ).rejects.toThrowError();

      expect(data.write).toHaveBeenCalledTimes(0);
    });
  });

  describe('saveImportedProcessDefinition', () => {
    it('saves the definition of an imported process into the file the importing process definition is stored in', async () => {
      await db.saveImportedProcessDefinition(
        'importingProcess',
        'importedProcess',
        OneProcessDefinition
      );

      expect(data.write).toHaveBeenCalledTimes(1);
      expect(data.write).toHaveBeenCalledWith(
        'importingProcess/import_importedProcess',
        OneProcessDefinition
      );
    });
  });

  describe('saveHTMLString', () => {
    beforeEach(() => {
      data.read.mockResolvedValueOnce('Process exists');
    });

    it('saves the html of an user task into the file the process containing it is stored in', async () => {
      await db.saveHTMLString('processDefinitionId', 'taskFileName', 'HTML');

      expect(data.write).toHaveBeenCalledTimes(1);
      expect(data.write).toHaveBeenCalledWith('processDefinitionId/html_taskFileName', 'HTML');
    });

    it('rejects on missing html', async () => {
      await expect(db.saveHTMLString('processDefinitionId', 'taskFileName')).rejects.toThrowError();
    });

    it('saves the html of an user task in an imported process into the file containing the main process', async () => {
      await db.saveHTMLString('processDefinitionId', 'taskFileName', 'HTML', true);

      expect(data.write).toHaveBeenCalledTimes(1);
      expect(data.write).toHaveBeenCalledWith(
        'processDefinitionId/import_html_taskFileName',
        'HTML'
      );
    });
  });

  describe('getter functions', () => {
    beforeEach(() => {
      data.read.mockResolvedValueOnce({
        bpmn: OneProcessDefinition,
        html_html1: 'HTML1',
        import_importProcess1: 'IMPORT1',
        import_html_importHtml1: 'IMPORTHTML1',
        import_importProcess2: 'IMPORT2',
        deploymentDate: 1337,
      });
    });

    describe('getProcessInfo', () => {
      it('returns information about a stored process', async () => {
        const process = await db.getProcessInfo('_a04f4854-6e50-408f-8ec5-18f4541c32e9');
        expect(process).toStrictEqual({
          bpmn: OneProcessDefinition,
          deploymentDate: 1337,
          definitionId: '_a04f4854-6e50-408f-8ec5-18f4541c32e9',
          definitionName: 'OneProcess',
          deploymentMethod: 'dynamic',
        });
        expect(data.read).toHaveBeenCalledTimes(1);
      });
    });

    describe('getImportedProcesses', () => {
      it('returns an object containing the imported processes for a process and their definitions', async () => {
        expect(await db.getImportedProcesses('importingProcessDefinitionId')).toStrictEqual({
          importProcess1: 'IMPORT1',
          importProcess2: 'IMPORT2',
        });
        expect(data.read).toHaveBeenCalledTimes(1);
      });
    });

    describe('getImportedProcess', () => {
      it('returns the description for the requested imported process', async () => {
        expect(await db.getImportedProcess('importingProcessDefinitionId', 'importProcess1')).toBe(
          'IMPORT1'
        );
        expect(data.read).toHaveBeenCalledTimes(1);
      });
    });

    describe('getAllUserTasks', () => {
      it('returns the file names for all user task data', async () => {
        expect(await db.getAllUserTasks('processDefinitionId')).toStrictEqual(['html1']);
        expect(data.read).toHaveBeenCalledTimes(1);
      });
      it('returns the file names for all user task data in processes imported by the given process', async () => {
        expect(await db.getAllUserTasks('processDefinitionId', true)).toStrictEqual([
          'importHtml1',
        ]);
        expect(data.read).toHaveBeenCalledTimes(1);
      });
    });
  });

  describe('isProcessValid', () => {
    it('returns true for a process without any user tasks and imports and sets validated flag in file', async () => {
      data.read.mockResolvedValueOnce({
        bpmn: OneProcessDefinition,
        validated: 'false',
      });

      const result = await db.isProcessValid('processDefinitionId');

      expect(result).toBe(true);
      expect(data.write).toHaveBeenCalledWith('processDefinitionId/validated', 'true');
    });
    it('returns true immediately when validated flag is set in the process file', async () => {
      data.read.mockResolvedValueOnce({
        bpmn: OneProcessDefinition,
        validated: 'true',
      });

      const result = await db.isProcessValid('processDefinitionId');

      expect(result).toBe(true);
      expect(data.write).toBeCalledTimes(0);
    });
    it('returns false for missing user task html', async () => {
      data.read.mockResolvedValueOnce({
        bpmn: OneUserTaskDefinition,
      });

      const result = await db.isProcessValid('processDefinitionId');

      expect(result).toBe(false);
    });
    it('returns true for process with user task and existing html', async () => {
      data.read.mockResolvedValueOnce({
        bpmn: OneUserTaskDefinition,
        html_User_Task_1: 'HTML',
      });

      const result = await db.isProcessValid('processDefinitionId');

      expect(result).toBe(true);
    });
    it('returns false for missing definition of imported process', async () => {
      data.read.mockResolvedValueOnce({
        bpmn: OneImportDefinition,
      });

      const result = await db.isProcessValid('processDefinitionId');

      expect(result).toBe(false);
    });
    it('returns false for missing html for user task in imported process', async () => {
      data.read.mockResolvedValueOnce({
        bpmn: OneImportDefinition,
        import_OneUserTask: OneUserTaskDefinition,
      });

      const result = await db.isProcessValid('processDefinitionId');

      expect(result).toBe(false);
    });
    it('returns true for existing imported process with existing user task html', async () => {
      data.read.mockResolvedValueOnce({
        bpmn: OneImportDefinition,
        import_OneUserTask: OneUserTaskDefinition,
        import_html_User_Task_1: 'HTML',
      });

      const result = await db.isProcessValid('processDefinitionId');

      expect(result).toBe(true);
    });
    it("returns false if the process id referenced in the importing file doesn't match the process id in the referenced file", async () => {
      data.read.mockResolvedValueOnce({
        bpmn: OneImportWrongProcessRefDefinition,
        import_OneUserTask: OneUserTaskDefinition,
        import_html_User_Task_1: 'HTML',
      });

      const result = await db.isProcessValid('processDefinitionId');

      expect(result).toBe(false);
    });
    it('returns true when deployment method is static and missing user task is supposed to be executed on another machine', async () => {
      data.read.mockResolvedValueOnce({
        bpmn: TwoUserTasksStaticDefinition,
        html_User_Task_2: 'HTML',
      });

      const result = await db.isProcessValid('processDefinitionId');

      expect(result).toBe(true);
    });
  });
  describe('archiveInstance', () => {
    it('stores the instance information inside the process file', async () => {
      await db.archiveInstance('processDefinitionId', 'instanceId', {
        info: 'interesting instance information',
      });

      expect(data.write).toHaveBeenCalledWith(
        'processDefinitionId/archived_instance_instanceId',
        '{"info":"interesting instance information"}'
      );
    });
  });
  describe('getArchivedInstances', () => {
    it('returns all archived instances for a specific process', async () => {
      data.read.mockResolvedValueOnce({
        bpmn: OneProcessDefinition,
        html_html1: 'HTML1',
        import_importProcess1: 'IMPORT1',
        import_html_importHtml1: 'IMPORTHTML1',
        archived_instance_1: '{"info":"interesting instance info"}',
        archived_instance_2: '{"info":"other interesting instance info"}',
        deploymentDate: 1337,
      });

      expect(await db.getArchivedInstances('definitionId')).toStrictEqual({
        1: { info: 'interesting instance info' },
        2: { info: 'other interesting instance info' },
      });
    });
  });
});
