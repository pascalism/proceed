const {
  getDeploymentMethod,
  getUserTaskFileNameMapping,
  getCallActivityDefinitionIdMapping,
  getElementMachineMapping,
  validateCalledProcess,
} = require('@proceed/bpmn-helper');

const { information } = require('@proceed/machine');

async function validateProcess(bpmn, knownUserTaskFiles, knownImports, knownImportedUserTaskFiles) {
  const deployMethod = await getDeploymentMethod(bpmn);
  const userTasksToKnow = await getUserTaskFileNameMapping(bpmn);
  let importsToKnow = await getCallActivityDefinitionIdMapping(bpmn);

  // for static deployment filter which information we actually need to know on this machine
  // for dynamic deployment we expect to know all information
  if (deployMethod === 'static') {
    const taskToMachineMapping = await getElementMachineMapping(bpmn);

    let locallyExecuted;
    try {
      const { id, ip } = await information.getMachineInformation(['id', 'ip']);
      // create array of tasks that are executed on this machine
      locallyExecuted = Object.entries(taskToMachineMapping).reduce(
        (prev, [taskId, machineInfo]) => {
          const newArr = [...prev];

          if (machineInfo.machineId) {
            if (machineInfo.machineId === id) {
              newArr.push(taskId);
            }
          } else if (machineInfo.machineAddress) {
            const address = machineInfo.machineAddress.replace(
              /\[?((?:(?:\d|\w)|:|\.)*)\]?:(\d*)/g,
              '$1+$2'
            );
            const [machineIp] = address.split('+');
            if (machineIp === ip) {
              newArr.push(taskId);
            }
          } else {
            throw new Error(
              `No usable information about the machine the task ${taskId} is supposed to be executed on.`
            );
          }

          return newArr;
        },
        []
      );
    } catch (err) {
      return false;
    }

    // remove all entries that are not executed locally from the to know lists
    Object.keys(userTasksToKnow).forEach((taskId) => {
      if (!locallyExecuted.includes(taskId)) {
        delete userTasksToKnow[taskId];
      }
    });
    Object.keys(importsToKnow).forEach((taskId) => {
      if (!locallyExecuted.includes(taskId)) {
        delete importsToKnow[taskId];
      }
    });
  }

  // get the unique name for all user task files needed to execute the process
  const userTaskFilesToKnow = [
    ...new Set(Object.values(userTasksToKnow).map((info) => info.fileName)),
  ];
  importsToKnow = Object.values(importsToKnow);

  // check if we have HTML for all User Tasks that might be encountered
  if (userTaskFilesToKnow.some((taskId) => !knownUserTaskFiles.includes(taskId))) {
    return false;
  }

  // check if we have the descriptions for all processes directly imported by the main process
  if (importsToKnow.some(({ definitionId }) => !Object.keys(knownImports).includes(definitionId))) {
    return false;
  }

  // check if every directly imported process is valid
  const validities = await Promise.all(
    importsToKnow.map(async ({ definitionId, processId }) => {
      // check if the imported process includes the referenced process id and has only one non typed start event
      let directValidity;

      try {
        directValidity = await validateCalledProcess(knownImports[definitionId], processId);
      } catch (err) {
        directValidity = false;
      }

      //check if nested imported processes and user-tasks in the imported process are valid
      const deepValidity = await validateProcess(
        knownImports[definitionId],
        knownImportedUserTaskFiles,
        knownImports,
        knownImportedUserTaskFiles
      );

      return directValidity && deepValidity;
    })
  );

  if (!validities.every((value) => value === true)) {
    return false;
  }

  return true;
}

module.exports = {
  validateProcess,
};
