const { network } = require('@proceed/system');
const db = require('../database/db');
const APIError = require('./APIError');

module.exports = (path) => {
  network.put(`${path}/:definitionId/user-tasks/:fileName`, { cors: true }, async (req) => {
    const { definitionId } = req.params;
    const { fileName } = req.params;
    const { body } = req;
    const { html } = body;

    await db.saveHTMLString(definitionId, fileName, html);

    return '';
  });

  network.get(`${path}/:definitionId/user-tasks/:fileName`, { cors: true }, async (req) => {
    const { definitionId } = req.params;
    const { fileName } = req.params;

    const html = await db.getHTML(definitionId, fileName);
    if (!html) {
      throw new APIError(404, 'HTML with given ID does not exist!');
    }

    return JSON.stringify({ html });
  });

  network.get(`${path}/:definitionId/user-tasks`, { cors: true }, async (req) => {
    const { definitionId } = req.params;

    const html = await db.getAllUserTasks(definitionId);

    return JSON.stringify(html);
  });

  network.put(
    `${path}/:definitionId/imported/user-tasks/:fileName`,
    { cors: true },
    async (req) => {
      const { definitionId } = req.params;
      const { fileName } = req.params;
      const { body } = req;
      const { html } = body;

      await db.saveHTMLString(definitionId, fileName, html, true);

      return '';
    }
  );

  network.get(
    `${path}/:definitionId/imported/user-tasks/:fileName`,
    { cors: true },
    async (req) => {
      const { definitionId } = req.params;
      const { fileName } = req.params;

      const html = await db.getHTML(definitionId, fileName, true);
      if (!html) {
        throw new APIError(404, 'HTML with given ID does not exist!');
      }

      return JSON.stringify({ html });
    }
  );
};
