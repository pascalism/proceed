const { network } = require('@proceed/system');
const { version } = require('../../../../native/node/package.json');

module.exports = (path) => {
  network.get(`${path}/`, { cors: true }, async () => JSON.stringify({ running: true, version }));
};
