const { network } = require('@proceed/system');
const { logging } = require('@proceed/machine');
const db = require('../database/db');
const APIError = require('./APIError');

module.exports = (path, management) => {
  const log = logging.getLogger({ moduleName: 'DISTRIBUTION' });

  network.put(`${path}/:definitionId`, { cors: true }, async (req) => {
    const { definitionId } = req.params;
    const { body } = req;
    const { bpmn } = body;

    await db.saveProcessDefinition(definitionId, bpmn);
    log.info(`Process deployed: ${definitionId}`);
    return JSON.stringify(bpmn);
  });

  network.get(`${path}/:definitionId`, { cors: true }, async (req) => {
    const { definitionId } = req.params;

    let process;
    try {
      process = await db.getProcessInfo(definitionId);
    } catch {
      throw new APIError(404, 'There is no process with the given definitionId!');
    }

    return JSON.stringify(process);
  });

  network.delete(`${path}/:definitionId`, { cors: true }, async (req) => {
    const { definitionId } = req.params;

    await db.deleteProcess(definitionId);
    log.info(`Process deleted: ${definitionId}`);
    return JSON.stringify({});
  });

  // endpoints for processes used as call acitivities in the main process
  network.get(`${path}/:definitionId/imported`, { cors: true }, async (req) => {
    const definitionId = req.params.definitionId;

    let imported;
    try {
      imported = await db.getImportedProcesses(definitionId);
    } catch (err) {
      throw new APIError(404, 'There is no process with the given definitionId!');
    }

    return JSON.stringify(imported);
  });

  network.put(
    `${path}/:definitionId/imported/:importedDefinitionId`,
    { cors: true },
    async (req) => {
      const definitionId = req.params.definitionId;
      const importedDefinitionId = req.params.importedDefinitionId;
      const { body } = req;
      const { bpmn: importedBpmn } = body;

      try {
        await db.saveImportedProcessDefinition(definitionId, importedDefinitionId, importedBpmn);
      } catch (err) {
        throw new APIError(404, 'There is no process with the given definitionId!');
      }

      return importedBpmn;
    }
  );

  network.get(
    `${path}/:definitionId/imported/:importedDefinitionId`,
    { cors: true },
    async (req) => {
      const definitionId = req.params.definitionId;
      const importedDefinitionId = req.params.importedDefinitionId;

      let imported;
      try {
        imported = await db.getImportedProcess(definitionId, importedDefinitionId);
      } catch (err) {
        throw new APIError(404, 'There is no process with the given definitionId!');
      }

      if (!imported) {
        throw new APIError(
          404,
          'There is no imported process with the given definitionId for the given process!'
        );
      }

      return JSON.stringify({ bpmn: imported });
    }
  );

  /**
   * Start listening for new engine instance calls over HTTP.
   * This method tells the management to start up a new engine.
   */
  network.post(`${path}/:definitionId/instance`, { cors: true }, async (req) => {
    const definitionId = req.params.definitionId;
    const { variables, activityID } = req.body;

    const engine = await management.createInstance(definitionId, variables, activityID);

    if (!engine) {
      throw new APIError(406, 'Engine not allowed to start the instance!');
    }

    log.debug(`New process instance (POST) created via API: ${definitionId}`);
    return {
      statusCode: 201,
      mimeType: 'application/json',
      response: JSON.stringify({ instanceId: engine.instanceIDs[0] }),
    };
  });

  network.get(`${path}/:definitionId/instance`, { cors: true }, async (req) => {
    const definitionId = req.params.definitionId;
    const { state: queriedState } = req.query;

    // Test if there is a process with the given id
    try {
      await db.getProcess(definitionId);
    } catch {
      throw new APIError(404, 'There is no process with the given definitionId!');
    }

    const engines = management.getEnginesWithDefinitionId(definitionId);
    const archivedInstances = await db.getArchivedInstances(definitionId);
    const archivedInstanceIds = Object.keys(archivedInstances);

    const instances = engines.reduce((acc, engine) => {
      const newEntries = [];

      // get information for all instances on this engine object that were not archived yet
      engine.instanceIDs.forEach((id) => {
        if (!archivedInstanceIds.includes(id)) {
          newEntries.push(engine.getInstanceInformation(id));
        }
      });

      return acc.concat(newEntries);
    }, []);

    let allInstances = instances.concat(Object.values(archivedInstances));

    if (queriedState) {
      allInstances = allInstances.filter((instance) =>
        instance.instanceState.includes(queriedState)
      );
    }

    const instanceIds = allInstances.map((instance) => instance.processInstanceId);

    return JSON.stringify(instanceIds);
  });

  network.get(`${path}/:definitionId/instance/:instanceID`, { cors: true }, async (req) => {
    const { instanceID, definitionId } = req.params;

    let instanceInfo;
    const engine = management.getEngineWithID(instanceID);

    if (engine) {
      const engineInstanceInfo = engine.getInstanceInformation(instanceID);

      const isCurrentlyExecutedInBpmnEngine = !!engineInstanceInfo.instanceState.find(
        (state) => state === 'RUNNING' || state === 'READY' || state === 'DEPLOYMENT-WAITING'
      );

      instanceInfo = { ...engineInstanceInfo, isCurrentlyExecutedInBpmnEngine };
    } else {
      const archivedInstanceInfo = (await db.getArchivedInstances(definitionId))[instanceID];
      instanceInfo = { ...archivedInstanceInfo, isCurrentlyExecutedInBpmnEngine: false };
    }

    if (!instanceInfo) {
      throw new APIError(404, 'There is no instance with the given id!');
    }

    return JSON.stringify(instanceInfo);
  });

  network.put(
    `${path}/:definitionId/instance/:instanceID/instanceState`,
    { cors: true },
    async (req) => {
      const { definitionId, instanceID } = req.params;
      const { body } = req;

      log.debug(`Status update for process instance ${instanceID}. Body: ${JSON.stringify(body)}`);

      const { instanceState } = body;

      if (instanceState === 'resume') {
        await management.resumeInstance(definitionId, instanceID);
      } else {
        const engine = management.getEngineWithID(instanceID);
        if (!engine) {
          throw new APIError(404, 'There is no instance with the given id!');
        }

        if (instanceState === 'stopped') {
          engine.stopInstance(instanceID);
        } else if (instanceState === 'paused') {
          engine.pauseInstance(instanceID);
        } else if (instanceState === 'aborted') {
          engine.abortInstance(instanceID);
        }
      }

      return '';
    }
  );

  network.get(`${path}/`, { cors: true }, async () => {
    const processes = await db.getAllProcesses();
    let processesInfos = Object.keys(processes).map(async (definitionId) => ({
      ...(await db.getProcessInfo(definitionId)),
    }));
    processesInfos = await Promise.all(processesInfos);
    return JSON.stringify(processesInfos);
  });

  network.put(`${path}/:definitionId/instance/:instanceID`, { cors: true }, async (req) => {
    const { definitionId } = req.params;
    const instance = req.body;

    const engine = await management.continueInstance(definitionId, instance);

    if (!engine) {
      throw new APIError(406, 'Engine not allowed to continue the instance!');
    }

    log.debug(
      `Continuing process instance (PUT) via API: DefinitionId = ${definitionId}, InstanceId = ${instance.id}`
    );
    log.trace(
      `Continuing process instance (PUT) via API: DefinitionId = ${definitionId}, InstanceId = ${instance.id}, Body: ${instance}`
    );
    return {
      statusCode: 201,
      mimeType: 'application/json',
      response: JSON.stringify(instance),
    };
  });
};
