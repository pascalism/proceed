const processRoutes = require('./processRoutes');
const HTMLRoutes = require('./HTMLRoutes');
const distributionRoutes = require('./distributionRoutes');

module.exports = (management) => {
  processRoutes('/process', management);
  HTMLRoutes('/process');
  distributionRoutes('/status');
};
