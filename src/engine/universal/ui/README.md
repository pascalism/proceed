# Display Item Development

## Introduction

The PROCEED Engine offers a web page to work on/with the process. This is usually used for the tasklist, so that a user can work on his tasks which he gets assigned by the process. But it is also used for configuring and monitoring the Engine on devices where it is not easy to do this things on a command line (Android, iOS).

The offered web page is a single page app with multiple "tabs" - the so-called _display items_.
You can add a display item by subclassing the `DisplayItem` class of the UI module (see `../ui/module.js`).
Then you need to set the `content` attribute with the HTML that the display item should show for a tab. (Here, it is recommended to use a minified and condensed version created by webpack that contains the complete HTML, CSS and JS.)  
Afterwards you need to configure communication endpoints and need to add the display item to the `UI` module via the `addDisplayItem()` method.

One of the main things to achieve is the communication of the DisplayItem with the PROCEED engine, e.g. to get the current User Task or transmit the results of a User Task.
This communication can be complicated because the UI should run in two environments: 1. in a WebView (Android, iOS and Browser Engine), and 2. in a Web Page offered by a _Web Server_ running on the Node Engine (Win, Linux, MacOS). So, two different communication technologies needs to be implemented for either environments.

To reduce this challenge, a variable is injected into the context of the DisplayItem: `window.PROCEED_DATA`. With this variable you can access a predefined API (methods: _get_ and _post_) to communicate with the PROCEED Engine. Therefore, you need to override the `getEndpoints()` method (see `.../ui/module.js`) to define the needed API for the new DisplayItem, e.g. `'/api/userTask': { get: this.getUserTask.bind(this), post: this.postUserTask.bind(this) }`. Then, you define the binded methods (in this case `getUserTask` and `postUserTask`), which have access to all PROCEED dependencies.

Afterwards, in your display item you can use `window.PROCEED_Data.<method>('/<DisplayItem-Name>/<Defined-API>', [<Method-Variables>] )` to communicate with the Engine and the binded, predefined methods. (Only `get` and `post` are currently allowed for `<method>`) E.g.

```
window.PROCEED_DATA.get('/tasklist/api/userTask', {
    instanceID,
    userTaskID,
    processChain,
    callChain,
  }).then( ...
```

For further, non-technical details please take a look at the [UI module section](https://gitlab.com/dBPMS-PROCEED/proceed/-/wikis/PROCEED-Engine-UI-Module) in the wiki.

## Development

- In order to develop the HTML, CSS, and JS for the new display item, you should create the files in a _development_ directory (like for the _tasklist_ display item). Therefore, it is recommended that you copy the _tasklist_ folder, so that you already have a working webpack configuration and development environment.
- Add scripts inside the `.../ui/package.json` to build the new display item and to start the dev server. E.g. the tasklist scripts are called `tasklist-build` and `tasklist-dev-server`
- On the command line, move into the directore `src/engine/universal/ui`
- By running your command, e.g. `yarn tasklist-dev-server`, you can start a dev-server on port `9000` for the files in the new directory.

For development, you will need to test the data exchange with the Engine. Therefore, you can either define local mock functions (aka `window.PROCEED_DATA`) or set the `src` attribute of the iframe in the `UI` module to `http://localhost:9000` to load the dev-server.
(Temporarily remove the DEV comment at the `script()` method in `/modules/ui/src/uiHTML.js` and run `yarn dev`)

## Production

- Once the display item's content is ready for production you can run the build command, e.g. `yarn build-tasklist`, which will use webpack to bundle all files into one final HTML file under `.../dist/`.
- Now, you need to copy this condensed HTML/CSS/JS string into the `content` variable of your `DisplayItem` subclass (see `.../ui/module.js`). However, directly copying this HTML (from `.../dist/index.html`) into a template string will most likely cause syntax erros due to not properly escaped characters.
  For that reason the build command will also create a TXT file called `template.txt` for direct copying into a template string.
  Use that to create the `content` of your display item and you're ready to go!
