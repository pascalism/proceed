/* eslint-disable class-methods-use-this */
const distribution = require('@proceed/distribution');
const whiskers = require('whiskers/dist/whiskers.min.js');
const { config, logging } = require('@proceed/machine');

let _management;

const { ui, DisplayItem } = require('./src/module.js');

class UserTaskList extends DisplayItem {
  constructor() {
    super('User Tasks', 'tasklist');
    this.content = `<!doctype html><html><head><style>body,html{margin:0;padding:0;font-family:sans-serif;font-size:16px;color:#464646;height:100%}#wrapper{width:auto;display:flex;flex-direction:row;height:100%}.infoBox{text-align:center;margin:40px 10px 10px;font-size:1.1em;color:#a5a5a5;letter-spacing:.03em}#tasks{box-shadow:0 0 6px -3px #9a9a9a;z-index:1;flex-shrink:0}#tasks .task{background-color:#fff;margin:10px;box-shadow:0 0 5px -3px #6d6d6d;border-radius:12px;color:#868686;position:relative}#tasks .taskInfo{height:90px;display:flex;flex-direction:row;flex-shrink:0;justify-content:flex-end;cursor:pointer;position:relative;border-radius:inherit}#tasks .taskInfo>div{margin:0 20px}#tasks .taskInfo:hover{background-color:#f9f9f9}#tasks .taskInfo .title{font-size:1.3em;letter-spacing:.03em;color:#464646;flex-grow:1;min-width:100px}#tasks .taskInfo .title,#tasks .taskInfo .user{display:flex;flex-direction:column;justify-content:center}#tasks .taskInfo .user{text-align:center;height:100%}#tasks .taskInfo .user svg{height:1.7em}#tasks .taskInfo .user .name{margin-top:5px;font-size:.7em}#tasks .taskInfo .time{display:flex;flex-direction:column;justify-content:center;font-size:.85em;letter-spacing:.03em}#tasks .taskInfo .time svg{height:1em;vertical-align:top}#tasks .taskInfo .time>div{margin-top:5px}#tasks .taskInfo .time>div:first-child{margin-top:0}#tasks .taskInfo .id{position:absolute;bottom:1px;font-size:.5em;color:#c3c3c3;display:none;margin:0;width:100%;text-align:center}#tasks .taskInfo:hover .id{display:block}#tasks .task .close{display:none}#tasks .task.form .taskInfo{background-color:#0094a0;color:#fff}#tasks .task.form .taskInfo .title{color:#fff}#formView{flex-grow:1;background-color:#fff;font-size:0}#formView .form{width:100%;height:100%;border:none;flex-grow:1}@media (max-width:1024px){body,html{font-size:14px}#tasks{font-size:.85em}#tasks .task{border-radius:9px}#tasks .taskInfo{height:80px}#tasks .taskInfo>div{margin:0 10px}}@media (max-width:768px){body,html{font-size:10px}#tasks .task{margin:5px;border-radius:7px}#tasks .taskInfo{height:45px}#tasks .taskInfo>div{margin:0 5px}}@media (max-width:425px){body,html{height:auto;font-size:16px}#wrapper{width:auto;flex-direction:column;height:100vh}#tasks{box-shadow:none;z-index:auto;width:100%}#tasks .task{margin:7px}#tasks .task.hidden{display:none}#tasks .task .taskInfo{height:80px}#tasks .taskInfo>div{margin:0 15px}#tasks .task.form{display:flex;flex-direction:column;margin-top:50px;margin-bottom:0}#tasks .task .form{width:100%;height:100%;border:none;flex-grow:1}#tasks .task.form .taskInfo{border-bottom:1px solid #efefef;border-bottom-left-radius:0;border-bottom-right-radius:0}#tasks .taskInfo:hover{background-color:inherit}#tasks .task.form .close{display:block;position:absolute;top:-40px;right:0;font-size:24.48px;color:#fff;background-color:#7d7d7d;height:30px;width:30px;text-align:center;border-radius:100%;line-height:28px;font-weight:100}#tasks .task.form .close:hover{background-color:#464646;cursor:pointer}#formView{margin:0 7px 7px;box-shadow:0 0 5px -3px #6d6d6d;border-radius:0 0 7px 7px}#formView:empty{display:none}}@media (max-width:375px){body,html{font-size:13px}#tasks .task .taskInfo{height:70px}#tasks .taskInfo>div{margin:0 10px}}@media (max-width:320px){body,html{font-size:12px}#tasks .task .taskInfo{height:65px}#tasks .taskInfo>div{margin:0 8px}}</style></head><body style="background: #fafafa;"><div id="wrapper"><div id="tasks"><div class="infoBox">There are currently no tasks in your queue.</div></div><div id="formView"></div></div><script>!function(e){var t={};function n(a){if(t[a])return t[a].exports;var s=t[a]={i:a,l:!1,exports:{}};return e[a].call(s.exports,s,s.exports,n),s.l=!0,s.exports}n.m=e,n.c=t,n.d=function(e,t,a){n.o(e,t)||Object.defineProperty(e,t,{enumerable:!0,get:a})},n.r=function(e){"undefined"!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(e,Symbol.toStringTag,{value:"Module"}),Object.defineProperty(e,"__esModule",{value:!0})},n.t=function(e,t){if(1&t&&(e=n(e)),8&t)return e;if(4&t&&"object"==typeof e&&e&&e.__esModule)return e;var a=Object.create(null);if(n.r(a),Object.defineProperty(a,"default",{enumerable:!0,value:e}),2&t&&"string"!=typeof e)for(var s in e)n.d(a,s,function(t){return e[t]}.bind(null,s));return a},n.n=function(e){var t=e&&e.__esModule?function(){return e.default}:function(){return e};return n.d(t,"a",t),t},n.o=function(e,t){return Object.prototype.hasOwnProperty.call(e,t)},n.p="",n(n.s=0)}([function(e,t,n){"use strict";n.r(t);n(1);function a(e){const t=document.querySelector("#tasks .task.form");t&&s(t);let n=e.target;for(;!n.classList.contains("task");)n=n.parentElement;const c=n.dataset.instanceid,r=n.dataset.id,o=n.dataset.processChain,l=n.dataset.callChain;n.removeEventListener("click",a),document.querySelectorAll("#tasks .task").forEach(e=>{e.classList.add("hidden")}),window.PROCEED_DATA.get("/tasklist/api/userTask",{instanceID:c,userTaskID:r,processChain:o,callChain:l}).then(e=>{n.classList.add("form");const t=document.createElement("iframe");t.className="form";document.querySelector("#formView").append(t),t.contentWindow.document.open(),t.contentWindow.document.write(e),t.contentWindow.document.close(),t.contentWindow.onsubmit=e=>function(e,t,n,a,c){c.preventDefault();const r=document.querySelector(\`#tasks .task[data-instanceid="\${e}"][data-id="\${t}"]\`),o={},l=new FormData(c.target).entries();let d=l.next();for(;!d.done;)[,o[d.value[0]]]=d.value,d=l.next();window.PROCEED_DATA.post("/tasklist/api/userTask",o,{instanceID:e,userTaskID:t,processChain:n,callChain:a}).then(()=>{s(r),i()})}(c,r,o,l,e)})}function s(e){e.classList.remove("form"),document.querySelector("iframe.form").remove(),document.querySelectorAll(".task.hidden").forEach(e=>e.classList.remove("hidden")),e.addEventListener("click",a)}function i(){window.PROCEED_DATA.get("/tasklist/api/").then(e=>{!function(e){const t=document.querySelector("#tasks"),n=Array.from(t.querySelectorAll(".task")).map(e=>({id:e.dataset.id,instanceID:e.dataset.instanceid,processChain:e.dataset.processChain,callChain:e.dataset.callChain})),i=e.filter(e=>n.every(t=>t.id!==e.id||t.instanceID!==e.instanceID)),c=n.filter(t=>e.every(e=>e.id!==t.id||e.instanceID!==t.instanceID));i.length+n.length-c.length!=0?(0===n.length&&null!==t.firstElementChild&&t.removeChild(t.firstElementChild),c.forEach(e=>t.querySelector(\`.task[data-instanceid="\${e.instanceID}"][data-id="\${e.id}"]\`).remove()),i.forEach(e=>{const t=\`\\n<div class="taskInfo">\\n  <div class="title">\${e.attrs.name||e.id}</div>\\n  <div class="appointees"><div class="user"><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="user" class="svg-inline--fa fa-user fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M224 256c70.7 0 128-57.3 128-128S294.7 0 224 0 96 57.3 96 128s57.3 128 128 128zm89.6 32h-16.7c-22.2 10.2-46.9 16-72.9 16s-50.6-5.8-72.9-16h-16.7C60.2 288 0 348.2 0 422.4V464c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48v-41.6c0-74.2-60.2-134.4-134.4-134.4z"></path></svg><div class="name">Max Mustermann</div></div></div>\\n  <div class="time">\\n    <div class="added"><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="history" class="svg-inline--fa fa-history fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M504 255.531c.253 136.64-111.18 248.372-247.82 248.468-59.015.042-113.223-20.53-155.822-54.911-11.077-8.94-11.905-25.541-1.839-35.607l11.267-11.267c8.609-8.609 22.353-9.551 31.891-1.984C173.062 425.135 212.781 440 256 440c101.705 0 184-82.311 184-184 0-101.705-82.311-184-184-184-48.814 0-93.149 18.969-126.068 49.932l50.754 50.754c10.08 10.08 2.941 27.314-11.313 27.314H24c-8.837 0-16-7.163-16-16V38.627c0-14.254 17.234-21.393 27.314-11.314l49.372 49.372C129.209 34.136 189.552 8 256 8c136.81 0 247.747 110.78 248 247.531zm-180.912 78.784l9.823-12.63c8.138-10.463 6.253-25.542-4.21-33.679L288 256.349V152c0-13.255-10.745-24-24-24h-16c-13.255 0-24 10.745-24 24v135.651l65.409 50.874c10.463 8.137 25.541 6.253 33.679-4.21z"></path></svg> 3h ago</div>\\n    <div class="due"><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="stopwatch" class="svg-inline--fa fa-stopwatch fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M432 304c0 114.9-93.1 208-208 208S16 418.9 16 304c0-104 76.3-190.2 176-205.5V64h-28c-6.6 0-12-5.4-12-12V12c0-6.6 5.4-12 12-12h120c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12h-28v34.5c37.5 5.8 71.7 21.6 99.7 44.6l27.5-27.5c4.7-4.7 12.3-4.7 17 0l28.3 28.3c4.7 4.7 4.7 12.3 0 17l-29.4 29.4-.6.6C419.7 223.3 432 262.2 432 304zm-176 36V188.5c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12V340c0 6.6 5.4 12 12 12h40c6.6 0 12-5.4 12-12z"></path></svg> 30min left</div>\\n  </div>\\n  <div class="id">\\n    <span class="process">Process: \${e.instanceID}</span>\\n    <span class="process">Task: \${e.id}</span>\\n  </div>\\n</div>\\n<div class="close">&#x00D7;</close>\`,n=document.createElement("div");n.className="task",n.dataset.id=e.id,n.dataset.instanceid=e.instanceID,n.dataset.processChain=e.processChain,n.dataset.callChain=e.callChain,n.innerHTML=t,document.querySelector("#tasks").appendChild(n),n.addEventListener("click",a),n.querySelector(".close").addEventListener("click",e=>{let t=e.target;for(;!t.classList.contains("task");)t=t.parentElement;e.stopPropagation(),s(t)})})):0===i.length&&0===c.length||(t.innerHTML='\\n      <div class="infoBox">There are currently no tasks in your queue.</div>\\n      ')}(e)})}window.addEventListener("DOMContentLoaded",()=>{window.setInterval(i,1e3),i()})},function(e,t,n){}]);</script></body></html>`;
    // this.badge = 'New';
  }

  getEndpoints() {
    return {
      '/api/': this.getAPI.bind(this),
      '/api/userTask': { get: this.getUserTask.bind(this), post: this.postUserTask.bind(this) },
    };
  }

  async getAPI() {
    const tasks = _management.getPendingUserTasks();
    const taskInfos = tasks.map((task) => {
      // the instance the user task was encountered in might have been in a call activity inside another instance
      // try to reproduce the callChain from the main instance to the current callActivity instance
      let callChain = task.processInstance.id;
      let currentInstance = task.processInstance.callingInstance;

      while (currentInstance) {
        callChain = `${currentInstance.id}|${callChain}`;
        currentInstance = currentInstance.callingInstance;
      }

      return {
        id: task.id,
        instanceID: task.processInstance.id,
        callChain,
        processChain: task.processChain,
        attrs: task.attrs,
      };
    });
    return taskInfos;
  }

  async getUserTask(query) {
    const [mainInstanceId] = query.callChain.split('|');
    const processChain = query.processChain.split('|');
    // get the engine that is running the main instance that might have called another instance of a callActivity where the process was encountered
    let engine = _management.getEngineWithID(mainInstanceId);
    const { definitionId } = engine;
    if (!engine) {
      throw new Error(`No process running with id ${mainInstanceId}`);
    }

    // if the processChain is longer than one the user task was found inside a callActivity => find the engine running the callActivity
    if (processChain.length > 1) {
      for (let processId of processChain.slice(1)) {
        engine = engine.callActivityExecutors[processId];

        if (!engine) {
          throw new Error(
            "Couldn't reconstruct execution chain to get engine running the callActivity!"
          );
        }
      }
    }
    const { processInstance, attrs } = engine.userTasks.find(
      (userTask) => userTask.id === query.userTaskID
    );

    const { 'proceed:fileName': userTaskFileName } = attrs;

    if (!processInstance) {
      throw new Error(`No pending user task with id ${query.userTaskID}`);
    }

    const variables = processInstance.getVariables();
    const html = await distribution.db.getHTML(
      definitionId,
      userTaskFileName,
      processChain.length > 1
    );

    const parsedHtml = whiskers.render(html, variables);

    return parsedHtml;
  }

  async postUserTask(body, query) {
    const [mainInstanceId] = query.callChain.split('|');
    const processChain = query.processChain.split('|');
    // get the engine that is running the main instance that might have called another instance of a callActivity where the process was encountered
    let engine = _management.getEngineWithID(mainInstanceId);

    if (!engine) {
      throw new Error(`No process running with id ${query.instanceID}`);
    }

    // if the processChain is longer than one the user task was found inside a callActivity => find the engine running the callActivity
    if (processChain.length > 1) {
      for (let processId of processChain.slice(1)) {
        engine = engine.callActivityExecutors[processId];

        if (!engine) {
          throw new Error(
            "Couldn't reconstruct execution chain to get engine running the callActivity!"
          );
        }
      }
    }

    console.log('--> Form submit: ', body);
    engine.completeUserTask(query.instanceID, query.userTaskID, body);
    return 'true';
  }
}
// Config Tab
class ConfigTab extends DisplayItem {
  //  Conf
  constructor() {
    super('Configuration', 'config');
    this.content = `<!doctype html><html lang="de"><head><meta charset="utf-8"><title>Configuration</title><style>#header>button{flex-grow:1;min-height:40px}.array-node,.config-sub-object{padding:0}.config-sub-object{margin-bottom:5px}.array-node{width:90%;margin:5px auto}.array-node-add{text-align:center}.title{flex:1}.back-arrow{height:100%;font-weight:700}.array-node-body,.config-sub-object-body{display:none;background-color:#fff}.config-sub-object-body{background-color:#f7f3f3}.show{display:block}.input-line{margin:4px;display:flex;flex-wrap:wrap;justify-content:space-evenly;align-items:center}.input-label-cell{margin:0 5px}.input-input-cell,.input-label-cell{max-width:50%;flex-grow:1;text-align:center}.input-input-cell{margin:4px 5px}.input-input-cell>input{width:100%;border:none;height:30px;background-color:#d4dbdd}.input-remove-cell{width:20px;text-align:center}</style></head><body><div id="header"><button onclick="onDeleteUserConfig()">restore system defaults</button> <button onclick="onRefreshData()">reload</button> <button onclick="onWriteUserConfig()">submit</button></div><div id="root"></div><script>!function(e){var t={};function n(o){if(t[o])return t[o].exports;var i=t[o]={i:o,l:!1,exports:{}};return e[o].call(i.exports,i,i.exports,n),i.l=!0,i.exports}n.m=e,n.c=t,n.d=function(e,t,o){n.o(e,t)||Object.defineProperty(e,t,{enumerable:!0,get:o})},n.r=function(e){"undefined"!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(e,Symbol.toStringTag,{value:"Module"}),Object.defineProperty(e,"__esModule",{value:!0})},n.t=function(e,t){if(1&t&&(e=n(e)),8&t)return e;if(4&t&&"object"==typeof e&&e&&e.__esModule)return e;var o=Object.create(null);if(n.r(o),Object.defineProperty(o,"default",{enumerable:!0,value:e}),2&t&&"string"!=typeof e)for(var i in e)n.d(o,i,function(t){return e[t]}.bind(null,i));return o},n.n=function(e){var t=e&&e.__esModule?function(){return e.default}:function(){return e};return n.d(t,"a",t),t},n.o=function(e,t){return Object.prototype.hasOwnProperty.call(e,t)},n.p="",n(n.s=1)}([function(e,t,n){},function(e,t,n){"use strict";n.r(t);n(0);function o(e,t,n){let o=document.createElement(t);return e&&e.appendChild(o),n&&Object.keys(n).forEach((function(e){o.setAttribute(e,n[e])})),o}let i,c=document.getElementById("root");function r(){c&&PROCEED_DATA.get("/config/api/config",{}).then(e=>l(e))}function l(e){i=e,c.innerHTML="";a(o(c,"div",{class:"container"}),i)}function a(e,t){Object.entries(t).forEach(([n,i])=>{if(i&&"object"==typeof i)Array.isArray(i)?function(e,t,n){const i=o(e,"div",{class:"container array-node"}),c=o(i,"div",{class:"title-container array-node-title"});o(c,"div",{class:"title"}).textContent=t;const r=o(i,"div",{class:"content-container array-node-body"});c.onclick=()=>{u(e,i)};const l=o(r,"div",{class:"array-node-content"}),a=o(r,"div",{class:"array-node-add"});a.textContent="[+]";let d=[];const f=function(e,t,i){d[t]=e;const c=s(l,void 0,i,e=>{n[d[t]]=e}),r=o(c,"div",{class:"input-remove-cell"});r.textContent="x",r.onclick=()=>{l.removeChild(c),n.splice(d[t],1),d=d.map(e=>e>d[t]?--e:e)}};n.forEach((e,t)=>{f(t,t,e)}),a.onclick=()=>{const e=n.length,t=d.length;d.push(e),n.push(""),f(e,t,"")}}(e,n,i):function(e,t,n){const i=o(e,"div",{class:"container config-sub-object"}),c=o(i,"div",{class:"title-container config-sub-object-title"});o(c,"div",{class:"title"}).textContent=t;const r=o(i,"div",{class:"content-container config-sub-object-body"});c.onclick=()=>{u(e,i)},a(r,n)}(e,n,i);else{s(e,n,i,e=>{t[n]=e})}})}function s(e,t,n,i){let c;const r=t,l=o(e,"div",{class:"input-line"});if(t){o(l,"label",{class:"input-label-cell",for:r}).textContent=t+":"}const a=o(l,"div",{class:"input-input-cell"});return t||(a.style.maxWidth="100%"),function(e){switch(typeof e){case"boolean":c=o(a,"input",{type:"checkbox",class:"booleanInput",id:r}),e&&c.setAttribute("checked",!0),c.onchange=function(){i(Boolean(c.checked))};break;case"string":c=o(a,"input",{type:"text",class:"stringInput",value:e,id:r}),c.onchange=function(){i(String(c.value))};break;case"number":c=o(a,"input",{type:"number",class:"numberInput",value:e,id:r}),c.onchange=function(){i(Number(c.value))}}}(n),l}function u(e,t){const n=t.querySelector(".content-container");n.classList.toggle("show");if(e.getBoundingClientRect().width>720||c.firstChild===t)return;const o=c.querySelector("div"),i=t.parentNode,r=Array.prototype.indexOf.call(i.children,t);c.removeChild(o),c.appendChild(t);const l=t.querySelector(".title-container"),a=l.querySelector(".title"),s=document.createElement("div");s.classList.add("back-arrow"),s.innerHTML="&larr;",l.insertBefore(s,a),s.onclick=e=>{if(c.removeChild(t),c.appendChild(o),r===i.children.length)i.appendChild(t);else{const e=Array.from(i.children)[r];i.insertBefore(t,e)}l.removeChild(s),n.classList.remove("show"),e.stopPropagation()}}window.onDeleteUserConfig=function(){PROCEED_DATA.post("/config/api/config").then(e=>l(e))},window.onWriteUserConfig=function(){PROCEED_DATA.post("/config/api/config",i).then(e=>l(e))},window.onRefreshData=r,r()}]);</script></body></html>`;
  }

  getEndpoints() {
    return {
      '/api/config': { get: this.getConfig.bind(this), post: this.writeConfig.bind(this) },
    };
  }
  async getConfig() {
    return await config.readConfig();
  }

  async writeConfig(body, query) {
    if (Object.keys(body).length === 0) return await config.writeConfig({}, true);
    //delete config vals
    else {
      return await config.writeConfig(body);
    }
  }
}

class LoggingTab extends DisplayItem {
  //  LOG
  constructor() {
    super('Monitoring', 'monitoring');
    this.content = `<!doctype html><html><head><meta charset="utf-8"><title>Logging</title><style>.LogElem{margin:0 7px 7px;box-shadow:0 0 5px -3px #6d6d6d;border-radius:0 0 7px 7px;display:flex;flex-wrap:wrap}.data{margin:0 3px}.debug{background-color:rgba(255,235,206,.349)}.error>.level{color:red}.info>.level{color:green}.info>.warn{color:orange}#instance-select{display:none}.instance-container{padding:0}</style></head><body><div id="header"><button type="button" id="toggleBtn" onclick="toggleRefresh()">⏸</button> <select id="table-select"></select> <select id="instance-select"></select></div><div><div id="LogRoot"></div></div><script>!function(e){var t={};function n(o){if(t[o])return t[o].exports;var r=t[o]={i:o,l:!1,exports:{}};return e[o].call(r.exports,r,r.exports,n),r.l=!0,r.exports}n.m=e,n.c=t,n.d=function(e,t,o){n.o(e,t)||Object.defineProperty(e,t,{enumerable:!0,get:o})},n.r=function(e){"undefined"!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(e,Symbol.toStringTag,{value:"Module"}),Object.defineProperty(e,"__esModule",{value:!0})},n.t=function(e,t){if(1&t&&(e=n(e)),8&t)return e;if(4&t&&"object"==typeof e&&e&&e.__esModule)return e;var o=Object.create(null);if(n.r(o),Object.defineProperty(o,"default",{enumerable:!0,value:e}),2&t&&"string"!=typeof e)for(var r in e)n.d(o,r,function(t){return e[t]}.bind(null,r));return o},n.n=function(e){var t=e&&e.__esModule?function(){return e.default}:function(){return e};return n.d(t,"a",t),t},n.o=function(e,t){return Object.prototype.hasOwnProperty.call(e,t)},n.p="",n(n.s=1)}([function(e,t,n){},function(e,t,n){"use strict";n.r(t);n(0);function o(e,t,n){let o=document.createElement(t);return e&&e.appendChild(o),n&&Object.keys(n).forEach((function(e){o.setAttribute(e,n[e])})),o}let r=!0;window.toggleRefresh=function(){document.getElementById("toggleBtn").innerHTML=i(r?"play":"pause"),r=!r};let c={info:"➤",debug:"🛠",error:"⚠",trace:"🛡",CORE:"◉",SYSTEM:"⚙",pause:"⏸",play:"▶"};function i(e){let t=c[e];return t||e}window.setInterval((function(){a&&r&&window.PROCEED_DATA.get("/monitoring/api/log").then(e=>{p(e)})}),1e3);const a=document.getElementById("LogRoot"),l=document.getElementById("table-select"),u=document.getElementById("instance-select");let s,d,f="standard";function m(e){f=e,u.style.display="standard"===e?"":"block"}function p(e){(function(e){if(!s)return!0;const t=Object.keys(s),n=Object.keys(e);return t.length!==n.length||t.some(e=>!n.includes(e))})(e)&&y(Object.keys(e),l,f,e=>{m(e),p(s)}),s=e,a.innerHTML="",e[f]||m("standard"),"standard"===f?e.standard.forEach(e=>{const[t]=Object.values(e);a.appendChild(b(t))}):function(e){const t={};e.forEach(e=>{[e]=Object.values(e),t[e.instanceId]||(t[e.instanceId]=[]),t[e.instanceId].push(e)}),y(["No instance",...Object.keys(t)],u,d,e=>{d=e,p(s)}),Object.values(t).forEach(e=>{if(d&&"No instance"!==d&&d!==e[0].instanceId)return;const t=o(a,"div",{class:"container instance-container"});o(t,"div",{class:"title-container"}).textContent=e[0].instanceId,e.forEach(e=>{t.appendChild(b(e))})})}(e[f])}function y(e,t,n,r){t.innerHTML="",e.forEach(e=>{const c=o(t,"option",{value:e});e===n&&c.setAttribute("selected",!0),c.onclick=()=>{r(e)},c.textContent=e})}function b(e){let{msg:t,level:n,time:r,moduleName:c}=e;r=new Intl.DateTimeFormat("en-GB",{hour:"numeric",minute:"numeric",second:"numeric",year:"numeric",month:"numeric",day:"numeric"}).format(new Date(r));let a=o(null,"div",{class:"LogElem "+n});return o(a,"div",{class:"data level icon"}).textContent=function(e){let t=i(e);return t===e?"":t}(n),o(a,"div",{class:"data level time"}).textContent=r,o(a,"div",{class:"data level text"}).textContent=n.toUpperCase()+": ",o(a,"div",{class:"data moduleName text"}).textContent=c,o(a,"div",{class:"data msg"}).textContent=t,a}}]);</script></body></html>`;
  }

  getEndpoints() {
    return {
      '/api/log': { get: this.getLogs.bind(this) },
    };
  }
  async getLogs() {
    return await logging.getAllLogTables();
  }
}

ui.addDisplayItem(new UserTaskList());
ui.addDisplayItem(new ConfigTab());
ui.addDisplayItem(new LoggingTab());

const tasklist = {
  serve(management) {
    _management = management;
    ui.init(); // FIXME: remove
  },
};

module.exports = tasklist;
