const util = require('./src/util.js');

const getters = require('./src/getters.js');

const setters = require('./src/setters.js');

const validators = require('./src/validators.js');

module.exports = {
  ...util,
  ...getters,
  ...setters,
  ...validators,
};
