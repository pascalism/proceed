const {
  toObject,
  toXML,
  getElementsByTagName,
  getElementById,
  getAllElements,
} = require('./util.js');

const ConstraintParser = require('@proceed/constraint-parser-xml-json');

const constraintParser = new ConstraintParser();

/**
 * @module @proceed/bpmn-helper
 */

/**
 * Function that returns ids of all start events in a bpmn process model
 *
 * @param {string} xml the bpmn process
 * @returns {array} - the ids of all startEvents
 */
async function getStartEvents(xml) {
  const xmlObj = await toObject(xml);
  return getElementsByTagName(xmlObj, 'bpmn:StartEvent').map((moddleElement) => moddleElement.id);
}

function getDefinitionsIdForCallActivityByObject(xmlObj, callActivityId) {
  const callActivityElement = getElementById(xmlObj, callActivityId);

  if (!callActivityElement || typeof callActivityElement.calledElement !== 'string') {
    return undefined;
  }

  // deconstruct 'p33c24:_e069937f-27b6-464b-b397-b88a2599f1b9' to 'p33c24'
  const [prefix, processId] = callActivityElement.calledElement.split(':');

  const [calledBpmnDefinitions] = getElementsByTagName(xmlObj, 'bpmn:Definitions');

  const targetNamespace = calledBpmnDefinitions.$attrs[`xmlns:${prefix}`];

  if (typeof targetNamespace === 'string') {
    // retrieve proceed id from target namespace https://docs.proceed-labs.org/_52c610b0-7138-4854-8eb0-38bb31a51477
    return targetNamespace.split('/').pop();
  } else {
    return undefined;
  }
}

/**
 * Returns id of the given process definition
 *
 * @param {String} xml
 * @returns {string} - The id stored in the definitions field of the given bpmn process
 */
async function getId(xml) {
  const definitions = await toObject(xml);
  return definitions.id;
}

/**
 * Returns the name of the given bpmn process definition
 *
 * @param {string} xml
 * @returns {string} - The name stored in the definitions field of the given bpmn process
 */
async function getName(xml) {
  const definitions = await toObject(xml);
  return definitions.name;
}

/**
 * Gets deployment method of the given process
 *
 * @param {string} xml the bpmn diagram that contains the process
 * @returns {string} - the deployment method used for the given process
 */
async function getDeploymentMethod(xml) {
  const definitions = await toObject(xml);
  const [process] = getElementsByTagName(definitions, 'bpmn:Process');
  return process.deploymentMethod;
}

/**
 * Returns a mapping of the ids of the process nodes to the machines they are mapped to
 *
 * @param {string} xml
 * @returns {object} - the mapping from a node id to information about the machine it is mapped to
 */
async function getElementMachineMapping(xml) {
  const xmlObj = await toObject(xml);
  const [process] = getElementsByTagName(xmlObj, 'bpmn:Process');
  const { flowElements } = process;
  const elementMachineMapping = {};
  const flowNodes = flowElements.filter((node) => node.$type !== 'bpmn:SequenceFlow');
  flowNodes.forEach((node) => {
    elementMachineMapping[node.id] = {
      machineAddress: node.machineAddress,
      machineId: node.machineId,
    };
  });
  return elementMachineMapping;
}

async function getUserTaskFileNameMapping(xml) {
  const xmlObj = await toObject(xml);
  const userTasks = getElementsByTagName(xmlObj, 'bpmn:UserTask');
  const mapping = {};
  userTasks.forEach((task) => {
    mapping[task.id] = {
      fileName: task.fileName,
      implementation: task.implementation,
    };
  });
  return mapping;
}

async function getFileNameTaskIdMapping(xml) {
  const xmlObj = await toObject(xml);
  const userTasks = getElementsByTagName(xmlObj, 'bpmn:UserTask');
  const mapping = {};
  userTasks.forEach((task) => (mapping[task.fileName] = task.id));
  return mapping;
}

async function getTaskIdFileNameMapping(xml) {
  const xmlObj = await toObject(xml);
  const userTasks = getElementsByTagName(xmlObj, 'bpmn:UserTask');
  const mapping = {};
  userTasks.forEach((task) => (mapping[task.id] = task.fileName));
  return mapping;
}

/**
 * Exporter name is used in the bpmn definitions parts
 *
 * @returns {string} static exporter name
 */
function getExporterName() {
  return `PROCEED Management System`;
}

/**
 * Exporter version is used in the bpmn definitions parts
 *
 * This version should be adjusted when this module (bpmn-helper)
 * changes or any changes in the management-system occur regarding the export
 *
 * @returns {string} static exporter version
 */
function getExporterVersion() {
  return `0.0.1`;
}

/**
 * Returns a xml with Diagram Elements just from the given subprocess and their nested Processes
 *
 * Structure of XMl:
 * Defintions
 *  - Process
 *    -FlowElements of the Process (BPMN Part)
 *  - Process End
 *  - Diagram (How to visualize the XML in the viewer)
 * Definitions End
 *
 * This function only remove all Diagram Parts that are not part of the subprocess - the flowElements are still part of the xml
 *
 * @param {string} xml - XML of the main process
 * @param {string} subprocessId - id of subprocess you want to show
 * @return {string} - xml with only diagram parts for the subprocess
 */
async function getSubprocessFromXML(xml, subprocessId) {
  const bpmnObject = await toObject(xml);

  // Get the Subprocess Part of the whole XML
  const subprocess = getElementById(bpmnObject, subprocessId);

  // Create an Array for all Elements which are nested into the subprocess
  const nestedElementsId = getAllElements(subprocess)
    .filter((element) => element.id)
    .map((element) => element.id);

  // Add all plane Elements of the subprocess and nested subprocesses into the diagram - this removes all other Shapes of the original xml
  bpmnObject.diagrams[0].plane.planeElement = bpmnObject.diagrams[0].plane.planeElement.filter(
    (element) => element.bpmnElement && nestedElementsId.includes(element.bpmnElement.id)
  );

  const newXml = await toXML(bpmnObject);

  return newXml;
}

async function getProcessIds(xml) {
  const xmlObj = await toObject(xml);
  const processes = getElementsByTagName(xmlObj, 'bpmn:Process');
  return processes.map((process) => process.id);
}

async function getAllBpmnElementIds(xml) {
  const xmlObj = await toObject(xml);
  const allElements = getAllElements(xmlObj);
  const getAllBpmnElements = allElements.filter(
    (element) =>
      typeof element.$type === 'string' &&
      element.$type.startsWith('bpmn:') &&
      (element.$type.includes('Task') ||
        element.$type.includes('Event') ||
        element.$type.includes('CallActivity') ||
        element.$type.includes('SubProcess') ||
        element.$type.includes('Gateway'))
  );
  return getAllBpmnElements.map((element) => element.id);
}

/**
 * Gets definitionIds for all imported Processes used in call Activities
 *
 * @param {String} xml the process definition
 */
async function getCallActivityDefinitionIdMapping(xml, throwErrors = true) {
  const definitions = await toObject(xml);
  const { imports } = definitions;

  const map = {};
  const callActivities = getElementsByTagName(definitions, 'bpmn:CallActivity');

  callActivities.forEach((callActivity) => {
    if (!callActivity.calledElement) {
      if (throwErrors) {
        throw new Error(`No called element referenced for callActivity with id ${callActivity.id}`);
      }
    } else {
      const [prefix, processId] = callActivity.calledElement.split(':');
      const namespace = definitions.$attrs[`xmlns:${prefix}`];

      if (!namespace) {
        if (throwErrors) {
          throw new Error(
            `No namespace attribute found for the referenced process in callActivity ${callActivity.id}`
          );
        }
      }

      const imp = imports.find((i) => i.namespace === namespace);

      if (!imp) {
        if (throwErrors) {
          throw new Error(
            `No import found for the referenced process in callActivity ${callActivity.id}`
          );
        }
      } else {
        map[callActivity.id] = { definitionId: imp.location, processId };
      }
    }
  });

  return map;
}

async function getCallActivitiesProcessIds(xml) {
  const definitions = await toObject(xml);
  const { imports } = definitions;

  const processIds = {};
  const callActivities = getElementsByTagName(definitions, 'bpmn:CallActivity');

  callActivities.forEach((callActivity) => {
    if (callActivity.calledElement) {
      const [prefix, processId] = callActivity.calledElement.split(':');
      const namespace = definitions.$attrs[`xmlns:${prefix}`];

      if (namespace) {
        const imp = imports.find((i) => i.namespace === namespace);

        if (imp) {
          let definitionsId = imp.namespace.split('/');
          definitionsId = definitionsId[definitionsId.length - 1];
          if (definitionsId) {
            processIds[definitionsId] = callActivity.id;
          }
        }
      }
    }
  });

  return processIds;
}

/**
 * Returns an array of import elements for a given bpmn xml
 *
 * @param {string} xml
 * @returns {Object[]} - Arry of of import elements inside the given xml
 */
async function getImports(xml) {
  const xmlObj = await toObject(xml);
  const importElements = getElementsByTagName(xmlObj, 'bpmn:Import');
  return importElements;
}

async function getTaskConstraintMappingFromXml(xml) {
  const elementIds = await getAllBpmnElementIds(xml);
  const xmlObj = await toObject(xml);
  const taskConstraintMapping = {};
  elementIds.forEach(async (id) => {
    const element = getElementById(xmlObj, id);
    taskConstraintMapping[id] = {
      hardConstraints: [],
      softConstraints: [],
    };
    const { extensionElements } = element;
    if (extensionElements && extensionElements.values) {
      const constraints = extensionElements.values.find(
        (e) => e.$type === 'proceed:ProcessConstraints'
      );
      if (constraints && (constraints.hardConstraints || constraints.softConstraints)) {
        const constraintsXml = await toXML(constraints);
        const constraintsJs = constraintParser.fromXmlToJs(constraintsXml);
        if (constraintsJs && constraintsJs.processConstraints) {
          taskConstraintMapping[id] = {
            hardConstraints: constraintsJs.processConstraints.hardConstraints || [],
            softConstraints: constraintsJs.processConstraints.softConstraints || [],
          };
        }
      }
    }
  });
  return taskConstraintMapping;
}

async function getProcessConstraintsFromXml(xml) {
  const processIds = await getProcessIds(xml);
  const xmlObj = await toObject(xml);
  let processConstraints = {
    softConstraints: [],
    hardConstraints: [],
  };
  const element = getElementById(xmlObj, processIds[0]);
  const { extensionElements } = element;
  if (extensionElements && extensionElements.values) {
    const constraints = extensionElements.values.find(
      (e) => e.$type === 'proceed:ProcessConstraints'
    );
    if (constraints && (constraints.hardConstraints || constraints.softConstraints)) {
      const constraintsXml = await toXML(constraints);
      const constraintsJs = constraintParser.fromXmlToJs(constraintsXml);
      if (constraintsJs && constraintsJs.processConstraints) {
        processConstraints = {
          hardConstraints: constraintsJs.processConstraints.hardConstraints || [],
          softConstraints: constraintsJs.processConstraints.softConstraints || [],
        };
      }
    }
  }
  return processConstraints;
}

async function getProcessDescription(xml) {
  const bpmnObj = await toObject(xml);
  const [process] = getElementsByTagName(bpmnObj, 'bpmn:Process');
  return getProcessDescriptionFromObject(process);
}

function getProcessDescriptionFromObject(processObject) {
  const docs = processObject.get('documentation');
  if (docs.length > 0) {
    const description = docs[0].text;
    return description || '';
  } else {
    return '';
  }
}

module.exports = {
  getStartEvents,
  getDefinitionsIdForCallActivityByObject,
  getId,
  getName,
  getDeploymentMethod,
  getElementMachineMapping,
  getProcessIds,
  getExporterName,
  getExporterVersion,
  getSubprocessFromXML,
  getProcessIds,
  getUserTaskFileNameMapping,
  getCallActivityDefinitionIdMapping,
  getFileNameTaskIdMapping,
  getTaskIdFileNameMapping,
  getAllBpmnElementIds,
  getImports,
  getTaskConstraintMappingFromXml,
  getProcessConstraintsFromXml,
  getCallActivitiesProcessIds,
  getProcessDescription,
  getProcessDescriptionFromObject,
};
