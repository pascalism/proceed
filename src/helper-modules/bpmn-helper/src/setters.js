const {
  moddle,
  toObject,
  toXML,
  getElementById,
  getElementsByTagName,
  manipulateElementsByTagName,
  manipulateElementById,
} = require('./util.js');

const { getExporterName, getExporterVersion } = require('./getters.js');

const ConstraintParser = require('@proceed/constraint-parser-xml-json');

const constraintParser = new ConstraintParser();

/**
 * @module @proceed/bpmn-helper
 */

/**
 *  Sets id in definitions element to given id
 *
 * @param {string} bpmn the xml we want to update
 * @param {string} id the id we want to set the definitions element to
 * @returns {string} - the updated xml
 */
async function setId(bpmn, id) {
  const newBPMN = await manipulateElementsByTagName(bpmn, 'bpmn:Definitions', (definitions) => {
    definitions.id = `${id}`;
  });
  return newBPMN;
}

/**
 * Sets id in process element to given id
 *
 * @param {string} bpmn the xml we want to update
 * @param {string} id the id we want to set for the process inside the bpmn
 * @returns {string} - the updated xml
 */
async function setProcessId(bpmn, id) {
  const newBPMN = await manipulateElementsByTagName(bpmn, 'bpmn:Process', (process) => {
    process.id = `${id}`;
  });
  return newBPMN;
}

/**
 *  Adds name attribute to definitions element and sets it to the given name
 *
 * @param {string} bpmn the xml we want to update
 * @param {string} name the name we want to set for our bpmn
 * @returns {string} - the updated xml
 */
async function setName(bpmn, name) {
  const newBPMN = await manipulateElementsByTagName(bpmn, 'bpmn:Definitions', (definitions) => {
    definitions.name = name;
  });
  return newBPMN;
}

/**
 * Sets targetNamespace in definitions element to https://docs.proceed-labs.org/${id}
 *
 * @param {*} bpmn the xml we want to update
 * @param {*} id the id we want to add
 * @returns {string} - the updated xml
 */
async function setTargetNamespace(bpmn, id) {
  const newBPMN = await manipulateElementsByTagName(bpmn, 'bpmn:Definitions', (definitions) => {
    definitions.targetNamespace = `https://docs.proceed-labs.org/${id}`;
  });
  return newBPMN;
}

/**
 * Sets Exporter in definitions element to PROCEED Management System
 * Sets exporterVersion in definitions element to Version
 *
 * @param {String} bpmn - the xml we want to update
 * @param {String} [exporterVersion] - the exporter version as optional value
 * @returns {string} the updated xml
 */
async function setStandardDefinitions(bpmn) {
  const newBPMN = await manipulateElementsByTagName(bpmn, 'bpmn:Definitions', (definitions) => {
    setStandardDefinitionsOnDefinitionsObject(definitions);
  });
  return newBPMN;
}

function setStandardDefinitionsOnDefinitionsObject(definitionsObject) {
  const exporterName = getExporterName();
  const exporterVersion = getExporterVersion();

  if (definitionsObject.exporter && definitionsObject.exporter !== exporterName) {
    definitionsObject.originalExporter = definitionsObject.exporter;
    definitionsObject.originalExporterVersion = definitionsObject.exporterVersion;
  }

  if (definitionsObject.exporterVersion && definitionsObject.exporterVersion !== exporterVersion) {
    definitionsObject.originalExporterVersion = definitionsObject.exporterVersion;
  }

  definitionsObject.exporter = exporterName;
  definitionsObject.exporterVersion = exporterVersion;
  definitionsObject.expressionLanguage = 'https://ecma-international.org/ecma-262/8.0/';
  definitionsObject.typeLanguage =
    'https://www.ecma-international.org/publications/files/ECMA-ST/ECMA-404.pdf';
  if (typeof definitionsObject.$attrs != 'object') definitionsObject.$attrs = {};
  delete definitionsObject.$attrs['xmlns:bpmn'];
  definitionsObject.$attrs['xmlns'] = 'http://www.omg.org/spec/BPMN/20100524/MODEL';
  definitionsObject.$attrs['xmlns:proceed'] = 'https://docs.proceed-labs.org/BPMN';
  const proceedXSIs = [
    'https://docs.proceed-labs.org/BPMN https://docs.proceed-labs.org/xsd/XSD-PROCEED.xsd',
    'http://www.omg.org/spec/BPMN/20100524/MODEL https://www.omg.org/spec/BPMN/20100501/BPMN20.xsd',
  ];

  if (typeof definitionsObject.$attrs['xmlns:xsd'] !== 'string') {
    definitionsObject.$attrs['xmlns:xsd'] = 'http://www.w3.org/2001/XMLSchema';
  }

  if (typeof definitionsObject.$attrs['xsi:schemaLocation'] !== 'string') {
    definitionsObject.$attrs['xsi:schemaLocation'] = '';
  }

  if (typeof definitionsObject.creatorEnvironmentId !== 'string') {
    definitionsObject.creatorEnvironmentId = '';
  }

  if (typeof definitionsObject.creatorEnvironmentName !== 'string') {
    definitionsObject.creatorEnvironmentName = '';
  }

  for (const xsi of proceedXSIs) {
    if (!definitionsObject.$attrs['xsi:schemaLocation'].includes(xsi)) {
      definitionsObject.$attrs['xsi:schemaLocation'] += ' ' + xsi;
    }
  }

  definitionsObject.$attrs['xsi:schemaLocation'] = definitionsObject.$attrs[
    'xsi:schemaLocation'
  ].trim();
}

async function saveOriginalDefinitionsAsBackup(bpmn, originalDefinitions, newId) {
  const newBPMN = await manipulateElementsByTagName(bpmn, 'bpmn:Definitions', (definitions) => {
    definitions.id = newId;
    definitions.originalId = originalDefinitions.id;
    definitions.originalExporter = originalDefinitions.exporter;
    definitions.originalExporterVersion = originalDefinitions.exporterVersion;
    definitions.originalTargetNamespace = originalDefinitions.targetNamespace;
  });
  return newBPMN;
}

/**
 * Sets deployment method of a process
 *
 * @param {*} bpmn the xml we want to update
 * @param {*} method the method we want to set (dynamic/static)
 * @returns {string} - the updated xml
 */
async function setDeploymentMethod(bpmn, method) {
  const newBPMN = await manipulateElementsByTagName(bpmn, 'bpmn:Process', (process) => {
    process.deploymentMethod = method;
  });
  return newBPMN;
}

async function setUserTaskFileNames(xml, mapping) {
  const obj = await toObject(xml);
  if (mapping) {
    const elementIds = Object.keys(mapping);
    elementIds.forEach(async (elementId) => {
      const element = getElementById(obj, elementId);
      if (element) {
        element.fileName = mapping[elementId].fileName;
        element.implementation = mapping[elementId].implementation;
      }
    });
  }
  const newXml = toXML(obj);

  return newXml;
}

/**
 * Function that sets the machineInfo of all elements in the given xml with the given machineIds
 *
 * @param {*} xml the xml we want to make changes in
 * @param {*} machineInfo the machineAddresses and machineIps of all the elements we want to set
 */
async function setMachineInfo(xml, machineInfo) {
  const obj = await toObject(xml);

  const elementIds = Object.keys(machineInfo);
  elementIds.forEach(async (elementId) => {
    const element = getElementById(obj, elementId);
    element.machineAddress = machineInfo[elementId].machineAddress;
    element.machineId = machineInfo[elementId].machineId;
  });
  const newXml = await toXML(obj);

  return newXml;
}

async function addConstraintsToElement(element, cons) {
  if (element) {
    let { extensionElements } = element;

    if (!extensionElements) {
      extensionElements = moddle.create('bpmn:ExtensionElements');
      extensionElements.values = [];
      element.extensionElements = extensionElements;
    }

    if (!extensionElements.values) {
      extensionElements.values = [];
    }

    extensionElements.values = extensionElements.values.filter(
      (child) => child.$type !== 'proceed:ProcessConstraints'
    );

    if (cons) {
      const { hardConstraints, softConstraints } = cons;
      const constraints = { processConstraints: { hardConstraints, softConstraints } };
      let constraintXML = constraintParser.fromJsToXml(constraints);
      constraintXML = `<?xml version="1.0" encoding="UTF-8"?>
        <bpmn2:extensionElements xmlns:bpmn2="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:proceed="https://docs.proceed-labs.org/BPMN">
          ${constraintXML}
        </bpmn2:extensionElements>`;
      const constraintObj = await toObject(constraintXML, 'bpmn:ExtensionElements');
      extensionElements.values.push(constraintObj.values[0]);
    }
  }
}

/**
 * Adds process and task constraints as extension elements to XML after checking for inconsistencies
 * @param bpmnXML
 * @param processConstraints
 * @param taskConstraintMapping
 * @returns modified bpmnXML
 */
async function addConstraints(bpmnXML, { processConstraints, taskConstraintMapping }) {
  const bpmnObj = await toObject(bpmnXML);
  const processNode = getElementsByTagName(bpmnObj, 'bpmn:Process')[0];

  const promises = [];
  if (processConstraints) {
    promises.push(addConstraintsToElement(processNode, processConstraints));
  }

  if (taskConstraintMapping) {
    const taskIds = Object.keys(taskConstraintMapping);

    taskIds.forEach((id) => {
      promises.push(
        addConstraintsToElement(getElementById(bpmnObj, id), taskConstraintMapping[id])
      );
    });
  }

  await Promise.all(promises);

  const newBPMN = await toXML(bpmnObj);
  return newBPMN;
}

/**
 * Add meta information of the called bpmn process to the bpmn file where it's getting called from. This includes a custom namespace in the definitions part,
 * an import element as first child of definitions and the calledElement attribute of the call activity bpmn element
 *
 * @param {String} rootBpmn The bpmn file as string where the call activity is in
 * @param {String} callActivityId The ID of the call activity bpmn element inside the rootBpmn
 * @param {String} calledBpmn The bpmn file of the called process
 * @param {String} calledProcessLocation The DefinitionId of the calledBpmn. Combination of process name and process id
 */
async function addCallActivityReference(
  rootBpmn,
  callActivityId,
  calledBpmn,
  calledProcessLocation
) {
  // checks if there is already a reference for this call activity and remove it with all related informations (namespace definitions/imports)
  const rootBpmnObject = await toObject(rootBpmn);
  const callActivity = await getElementById(rootBpmnObject, callActivityId);
  if (callActivity.calledElement) {
    rootBpmn = await removeCallActivityReference(rootBpmn, callActivityId);
  }

  // Retrieving all necessary informations from the called bpmn
  const calledBpmnObject = await toObject(calledBpmn);
  const [calledBpmnDefinitions] = getElementsByTagName(calledBpmnObject, 'bpmn:Definitions');
  const [calledProcess] = getElementsByTagName(calledBpmnObject, 'bpmn:Process');
  const targetNamespace = calledBpmnDefinitions.targetNamespace;

  // Construct namespace in format p+(last 5 chars from the imported namespace), for example 'p33c24'
  const nameSpacePrefix = 'p' + targetNamespace.substring(targetNamespace.length - 5);

  // Adding the prefixed namespace attribute and the import child-element to the definitions
  let newBpmn = await manipulateElementsByTagName(rootBpmn, 'bpmn:Definitions', (definitions) => {
    definitions.$attrs[`xmlns:${nameSpacePrefix}`] = targetNamespace;

    if (!Array.isArray(definitions.imports)) {
      definitions.imports = [];
    }

    // Checks if there is no import element with the same namespace
    const processImport = definitions.imports.find(
      (element) => element.namespace === targetNamespace
    );

    if (!processImport) {
      let importElement = moddle.create('bpmn:Import');
      importElement.namespace = targetNamespace;
      importElement.location = calledProcessLocation;
      importElement.importType = 'http://www.omg.org/spec/BPMN/20100524/MODEL';

      definitions.imports.push(importElement);
    } else {
      // update process location which might have changed
      processImport.location = calledProcessLocation;
    }
  });

  // Adding the desired information to the bpmn call activity element
  newBpmn = await manipulateElementById(newBpmn, callActivityId, (callActivity) => {
    callActivity.calledElement = `${nameSpacePrefix}:${calledProcess.id}`;
    callActivity.name = calledBpmnDefinitions.name;
  });

  return newBpmn;
}

/**
 * Remove the reference to the called process added in {@link addCallActivityReference} but remains the actual bpmn element
 *
 * @param {String} bpmn The bpmn file as string with a call activity and the meta information regarding the called process
 * @param {String} callActivityId The ID of the bpmn element for which the meta information should be removed
 */
async function removeCallActivityReference(bpmn, callActivityId) {
  let bpmnObject = await toObject(bpmn);
  const callActivityElement = getElementById(bpmnObject, callActivityId);

  if (typeof callActivityElement.calledElement !== 'string') return bpmn;

  // deconstruct 'p33c24:_e069937f-27b6-464b-b397-b88a2599f1b9' to 'p33c24'
  const [prefix, processId] = callActivityElement.calledElement.split(':');

  // remove the reference values from the call activity
  let newBpmn = await manipulateElementById(bpmn, callActivityId, (callActivity) => {
    delete callActivity.calledElement;
    callActivity.name = '';
  });

  bpmnObject = await toObject(newBpmn);

  const callActivities = getElementsByTagName(bpmnObject, 'bpmn:CallActivity').filter(
    (callActivity) => typeof callActivity.calledElement === 'string'
  );
  // remove import and namespace in definitions if there is no other call activity referencing the same process
  if (callActivities.every((callActivity) => !callActivity.calledElement.startsWith(prefix))) {
    newBpmn = await manipulateElementsByTagName(newBpmn, 'bpmn:Definitions', (definitions) => {
      const importedNamespace = definitions.$attrs[`xmlns:${prefix}`];
      delete definitions.$attrs[`xmlns:${prefix}`];

      if (Array.isArray(definitions.imports)) {
        definitions.imports = definitions.imports.filter(
          (element) => element.namespace !== importedNamespace
        );
      }
    });
  }
  return newBpmn;
}

/**
 * Look up the given bpmn document for unused imports/custom namespaces which don't get referenced by a call activity inside this bpmn document.
 *
 * @param {String} bpmn a bpmn document with maybe unused references to other processes
 */
async function removeUnusedCallActivityReferences(bpmn) {
  const bpmnObject = await toObject(bpmn);
  const callActivityElements = getElementsByTagName(bpmnObject, 'bpmn:CallActivity').filter(
    (element) => element.calledElement
  );

  let newBpmn = await manipulateElementsByTagName(bpmn, 'bpmn:Definitions', (definitions) => {
    // there can only be imports and namespaces for non-existent CallActivities if there are actual imports existent and if there are more imports than call activities
    if (Array.isArray(definitions.imports)) {
      // will be used for comparison later
      const importedNamespaces = definitions.imports.map(
        (importElement) => importElement.namespace
      );

      // iterate over all custom namespaces
      for (const key in definitions.$attrs) {
        // filter namespaces that occur in the imported namespaces
        if (importedNamespaces.includes(definitions.$attrs[key])) {
          // deconstruct 'xmlns:p33c24' to 'p33c24'
          const [_, prefix] = key.split(':');

          // checks if there is no actual call activity with this prefix in the calledElement attribute
          if (
            !callActivityElements.some((callActivityElement) =>
              callActivityElement.calledElement.startsWith(prefix)
            )
          ) {
            // remove the unused import element
            definitions.imports = definitions.imports.filter(
              (element) => element.namespace !== definitions.$attrs[key]
            );
            // remove the unused namespace in definitions
            delete definitions.$attrs[key];
          }
        }
      }
    }
  });

  return newBpmn;
}

async function addDocumentation(bpmn, description) {
  const bpmnObj = await toObject(bpmn);
  const [process] = getElementsByTagName(bpmnObj, 'bpmn:Process');
  if (process) {
    addDocumentationToProcessObject(process, description);
    const newBpmn = await toXML(bpmnObj);
    return newBpmn;
  } else {
    return bpmn;
  }
}

async function addDocumentationToProcessObject(processObj, description) {
  const docs = processObj.get('documentation');
  const documentation = moddle.create('bpmn:Documentation', { text: description || '' });
  if (docs.length > 0) {
    docs[0] = documentation;
  } else {
    docs.push(documentation);
  }
}

module.exports = {
  setId,
  setProcessId,
  setName,
  setTargetNamespace,
  setStandardDefinitions,
  setStandardDefinitionsOnDefinitionsObject,
  setDeploymentMethod,
  setMachineInfo,
  setUserTaskFileNames,
  addConstraints,
  addCallActivityReference,
  removeCallActivityReference,
  removeUnusedCallActivityReferences,
  addDocumentation,
  addDocumentationToProcessObject,
  saveOriginalDefinitionsAsBackup,
};
