const BPMNModdleModule = require('bpmn-moddle');
const BPMNModdle = BPMNModdleModule.default || BPMNModdleModule;
const bpmnSchema = require('../customSchema.json');

const moddle = new BPMNModdle({ proceed: bpmnSchema });

/**
 * @module @proceed/bpmn-helper
 */

/**
 * Function that converts the given xml to a traversable object representation
 *
 * @param {string} xml the bpmn xml we want to convert
 * @param {string} typename name of the root element
 * @returns {Promise<object|Error>} - a taversable object representation of the given xml
 */
function toObject(xml, typename) {
  return new Promise((resolve, reject) => {
    moddle.fromXML(xml, typename, (err, obj) => {
      if (err) {
        reject(err);
      } else {
        resolve(obj);
      }
    });
  });
}

/**
 * Function that converts the given bpmn object to xml
 *
 * @param {object} bpmn traversable object representation
 * @returns {Promise<string|Error>} - a xml representation of the given object
 */
function toXML(obj) {
  return new Promise((resolve, reject) => {
    moddle.toXML(obj, { format: true }, (saveErr, xml) => {
      if (saveErr) {
        reject(saveErr);
      }
      resolve(xml);
    });
  });
}

/**
 * Finds all kinds of childnodes in a given node
 *
 * @param {object} travObj object of which we want to know the childnodes
 * @returns {array} - all childnodes of the given node
 */
function getChildren(travObj) {
  const childNodeTypes = [
    'rootElements',
    'flowElements',
    'values',
    'diagrams',
    'imports',
    'extensionElements',
  ];

  const allChildren = childNodeTypes
    .filter((childNodeType) => travObj[childNodeType])
    .flatMap((childNodeType) => travObj[childNodeType]);

  return allChildren;
}

/**
 * A function that given a traversable object returns all occurences
 *
 * @param {object} travObj the object we want to search in
 * @returns {array} - all nodes within the object
 */
function getAllElements(travObj) {
  // retrieve children for current object
  const allElements = getChildren(travObj)
    // retrieve all grandchilds
    .flatMap((child) => getAllElements(child));

  allElements.push(travObj);

  return allElements;
}

/**
 * A function that given a traversable object returns all occurences of a given tagName
 *
 * @param {object} travObj the object we want to search in
 * @param {string} tagname the name we are searching for
 * @returns {array} - all nodes with the given tagName
 */
function getElementsByTagName(travObj, tagname) {
  const matches = getChildren(travObj)
    // recursively search in all children
    .flatMap((child) => getElementsByTagName(child, tagname));

  if (travObj.$type === tagname) {
    matches.push(travObj);
  }

  return matches;
}

/**
 * A function that given a traversable object returns the nested object with the given id
 *
 * @param {object} travObj the object we want to search in
 * @param {string} id the id of the object we want to find
 * @returns {object|undefined} - returns the found object or undefined when no matching object was found
 */
function getElementById(travObj, id) {
  if (travObj.id === id) {
    return travObj;
  }

  const matchedElement = getChildren(travObj)
    .map((child) => getElementById(child, id))
    .find((matchInChild) => matchInChild);

  return matchedElement;
}

/**
 * Function that changes an element in the given xml using the given manipulation function
 *
 * @param {object} xml the bpmn xml we want to change
 * @param {string} id the id of the element we want to change
 * @param {function} manipFunc the function that will be used to change the element
 */
async function manipulateElementById(xml, id, manipFunc) {
  const obj = await toObject(xml);

  const element = getElementById(obj, id);

  manipFunc(element);

  const newXml = await toXML(obj);

  return newXml;
}

/**
 * Function that changes all elements in the given xml with the given tagname using the given function
 *
 * @param {*} xml the xml we want to make changes in
 * @param {*} tagName the tagname of the elements we want to change
 * @param {*} manipFunc the function that gets called on each element
 */
async function manipulateElementsByTagName(xml, tagName, manipFunc) {
  const obj = await toObject(xml);

  const elements = getElementsByTagName(obj, tagName);
  elements.forEach(manipFunc);

  const newXml = await toXML(obj);

  return newXml;
}

module.exports = {
  moddle,
  toObject,
  toXML,
  getChildren,
  getElementsByTagName,
  getAllElements,
  getElementById,
  manipulateElementById,
  manipulateElementsByTagName,
};
