const { exec } = require('child_process');

const frontendDevServerProcess = exec('yarn web:dev-serve-frontend', { cwd: __dirname }, (err) => {
  if (err) {
    console.error(err);
    return;
  }
});

frontendDevServerProcess.stdout.on('data', (data) => {
  const dataString = data.toString();
  console.log('Webpack-Dev-Server-Frontend: ', dataString);
});
frontendDevServerProcess.stderr.on('data', (data) => {
  const dataString = data.toString();
  console.error('Webpack-Dev-Server-Frontend: ', dataString);
});

const serverProcess = exec('yarn web:dev-start-backend', { cwd: __dirname }, (err) => {
  if (err) {
    console.error(err);
    return;
  }
});

const backendPuppeteerDevServerProcess = exec(
  'yarn web:dev-serve-backend-puppeteer',
  { cwd: __dirname },
  (err) => {
    if (err) {
      console.error(err);
      return;
    }
  }
);

backendPuppeteerDevServerProcess.stdout.on('data', (data) => {
  const dataString = data.toString();
  console.log('Webpack-Dev-Server-Backend-Puppeteer: ', dataString);
});
backendPuppeteerDevServerProcess.stderr.on('data', (data) => {
  const dataString = data.toString();
  console.error('Webpack-Dev-Server-Backend-Puppeteer: ', dataString);
});

serverProcess.stdout.on('data', (data) => {
  const dataString = data.toString();
  console.log('Server: ', dataString);
});
serverProcess.stderr.on('data', (data) => {
  const dataString = data.toString();
  console.error('Server: ', dataString);
});
