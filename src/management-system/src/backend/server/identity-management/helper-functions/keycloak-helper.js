import __dirname from '../../dirname-node.js';
import fs from 'fs';
import path from 'path';

// reads all keycloak realm configuration files that are named as keycloak.{realmname or tenant}.json for example keycloak.siemens-ag.json or keycloak.proceed.json
let keycloakRealmConfigurations = [];
const regex = /(keycloak)\..*\.json/i;

async function readKeycloakRealmConfigurationFiles(filePath) {
  const files = await fs.promises.readdir(filePath);
  files
    .filter((file) => regex.test(file))
    .forEach(async (file) => {
      try {
        const data = await fs.promises.readFile(path.join(filePath, `/${file}`));
        keycloakRealmConfigurations.push(JSON.parse(data));
      } catch (error) {
        if (error.code === 'ENOENT') {
          console.log('File not found!');
        } else {
          throw error;
        }
      }
    });
}

readKeycloakRealmConfigurationFiles(
  path.join(__dirname, './identity-management/keycloak/configuration-files')
);

export default keycloakRealmConfigurations;
