import keycloak from '../keycloak/keycloak.js';
import qs from 'qs';
import { packRules } from '@casl/ability/extra';

/**
 * this function builds the axios config to receive an rpt token from the keycloak authorization server with the usage of an access token that is stored serverside in the user session
 * requesting party token (RPT): An UMA access token associated with a set of authorization data, used by the client to gain access to protected resources at the resource server.
 * https://docs.kantarainitiative.org/uma/ed/uma-core-2.0-01.html#authorization-api
 */
function buildRptTokenOptions(accessToken) {
  return {
    method: 'POST',
    url: `${
      keycloak.keycloakPerRealm[keycloak.getRealmName].config.realmUrl
    }/protocol/openid-connect/token`,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Cache-Control': 'no-cache, no-store, must-revalidate',
      Pragma: 'no-cache',
      Accept: 'application/json',
      Authorization: `Bearer ${accessToken}`,
    },
    data: qs.stringify({
      grant_type: 'urn:ietf:params:oauth:grant-type:uma-ticket',
      audience: keycloak.keycloakPerRealm[keycloak.getRealmName].config.clientId,
    }),
  };
}
/**
 * this function builds the axios config to receive a new access token with the usage of a refresh token that is stored serverside in the user session
 * https://tools.ietf.org/html/rfc6749#section-1.5
 */
function buildRefreshTokenOptions(refreshToken) {
  return {
    method: 'POST',
    url: `${
      keycloak.keycloakPerRealm[keycloak.getRealmName].config.realmUrl
    }/protocol/openid-connect/token`,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    data: qs.stringify({
      client_id: keycloak.keycloakPerRealm[keycloak.getRealmName].config.clientId,
      grant_type: 'refresh_token',
      refresh_token: refreshToken,
      client_secret: keycloak.keycloakPerRealm[keycloak.getRealmName].config.secret,
    }),
  };
}

/**
 * this funtion builds permissions for the frontend based on the requesting party token (RPT) and stores permissions in the session to use it in websockets too because express and socket.io share same session
 * uses CASL library for evaluating the created permissions in the frontend and in websockets
 * https://github.com/stalniy/casl
 * https://casl.js.org/v5/en/guide/intro
 */
function buildPermissions(token) {
  if (token.authorization) {
    const views = token.authorization.permissions
      .filter((permission) => permission.rsname.includes(':'))
      .map((view) => view.rsname.substr(0, view.rsname.indexOf(':')));
    let entities = token.authorization.permissions
      .filter((permission) => !permission.rsname.includes(':') && !permission.rsname.includes('_'))
      .map((entity) => ({ action: entity.scopes, subject: entity.rsname }));
    let sharedProcesses = token.authorization.permissions
      .filter((permission) => permission.rsname.includes('_'))
      .map((sharedProcess) => ({
        action: sharedProcess.scopes,
        subject: 'Process',
        conditions: { id: `_${sharedProcess.rsname.split('_')[1]}` },
      }));
    const processIndex = entities.findIndex((rule) => rule.subject === 'Process');
    if (processIndex >= 0 && token.groups.length > 0) {
      entities[processIndex].conditions = {
        groupId: { $in: token.groups },
      };
    } else if (processIndex >= 0 && token.groups.length === 0) {
      entities.splice(processIndex, 1);
    }
    if (views.includes('process')) {
      entities.push({ action: ['manage'], subject: 'Process', conditions: { userId: token.sub } });
    }
    return packRules([{ action: ['view'], subject: views }, ...entities, ...sharedProcesses]);
  }
  return null;
}

export { buildRptTokenOptions, buildRefreshTokenOptions, buildPermissions };
