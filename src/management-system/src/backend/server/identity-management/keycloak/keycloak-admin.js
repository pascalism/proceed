import KeycloakAdmin from 'keycloak-admin';
import keycloak from './keycloak.js';
import jwt from 'jsonwebtoken';
import * as userAdminClient from './keycloak-admin/users.js';
import * as roleAdminClient from './keycloak-admin/roles.js';
import * as groupAdminClient from './keycloak-admin/groups.js';
import * as umaPoliciesAdminClient from './keycloak-admin/umaPolicies.js';
import * as umaResourcesAdminClient from './keycloak-admin/umaResources.js';
import keycloakRealmConfigurations from '../helper-functions/keycloak-helper.js';

// this file is used to initialize one keycloak Admin Client for each keycloak realm configuration file that is named as keycloak.{realmname or tenant}.json
// it supports multi-tenancy what means, that we only run one instance of the PROCEED MS, but provide distinct and isolated environments for each tenant
// in PROCEED a tenant (realm) is an enterprise that contains multiple employees (users)
// so all tenants share the same app instance and hardware resources but have only access to their data

// used to get the correct constructor in production and development mode
let KeycloakAdminClient;
if (KeycloakAdmin.default) {
  KeycloakAdminClient = KeycloakAdmin.default;
} else {
  KeycloakAdminClient = KeycloakAdmin;
}

// Keycloak Connect Admin Class for administrative function (calls Keycloak REST API)
export default class AdminClient {
  constructor() {
    this.kcClient = {};
    // creates a keycloak admin client for each realm so this.kcClient[keycloak.getRealmName] gets the admin client for the active tenant
    keycloakRealmConfigurations.forEach((realm) => {
      this.kcClient[realm.realm] = new KeycloakAdminClient({
        baseUrl: realm['auth-server-url'].includes('auth/')
          ? realm['auth-server-url'].replace('auth/', 'auth')
          : realm['auth-server-url'],
        realmName: realm.realm,
      });
    });
  }

  // authenticates the service account with the client credentials flow only if access token is not valid anymore (each 5 minutes) at the keycloak authorization server
  async init() {
    let token;
    if (this.kcClient[keycloak.getRealmName].accessToken) {
      token = jwt.decode(this.kcClient[keycloak.getRealmName].accessToken);
    }
    if (!token || token.exp * 1000 < Date.now()) {
      try {
        return await this.kcClient[keycloak.getRealmName].auth({
          grantType: 'client_credentials',
          clientId: keycloak.keycloakPerRealm[keycloak.getRealmName].config.clientId,
          clientSecret: keycloak.keycloakPerRealm[keycloak.getRealmName].config.secret,
          offlineToken: true,
        });
      } catch (error) {
        console.log(error);
      }
    }
  }

  // USERS - all administrative functions that concern users

  async getAllUsers() {
    await this.init();
    return await userAdminClient.getAllUsers(this.kcClient[keycloak.getRealmName]);
  }

  async getUserById(userId) {
    await this.init();
    return await userAdminClient.getUserById(this.kcClient[keycloak.getRealmName], userId);
  }

  async getGroupsFromUser(userId) {
    await this.init();
    return await userAdminClient.getGroupsFromUser(this.kcClient[keycloak.getRealmName], userId);
  }

  async getRolesFromUser(userId) {
    await this.init();
    return await userAdminClient.getRolesFromUser(this.kcClient[keycloak.getRealmName], userId);
  }

  async getUsersFromRole(roleName) {
    await this.init();
    return await userAdminClient.getUsersFromRole(this.kcClient[keycloak.getRealmName], roleName);
  }

  async addRoleForUser(userId, data) {
    await this.init();
    return await userAdminClient.addRoleForUser(this.kcClient[keycloak.getRealmName], userId, data);
  }

  async addUser(data) {
    await this.init();
    return await userAdminClient.addUser(this.kcClient[keycloak.getRealmName], data);
  }

  async resetPassword(userId, username) {
    await this.init();
    return await userAdminClient.resetPassword(
      this.kcClient[keycloak.getRealmName],
      userId,
      username
    );
  }

  async updateUser(userId, data) {
    await this.init();
    return await userAdminClient.updateUser(this.kcClient[keycloak.getRealmName], userId, data);
  }

  async deleteRoleFromUser(userId, data) {
    await this.init();
    return await userAdminClient.deleteRoleFromUser(
      this.kcClient[keycloak.getRealmName],
      userId,
      data
    );
  }

  async deleteUser(userId) {
    await this.init();
    return await userAdminClient.deleteUser(this.kcClient[keycloak.getRealmName], userId);
  }

  // ROLES - all administrative functions that concern roles

  async getRoles() {
    await this.init();
    return await roleAdminClient.getRoles(this.kcClient[keycloak.getRealmName]);
  }

  async getRoleById(roleId) {
    await this.init();
    return await roleAdminClient.getRoleById(this.kcClient[keycloak.getRealmName], roleId);
  }

  async getRoleByName(roleName) {
    await this.init();
    return await roleAdminClient.getRoleByName(this.kcClient[keycloak.getRealmName], roleName);
  }

  async addRole(roleName) {
    await this.init();
    return await roleAdminClient.addRole(this.kcClient[keycloak.getRealmName], roleName);
  }

  async updateRoleById(roleId, data) {
    await this.init();
    return await roleAdminClient.updateRoleById(this.kcClient[keycloak.getRealmName], roleId, data);
  }

  async deleteRoleById(roleId) {
    await this.init();
    return await roleAdminClient.deleteRoleById(this.kcClient[keycloak.getRealmName], roleId);
  }

  // GROUPS - all administrative functions that concern groups

  async getAllGroups() {
    await this.init();
    return await groupAdminClient.getAllGroups(this.kcClient[keycloak.getRealmName]);
  }

  async getUsersInGroup(groupId) {
    await this.init();
    return await groupAdminClient.getUsersInGroup(this.kcClient[keycloak.getRealmName], groupId);
  }

  async getGroupById(groupId) {
    await this.init();
    return await groupAdminClient.getGroupById(this.kcClient[keycloak.getRealmName], groupId);
  }

  async addGroup(groupName) {
    await this.init();
    return await groupAdminClient.addGroup(this.kcClient[keycloak.getRealmName], groupName);
  }

  async addChildToGroup(groupId, groupName, attributes) {
    await this.init();
    return await groupAdminClient.addChildToGroup(
      this.kcClient[keycloak.getRealmName],
      groupId,
      groupName,
      attributes
    );
  }

  async addUserToGroup(groupId, userId) {
    await this.init();
    return await groupAdminClient.addUserToGroup(
      this.kcClient[keycloak.getRealmName],
      groupId,
      userId
    );
  }

  async removeUserFromGroup(userId, groupId) {
    await this.init();
    return await groupAdminClient.removeUserFromGroup(
      this.kcClient[keycloak.getRealmName],
      userId,
      groupId
    );
  }

  async updateGroup(groupId, data) {
    await this.init();
    return await groupAdminClient.updateGroup(this.kcClient[keycloak.getRealmName], groupId, data);
  }

  async deleteGroupById(groupId) {
    await this.init();
    return await groupAdminClient.deleteGroupById(this.kcClient[keycloak.getRealmName], groupId);
  }

  // POLICIES - all administrative functions that concern policies (these functions are not part of the keycloak-connect adapter, so this is a custom implementation)

  async getPoliciesByResourceId(resourceId) {
    await this.init();
    return await umaPoliciesAdminClient.getPoliciesByResourceId(
      this.kcClient[keycloak.getRealmName],
      resourceId
    );
  }

  async getPoliciesByName(name) {
    await this.init();
    return await umaPoliciesAdminClient.getPoliciesByName(
      this.kcClient[keycloak.getRealmName],
      name
    );
  }

  async addResourcePolicyToResource(resourceName, scopes, role, resourceId) {
    await this.init();
    return await umaPoliciesAdminClient.addResourcePolicyToResource(
      this.kcClient[keycloak.getRealmName],
      resourceName,
      scopes,
      role,
      resourceId
    );
  }

  async addViewPolicyToResource(viewName, role, resourceId) {
    await this.init();
    return await umaPoliciesAdminClient.addViewPolicyToResource(
      this.kcClient[keycloak.getRealmName],
      viewName,
      role,
      resourceId
    );
  }

  async updatePolicy(policy) {
    await this.init();
    return await umaPoliciesAdminClient.updatePolicy(this.kcClient[keycloak.getRealmName], policy);
  }

  async deletePolicy(policyId) {
    await this.init();
    return await umaPoliciesAdminClient.deletePolicy(
      this.kcClient[keycloak.getRealmName],
      policyId
    );
  }

  // RESOURCES - all administrative functions that concern resources (these functions are not part of the keycloak-connect adapter, so this is a custom implementation)

  // gets user managed resources based on a type or name
  async getUmaResourceByTypeOrName(type, name) {
    await this.init();
    return await umaResourcesAdminClient.getUmaResourceByTypeOrName(
      this.kcClient[keycloak.getRealmName],
      type,
      name
    );
  }
  // get user managed resource based on a resource id
  async getUmaResourceById(resourceId) {
    await this.init();
    return await umaResourcesAdminClient.getUmaResourceById(
      this.kcClient[keycloak.getRealmName],
      resourceId
    );
  }
}
