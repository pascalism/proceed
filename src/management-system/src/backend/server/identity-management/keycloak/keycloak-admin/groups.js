// all administrative functions that concern groups

// gets all groups of a realm
async function getAllGroups(kcClient) {
  const groups = await kcClient.groups.find();
  if (!groups) {
    throw new Error('Error fetching all groups.');
  }
  return groups;
}

// gets all users from a group based on a group id
async function getUsersInGroup(kcClient, groupId) {
  const groupMembers = await kcClient.groups.listMembers({
    id: groupId,
  });
  if (!groupMembers) {
    throw new Error('Error fetching group members.');
  }
  return groupMembers;
}

// gets a group based on a group id
async function getGroupById(kcClient, groupId) {
  const group = await kcClient.groups.findOne({
    id: groupId,
  });
  if (!group) {
    throw new Error('Error fetching group.');
  }
  return group;
}

// adds a group with a specific group name (doesn't have to be unique)
async function addGroup(kcClient, groupName) {
  const group = await kcClient.groups.create({
    name: groupName,
  });
  if (!group) {
    throw new Error('Error adding group.');
  }
  return group;
}

// adds a group as a child group of a parent group
async function addChildToGroup(kcClient, groupId, groupName, attributes) {
  const child = await kcClient.groups.setOrCreateChild(
    { id: groupId },
    { name: groupName, attributes: attributes }
  );
  if (!child) {
    throw new Error('Error setting child to group.');
  }
  return child;
}

// adds a user to a group based on user and group id
async function addUserToGroup(kcClient, groupId, userId) {
  const user = await kcClient.users.addToGroup({ groupId: groupId, id: userId });
  if (!user) {
    throw new Error('Error adding user to group.');
  }
  return user;
}

// removes a user grom a group based on user and group id
async function removeUserFromGroup(kcClient, userId, groupId) {
  const user = await kcClient.users.delFromGroup({ id: userId, groupId: groupId });
  if (!user) {
    throw new Error('Error removing user from group.');
  }
  return user;
}

// updates a group based on a group id (especially used for group attributes)
async function updateGroup(kcClient, groupId, data) {
  const group = await kcClient.groups.update({ id: groupId }, data);
  if (!group) {
    throw new Error('Error updating group.');
  }
  return group;
}

// deletes a group based on a group id
async function deleteGroupById(kcClient, groupId) {
  const group = await kcClient.groups.del({
    id: groupId,
  });
  if (!group) {
    throw new Error('Error deleting group.');
  }
  return group;
}

export default {
  getAllGroups,
  getUsersInGroup,
  getGroupById,
  addGroup,
  addChildToGroup,
  addUserToGroup,
  removeUserFromGroup,
  updateGroup,
  deleteGroupById,
};
