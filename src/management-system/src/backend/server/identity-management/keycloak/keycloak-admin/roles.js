// all administrative functions that concern roles

// gets all roles of the realm
async function getRoles(kcClient) {
  const roles = await kcClient.roles.find();
  if (!roles) {
    throw new Error('Error fetching all roles.');
  }
  return roles;
}

// gets a role based on a role id
async function getRoleById(kcClient, roleId) {
  const role = await kcClient.roles.findOneById({
    id: roleId,
  });
  if (!role) {
    throw new Error('Error fetching role by id.');
  }
  return role;
}

// gets a role based on the role's name
async function getRoleByName(kcClient, roleName) {
  const role = await kcClient.roles.findOneByName({ name: roleName });
  if (!role) {
    throw new Error('Error fetching role by name.');
  }
  return role;
}

// adds a realm role with a specific name that has to be unique
async function addRole(kcClient, roleName) {
  const role = await kcClient.roles.create({
    name: roleName,
  });
  if (!role) {
    throw new Error('Error adding role.');
  }
  return role;
}

// updates a role based on the role id (especially used for attributes)
async function updateRoleById(kcClient, roleId, data) {
  const role = await kcClient.roles.updateById({ id: roleId }, data);
  if (!role) {
    throw new Error('Error updating role.');
  }
  return role;
}

// deletes a role based on a role id
async function deleteRoleById(kcClient, roleId) {
  const role = await kcClient.roles.delById({
    id: roleId,
  });
  if (!role) {
    throw new Error('Error deleting role.');
  }
  return role;
}

export default { getRoles, getRoleById, getRoleByName, addRole, updateRoleById, deleteRoleById };
