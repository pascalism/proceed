// file extends the keycloak-admin with custom REST API calls
import keycloak from '../keycloak.js';
import axios from 'axios';

// all administrative functions that concern policies (these functions are not part of the keycloak-connect adapter, so this is a custom implementation)

// gets all policies of by resource id
async function getPoliciesByResourceId(kcClient, resourceId) {
  if (resourceId && kcClient.accessToken) {
    const policies = await doRequest(
      'GET',
      `${
        keycloak.keycloakPerRealm[keycloak.getRealmName].config.realmUrl
      }/authz/protection/uma-policy?resource=${resourceId}`,
      kcClient.accessToken
    );
    if (!policies) {
      throw new Error('Error fetching policies by id.');
    }
    return policies;
  } else {
    throw new Error('Error with parameters.');
  }
}

// gets a policy by name
async function getPoliciesByName(kcClient, name) {
  if (name && kcClient.accessToken) {
    const policies = await doRequest(
      'GET',
      `${
        keycloak.keycloakPerRealm[keycloak.getRealmName].config.realmUrl
      }/authz/protection/uma-policy?name=${name}`,
      kcClient.accessToken
    );
    if (!policies) {
      throw new Error('Error fetching policies by name.');
    }
    return policies;
  } else {
    throw new Error('Error with parameters.');
  }
}

// adds a resource policy to a resource
async function addResourcePolicyToResource(kcClient, resourceName, scopes, role, resourceId) {
  const policyBody = {
    name: `${resourceName}:${role}`,
    description: `Policy for resource ${resourceName} and role ${role}`,
    type: 'uma',
    scopes: scopes,
    logic: 'POSITIVE',
    decisionStrategy: 'UNANIMOUS',
    roles: [role],
  };
  if (resourceName && scopes && role && resourceId && kcClient.accessToken) {
    const resourcePolicy = await doRequest(
      'POST',
      `${
        keycloak.keycloakPerRealm[keycloak.getRealmName].config.realmUrl
      }/authz/protection/uma-policy/${resourceId}`,
      kcClient.accessToken,
      policyBody
    );
    if (!resourcePolicy) {
      throw new Error('Error creating resource policy.');
    }
    return resourcePolicy;
  } else {
    throw new Error('Error with parameters.');
  }
}

// adds a view policy to a view resource
async function addViewPolicyToResource(kcClient, viewName, role, resourceId) {
  const policyBody = {
    name: `${viewName}:uma:view`,
    description: `Permission to get ${viewName} view in PROCEED MS`,
    type: 'uma',
    scopes: ['view'],
    logic: 'POSITIVE',
    decisionStrategy: 'UNANIMOUS',
    roles: [role],
  };
  if (viewName && role && resourceId && kcClient.accessToken) {
    const viewPolicy = await doRequest(
      'POST',
      `${
        keycloak.keycloakPerRealm[keycloak.getRealmName].config.realmUrl
      }/authz/protection/uma-policy/${resourceId}`,
      kcClient.accessToken,
      policyBody
    );
    if (!viewPolicy) {
      throw new Error('Error creating view policy.');
    }
    return viewPolicy;
  } else {
    throw new Error('Error with parameters.');
  }
}

// updates a policy
async function updatePolicy(kcClient, policy) {
  if (policy && kcClient.accessToken) {
    const policy = await doRequest(
      'PUT',
      `${
        keycloak.keycloakPerRealm[keycloak.getRealmName].config.realmUrl
      }/authz/protection/uma-policy/${policy.id}`,
      kcClient.accessToken,
      policy
    );
    if (!policy) {
      throw new Error('Error updating policy.');
    }
    return policy;
  } else {
    throw new Error('Error with parameters.');
  }
}

// deletes a policy
async function deletePolicy(kcClient, policyId) {
  if (policyId && kcClient.accessToken) {
    const policy = await doRequest(
      'DELETE',
      `${
        keycloak.keycloakPerRealm[keycloak.getRealmName].config.realmUrl
      }/authz/protection/uma-policy/${policyId}`,
      kcClient.accessToken
    );
    if (!policy) {
      throw new Error('Error deleting policy.');
    }
    return policy;
  } else {
    throw new Error('Error with parameters.');
  }
}

// generic request function, that executes an axios request to the keycloak REST API
async function doRequest(method, url, accessToken, jsonBody = null) {
  let options = {
    method: method,
    url: url,
    headers: { Authorization: `Bearer ${accessToken}` },
  };

  if (jsonBody !== null) {
    options.headers = {
      Authorization: `Bearer ${accessToken}`,
      'Content-Type': 'application/json',
    };
    options.data = JSON.stringify(jsonBody);
  }

  const result = await axios(options);
  if (!result) {
    throw new Error('Error doing request.');
  } else {
    return result;
  }
}

export default {
  getPoliciesByResourceId,
  getPoliciesByName,
  addResourcePolicyToResource,
  addViewPolicyToResource,
  updatePolicy,
  deletePolicy,
};
