// file extends the keycloak-admin with custom REST API calls
import keycloak from '../keycloak.js';
import axios from 'axios';

// all administrative functions that concern resources (these functions are not part of the keycloak-connect adapter, so this is a custom implementation)

// gets user managed resources based on a type or name
async function getUmaResourceByTypeOrName(kcClient, type, name) {
  if (kcClient && (type || name)) {
    const clients = await kcClient.clients.find();
    if (!clients) {
      throw new Error('Error fetching clients.');
    }
    const clientId = clients.find(
      (client) =>
        client.clientId === keycloak.keycloakPerRealm[keycloak.getRealmName].config.clientId
    ).id;
    if (type && clientId && kcClient.accessToken) {
      const resources = await doRequest(
        'GET',
        `${
          keycloak.keycloakPerRealm[keycloak.getRealmName].config.realmAdminUrl
        }/clients/${clientId}/authz/resource-server/resource?type=${type}`,
        kcClient.accessToken
      );
      if (!resources) {
        throw new Error('Error fetching resources by type.');
      }
      return resources;
    } else if (name && clientId && kcClient.accessToken) {
      const resources = await doRequest(
        'GET',
        `${
          keycloak.keycloakPerRealm[keycloak.getRealmName].config.realmUrl
        }/authz/protection/uma-policy?name=${name}`,
        kcClient.accessToken
      );
      if (!resources) {
        throw new Error('Error fetching resource by name.');
      }
      return resources;
    } else {
      throw new Error('Error with parameters.');
    }
  }
}

// get user managed resource based on a resource id
async function getUmaResourceById(kcClient, resourceId) {
  if (resourceId && kcClient.accessToken) {
    const resource = await doRequest(
      'GET',
      `${
        keycloak.keycloakPerRealm[keycloak.getRealmName].config.realmUrl
      }/authz/protection/resource_set/${resourceId}`,
      kcClient.accessToken
    );
    if (!resource) {
      throw new Error('Error fetching resource by id.');
    }
    return resource;
  }
}

// generic request function, that executes an axios request to the keycloak REST API
async function doRequest(method, url, accessToken, jsonBody = null) {
  let options = {
    method: method,
    url: url,
    headers: { Authorization: `Bearer ${accessToken}` },
  };

  if (jsonBody !== null) {
    options.headers = {
      Authorization: `Bearer ${accessToken}`,
      'Content-Type': 'application/json',
    };
    options.data = JSON.stringify(jsonBody);
  }

  const result = await axios(options);
  if (!result) {
    throw new Error('Error doing request.');
  } else {
    return result;
  }
}

export default {
  getUmaResourceByTypeOrName,
  getUmaResourceById,
};
