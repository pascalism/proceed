// all administrative functions that concern users

// returns all users of a realm
async function getAllUsers(kcClient) {
  const users = await kcClient.users.find();
  if (!users) {
    throw new Error('Error fetching all users.');
  }
  return users;
}

// returns a user based on a user id
async function getUserById(kcClient, userId) {
  const user = await kcClient.users.findOne({ id: userId });
  if (!user) {
    throw new Error('Error fetching user by id.');
  }
  return user;
}

// returns all roles of a user basaed on a user id
async function getRolesFromUser(kcClient, userId) {
  const roles = await kcClient.users.listRealmRoleMappings({
    id: userId,
  });
  if (!roles) {
    throw new Error('Error fetching roles from user.');
  }
  return roles.filter((role) => {
    if (role.name != 'uma_authorization' && role.name != 'offline_access') {
      return role;
    }
  });
}

// returns all groups of a user based on a user id
async function getGroupsFromUser(kcClient, userId) {
  const groups = await kcClient.users.listGroups({ id: userId });
  if (!groups) {
    throw new Error('Error fetching groups from user.');
  }
  return groups;
}

// returns all users that have a specific role based on a role name (role names are unique)
async function getUsersFromRole(kcClient, roleName) {
  const users = await kcClient.roles.findUsersWithRole({
    name: roleName,
  });
  if (!users) {
    throw new Error('Error fetching users from role.');
  }
  return users;
}

// adds a role for a specific user based on a user id
async function addRoleForUser(kcClient, userId, data) {
  const role = await kcClient.users.addRealmRoleMappings({
    id: userId,
    roles: data.roles,
  });
  if (!role) {
    throw new Error('Error adding role to user.');
  }
  return role;
}

// adds a user and sets an action to UPDATE_PASSWORD, so that the user has to update his password on the #initial login
async function addUser(kcClient, data) {
  const user = await kcClient.users.create({
    ...data,
    emailVerified: true,
    enabled: true,
    requiredActions: ['UPDATE_PASSWORD'],
  });
  if (!user) {
    throw new Error('Error creating user.');
  }
  return user;
}

// sets the initial password
async function resetPassword(kcClient, userId, username) {
  const resetPasswd = await kcClient.users.resetPassword({
    id: userId,
    credential: {
      temporary: true,
      type: 'password',
      value: username,
    },
  });
  if (!resetPasswd) {
    throw new Error('Error resetting password.');
  }
  return resetPasswd;
}

// updates user account information based on a user id
async function updateUser(kcClient, userId, data) {
  const user = await kcClient.users.update({ id: userId }, data);
  if (!user) {
    throw new Error('Error updating user.');
  }
  return user;
}

// deletes a role from a user based on a user id and role
async function deleteRoleFromUser(kcClient, userId, data) {
  const role = await kcClient.users.delRealmRoleMappings({
    id: userId,
    roles: data.roles,
  });
  if (!role) {
    throw new Error('Error removing role from user.');
  }
  return role;
}

// deletes a user from the realm based on a user id
async function deleteUser(kcClient, userId) {
  const user = await kcClient.users.del({ id: userId });
  if (!user) {
    throw new Error('Error deleting user.');
  }
  return user;
}

export default {
  getAllUsers,
  getUserById,
  getRolesFromUser,
  getGroupsFromUser,
  getUsersFromRole,
  addRoleForUser,
  addUser,
  resetPassword,
  updateUser,
  deleteRoleFromUser,
  deleteUser,
};
