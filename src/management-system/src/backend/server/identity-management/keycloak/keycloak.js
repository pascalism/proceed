import Keycloak from 'keycloak-connect';
import ports from '../../../../../ports.js';
import composable_middleware from 'composable-middleware';
import store from '../login-session-store/login-session-store.js';
import keycloakRealmConfigurations from '../helper-functions/keycloak-helper.js';

// this file is used to initialize one keycloak client for each keycloak realm configuration file that is named as keycloak.{realmname or tenant}.json
// it supports multi-tenancy what means, that we only run one instance of the PROCEED MS, but provide distinct and isolated environments for each tenant
// in PROCEED a tenant (realm) is an enterprise that contains multiple employees (users)
// so all tenants share the same app instance and hardware resources but have only access to their data

// temporary store of the active tenant that wants to login (used for loginUrl)
var kcTenant = {};

// this function expects an array of keycloak realm configurations from the function above
function KeycloakMultiRealm(keycloakConfigs) {
  this.keycloakPerRealm = {};
  // standard realm is 'PROCEED'
  this.getRealmName = 'PROCEED';
  // determines the realm based on subdomain -> users access application with url's like: siemens-ag.proceed.com
  // in this case the subdomain would be siemens-ag -> keycloak realm has to be names as siemens-ag and the keycloak configuration file has to be named as keycloak.siemens-ag.json
  this.determinRealmName = function (req) {
    this.getRealmName = req.header('host').substr(0, req.header('host').indexOf('.'))
      ? req.header('host').substr(0, req.header('host').indexOf('.'))
      : 'PROCEED';
    kcTenant = this.keycloakPerRealm[this.getRealmName];
  };
  // creates a keycloak instance for each realm
  keycloakConfigs.forEach((kConfig) => {
    this.keycloakPerRealm[kConfig.realm] = new Keycloak(
      {
        store: store,
      },
      kConfig
    );
  });
  // sets the active keycloak tenant for loginUrl below
  // if there is a better option to get this variable into the loginUrl function, please replace the logic
  kcTenant = this.keycloakPerRealm[this.getRealmName];
}

// sets middleware for the active tenant
KeycloakMultiRealm.prototype.middleware = function (options) {
  return (req, res, next) => {
    // determines the realmname from the subdomain
    this.determinRealmName(req);
    const realmName = this.getRealmName;
    const keycloak = this.keycloakPerRealm[realmName];
    // creates middleware for each tenant and composes them as one middleware
    const middleware = composable_middleware(keycloak.middleware());
    middleware(req, res, next);
  };
};

// sets protect function for the active tenant
KeycloakMultiRealm.prototype.protect = function (spec) {
  return (req, res, next) => {
    // determines the realmname from the subdomain
    this.determinRealmName(req);
    const realmName = this.getRealmName;
    const keycloak = this.keycloakPerRealm[realmName];
    // creates protect middleware for each tenant that can be used to protect routes
    keycloak.protect(spec)(req, res, next);
  };
};

// sets enforcer function for the active tenant
KeycloakMultiRealm.prototype.enforcer = function (permissions, config) {
  return (req, res, next) => {
    // determines the realmname from the subdomain
    this.determinRealmName(req);
    const realmName = this.getRealmName;
    const keycloak = this.keycloakPerRealm[realmName];
    // creates enforcer middleware for each tenant that can be used to protect routes with fine grained permissions
    keycloak.enforcer(permissions, config)(req, res, next);
  };
};

// gets the keycloak instance from the active tenant
KeycloakMultiRealm.prototype.getKeycloakForRealm = function () {
  const realmName = this.getRealmName;
  const keycloak = this.keycloakPerRealm[realmName];
  return keycloak;
};

// disables redirect to keycloak authorization server for all routes that are not login or register
// should only redirect if it is a auth request (login or register) to routes /api/auth*
// not implemented by default in keycloak-connect library, so the following code overwrites the default behaviour
// https://github.com/keycloak/keycloak-nodejs-connect/blob/master/keycloak.d.ts
Keycloak.prototype.redirectToLogin = function (req) {
  var apiReqMatcher = /\/api\/auth\//i;
  console.log(`Original Url: ${req.originalUrl}`);
  console.log(`Url: ${req.url}`);
  console.log(`Redirect: ${apiReqMatcher.test(req.originalUrl || req.url)}`);
  return apiReqMatcher.test(req.originalUrl || req.url);
};

// changes prototype for loginUrl depending on the redirect URI that is set when clicking login or register in frontend app
// necessary to redirect to the correct endpoint, because the user expects to get to register when clicking on register
// not implemented by default in keycloak-connect library
Keycloak.prototype.loginUrl = function (uuid, redirectUrl) {
  let port = process.env.NODE_ENV === 'production' ? ports.frontend : ports.frontend;
  var url =
    kcTenant.config.realmUrl +
    '/protocol/openid-connect/' +
    encodeURIComponent(
      redirectUrl === `https://localhost:${port}/api/auth/register?auth_callback=1`
        ? 'registrations'
        : 'auth'
    ) +
    '?client_id=' +
    encodeURIComponent(kcTenant.config.clientId) +
    '&state=' +
    encodeURIComponent(uuid) +
    '&redirect_uri=' +
    encodeURIComponent(redirectUrl) +
    '&scope=' +
    encodeURIComponent(kcTenant.config.scope ? 'openid ' + kcTenant.config.scope : 'openid') +
    '&response_type=code';

  if (kcTenant.config && kcTenant.config.idpHint) {
    url += '&kc_idp_hint=' + encodeURIComponent(kcTenant.config.idpHint);
  }
  return url;
};

let keycloakMultiRealmSingleton;

if (!keycloakMultiRealmSingleton) {
  keycloakMultiRealmSingleton = new KeycloakMultiRealm(keycloakRealmConfigurations);
}

export default keycloakMultiRealmSingleton;
