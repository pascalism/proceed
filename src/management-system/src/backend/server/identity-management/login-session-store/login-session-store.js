import session from 'express-session';

var store = new session.MemoryStore();

export default store;
