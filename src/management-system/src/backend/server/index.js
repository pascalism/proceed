// This server app makes use of the "type": "module" field in the package.json
// of the MS in order to enable import statements. If moved elsewhere this needs
// to be copied over.
import https from 'https';
import express from 'express';
import session from 'express-session';
import cors from 'cors';
import path from 'path';
import fs from 'fs';
import __dirname from './dirname-node.js';
import { startWebsocketServer } from './socket.js';
import logger from '../shared-electron-server/logging.js';
import ports from '../../../ports.js';
import startWebviewWithPuppeteer from './puppeteerStartWebviewWithBpmnModeller.js';
import crypto from 'crypto';
import store from './identity-management/login-session-store/login-session-store.js';
import apiRouter from './rest-api/index.js';

/**
 * For explanation for the general server architecture, see:
 * https://gitlab.com/dBPMS-PROCEED/proceed/-/wikis/MS/Architecture-Server-and-Desktop-App#ms-server-architecture
 */

const backendServer = express();

const origin = [`https://localhost:${ports.puppeteer}`];

if (process.env.NODE_ENV === 'development') {
  origin.push(
    `https://localhost:${ports['dev-server'].frontend}`,
    `https://localhost:${ports['dev-server'].puppeteer}`
  );
}

backendServer.use(
  cors({
    origin,
    credentials: true,
  })
);

const backendPuppeteerApp = express();

// frontend is served by webpack-dev-server in development
if (process.env.NODE_ENV === 'production') {
  // serve bundled frontend files
  backendServer.use(express.static(path.join(__dirname, './frontend')));

  // server puppeteer via different port => 1. only one client allowed, and 2. most secure way (not reachable from outside world)
  backendPuppeteerApp.use(express.static(path.join(__dirname, './puppeteerPages')));
} else {
  // disables the server certificate verification -> ONLY USE IN DEV MODE!
  // makes TLS connections and HTTPS requests insecure by disabling server certificate verification, but necessary in DEV MODE because of self signed certificate, otherwise doesn't work with keycloak
  process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;
}

backendServer.use(express.text({ type: ['text/plain', 'text/html'] }));
backendServer.use(express.json());

// express-session middleware currently saves keycloak data and cookie data in memorystore DB
// TODO: replace memorystore with better DB like Redis (recommended by express-session) -> already working in a prototype and will be implemented later
// ATTENTION: secret possibly has to be set to a fixed secure and unguessable value if the PROCEED MS runs in multiple containers, to prevent that each container uses a different secret > if a loadbalancer would redirect a user to a container with a mismatched secret, the session would be invalidated, maybe this is also resolvable with an external session DB like Redis
// https://developer.mozilla.org/en-US/docs/Web/HTTP/Cookies
let loginSession = session({
  resave: false,
  secret: crypto.randomBytes(64).toString('hex'), // random secret
  saveUninitialized: false, // doesn't save uninitialized sessions to the store
  rolling: true, // resets cookie lifetime with each new request to specified maxAge
  store: store,
  cookie: {
    secure: process.env.NODE_ENV === 'production' ? true : 'auto', // VERY IMPORTANT, MUST BE SET! -> sends cookies only via https
    sameSite: process.env.NODE_ENV === 'production' ? 'lax' : false, // https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Set-Cookie/SameSite -> false for dev mode to send the cookie also to the REST API
    httpOnly: true, // VERY IMPORTANT, MUST BE SET! -> if true, the cookie is inaccessible to the JavaScript in the frontend
    maxAge: 30 * 60 * 60, // cookie currently valid for 30 minutes -> lifetime resets with every request to the REST API
  },
  name: 'id', // name of the cookie in the browser
});
backendServer.use(loginSession);

let certificate;
const certPath = path.join(__dirname, './ssl/certificate.pem');
if (fs.existsSync(certPath)) {
  logger.info('Own certificate found under ssl/certificate.pem');
  certificate = fs.readFileSync(certPath);
} else {
  // default to development self-signed certificate
  certificate = fs.readFileSync(path.join(__dirname, 'https-public-dev-certificate.pem'));
  logger.warn(
    'No own certificate found under "ssl/certificate.pem". Took development self-signed certificate.'
  );
}

let privateKey;
const keyPath = path.join(__dirname, './ssl/private-key.key');
if (fs.existsSync(keyPath)) {
  logger.info('Own private key found under ssl/private-key.key');
  privateKey = fs.readFileSync(keyPath);
} else {
  // default to development private key
  privateKey = fs.readFileSync(path.join(__dirname, 'https-private-dev-key.key'));
  logger.warn(
    'No own private key found under "ssl/private-key.key". Took development private key.'
  );
}

const options = {
  key: privateKey,
  cert: certificate,
};

backendServer.use('/api', apiRouter);

// Frontend + REST API
https.createServer(options, backendServer).listen(ports.frontend, () => {
  logger.info(
    `MS HTTPS server started on port ${ports.frontend}. Open: https://<IP>:${ports.frontend}/`
  );
});

// Puppeteer Endpoint
https.createServer(options, backendPuppeteerApp).listen(ports.puppeteer, 'localhost', () => {
  logger.debug(
    `HTTPS Server for Puppeteer started on port ${ports.puppeteer}. Open: https://localhost:${ports.frontend}/bpmn-modeller.html`
  );
});

// WebSocket Endpoint for Collaborative Editing
const websocketServer = https.createServer(options);
startWebsocketServer(websocketServer);

// Load BPMN Modeller for Server after Websocket Endpoint is started
startWebviewWithPuppeteer();
