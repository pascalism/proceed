import * as deployment from '../shared-electron-server/network/deployment.js';
import * as requests from '../shared-electron-server/network/requests.js';
import './machineInfo.js';
import logger from '../shared-electron-server/logging.js';

export function setupNetworkRequestHandlers(addListener) {
  addListener('deployment_remove', async (socket, id, processDefinitionsId) => {
    logger.debug(`Request to remove deployment of process with id ${processDefinitionsId}.`);
    socket.emit('deployment_remove', id, await deployment.removeDeployment(processDefinitionsId));
  });

  addListener('deployment_deploy', async (socket, id, processDefinitionsId, dynamic) => {
    logger.debug(
      `Request to ${
        dynamic ? 'dynamically' : 'statically'
      } deploy process with id ${processDefinitionsId}.`
    );
    let response = {};

    try {
      await deployment.deployProcess(processDefinitionsId, dynamic);
    } catch (err) {
      response.error = err.message;
    }

    socket.emit('deployment_deploy', id, response);
  });

  addListener(
    'instance_information_get',
    async (socket, id, machine, processDefinitionsId, instanceId) => {
      logger.debug(
        `Request to get instance information for instance with id ${instanceId} of process with processDefinitionsId ${processDefinitionsId} from machine with ip ${machine.ip}.`
      );
      let response = {};

      try {
        response = await deployment.getInstanceInformation(
          machine,
          processDefinitionsId,
          instanceId
        );
      } catch (err) {
        response.error = err.message;
      }

      socket.emit('instance_information_get', id, response);
    }
  );

  addListener('instance_start', async (socket, id, processDefinitionsId) => {
    logger.debug(`Request to start instance for process with id ${processDefinitionsId}.`);
    let response = {};

    try {
      await deployment.startInstance(processDefinitionsId);
    } catch (err) {
      response.error = err.message;
    }

    socket.emit('instance_start', id, response);
  });

  addListener('instance_stop', async (socket, id, processDefinitionsId, instanceId) => {
    logger.debug(
      `Request to stop instance with id ${instanceId} of process with id ${processDefinitionsId}.`
    );
    let response = {};

    try {
      await deployment.stopInstance(processDefinitionsId, instanceId);
    } catch (err) {
      response.error = err.message;
    }

    socket.emit('instance_stop', id, response);
  });

  addListener('instance_pause', async (socket, id, processDefinitionsId, instanceId) => {
    logger.debug(
      `Request to pause instance with id ${instanceId} of process with id ${processDefinitionsId}.`
    );
    let response = {};

    try {
      await deployment.pauseInstance(processDefinitionsId, instanceId);
    } catch (err) {
      response.error = err.message;
    }

    socket.emit('instance_pause', id, response);
  });

  addListener('instance_resume', async (socket, id, processDefinitionsId, instanceId) => {
    logger.debug(
      `Request to resume instance with id ${instanceId} of process with id ${processDefinitionsId}.`
    );
    let response = {};

    try {
      await deployment.resumeInstance(processDefinitionsId, instanceId);
    } catch (err) {
      response.error = err.message;
    }

    socket.emit('instance_resume', id, response);
  });

  addListener('machine_status_get', async (socket, id, machineId) => {
    logger.debug(`Request to get status of machine with id ${machineId}.`);
    try {
      socket.emit('machine_status_get', id, await requests.getStatus(machineId));
    } catch (err) {
      socket.emit('machine_status_get', id, { error: err });
    }
  });

  addListener('machine_properties_get', async (socket, id, machineId, properties) => {
    logger.debug(`Request to get properties of machine with id ${machineId}.`);
    try {
      socket.emit(
        'machine_properties_get',
        id,
        await requests.getMachineProperties(machineId, properties)
      );
    } catch (err) {
      socket.emit('machine_properties_get', id, { error: err });
    }
  });

  addListener('machine_configuration_get', async (socket, id, machineId) => {
    logger.debug(`Request to get configuration of machine with id ${machineId}.`);
    try {
      socket.emit('machine_configuration_get', id, await requests.getConfiguration(machineId));
    } catch (err) {
      socket.emit('machine_configuration_get', id, { error: err });
    }
  });

  addListener('machine_configuration_set', async (socket, id, machineId, configuration) => {
    logger.debug(`Request to set configuration of machine with id ${machineId}.`);
    try {
      socket.emit(
        'machine_configuration_set',
        id,
        await requests.sendConfiguration(machineId, configuration)
      );
    } catch (err) {
      socket.emit('machine_configuration_set', id, { error: err });
    }
  });

  addListener('machine_logs_get', async (socket, id, machineId) => {
    logger.debug(`Request to get logs of machine with id ${machineId}.`);
    try {
      socket.emit('machine_logs_get', id, await requests.getLogs(machineId));
    } catch (err) {
      socket.emit('machine_logs_get', id, { error: err });
    }
  });
}
