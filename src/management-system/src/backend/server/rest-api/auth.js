import express from 'express';
import keycloak from '../identity-management/keycloak/keycloak.js';
import ports from '../../../../ports.js';

// keycloak.protect() is middleware that checks, if the user is authenticated at the keycloak authorization server before executing controller functions
// if the checks fail, the middleware returns 403 - Forbidden
// More information: https://www.keycloak.org/docs/latest/securing_apps/#protecting-resources
const authRouter = express.Router();

// GET

authRouter.get('/login', keycloak.protect(), async (req, res) => {
  const tempSession = req.session;
  req.session.regenerate((err) => {
    Object.assign(req.session, tempSession);
    if (err) {
      req.session.destroy();
    }
    res.redirect(
      process.env.NODE_ENV === 'production'
        ? `${req.protocol}://${req.header('host')}`
        : `${req.protocol}://${req.hostname}:${ports['dev-server'].frontend}`
    );
  });
});

authRouter.get('/register', keycloak.protect(), async (req, res) => {
  const tempSession = req.session;
  req.session.regenerate((err) => {
    Object.assign(req.session, tempSession);
    if (err) {
      req.session.destroy();
    }
    res.redirect(
      process.env.NODE_ENV === 'production'
        ? `${req.protocol}://${req.header('host')}`
        : `${req.protocol}://${req.hostname}:${ports['dev-server'].frontend}`
    );
  });
});

export default authRouter;
