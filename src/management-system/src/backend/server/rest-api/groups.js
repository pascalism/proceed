import express from 'express';
import keycloak from '../identity-management/keycloak/keycloak.js';
import KeycloakAdminClient from '../identity-management/keycloak/keycloak-admin.js';

const keycloakAdminClient = new KeycloakAdminClient();

// one group equals to one department or one working group
// keycloak.enforcer('{Resource}:{scope}') uses keycloak authz module to grant or deny user access to resource based on the user's permissions
// example: keycloak.enforcer('Group:view') -> grants the requesting party access to resource Group if he has view rights
// if the checks fail, the middleware returns 403 - Forbidden
// More information: https://www.keycloak.org/docs/latest/securing_apps/#protecting-resources
const groupsRouter = express.Router();

// GET

/**
 * returns all groups from keycloak including all subgroups
 */
groupsRouter.get('/', keycloak.enforcer('Group:view'), async (req, res) => {
  try {
    const groups = await keycloakAdminClient.getAllGroups();
    if (groups.length === 0) {
      return res.status(204).json([]);
    }
    return res.status(200).json(groups);
  } catch (e) {
    console.log(e);
    return res.status(400).json("Couldn't get groups");
  }
});

/**
 * returns all users that are in the same group
 *
 * @param {String} id - the id of the group from keycloak as a path parameter
 */
groupsRouter.get('/:id/user', keycloak.enforcer('Group:view'), async (req, res) => {
  const { id } = req.params;
  if (id) {
    try {
      const users = await keycloakAdminClient.getUsersInGroup(id);
      if (users.length === 0) {
        return res.status(204).json([]);
      }
      return res.status(200).json(users);
    } catch (e) {
      console.log(e);
      return res.status(400).json("Couldn't get users from group");
    }
  } else {
    return res.status(400).json('Missing parameter id!');
  }
});

/**
 * returns the detailled group information of a group from keycloak
 *
 * @param {String} id - the id of the group from keycloak as a path parameter
 */
groupsRouter.get('/:id', keycloak.enforcer('Group:view'), async (req, res) => {
  const { id } = req.params;
  if (id) {
    try {
      const group = await keycloakAdminClient.getGroupById(id);
      if (group.length === 0) {
        return res.status(204).json([]);
      }
      return res.status(200).json(group);
    } catch (e) {
      console.log(e);
      return res.status(404).json("Couldn't get group");
    }
  } else {
    return res.status(400).json('Missing parameter id!');
  }
});

// POST

/**
 * adds a group to keycloak
 *
 * @param {String} groupName - the name of the group from keycloak in the body
 */
groupsRouter.post('/', keycloak.enforcer('Group:edit'), async (req, res) => {
  const { groupName } = req.body;
  if (groupName) {
    try {
      const groupId = await keycloakAdminClient.addGroup(groupName);
      if (groupId) {
        return res.status(201).json(groupId);
      } else {
        return res.status(400).json('Error adding group');
      }
    } catch (e) {
      console.log(e);
      return res.status(400).json("Couldn't add group");
    }
  } else {
    return res.status(400).json('Missing parameter groupName!');
  }
});

/**
 * adds a childgroup to an existing group in keycloak
 *
 * @param {String} groupName - the name of the group from keycloak in the body
 * @param {String} id - the id of the existing group from keycloak in the path params
 * @param {Object} data - attributes of the group
 */
groupsRouter.post('/:id/child', keycloak.enforcer('Group:edit'), async (req, res) => {
  const { groupName, data } = req.body;
  const { id } = req.params;
  if (id && groupName && data) {
    try {
      const childGroup = await keycloakAdminClient.addChildToGroup(id, groupName, data.attributes);
      return res.status(201).json(childGroup);
    } catch (e) {
      console.log(e);
      return res.status(400).json("Couldn't add child to group");
    }
  } else {
    return res.status(400).json('Missing parameter id or groupName or data!');
  }
});

// PUT

/**
 * updates an existing group in keycloak
 *
 * @param {String} id - the id of the existing group from keycloak in the path params
 * @param {Object} data - attributes of the group
 */
groupsRouter.put('/:id', keycloak.enforcer('Group:edit'), async (req, res) => {
  const { data } = req.body;
  const { id } = req.params;
  if (id && data) {
    try {
      await keycloakAdminClient.updateGroup(id, data);
    } catch (e) {
      console.log(e);
      return res.status(400).json("Couldn't update group");
    }
  } else {
    return res.status(400).json('Missing parameter id or data!');
  }
});

/**
 * adds a user to the specified group
 *
 * @param {String} groupId - the id of the group from keycloak in the path parameter
 * @param {String} userId - the id of the user from keycloak in the path parameter
 */
groupsRouter.put('/:groupId/user/:userId', keycloak.enforcer('Group:edit'), async (req, res) => {
  const { groupId, userId } = req.params;
  if (groupId && userId) {
    try {
      await keycloakAdminClient.addUserToGroup(groupId, userId);
      return res.status(200).json({ groupId, userId });
    } catch (e) {
      return res.status(400).json(e);
    }
  } else {
    return res.status(400).json('Missing parameter groupId and userId!');
  }
});

// DELETE

/**
 * removes a user from a specified group from keycloak
 *
 * @param {String} userId - the id of the user from keycloak as path parameter
 * @param {String} groupId - the id of the group from keycloak as path parameter
 */
groupsRouter.delete('/:groupId/user/:userId', keycloak.enforcer('Group:edit'), async (req, res) => {
  const { groupId, userId } = req.params;
  if (userId && groupId) {
    try {
      await keycloakAdminClient.removeUserFromGroup(userId, groupId);
      return res.status(200).json({ userId, groupId });
    } catch (e) {
      return res.status(400).json(e);
    }
  } else {
    return res.status(400).json('Missing parameter userId and groupId!');
  }
});

/**
 * deletes an existing group from keycloak
 *
 * @param {String} id - the id of the existing group from keycloak in the path param
 */
groupsRouter.delete('/:id', keycloak.enforcer('Group:delete'), async (req, res) => {
  const { id } = req.params;
  if (id) {
    try {
      const groups = await keycloakAdminClient.getAllGroups();
      if (groups.findIndex((val) => val.id === id) === 0) {
        return res.status(400).json('Cannot delete root group!');
      } else {
        await keycloakAdminClient.deleteGroupById(id);
        return res.status(200).json(id);
      }
    } catch (e) {
      console.log(e);
      return res.status(400).json("Couldn't delete group");
    }
  } else {
    return res.status(400).json('Missing parameter groupId!');
  }
});

export default groupsRouter;
