import express from 'express';
import keycloak from '../identity-management/keycloak/keycloak.js';
import axios from 'axios';

const healthRouter = express.Router();

/**
 * checks the health of the keycloak authorization server to adjust the PROCEED MS UI (show or hide login/logout component)
 */
healthRouter.get('/', async (req, res) => {
  try {
    if (keycloak.keycloakPerRealm[keycloak.getRealmName]) {
      const health = await axios.get(
        `${keycloak.keycloakPerRealm[keycloak.getRealmName].config.realmUrl}`
      );
      if (health) {
        return res.status(200).json('Authorization Server is running');
      }
      return res.status(500).json('Authorization Server is not reachable');
    }
    return res.status(500).json('Something went wrong with the keycloak configuration file!');
  } catch (e) {
    return res.status(500).json('Authorization Server not reachable');
  }
});

export default healthRouter;
