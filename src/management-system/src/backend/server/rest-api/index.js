import express from 'express';
import processRouter from './process.js';
import machinesRouter from './machine.js';
import speechAssistantRouter from './speechAssistant.js';
import authRouter from './auth.js';
import userRouter from './user.js';
import rolesRouter from './roles.js';
import groupsRouter from './groups.js';
import umaPoliciesRouter from './umaPolicies.js';
import umaResourcesRouter from './umaResources.js';
import healthRouter from './health.js';

// middleware for all routes to refactor the code

const apiRouter = express.Router();

apiRouter.use('/process', processRouter);
apiRouter.use('/machines', machinesRouter);
apiRouter.use('/speech', speechAssistantRouter);
// identity and access management routes
apiRouter.use('/auth', authRouter);
apiRouter.use('/users', userRouter);
apiRouter.use('/roles', rolesRouter);
apiRouter.use('/groups', groupsRouter);
apiRouter.use('/policies', umaPoliciesRouter);
apiRouter.use('/resources', umaResourcesRouter);
apiRouter.use('/health', healthRouter);

export default apiRouter;
