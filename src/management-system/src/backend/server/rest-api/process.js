import {
  getProcesses,
  getProcessBpmn,
  addProcess,
  updateProcess,
  removeProcess,
  getProcessUserTasks,
  getProcessUserTaskHtml,
  getProcessUserTasksHtml,
  saveProcessUserTask,
  deleteProcessUserTask,
} from '../../shared-electron-server/data/process.js';
import express from 'express';

const processRouter = express.Router();

function toExternalFormat(processMetaData) {
  const newFormat = { ...processMetaData };
  newFormat.definitionId = processMetaData.id;
  newFormat.definitionName = processMetaData.name;
  delete newFormat.id;
  delete newFormat.name;
  return newFormat;
}

processRouter.use('/', async (req, res, next) => {
  // TODO: maybe filter out all processes that the requester doesn't have access to here
  req.processes = getProcesses().map((process) => toExternalFormat(process));
  next();
});

processRouter.get('/', async (req, res) => {
  let { noBpmn } = req.query;

  try {
    const processes = await Promise.all(
      req.processes.map(async (process) => {
        if (noBpmn === 'true') {
          return process;
        } else {
          const bpmn = await getProcessBpmn(process.definitionId);
          return { ...process, bpmn };
        }
      })
    );

    res.status(200).json(processes);
  } catch (err) {
    res.status(500).send('Failed to get process info');
  }
});

processRouter.post('/', async (req, res) => {
  // TODO: wiki says to do post on /:definition-id endpoint, is that what we want?
  const { body } = req;

  if (typeof body !== 'object') {
    res.status(400).send('Expected { bpmn: ... } as body!');
    return;
  }

  const { bpmn } = body;

  try {
    const process = await addProcess(body);
    res.status(201).json(toExternalFormat({ ...process, bpmn }));
  } catch (err) {
    res.status(400).send(err.message);
  }
});

processRouter.use('/:definitionId', async (req, res, next) => {
  req.definitionsId = req.params.definitionId;
  // use processes from earlier middleware to allow filtering in one place
  req.process = req.processes.find((p) => p.definitionId === req.definitionsId);
  next();
});

processRouter.get('/:definitionId', async (req, res) => {
  const { process, definitionsId } = req;

  if (!process) {
    res.status(404).send(`Process with id ${definitionsId} could not be found!`);
    return;
  }

  try {
    const bpmn = await getProcessBpmn(definitionsId);
    // TODO: maybe add user task info here
    res.status(200).json({ ...process, bpmn });
  } catch (err) {
    res.status(400).send(err.message);
  }
});

processRouter.put('/:definitionId', async (req, res) => {
  const { process, definitionsId, body } = req;

  if (!process) {
    res.status(404).send('Process to update does not exist!');
    return;
  }

  if (typeof body !== 'object') {
    res.status(400).send('Expected { bpmn: ... } as body!');
    return;
  }

  let { bpmn } = body;

  try {
    const newProcessInfo = await updateProcess(definitionsId, body);
    bpmn = bpmn || (await getProcessBpmn(definitionsId));
    res.status(200).json(toExternalFormat({ ...newProcessInfo, bpmn }));
  } catch (err) {
    res.status(400).send(err.message);
  }
});

processRouter.delete('/:definitionId', async (req, res) => {
  const { process, definitionsId } = req;

  if (process) {
    await removeProcess(definitionsId);
  }

  res.status(200).end();
});

processRouter.use('/:definitionId/user-tasks', async (req, res, next) => {
  // early exit if the process doesn't exist
  if (!req.process) {
    res.status(404).send(`Process with id ${req.definitionsId} does not exist!`);
  } else {
    next();
  }
});

processRouter.get('/:definitionId/user-tasks', async (req, res) => {
  let { withHtml } = req.query;

  if (withHtml === 'true') {
    res.status(200).json(await getProcessUserTasksHtml(req.definitionsId));
  } else {
    res.status(200).json(await getProcessUserTasks(req.definitionsId));
  }
});

processRouter.use('/:definitionId/user-tasks/:userTaskFileName', async (req, res, next) => {
  req.userTaskFileName = req.params.userTaskFileName;
  try {
    // see if there already exists some data for this user task and make it accessible for later steps
    req.userTaskHtml = await getProcessUserTaskHtml(req.definitionsId, req.userTaskFileName);
  } catch (err) {}
  next();
});

processRouter.get('/:definitionId/user-tasks/:userTaskFileName', async (req, res) => {
  const { userTaskFileName } = req;

  if (req.userTaskHtml) {
    res.status(200).send(req.userTaskHtml);
  } else {
    res.status(404).send(`Found no html for user task filename ${userTaskFileName}`);
  }
});

processRouter.put('/:definitionId/user-tasks/:userTaskFileName', async (req, res) => {
  const { definitionsId, userTaskFileName, userTaskHtml } = req;
  const { body } = req;

  await saveProcessUserTask(definitionsId, userTaskFileName, body);

  if (!userTaskHtml) {
    res.status(201);
  } else {
    res.status(200);
  }

  res.end();
});

processRouter.delete('/:definitionId/user-tasks/:userTaskFileName', async (req, res) => {
  const { definitionsId, userTaskFileName, userTaskHtml } = req;

  if (userTaskHtml) {
    await deleteProcessUserTask(definitionsId, userTaskFileName);
  }

  res.status(200).send();
});

export default processRouter;
