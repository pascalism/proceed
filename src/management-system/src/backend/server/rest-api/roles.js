import express from 'express';
import keycloak from '../identity-management/keycloak/keycloak.js';
import KeycloakAdminClient from '../identity-management/keycloak/keycloak-admin.js';

const keycloakAdminClient = new KeycloakAdminClient();

// one role represents an occupational qualification of a user and provides the user with permissions to access views and functionalities and to manage resources with specific levels of permissions such as view, edit and delete
// keycloak.enforcer('{Resource}:{scope}') uses keycloak authz module to grant or deny user access to resource based on the user's permissions
// example: keycloak.enforcer('Role:view') -> grants the requesting party access to resource Role if he has view rights
// if the checks fail, the middleware returns 403 - Forbidden
// More information: https://www.keycloak.org/docs/latest/securing_apps/#protecting-resources
const rolesRouter = express.Router();

// GET

/**
 * returns the detailled role information of a role from keycloak
 *
 * @param {String} id - the id of the role from keycloak as a path param
 */
rolesRouter.get('/:id', keycloak.enforcer('Role:view'), async (req, res) => {
  const { id } = req.params;
  if (id) {
    try {
      const role = await keycloakAdminClient.getRoleById(id);
      return res.status(200).json(role);
    } catch (e) {
      console.log(e);
      return res.status(400).json("Couldn't get role");
    }
  } else {
    return res.status(400).json('Missing parameter id!');
  }
});

/**
 * returns all roles from keycloak or only one role by name
 */
rolesRouter.get('/', keycloak.enforcer('Role:view'), async (req, res) => {
  const { roleName } = req.query;
  try {
    if (roleName) {
      const role = await keycloakAdminClient.getRoleByName(roleName);
      if (role) return res.status(200).json(role);
    } else {
      const roles = await keycloakAdminClient.getRoles();
      if (roles) return res.status(200).json(roles);
    }
  } catch (e) {
    console.log(e);
    return res.status(400).json("Couldn't get role");
  }
});

// POST

/**
 * adds a role to keycloak
 *
 * @param {String} roleName - the name of the role from keycloak in the body
 */
rolesRouter.post('/', keycloak.enforcer('Role:edit'), async (req, res) => {
  const { roleName } = req.body;
  if (roleName) {
    try {
      await keycloakAdminClient.addRole(roleName);
      return res.status(201).json(roleName);
    } catch (e) {
      console.log(e);
      return res.status(400).json("Couldn't add role");
    }
  } else {
    return res.status(400).json('Missing parameter roleName!');
  }
});

// PUT

/**
 * updates an existing role in keycloak
 *
 * @param {String} id - the id of the existing role from keycloak in the path params
 * @param {Object} data - attributes of the role in the body
 */
rolesRouter.put('/:id', keycloak.enforcer('Role:edit'), async (req, res) => {
  const { data } = req.body;
  const { id } = req.params;
  if (id && data) {
    try {
      await keycloakAdminClient.updateRoleById(id, data);
      return res.status(200).json({ id });
    } catch (e) {
      console.log(e);
      return res.status(400).json("Couldn't update role");
    }
  } else {
    return res.status(400).json('Missing parameter id or data!');
  }
});

// DELETE

/**
 * deletes an existing role from keycloak
 *
 * @param {String} id - the id of the existing role from keycloak in the path params
 */
rolesRouter.delete('/:id', keycloak.enforcer('Role:delete'), async (req, res) => {
  const { id } = req.params;
  if (id) {
    try {
      await keycloakAdminClient.deleteRoleById(id);
      return res.status(200).json(id);
    } catch (e) {
      console.log(e);
      return res.status(400).json("Couldn't delete role");
    }
  } else {
    return res.status(400).json('Missing parameter id!');
  }
});

export default rolesRouter;
