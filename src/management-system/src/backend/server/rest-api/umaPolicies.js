import express from 'express';
import keycloak from '../identity-management/keycloak/keycloak.js';
import KeycloakAdminClient from '../identity-management/keycloak/keycloak-admin.js';

const keycloakAdminClient = new KeycloakAdminClient();

// UMA policies are enforced on protected resources when the Authorization Server makes authorization decisions when a user wants to access protected resources
// keycloak.enforcer('{Resource}:{scope}') uses keycloak authz module to grant or deny user access to resource based on the user's permissions
// if the checks fail, the middleware returns 403 - Forbidden
// More information: https://www.keycloak.org/docs/latest/securing_apps/#protecting-resources
const umaPoliciesRouter = express.Router();

// GET

/**
 * returns a user managed policy for a resource from keycloak
 *
 * @param {String} resourceId - the id of the resource as a path param
 */
umaPoliciesRouter.get('/:resourceId', keycloak.enforcer('Role:view'), async (req, res) => {
  const { resourceId } = req.params;
  if (resourceId) {
    try {
      const policies = await keycloakAdminClient.getPoliciesByResourceId(resourceId);
      return res.status(200).json(policies);
    } catch (e) {
      return res.status(400).json('Error fetching policies');
    }
  } else {
    return res.status(400).json('Missing parameter resourceId in query params!');
  }
});

/**
 * returns a user managed policy from keycloak by name
 *
 * @param {String} name - the name of the policy as a query parameter
 */
umaPoliciesRouter.get('/', keycloak.enforcer('Role:view'), async (req, res) => {
  const { name } = req.query;
  if (name) {
    try {
      const policies = await keycloakAdminClient.getPoliciesByEntityName(name);
      return res.status(200).json(policies);
    } catch (e) {
      return res.status(400).json('Error fetching policies');
    }
  } else {
    return res.status(400).json('Missing parameter entityName in query!');
  }
});

// POST

/**
 * adds a user managed policy for a resource
 *
 * @param {String} resourceName - the name of the resource
 * @param {String} scopes - the scopes for the policy
 * @param {Object} role - name of role that should get the policy
 * @param {String} resourceId - id of the resource
 */
umaPoliciesRouter.post('/resource', keycloak.enforcer('Role:edit'), async (req, res) => {
  const { resourceName, scopes, role, resourceId } = req.body;
  if (resourceName && scopes && role && resourceId) {
    try {
      const policy = await keycloakAdminClient.addResourcePolicyToResource(
        resourceName,
        scopes,
        role,
        resourceId
      );
      return res.status(201).json(policy);
    } catch (e) {
      return res.status(400).json('Error creating policy');
    }
  } else {
    return res
      .status(400)
      .json('Missing parameters resourceName, scopes, role, resourceId in body!');
  }
});

/**
 * adds a user managed policy for a view resource
 *
 * @param {String} viewName - the name of the view resource from keycloak
 * @param {Object} role - name of role that should get the policy
 * @param {String} resourceId - id of the resource
 */
umaPoliciesRouter.post('/view', keycloak.enforcer('Role:edit'), async (req, res) => {
  const { viewName, role, resourceId } = req.body;
  if (viewName && role && resourceId) {
    try {
      const policy = await keycloakAdminClient.addViewPolicyToResource(viewName, role, resourceId);
      return res.status(201).json(policy);
    } catch (e) {
      return res.status(400).json('Error creating policy');
    }
  } else {
    return res.status(400).json('Missing parameters viewName, role, resourceId in body!');
  }
});

// PUT

/**
 * updates a user managed policy
 *
 * @param {Object} policy - the entire policy
 */
umaPoliciesRouter.put('/', keycloak.enforcer('Role:edit'), async (req, res) => {
  const { policy } = req.body;
  if (policy) {
    try {
      await keycloakAdminClient.updatePolicy(policy);
      return res.status(200).json(policy);
    } catch (e) {
      return res.status(400).json('Error updating policy');
    }
  } else {
    return res.status(400).json('Missing parameter policy in body!');
  }
});

// DELETE

/**
 * deletes a user managed policy
 *
 * @param {String} id - the id of a user managed policy
 */
umaPoliciesRouter.delete('/:id', keycloak.enforcer('Role:edit'), async (req, res) => {
  const { id } = req.params;
  if (id) {
    try {
      await keycloakAdminClient.deletePolicy(id);
      return res.status(200).json(id);
    } catch (e) {
      return res.status(400).json('Error deleting policy');
    }
  } else {
    return res.status(400).json('Missing parameter policyId in body!');
  }
});

export default umaPoliciesRouter;
