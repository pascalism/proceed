import express from 'express';
import keycloak from '../identity-management/keycloak/keycloak.js';
import KeycloakAdminClient from '../identity-management/keycloak/keycloak-admin.js';

const keycloakAdminClient = new KeycloakAdminClient();

// UMA resources are abstractions or representations of the actual resource that has to be protected, they are used to get combined with authorization policies for making authorization decisions
// keycloak.enforcer('{Resource}:{scope}') uses keycloak authz module to grant or deny user access to resource based on the user's permissions
// if the checks fail, the middleware returns 403 - Forbidden
// More information: https://www.keycloak.org/docs/latest/securing_apps/#protecting-resources
const umaResourcesRouter = express.Router();

// GET

/**
 * returns a user managed resource by type or name
 *
 * @param {String} type - the type of a user managed resource as query parameter
 * @param {String} name - the name of a user managed resource as query parameter
 */
umaResourcesRouter.get('/', keycloak.enforcer('Process:share'), async (req, res) => {
  const { type, name } = req.query;
  if (type || name) {
    try {
      const resources = await keycloakAdminClient.getUmaResourceByTypeOrName(type, name);
      if (resources.length === 0) {
        return res.status(204).json([]);
      }
      return res.status(200).json(resources);
    } catch (e) {
      return res.status(400).json('Error fetching resource');
    }
  } else {
    return res.status(400).json('Missing parameter type or name in query!');
  }
});

/**
 * returns a user managed resource by id
 *
 * @param {String} id - the id of a user managed resource
 */
umaResourcesRouter.get('/:id', keycloak.enforcer('Role:view'), async (req, res) => {
  const { id } = req.params;
  if (id) {
    try {
      const resource = await keycloakAdminClient.getUmaResourceById(id);
      if (resource.length === 0) {
        return res.status(204).json([]);
      }
      return res.status(200).json(resource);
    } catch (e) {
      return res.status(400).json('Error fetching resource');
    }
  } else {
    return res.status(400).json('Missing parameter id in query!');
  }
});

export default umaResourcesRouter;
