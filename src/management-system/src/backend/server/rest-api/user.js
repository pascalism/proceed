import express from 'express';
import keycloak from '../identity-management/keycloak/keycloak.js';
import axios from 'axios';
import jwt from 'jsonwebtoken';
import crypto from 'crypto';
import KeycloakAdminClient from '../identity-management/keycloak/keycloak-admin.js';
import {
  buildRptTokenOptions,
  buildRefreshTokenOptions,
  buildPermissions,
} from '../identity-management/helper-functions/user-helper.js';

const keycloakAdminClient = new KeycloakAdminClient();

// a user is the end user who is operating the PROCEED MS
// keycloak.protect() is middleware that checks, if the user is authenticated at the keycloak authorization server before executing controller functions
// keycloak.enforcer('{Resource}:{scope}') uses keycloak authz module to grant or deny user access to resource based on the user's permissions
// example: keycloak.enforcer('User:view') -> grants the requesting party access to resource User if he has view rights
// if the checks fail, the middleware returns 403 - Forbidden
// More information: https://www.keycloak.org/docs/latest/securing_apps/#protecting-resources
const userRouter = express.Router();

// GET

/**
 * uses the OpenID Connect /userinfo endpoint to receive information about the authenticated user and additionally fetches all groups and roles from the user
 * can be executed from the user itself using it's own access token (req.kauth.grant.access_token.token) that is stored server-side in the user session
 */
userRouter.get('/user/userinfo', keycloak.protect(), async (req, res) => {
  try {
    const userinfoData = await keycloak.keycloakPerRealm[
      keycloak.getRealmName
    ].grantManager.userInfo(req.kauth.grant.access_token.token);
    //const roles = await keycloakAdminClient.getRolesFromUser(userinfoData.sub);
    const groups = await keycloakAdminClient.getGroupsFromUser(userinfoData.sub);
    return res.status(200).json({ ...userinfoData, groups });
  } catch (e) {
    console.log(e);
    return res.status(400).json("Couldn't get userinfo");
  }
});

/**
 * gets an rpt token and returns the permissions that are build with the function buildPermissions() to the frontend packed into a small package for quick transfer
 */
userRouter.get('/user/permissions', keycloak.protect(), async (req, res) => {
  let rptToken = null;
  let accessToken = null;
  try {
    // build new access token
    accessToken = await axios(buildRefreshTokenOptions(req.kauth.grant.refresh_token.token));
    if (accessToken.data) {
      try {
        // get rpt token
        const response = await axios(buildRptTokenOptions(accessToken.data.access_token));
        rptToken = jwt.decode(response.data.access_token);
      } catch (err) {
        console.log(err);
      }
    }
  } catch (err) {
    try {
      // use existing access token if new access token couldn't be build to get rpt token
      const response = await axios(buildRptTokenOptions(req.kauth.grant.access_token.token));
      rptToken = jwt.decode(response.data.access_token);
    } catch (err) {
      console.log(err);
      return res.status(400).json('Error creating permissions');
    }
  }
  if (rptToken) {
    // build permissions
    const rules = buildPermissions(rptToken);
    if (rules) {
      req.session.permissions = rules;
      return res
        .status(200)
        .json(jwt.sign({ token: rules }, crypto.randomBytes(64).toString('hex')));
    } else {
      return res.status(400).json('Error creating permissions');
    }
  } else {
    return res.status(400).json('Error creating permissions');
  }
});

/**
 * returns all users from keycloak authorization server
 */
userRouter.get('/', keycloak.enforcer('User:view'), async (req, res) => {
  try {
    const users = await keycloakAdminClient.getAllUsers();
    if (users.length === 0) {
      return res.status(204).json([]);
    }
    return res.status(200).json(users);
  } catch (e) {
    console.log(e);
    return res.status(400).json("Couldn't get all users");
  }
});

/**
 * returns the detailled user representation from keycloak
 *
 * @param {String} id - the id of the user from keycloak as query param
 */
userRouter.get('/:id', keycloak.enforcer('User:view'), async (req, res) => {
  const { id } = req.params;
  if (id) {
    try {
      const user = await keycloakAdminClient.getUserById(id);
      const roles = await keycloakAdminClient.getRolesFromUser(user.id);
      const groups = await keycloakAdminClient.getGroupsFromUser(user.id);
      return res.status(200).json({ ...user, roles, groups });
    } catch (e) {
      console.log(e);
      return res.status(400).json("Couldn't get user");
    }
  } else {
    return res.status(400).json('Missing parameter id!');
  }
});

/**
 * returns the roles from a user
 *
 * @param {String} id - the id of the user from keycloak as query param
 */
userRouter.get('/:id/roles', keycloak.enforcer('User:view'), async (req, res) => {
  const { id } = req.params;
  if (id) {
    try {
      const roles = await keycloakAdminClient.getRolesFromUser(id);
      if (roles.length === 0) {
        return res.status(204).json([]);
      }
      return res.status(200).json(roles);
    } catch (e) {
      console.log(e);
      return res.status(400).json("Couldn't get roles from user");
    }
  } else {
    return res.status(400).json('Missing parameter id!');
  }
});

/**
 * returns the groups from a user
 *
 * @param {String} id - the id of the user from keycloak as query param
 */
userRouter.get('/:id/groups', keycloak.enforcer('User:view'), async (req, res) => {
  const { id } = req.params;
  if (id) {
    try {
      const groups = await keycloakAdminClient.getGroupsFromUser(id);
      if (groups.length === 0) {
        return res.status(204).json([]);
      }
      return res.status(200).json(groups);
    } catch (e) {
      console.log(e);
      return res.status(400).json("Couldn't get groups from user");
    }
  } else {
    return res.status(400).json('Missing parameter id!');
  }
});

/**
 * returns the users that have a specified role
 *
 * @param {String} name - the name of the role from keycloak as query param
 */
userRouter.get('/:role/users', keycloak.enforcer('User:view'), async (req, res) => {
  const { role } = req.params;
  if (role) {
    try {
      const users = await keycloakAdminClient.getUsersFromRole(role);
      if (users.length === 0) {
        return res.status(204).json([]);
      }
      return res.status(200).json(users);
    } catch (e) {
      console.log(e);
      return res.status(400).json("Couldn't get users from role");
    }
  } else {
    return res.status(400).json('Missing parameter name!');
  }
});

// POST

/**
 * adds a role to a specified user
 *
 * @param {String} id - the id of the user from keycloak in the query param
 * @param {Object} data - contains role for the user in the body
 */
userRouter.post('/:id/roles', keycloak.enforcer('User:edit'), async (req, res) => {
  const { data } = req.body;
  const { id } = req.params;
  if (id && data) {
    try {
      await keycloakAdminClient.addRoleForUser(id, data);
      return res.status(201).json({ id, data });
    } catch (e) {
      console.log(e);
      return res.status(400).json("Couldn't add role to user");
    }
  } else {
    return res.status(400).json('Missing parameter id or data!');
  }
});

/**
 * adds a new user to the realm
 *
 * @param {Object} data - representation of a user in the body
 */
userRouter.post('/', keycloak.enforcer('User:edit'), async (req, res) => {
  const { data } = req.body;
  if (data) {
    try {
      const id = await keycloakAdminClient.addUser(data);
      if (id) {
        try {
          await keycloakAdminClient.resetPassword(id.id, data.username);
          return res.status(201).json(id);
        } catch (e) {
          console.log(e);
          return res.status(400).json('Error setting temporary password!');
        }
      }
    } catch (e) {
      console.log(e);
      return res.status(400).json("Couldn't add user");
    }
  } else {
    return res.status(400).json('Missing parameter data!');
  }
});

// PUT

/**
 * updates a specified user from keycloak
 *
 * @param {String} id - the id of the user from keycloak in the query param
 * @param {Object} data - representation of a user from keycloak in the body
 */
userRouter.put('/:id', keycloak.enforcer('User:edit'), async (req, res) => {
  const { data } = req.body;
  const { id } = req.params;
  if (id && data) {
    try {
      await keycloakAdminClient.updateUser(id, data);
      return res.status(200).json({ id });
    } catch (e) {
      console.log(e);
      return res.status(400).json("Couldn't update user");
    }
  } else {
    return res.status(400).json('Missing parameter id and data!');
  }
});

/**
 * removes a role of a specified user from keycloak
 *
 * @param {String} id - the id of the user from keycloak in the query param
 * @param {Object} data - Array of roles in the body
 */
userRouter.put('/:id/roles', keycloak.enforcer('User:edit'), async (req, res) => {
  const { data } = req.body;
  const { id } = req.params;
  if (id && data) {
    try {
      await keycloakAdminClient.deleteRoleFromUser(id, data);
      return res.status(200).json({ id, data });
    } catch (e) {
      console.log(e);
      return res.status(400).json("Couldn't delete role from user");
    }
  } else {
    return res.status(400).json('Missing parameter id and data!');
  }
});

// DELETE

/**
 * deletes a user from keycloak
 *
 * @param {String} id - the id of the user from keycloak in the body
 */
userRouter.delete('/:id', keycloak.enforcer('User:delete'), async (req, res) => {
  const { id } = req.params;
  if (id) {
    try {
      await keycloakAdminClient.deleteUser(id);
      return res.status(200).json({ id });
    } catch (e) {
      console.log(e);
      return res.status(400).json("Couldn't delete user");
    }
  } else {
    return res.status(400).json('Missing parameter id!');
  }
});

export default userRouter;
