export default {
  startEngineAtStartup: false,
  logLevel: 'info',
  machinePollingInterval: 10,
  closeOpenEditorsInMs: 300000,
};
