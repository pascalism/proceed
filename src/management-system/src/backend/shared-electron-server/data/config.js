import eventHandler from '../../../frontend/backend-api/event-system/EventHandler.js';
import defaultBackendConfig from '../config_backend_default.js';
import helperEx from '../../../shared-frontend-backend/helpers/helpers.js';
const { mergeIntoObject } = helperEx;
import store from './store.js';

// The merged config
let config = { ...defaultBackendConfig };

/**
 * Creates the initial config which is a merge of the user config and the default config
 *
 * @returns {Object} the initial config
 */
export function init() {
  config = { ...defaultBackendConfig };

  const backendConfig = store.get('config');

  // merge user config values that correspond to a default config key into the overall config
  mergeIntoObject(config, backendConfig, true, true, true);

  return config;
}

/**
 * Returns the current config values
 *
 * @returns {Object} the current config values
 */
export function getBackendConfig() {
  return config;
}

/**
 * Merges proposed config changes into the backend config object, updates the store and emits an event if at least one entry was changed
 *
 * @param {Object} newValues key value pairs of proposed config changes
 */
export function changeBackendConfig(newValues) {
  const changedValues = mergeIntoObject(config, newValues, true, true, true);

  // flag values that got set back to default for removal from user config file
  const cleanedValues = Object.entries(changedValues).reduce((acc, [key, value]) => {
    if (defaultBackendConfig[key] === value) {
      acc[key] = undefined;
    } else {
      acc[key] = value;
    }

    return acc;
  }, {});

  Object.entries(cleanedValues).forEach(([key, value]) => {
    store.set('config', key, value);
  });

  if (Object.keys(changedValues).length) {
    eventHandler.dispatch('backendConfigChanged', changedValues);
  }
}

init();
