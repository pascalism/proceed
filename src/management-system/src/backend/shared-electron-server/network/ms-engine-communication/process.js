import networkEx from '@proceed/system';
const { network } = networkEx;

/**
 * Requests information about all deployed processes of a machine
 *
 * @param {object} machine contains machine information
 * @param {string} machine.host the ip address of the machine
 * @param {number} machine.port the port of the machine
 * @returns {array} definitionIds of all processes deployed to the given machine
 */
export async function getDeployedProcesses(machine) {
  const { body } = await network.sendRequest(machine.host, machine.port, '/process');

  return JSON.parse(body);
}

/**
 * Requests process bpmn of process with given id from given machine
 *
 * @param {object} machine contains machine information
 * @param {string} machine.host the ip address of the machine
 * @param {number} machine.port the port of the machine
 * @param {string} definitionId name of the file the process is saved in
 * @returns {string} the process bpmn xml
 */
export async function getDeploymentBPMN(machine, definitionId) {
  const { body } = await network.sendRequest(
    machine.host,
    machine.port,
    `/process/${definitionId}`
  );

  return JSON.parse(body).bpmn;
}

/**
 * Sends process definitionid and process bpmn to deploy process on engine
 *
 * @param {object} machine contains machine information
 * @param {string} machine.host the ip address of the machine
 * @param {number} machine.port the port of the machine
 * @param {string} definitionId name of the file the process is saved in
 * @param {string} bpmn the description of the process in xml
 */
export async function deployProcess(machine, definitionId, bpmn) {
  await network.sendData(
    machine.host,
    machine.port,
    `/process/${definitionId}`,
    'PUT',
    'application/json',
    { bpmn }
  );
}

/**
 * Sends request to delete the deployment of process with given id
 *
 * @param {object} machine contains machine information
 * @param {string} machine.host the ip address of the machine
 * @param {number} machine.port the port of the machine
 * @param {string} definitionId name of the file the process is saved in
 */
export async function removeDeployment(machine, definitionId) {
  await network.sendRequest(machine.host, machine.port, `/process/${definitionId}/`, {
    method: 'DELETE',
  });
}

/**
 *
 * @param {object} machine contains machine information
 * @param {string} machine.host the ip address of the machine
 * @param {number} machine.port the port of the machine
 * @param {string} definitionId name of the file the process is saved in
 * @param {string} userTaskId id of the user task
 * @param {string} html the html of the user task
 */
export async function sendUserTaskHTML(machine, definitionId, userTaskId, html, imported) {
  await network.sendData(
    machine.host,
    machine.port,
    `/process/${definitionId}/${imported ? 'imported/' : ''}user-tasks/${userTaskId}`,
    'PUT',
    'application/json',
    { html }
  );
}

/**
 * Gets HTML for user task with given id in process with given id
 *
 * @param {object} machine contains machine information
 * @param {string} machine.host the ip address of the machine
 * @param {number} machine.port the port of the machine
 * @param {string} definitionId name of the file the process is saved in
 * @param {string} userTaskId id of the user task
 * @returns {string} the user task html
 */
export async function getUserTaskHTML(machine, definitionId, userTaskId, imported) {
  const { body } = await network.sendRequest(
    machine.host,
    machine.port,
    `/process/${definitionId}/${imported ? 'imported/' : ''}user-tasks/${userTaskId}`
  );

  return JSON.parse(body).html;
}

export async function sendImportedProcess(machine, definitionId, importedDefinitionId, bpmn) {
  network.sendData(
    machine.host,
    machine.port,
    `/process/${definitionId}/imported/${importedDefinitionId}`,
    'PUT',
    'application/json',
    { bpmn }
  );
}

/**
 * Requests information about all instances of process with given id
 *
 * @param {object} machine contains machine information
 * @param {string} machine.host the ip address of the machine
 * @param {number} machine.port the port of the machine
 * @param {string} definitionId name of the file the process is saved in
 * @return {array} array with information objects for each instance of the process
 */
export async function getProcessInstances(machine, definitionId) {
  const { body } = await network.sendRequest(
    machine.host,
    machine.port,
    `/process/${definitionId}/instance`
  );

  return JSON.parse(body);
}

/**
 * Sends request to start an instance of a process with process variables
 *
 * @param {object} machine contains machine information
 * @param {string} machine.host the ip address of the machine
 * @param {number} machine.port the port of the machine
 * @param {string} definitionId name of the file the process is saved in
 * @param {object} variables start values for process variables
 * @returns {string} the id of the created isntance
 */
export async function startProcessInstance(machine, definitionId, variables) {
  const { body } = await network.sendData(
    machine.host,
    machine.port,
    `/process/${definitionId}/instance`,
    'POST',
    'application/json',
    { variables }
  );

  return JSON.parse(body).instanceId;
}

/**
 * Request information about a certain process instance
 *
 * @param {object} machine contains machine information
 * @param {string} machine.host the ip address of the machine
 * @param {number} machine.port the port of the machine
 * @param {string} definitionId name of the file the process is saved in
 * @param {string} instanceId id of the specific instance
 * @returns {object} instance information object
 */
export async function getInstanceInformation(machine, definitionId, instanceId) {
  const { body } = await network.sendRequest(
    machine.host,
    machine.port,
    `/process/${definitionId}/instance/${instanceId}`
  );

  return JSON.parse(body);
}

/**
 * Sends request to stop a certain process instance
 *
 * @param {object} machine contains machine information
 * @param {string} machine.host the ip address of the machine
 * @param {number} machine.port the port of the machine
 * @param {string} definitionId name of the file the process is saved in
 * @param {string} instanceId id of the specific instance
 */
export async function stopProcessInstance(machine, definitionId, instanceId) {
  await network.sendData(
    machine.host,
    machine.port,
    `/process/${definitionId}/instance/${instanceId}/instanceState`,
    'PUT',
    'application/json',
    { instanceState: 'stopped' }
  );
}

/**
 * Sends request to pause a certain process instance
 *
 * @param {object} machine contains machine information
 * @param {string} machine.host the ip address of the machine
 * @param {number} machine.port the port of the machine
 * @param {string} definitionId name of the file the process is saved in
 * @param {string} instanceId id of the specific instance
 */
export async function pauseProcessInstance(machine, definitionId, instanceId) {
  await network.sendData(
    machine.host,
    machine.port,
    `/process/${definitionId}/instance/${instanceId}/instanceState`,
    'PUT',
    'application/json',
    { instanceState: 'paused' }
  );
}

/**
 * Sends request to resume a certain process instance
 *
 * @param {object} machine contains machine information
 * @param {string} machine.host the ip address of the machine
 * @param {number} machine.port the port of the machine
 * @param {string} definitionId name of the file the process is saved in
 * @param {string} instanceId id of the specific instance
 */
export async function resumeProcessInstance(machine, definitionId, instanceId) {
  await network.sendData(
    machine.host,
    machine.port,
    `/process/${definitionId}/instance/${instanceId}/instanceState`,
    'PUT',
    'application/json',
    { instanceState: 'resume' }
  );
}
