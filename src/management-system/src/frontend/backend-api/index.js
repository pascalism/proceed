import EngineInterface from './EngineInterface.js';
import DataInterface from './DataInterface.js';
import EngineNetworkInterface from './EngineNetworkInterface.js';
import ProcessInterface from './ProcessInterface.js';
import eventHandler from './event-system/EventHandler.js';

const engineInterface = new EngineInterface();

const dataInterface = new DataInterface();

const engineNetworkInterface = new EngineNetworkInterface();

const processInterface = new ProcessInterface();

export { engineInterface, eventHandler, dataInterface, engineNetworkInterface, processInterface };
