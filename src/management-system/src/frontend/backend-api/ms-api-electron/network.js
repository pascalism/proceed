import * as backendDeployment from '../../../backend/shared-electron-server/network/deployment.js';
import * as backendRequests from '../../../backend/shared-electron-server/network/requests.js';
import * as backendMachineInfo from '../../../backend/shared-electron-server/network/machines/machineInfo.js';

async function removeDeployment(processDefinitionsId) {
  await backendDeployment.removeDeployment(processDefinitionsId);
}

async function startInstance(processDefinitionsId) {
  await backendDeployment.startInstance(processDefinitionsId);
}

async function stopInstance(processDefinitionsId, instanceId) {
  await backendDeployment.stopInstance(processDefinitionsId, instanceId);
}

async function pauseInstance(processDefinitionsId, instanceId) {
  await backendDeployment.pauseInstance(processDefinitionsId, instanceId);
}

async function resumeInstance(processDefinitionsId, instanceId) {
  await backendDeployment.resumeInstance(processDefinitionsId, instanceId);
}

async function deployProcess(processDefinitionsId, dynamic) {
  await backendDeployment.deployProcess(processDefinitionsId, dynamic);
}

async function getStatus(machineId) {
  const running = backendRequests.getStatus(machineId);

  return running;
}

async function getInstanceInformation(machine, processDefinitionsId, instanceId) {
  const instanceInformation = await backendDeployment.getInstanceInformation(
    machine,
    processDefinitionsId,
    instanceId
  );

  return instanceInformation;
}

async function getMachineProperties(machineId, properties) {
  const machineProperties = await backendRequests.getMachineProperties(machineId, properties);

  return machineProperties;
}

async function sendConfiguration(machineId, configuration) {
  await backendRequests.sendConfiguration(machineId, configuration);
}

async function getConfiguration(machineId) {
  const configuration = await backendRequests.getConfiguration(machineId);

  return configuration;
}

async function getLogs(machineId) {
  const logs = await backendRequests.getLogs(machineId);

  return logs;
}

async function startMachinePolling() {
  backendMachineInfo.startPolling();
}

async function stopMachinePolling() {
  backendMachineInfo.stopPolling();
}

async function subscribeForDeploymentsUpdates() {
  backendMachineInfo.getDeployments();
}

async function unsubscribeFromDeploymentsUpdates() {
  backendMachineInfo.dontGetDeployments();
}

async function subscribeToMachine(machineId) {
  backendMachineInfo.addMachineSubscription(machineId);
}

async function unsubscribeFromMachine(machineId) {
  backendMachineInfo.removeMachineSubscription(machineId);
}

async function subscribeToMachineLogs(machineId) {
  backendMachineInfo.addMachineLogsSubscription(machineId);
}

async function unsubscribeFromMachineLogs(machineId) {
  backendMachineInfo.removeMachineLogsSubscription(machineId);
}

export default {
  removeDeployment,
  startInstance,
  stopInstance,
  pauseInstance,
  resumeInstance,
  deployProcess,
  getStatus,
  getInstanceInformation,
  getMachineProperties,
  sendConfiguration,
  getConfiguration,
  getLogs,
  startMachinePolling,
  stopMachinePolling,
  subscribeForDeploymentsUpdates,
  unsubscribeFromDeploymentsUpdates,
  subscribeToMachine,
  unsubscribeFromMachine,
  subscribeToMachineLogs,
  unsubscribeFromMachineLogs,
};
