import * as backendProcesses from '../../../backend/shared-electron-server/data/process.js';
import {
  getBPMN,
  updateProcess,
  saveScriptTaskJS,
} from '../../../backend/shared-electron-server/data/fileHandling.js';
import { setName, addDocumentation } from '@proceed/bpmn-helper';

async function getProcess(id) {
  const process = backendProcesses.getProcesses().find((p) => p.id === id);
  const bpmn = await backendProcesses.getProcessBpmn(id);
  return { ...process, bpmn };
}

async function updateProcessName(processDefinitionsId, newName) {
  let bpmn = await getBPMN(processDefinitionsId);
  bpmn = await setName(bpmn, newName);
  // don't use backendProcess.updateProcess to avoid having to parse the bpmn
  await updateProcess(processDefinitionsId, bpmn);
  await backendProcesses.updateProcessMetaData(processDefinitionsId, { name: newName });
}

async function updateProcessDescription(processDefinitionsId, processId, description) {
  let bpmn = await backendProcesses.getProcessBpmn(processDefinitionsId);
  bpmn = await addDocumentation(bpmn, description);
  await updateProcess(processDefinitionsId, bpmn);
  await backendProcesses.updateProcessMetaData(processDefinitionsId, { description });
}

export default {
  getProcesses: backendProcesses.getProcesses,
  getProcess,
  addProcess: backendProcesses.addProcess,
  updateProcess: backendProcesses.updateProcess,
  updateWholeXml: backendProcesses.updateProcess,
  updateProcessMetaData: backendProcesses.updateProcessMetaData,
  updateProcessName,
  updateProcessDescription,
  removeProcess: backendProcesses.removeProcess,
  observeProcessEditing: () => {},
  stopObservingProcessEditing: () => {},
  blockProcess: () => {},
  unblockProcess: () => {},
  blockTask: () => {},
  unblockTask: () => {},
  broadcastBPMNEvents: () => {},
  broadcastScriptChangeEvent: () => {},
  getUserTasksHTML: backendProcesses.getProcessUserTasksHtml,
  saveUserTaskHTML: backendProcesses.saveProcessUserTask,
  deleteUserTaskHTML: backendProcesses.deleteProcessUserTask,
  saveScriptTaskJS,
  updateConstraints: () => {},
};
