import { getProcessInfo } from '../../../shared-frontend-backend/helpers/processHelpers.js';
import { mergeIntoObject } from '../../../shared-frontend-backend/helpers/helpers.js';
import { setName, addDocumentation } from '@proceed/bpmn-helper';

/**
 * Returns all processes that are stored in the browser
 *
 * @returns {Object} an object containing all locally stored processes
 */
export function getProcesses() {
  let processes = localStorage.processes;

  if (!processes) {
    processes = {};
  } else {
    try {
      processes = JSON.parse(processes);
    } catch (err) {
      processes = {};
    }
  }

  return processes;
}

/**
 * Returns if a process with the given id is stored in the browser
 *
 * @param {String} processDefinitionsId
 * @returns {Boolean}
 */
export function hasProcess(processDefinitionsId) {
  if (localStorage.processes) {
    try {
      const processes = JSON.parse(localStorage.processes);

      if (processes[processDefinitionsId]) {
        return true;
      }
    } catch (err) {}
  }

  return false;
}

/**
 * Returns the locally stored process with the given id
 *
 * @param {String} processDefinitionsId
 * @param {Object} [processes] the list of objects to search (to avoid redundant parsing)
 * @returns {Object} an Object containing the processes information
 */
export function getProcess(processDefinitionsId, processes = getProcesses()) {
  if (!processes[processDefinitionsId]) {
    throw new Error('There is no local process with the given id!');
  }

  return processes[processDefinitionsId];
}

/**
 * Adds a new process to the browsers localStorage
 *
 * @param {String} processData the process definition and additional process info
 */
export async function addProcess(initData) {
  const { bpmn } = initData;
  const date = new Date().toUTCString();
  // create meta info object
  const processData = {
    createdOn: date,
    lastEdited: date,
    inEditingBy: [],
    departments: [],
    variables: [],
    bpmn,
    local: true,
    ...initData,
    ...(await getProcessInfo(bpmn)),
  };

  const processes = getProcesses();

  processes[processData.id] = { processData, html: {}, js: {} };

  localStorage.processes = JSON.stringify(processes);
}

/**
 * Removes a process from the localStorage
 *
 * @param {String} processDefinitionsId
 */
export function removeProcess(processDefinitionsId) {
  const processes = getProcesses();
  delete processes[processDefinitionsId];
  localStorage.processes = JSON.stringify(processes);
}

/**
 * Updates the process definition of a process in the local storage
 *
 * @param {String} processDefinitionsId
 * @param {String} bpmn the new process definition
 */
export function updateBPMN(processDefinitionsId, bpmn) {
  const processes = getProcesses();

  const process = getProcess(processDefinitionsId, processes);

  process.processData.bpmn = bpmn;

  localStorage.processes = JSON.stringify(processes);
}

/**
 * Updates the meta data about a process in the local storage
 *
 * @param {String} processDefinitionsId
 * @param {String} metaChanges contains the entries to change
 */
export function updateProcessMetaData(processDefinitionsId, metaChanges) {
  const processes = getProcesses();

  const process = getProcess(processDefinitionsId, processes);

  const processData = { ...process.processData, lastEdited: new Date().toUTCString() };

  mergeIntoObject(processData, metaChanges, true, true, true);

  process.processData = processData;

  localStorage.processes = JSON.stringify(processes);
}

/**
 * Changes the name of a process in its definition and meta data
 *
 * @param {String} processDefinitionsId
 * @param {String} newName
 */
export async function updateProcessName(processDefinitionsId, newName) {
  const processes = getProcesses();

  const process = getProcess(processDefinitionsId, processes);

  let { bpmn } = process.processData;

  bpmn = await setName(bpmn, newName);

  process.processData.bpmn = bpmn;
  process.processData.name = newName;

  localStorage.processes = JSON.stringify(processes);
}

/**
 * Updates a processes description in its definition and meta data
 *
 * @param {String} processDefinitionsId
 * @param {String} description
 */
export async function updateProcessDescription(processDefinitionsId, description) {
  const processes = getProcesses();

  const process = getProcess(processDefinitionsId, processes);

  let { bpmn } = process.processData;

  bpmn = await addDocumentation(bpmn, description);

  process.processData.bpmn = bpmn;
  process.processData.description = description;

  localStorage.processes = JSON.stringify(processes);
}

/**
 * Returns the user task data for all user tasks in a process
 *
 * @param {String} processDefinitionsId
 * @returns {Object} contains mapping from user task filename to user task data
 */
export async function getUserTasksHTML(processDefinitionsId) {
  return getProcess(processDefinitionsId).html;
}

/**
 * Saves user task data for a specific process
 *
 * @param {String} definitionsId
 * @param {String} taskFileName the tasks filename that would be used if it was stored on the server
 * @param {String} html the user task data
 */
export async function saveUserTaskHTML(definitionsId, taskFileName, html) {
  const processes = getProcesses();

  const process = getProcess(definitionsId, processes);

  process.html[taskFileName] = html;

  localStorage.processes = JSON.stringify(processes);
}

/**
 * Removes data for a specific user task
 *
 * @param {String} processDefinitionsId
 * @param {String} taskFileName
 */
export async function deleteUserTaskHTML(processDefinitionsId, taskFileName) {
  const processes = getProcesses();

  const process = getProcess(processDefinitionsId, processes);

  delete process.html[taskFileName];

  localStorage.processes = JSON.stringify(processes);
}

/**
 * Saves script task data for a specific task inside a process
 *
 * @param {String} processDefinitionsId
 * @param {String} taskId
 * @param {String} js
 */
export function saveScriptTaskJS(processDefinitionsId, taskId, js) {
  const processes = getProcesses();

  const process = getProcess(processDefinitionsId, processes);

  process.js[taskId] = js;

  localStorage.processes = JSON.stringify(processes);
}
