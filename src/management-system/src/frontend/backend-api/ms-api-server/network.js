import { listen, emit, request } from './socket.js';
import eventHandler from '@/backend-api/event-system/EventHandler.js';

async function removeDeployment(processDefinitionsId) {
  await request('deployment_remove', processDefinitionsId);
}

async function startInstance(processDefinitionsId) {
  const { error } = await request('instance_start', processDefinitionsId);

  if (error) {
    throw new Error(error);
  }
}

async function stopInstance(processDefinitionsId, instanceId) {
  await request('instance_stop', processDefinitionsId, instanceId);
}

async function pauseInstance(processDefinitionsId, instanceId) {
  await request('instance_pause', processDefinitionsId, instanceId);
}

async function resumeInstance(processDefinitionsId, instanceId) {
  await request('instance_resume', processDefinitionsId, instanceId);
}

async function deployProcess(processDefinitionsId, dynamic) {
  const { error } = await request('deployment_deploy', processDefinitionsId, dynamic);

  if (error) {
    throw new Error(error);
  }
}

async function getStatus(machineId) {
  const [running] = await request('machine_status_get', machineId);
  return running;
}

async function getInstanceInformation(machine, processDefinitionsId, instanceId) {
  const [instanceInformation] = await request(
    'instance_information_get',
    machine,
    processDefinitionsId,
    instanceId
  );
  return instanceInformation;
}

async function getMachineProperties(machineId, properties) {
  const [resProperties] = await request('machine_properties_get', machineId, properties);
  return resProperties;
}

async function sendConfiguration(machineId, configuration) {
  await request('machine_configuration_set', machineId, configuration);
}

async function getConfiguration(machineId) {
  const [configuration] = await request('machine_configuration_get', machineId);
  return configuration;
}

// TODO: fix error with native-fs: unexpected end of json input
async function getLogs(machineId) {
  const [logs] = await request('machine_logs_get', machineId);
  return logs;
}

async function startMachinePolling() {
  await request('machines_polling_start');
}

async function stopMachinePolling() {
  await request('machines_polling_stop');
}

async function subscribeForDeploymentsUpdates() {
  await request('machines_deployments_updates_subscribe');
}

async function unsubscribeFromDeploymentsUpdates() {
  await request('machines_deployments_updates_unsubscribe');
}

async function subscribeToMachine(machineId) {
  emit('machine_info_subscribe', machineId);
}

async function unsubscribeFromMachine(machineId) {
  emit('machine_info_unsubscribe', machineId);
}

async function subscribeToMachineLogs(machineId) {
  emit('machine_logs_subscribe', machineId);
}

async function unsubscribeFromMachineLogs(machineId) {
  emit('machine_logs_unsubscribe', machineId);
}

listen('new-machine-info', (machineId, info) => {
  eventHandler.dispatch('newMachineInfo', { id: machineId, info });
});

listen('new-machine-logs', (machineId, logs) => {
  eventHandler.dispatch('newMachineLogs', { id: machineId, logs });
});

export default {
  removeDeployment,
  startInstance,
  stopInstance,
  pauseInstance,
  resumeInstance,
  deployProcess,
  getStatus,
  getInstanceInformation,
  getMachineProperties,
  sendConfiguration,
  getConfiguration,
  getLogs,
  startMachinePolling,
  stopMachinePolling,
  subscribeForDeploymentsUpdates,
  unsubscribeFromDeploymentsUpdates,
  subscribeToMachine,
  unsubscribeFromMachine,
  subscribeToMachineLogs,
  unsubscribeFromMachineLogs,
};
