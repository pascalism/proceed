import { listen, request, io } from './socket.js';
import restRequest from './rest.js';
import eventHandler from '@/backend-api/event-system/EventHandler.js';
import * as browserStorage from './browser-storage.js';

function toInternalFormat(processInformation) {
  const internalFormat = { ...processInformation };
  internalFormat.id = processInformation.definitionId;
  internalFormat.name = processInformation.definitionName;
  delete internalFormat.definitionId;
  delete internalFormat.definitionName;
  return internalFormat;
}

async function getProcessesViaWebsocket() {
  const [backendProcesses] = await request('data_get_processes');
  return backendProcesses;
}

async function getProcessesViaRest() {
  const processes = await restRequest('process', 'noBpmn=true');
  return processes.map((process) => toInternalFormat(process));
}

async function getProcesses(viaWebsocket = false) {
  let backendProcesses;

  if (viaWebsocket) {
    backendProcesses = await getProcessesViaWebsocket();
  } else {
    backendProcesses = await getProcessesViaRest();
  }

  await Promise.all(
    backendProcesses.map(async (process) => {
      await observeProcess(process.id);
    })
  );

  // get locally stored processes and merge with the ones from the server
  const localProcesses = Object.values(browserStorage.getProcesses()).map((p) => {
    delete p.processData.bpmn;
    return p.processData;
  });

  return [...backendProcesses, ...localProcesses];
}

/**
 * Contains process specific connections
 */
const processSockets = {};

/**
 * Unregisters from process namespaces
 *
 * @param {String} processDefinitionsId
 */
async function stopObserving(processDefinitionsId) {
  if (!processSockets[processDefinitionsId]) {
    return;
  }

  const { edit, observe } = processSockets[processDefinitionsId];
  delete processSockets[processDefinitionsId];

  observe.disconnect();
  edit.disconnect();
}

/**
 * Connects to process namespace and sets up handlers for server side events
 *
 * @param {String} processDefinitionsId
 */
export async function observeProcess(processDefinitionsId) {
  // only connect if there a connection attempt wasn't previously done
  if (!processSockets[processDefinitionsId]) {
    processSockets[processDefinitionsId] = {};

    const observeSocket = io(
      `https://${window.location.hostname}:33081/process/${processDefinitionsId}/view`
    );
    processSockets[processDefinitionsId].observe = observeSocket;

    // check if connection can be established
    processSockets[processDefinitionsId].observeConnect = new Promise((resolve, reject) => {
      observeSocket.on('connect', () => {
        resolve();
      });

      observeSocket.on('connect_error', (err) => {
        reject(`Failed connection to observer socket: ${err.message}`);
      });
    });

    observeSocket.on('bpmn_modeler_event_broadcast', (type, context) => {
      console.log(`Received update for ${processDefinitionsId}`);
      eventHandler.dispatch('processBPMNEvent', {
        processDefinitionsId,
        type,
        context: JSON.parse(context),
      });
    });

    observeSocket.on('process_xml_changed', (processDefinitionsId, newXml) => {
      console.log(`Received update for xml of ${processDefinitionsId}`);
      eventHandler.dispatch('processXmlChanged', { processDefinitionsId, newXml });
    });

    observeSocket.on('user_task_html_changed', (taskId, newHtml) => {
      console.log(`Received update for html in ${processDefinitionsId}`);
      eventHandler.dispatch('processTaskHtmlChanged', { processDefinitionsId, taskId, newHtml });
    });

    observeSocket.on('script_changed_event_broadcast', (elId, elType, script, change) => {
      console.log(`Received update for script in ${processDefinitionsId}`);
      eventHandler.dispatch('processScriptChanged', {
        processDefinitionsId,
        elId,
        elType,
        script,
        change,
      });
    });

    observeSocket.on('element_constraints_changed', (elementId, constraints) => {
      console.log(`Received update for ${processDefinitionsId} costraints`);
      eventHandler.dispatch('elementConstraintsChanged', {
        processDefinitionsId,
        elementId,
        constraints,
      });
    });

    observeSocket.on('process_updated', (oldId, updatedInfo) => {
      eventHandler.dispatch('processUpdated', { oldId, updatedInfo });
    });

    const editSocket = io(
      `https://${window.location.hostname}:33081/process/${processDefinitionsId}/edit`
    );
    processSockets[processDefinitionsId].edit = editSocket;

    processSockets[processDefinitionsId].editConnect = new Promise((resolve) => {
      editSocket.on('connect', () => {
        resolve();
      });
    });

    observeSocket.on('process_removed', (processDefinitionsId) => {
      eventHandler.dispatch('processRemoved', { processDefinitionsId });
      stopObserving(processDefinitionsId);
    });
  }

  // wait until the connections were succesful
  await processSockets[processDefinitionsId].observeConnect;
  await processSockets[processDefinitionsId].editConnect;
}

async function getProcess(id) {
  let process;

  if (browserStorage.hasProcess(id)) {
    ({ processData: process } = browserStorage.getProcess(id));
  } else {
    process = toInternalFormat(await restRequest(`process/${id}`));
  }

  return process;
}

async function addProcess(processData, local) {
  if (local) {
    await browserStorage.addProcess(processData);
  } else {
    await restRequest('process', undefined, 'POST', 'application/json', processData);
    await observeProcess(processData.id);
  }
}

async function updateProcess(id, { bpmn }) {
  // non local processes are updated by the backend using events send by the clients
  if (browserStorage.hasProcess(id)) {
    browserStorage.updateBPMN(id, bpmn);
  }
}

async function updateWholeXml(id, newXml) {
  if (browserStorage.hasProcess(id)) {
    browserStorage.updateBPMN(id, newXml);
  } else {
    await restRequest(`process/${id}`, undefined, 'PUT', 'application/json', { bpmn: newXml });
  }
}

/**
 * Sends the new bpmn of a process for the backend to save
 *
 * @param {String} id id of the process
 * @param {String} bpmn
 * @param {String} processChanges changes to the process meta information that should be merged on the server
 */
async function updateProcessViaWebsocket(id, bpmn, processChanges = {}) {
  processSockets[id].edit.emit('data_updateProcess', bpmn, processChanges);
}

async function updateProcessMetaData(processDefinitionsId, metaDataChanges) {
  if (browserStorage.hasProcess(processDefinitionsId)) {
    browserStorage.updateProcessMetaData(processDefinitionsId, metaDataChanges);
  } else {
    await new Promise((resolve) => {
      processSockets[processDefinitionsId].edit.emit(
        'data_process_meta_update',
        metaDataChanges,
        () => resolve() // will be called once the server sends acknowledgement
      );
    });
  }
}

async function updateProcessName(id, newName) {
  if (browserStorage.hasProcess(id)) {
    await browserStorage.updateProcessName(id, newName);
    return;
  }

  // namechange is send via the modeler events
  // only use this when the name is updated from outside the modeler (e.g. process view)

  // create modeler event
  const type = 'definitions.updateProperties';
  const context = { properties: { name: newName } };

  // broadcast event to other clients
  processSockets[id].edit.emit('bpmn_modeler_event', type, JSON.stringify(context));
}

async function updateProcessDescription(processDefinitionsId, processId, description) {
  if (browserStorage.hasProcess(processDefinitionsId)) {
    await browserStorage.updateProcessDescription(processDefinitionsId, description);
    return;
  }

  // description update is send via the modeler events
  // only use this when the description is updated from outside the modeler (e.g. process view)

  // create modeler event
  const type = 'element.updateDocumentation';
  const context = { elementId: processId, documentation: description };

  // broadcast event to other clients
  processSockets[processDefinitionsId].edit.emit(
    'bpmn_modeler_event',
    type,
    JSON.stringify(context)
  );
}

async function removeProcess(id) {
  if (browserStorage.hasProcess(id)) {
    browserStorage.removeProcess(id);
  } else {
    stopObserving(id);
    await restRequest(`process/${id}`, undefined, 'DELETE');
  }
}

async function observeProcessEditing(processDefinitionsId) {
  // cant observe editing for local processes
  if (!browserStorage.hasProcess(processDefinitionsId)) {
    if (!processSockets[processDefinitionsId]) {
      throw new Error('Not connected to process socket!');
    }

    processSockets[processDefinitionsId].observe.emit('data_observe_modeling');
  }
}

async function stopObservingProcessEditing(processDefinitionsId) {
  // cant observe editing for local processes
  if (!browserStorage.hasProcess(processDefinitionsId)) {
    if (!processSockets[processDefinitionsId]) {
      throw new Error('Not connected to process socket!');
    }

    processSockets[processDefinitionsId].observe.emit('data_stop_observing_modeling');
  }
}

async function blockProcess(processDefinitionsId) {
  if (!browserStorage.hasProcess(processDefinitionsId)) {
    processSockets[processDefinitionsId].edit.emit('data_edit_process');
  }
}

async function unblockProcess(processDefinitionsId) {
  if (!browserStorage.hasProcess(processDefinitionsId)) {
    processSockets[processDefinitionsId].edit.emit('data_stop_editing_process');
  }
}

async function blockTask(processDefinitionsId, taskId) {
  if (!browserStorage.hasProcess(processDefinitionsId)) {
    processSockets[processDefinitionsId].edit.emit('data_blockTask', taskId);
  }
}

async function unblockTask(processDefinitionsId, taskId) {
  if (!browserStorage.hasProcess(processDefinitionsId)) {
    processSockets[processDefinitionsId].edit.emit('data_unblockTask', taskId);
  }
}

async function broadcastBPMNEvents(processDefinitionsId, type, context) {
  // prevent updates for local processes from being broadcasted
  if (!browserStorage.hasProcess(processDefinitionsId)) {
    processSockets[processDefinitionsId].edit.emit(
      'bpmn_modeler_event',
      type,
      JSON.stringify(context)
    );
  }
}

async function broadcastScriptChangeEvent(processDefinitionsId, elId, elType, script, change) {
  // prevent updates for local processes from being broadcasted
  if (!browserStorage.hasProcess(processDefinitionsId)) {
    processSockets[processDefinitionsId].edit.emit(
      'script_changed_event',
      elId,
      elType,
      script,
      JSON.stringify(change)
    );
  }
}

async function updateConstraints(processDefinitionsId, elementId, constraints) {
  // prevent updates for local processes from being broadcasted
  if (!browserStorage.hasProcess(processDefinitionsId)) {
    processSockets[processDefinitionsId].edit.emit(
      'data_updateConstraints',
      elementId,
      constraints
    );
  }
}

async function getUserTasksHTML(processDefinitionsId) {
  let taskIdHTMLMap;

  if (browserStorage.hasProcess(processDefinitionsId)) {
    taskIdHTMLMap = browserStorage.getUserTasksHTML(processDefinitionsId);
  } else {
    taskIdHTMLMap = await restRequest(
      `process/${processDefinitionsId}/user-tasks`,
      'withHtml=true'
    );
  }

  return taskIdHTMLMap;
}

async function saveUserTaskHTML(definitionsId, taskFileName, html) {
  if (browserStorage.hasProcess(definitionsId)) {
    browserStorage.saveUserTaskHTML(definitionsId, taskFileName, html);
  } else {
    processSockets[definitionsId].edit.emit('data_saveUserTaskHTML', taskFileName, html);
  }
}

async function deleteUserTaskHTML(processDefinitionsId, taskFileName) {
  if (browserStorage.hasProcess(processDefinitionsId)) {
    browserStorage.deleteUserTaskHTML(processDefinitionsId, taskFileName);
  } else {
    processSockets[processDefinitionsId].edit.emit('data_deleteUserTaskHTML', taskFileName);
  }
}

async function saveScriptTaskJS(processDefinitionsId, taskId, js) {
  if (browserStorage.hasProcess(processDefinitionsId)) {
    browserStorage.saveScriptTaskJS(processDefinitionsId, taskId, js);
  } else {
    processSockets[processDefinitionsId].edit.emit('data_saveScriptTaskJS', taskId, js);
  }
}

listen('process_added', async (process) => {
  eventHandler.dispatch('processAdded', { process });
  await observeProcess(process.id);
});

export default {
  getProcesses,
  getProcess,
  addProcess,
  updateProcess,
  updateWholeXml,
  updateProcessViaWebsocket,
  updateProcessMetaData,
  updateProcessName,
  updateProcessDescription,
  removeProcess,
  observeProcessEditing,
  stopObservingProcessEditing,
  blockProcess,
  unblockProcess,
  blockTask,
  unblockTask,
  broadcastBPMNEvents,
  broadcastScriptChangeEvent,
  updateConstraints,
  getUserTasksHTML,
  saveUserTaskHTML,
  deleteUserTaskHTML,
  saveScriptTaskJS,
};
