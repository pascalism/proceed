export default async function request(endpoint, query, method, dataType, data) {
  let options;

  if (dataType === 'application/json' && typeof data !== 'string') {
    data = JSON.stringify(data);
  }

  if (method) {
    options = {
      method,
    };

    if (data) {
      options.headers = {
        'Content-Type': dataType,
      };
      options.body = data;
    }
  }

  let uri = `/api/${endpoint}`;

  if (query) {
    uri = `${uri}?${query}`;
  }

  const response = await fetch(uri, options);

  if (!response.ok) {
    throw new Error(
      `Request failed with status code ${response.status}.\nReason: ${await response.text()}`
    );
  }

  const contentType = response.headers.get('content-type');

  if (!contentType) {
    return response;
  }

  if (contentType.includes('application/json')) {
    const obj = await response.json();
    return obj;
  }

  if (contentType.includes('text/plain')) {
    const text = await response.text();
    return text;
  }

  if (contentType.includes('text/html')) {
    const html = await response.html();
    return html;
  }

  throw new Error('Received response with unknown content type!');
}
