function UpdateCalledProcessHandler(elementRegistry, moddle) {
  this.elementRegistry = elementRegistry;
  this.moddle = moddle;
}

UpdateCalledProcessHandler.$inject = ['elementRegistry', 'moddle'];

module.exports = UpdateCalledProcessHandler;

/**
 * Returns the definitions object of the process
 *
 * @param {Object} businessObject the businessObject of the root of the process
 */
function getDefinitions(businessObject) {
  let parent = businessObject;

  while (parent.$parent) {
    parent = parent.$parent;
  }

  return parent;
}

UpdateCalledProcessHandler.prototype.removeCallActivityReference = function (
  context,
  callActivity
) {
  const { businessObject } = callActivity;

  if (typeof businessObject.calledElement !== 'string') {
    return;
  }

  // deconstruct e.g. 'p33c24:_e069937f-27b6-464b-b397-b88a2599f1b9' to 'p33c24'
  const [prefix] = businessObject.calledElement.split(':');

  const callActivities = this.elementRegistry.filter((el) => {
    return el.type === 'bpmn:CallActivity' && el !== callActivity;
  });

  // remove import and namespace in definitions if there is no other call activity referencing the same process
  if (
    callActivities.every(
      (callActivity) =>
        !callActivity.businessObject.calledElement ||
        !callActivity.businessObject.calledElement.startsWith(prefix)
    )
  ) {
    const definitions = getDefinitions(businessObject);

    context.oldImportedNamespace = definitions.$attrs[`xmlns:${prefix}`];

    // remove calledElement from callActivity
    delete definitions.$attrs[`xmlns:${prefix}`];

    context.oldImport = definitions.imports.find(
      (importedProcess) => importedProcess.namespace === context.oldImportedNamespace
    );

    if (context.oldImport) {
      definitions.imports = definitions.imports.filter(
        (processImport) => processImport !== context.oldImport
      );
    }
  }

  context.oldCalledElement = businessObject.calledElement;
  context.oldName = businessObject.name;

  businessObject.calledElement = null;
  businessObject.name = null;
};

UpdateCalledProcessHandler.prototype.addCallActivityReference = function (
  context,
  callActivity,
  processId,
  processName,
  targetNamespace,
  processLocation
) {
  // Construct namespace in format p+(last 5 chars from the imported namespace), for example 'p33c24'
  const nameSpacePrefix = 'p' + targetNamespace.substring(targetNamespace.length - 5);

  const definitions = getDefinitions(callActivity.businessObject);

  if (!definitions.imports) {
    definitions.imports = [];
  }

  // add import if it doesn't already exist
  if (!definitions.imports.some((imp) => imp.namespace === targetNamespace)) {
    const newImport = this.moddle.create('bpmn:Import', {
      namespace: targetNamespace,
      location: processLocation,
      importType: 'http://www.omg.org/spec/BPMN/20100524/MODEL',
    });

    context.addedImport = newImport;
    definitions.imports.push(newImport);
    definitions.$attrs[`xmlns:${nameSpacePrefix}`] = targetNamespace;
    context.addedNamespace = targetNamespace;
  }

  callActivity.businessObject.calledElement = `${nameSpacePrefix}:${processId}`;
  callActivity.businessObject.name = processName;
};

UpdateCalledProcessHandler.prototype.execute = function (context) {
  let {
    elementId,
    calledProcessId,
    calledProcessName,
    calledProcessTargetNamespace,
    calledProcessLocation,
  } = context;

  if (!elementId || typeof elementId !== 'string') {
    throw new Error('Not given a valid element id string!');
  }

  const element = this.elementRegistry.get(elementId);

  if (!element) {
    throw new Error(`Unable to find element with given id (${elementId})!`);
  }

  if (element.type !== 'bpmn:CallActivity') {
    throw new Error(`Expected element of type callActivity but type was: ${element.type}!`);
  }

  this.removeCallActivityReference(context, element);

  if (calledProcessId) {
    this.addCallActivityReference(
      context,
      element,
      calledProcessId,
      calledProcessName,
      calledProcessTargetNamespace,
      calledProcessLocation
    );
  }

  context.changed = element;

  return context.changed;
};

UpdateCalledProcessHandler.prototype.revert = function (context) {
  const {
    oldImportedNamespace,
    oldImport,
    oldCalledElement,
    oldName,
    addedImport,
    addedNamespace,
  } = context;

  const definitions = getDefinitions(context.changed.businessObject);

  if (addedImport) {
    definitions.imports = definitions.imports.filter((imp) => imp !== addedImport);
  }

  if (addedNamespace) {
    const nameSpacePrefix = 'p' + addedNamespace.substring(addedNamespace.length - 5);
    delete definitions.$attrs[`xmlns:${nameSpacePrefix}`];
  }

  if (oldImportedNamespace) {
    const nameSpacePrefix = 'p' + oldImportedNamespace.substring(oldImportedNamespace.length - 5);
    definitions.$attrs[`xmlns:${nameSpacePrefix}`] = oldImportedNamespace;
  }

  if (oldImport) {
    definitions.imports.push(oldImport);
  }

  context.changed.businessObject.calledElement = oldCalledElement;

  context.changed.businessObject.name = oldName;
};
