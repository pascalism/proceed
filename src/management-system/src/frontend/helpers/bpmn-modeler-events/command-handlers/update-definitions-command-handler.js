function UpdateDefinitionsHandler(canvas) {
  this.canvas = canvas;
}

UpdateDefinitionsHandler.$inject = ['canvas'];

module.exports = UpdateDefinitionsHandler;

/**
 * Returns the definitions object of the process
 *
 * @param {Object} businessObject the businessObject of the root of the process
 */
function getDefinitions(businessObject) {
  return businessObject.$parent;
}

function getProperties(definitions, propertyNames) {
  return propertyNames.reduce((curr, pName) => {
    curr[pName] = definitions.get(pName);
    return curr;
  }, {});
}

function setProperties(definitions, properties) {
  Object.entries(properties).forEach(([key, value]) => {
    if (value === null) {
      definitions.set(key, undefined);
      return;
    }

    definitions.set(key, value);
  });
}

UpdateDefinitionsHandler.prototype.execute = function (context) {
  const { properties } = context;
  const definitions = getDefinitions(this.canvas.getRootElement().businessObject);

  if (!properties) {
    throw new Error('properties required');
  }

  if (!definitions) {
    throw new Error('could not find process definitions');
  }

  const oldProperties = getProperties(definitions, Object.keys(properties));

  setProperties(definitions, properties);

  context.oldProperties = oldProperties;
  context.changed = [definitions];

  return context.changed;
};

UpdateDefinitionsHandler.prototype.revert = function (context) {
  const { oldProperties, changed } = context;
  const [definitions] = changed;

  setProperties(definitions, oldProperties);
};
