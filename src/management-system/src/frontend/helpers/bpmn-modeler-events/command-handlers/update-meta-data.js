function UpdateMetaDataHandler(elementRegistry, moddle) {
  this.elementRegistry = elementRegistry;
  this.moddle = moddle;
}

UpdateMetaDataHandler.$inject = ['elementRegistry', 'moddle'];

module.exports = UpdateMetaDataHandler;

function ensureMetaElement(element, moddle) {
  let { extensionElements } = element.businessObject;

  if (!extensionElements) {
    extensionElements = moddle.create('bpmn:ExtensionElements');
    extensionElements.values = [];
    element.businessObject.extensionElements = extensionElements;
  }

  let meta = extensionElements.values.find((child) => child.$type == 'proceed:Meta');

  if (!meta) {
    meta = moddle.create('proceed:Meta');
    extensionElements.values.push(meta);
  }

  return meta;
}

function updateMetaData(meta, type, value, moddle) {
  // remember old value for possible revert
  const oldValue = meta[type];

  if (value) {
    // add new value
    meta[type] = moddle.create(`proceed:${type}`);
    meta[type].value = value;
  } else {
    meta[type] = undefined;
  }

  return oldValue;
}

function cleanup(element) {
  const { extensionElements } = element.businessObject;

  const meta = extensionElements.values.find((child) => child.$type == 'proceed:Meta');

  // meta element doesn't contain any info => remove
  if (!meta.cost && !meta.timePlannedDuration && !meta.timePlannedOccurrence) {
    extensionElements.values = extensionElements.values.filter((el) => el !== meta);
  }

  // extensionElements doesn't contain any info => remove
  if (!extensionElements.values.length) {
    delete element.businessObject.extensionElements;
  }
}

UpdateMetaDataHandler.prototype.execute = function (context) {
  const { elementId, metaData } = context;

  if (!metaData) {
    throw new Error('Meta data required');
  }

  if (!elementId) {
    throw new Error('Element id required');
  }

  const element = this.elementRegistry.get(elementId);

  if (!element) {
    throw new Error(`Could not find element with id ${elementId}.`);
  }

  const oldMetaData = {};

  const meta = ensureMetaElement(element, this.moddle);

  if (metaData.hasOwnProperty('cost')) {
    oldMetaData.cost = updateMetaData(meta, 'cost', metaData.cost, this.moddle);
  }

  if (metaData.hasOwnProperty('timePlannedDuration')) {
    oldMetaData.timePlannedDuration = updateMetaData(
      meta,
      'timePlannedDuration',
      metaData.timePlannedDuration,
      this.moddle
    );
  }

  if (metaData.hasOwnProperty('timePlannedOccurrence')) {
    oldMetaData.timePlannedOccurrence = updateMetaData(
      meta,
      'timePlannedOccurrence',
      metaData.timePlannedOccurrence,
      this.moddle
    );
  }

  context.oldMetaData = oldMetaData;
  context.element = element;

  cleanup(element);
};

UpdateMetaDataHandler.prototype.revert = function (context) {
  const { oldMetaData, element } = context;

  const meta = ensureMetaElement(element);

  if (oldMetaData.hasOwnProperty('cost')) {
    meta.cost = oldMetaData.cost;
  }

  if (oldMetaData.hasOwnProperty('timePlannedDuration')) {
    meta.timePlannedDuration = oldMetaData.timePlannedDuration;
  }

  if (oldMetaData.hasOwnProperty('timePlannedOccurrence')) {
    meta.timePlannedOccurrence = oldMetaData.timePlannedOccurrence;
  }

  cleanup(element);
};
