export function statusToType(status) {
  switch (status) {
    case 'RUNNING':
    case 'PAUSED':
    case 'READY':
    case 'DEPLOYMENT_WAITING':
      return 'warning';
    case 'COMPLETED':
    case 'FORWARDED':
    case 'ENDED':
      return 'success';
    case 'ABORTED':
    case 'ERROR_SEMANTIC':
    case 'ERROR_TECHNICAL':
    case 'ERROR_CONSTRAINT_UNFULFILLED':
    case 'STOPPED':
      return 'error';
    default:
      return 'info';
  }
}
