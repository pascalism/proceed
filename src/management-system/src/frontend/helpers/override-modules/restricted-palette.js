export default class CustomPalette {
  constructor(palette, lassoTool, translate) {
    this.lassoTool = lassoTool;
    this.translate = translate;
    palette.registerProvider(this);
  }

  getPaletteEntries() {
    const { lassoTool, translate } = this;
    return {
      'lasso-tool': {
        group: 'tools',
        className: 'bpmn-icon-lasso-tool',
        title: translate('Activate the lasso tool'),
        action: {
          click: function activateLasso(event) {
            lassoTool.activateSelection(event);
          },
        },
      },
    };
  }
}

CustomPalette.$inject = ['palette', 'lassoTool', 'translate'];
