import { processInterface } from '@/backend-api/index.js';
import { flattenSubprocesses } from '@/helpers/process-hierarchy.js';
import { getSubprocessFromXML, getImports } from '@proceed/bpmn-helper';
/**
 * @param {Object} process
 * @returns - Returns xml for a process from the store
 */
export async function getXmlByProcess(process) {
  if (!process.id || !process.name) {
    console.warn('Try to retrieve xml for invalid process: ', process);
    return null;
  }
  const bpmn = (await processInterface.getProcess(process.id)).bpmn;
  return bpmn;
}

/**
 * @param {Object} process
 * @returns - Returns usertasks for a process from the store
 */
export async function getUserTasksByProcess(process) {
  if (!process.id || !process.name) {
    console.warn('Try to retrieve user tasks for invalid process: ', process);
    return null;
  }
  const userTasks = await processInterface.getUserTasksHTML(process.id);
  return userTasks;
}

export function getCleanedUpName(name) {
  return name
    .replace(/[`@^()_={}[\]|;!=&/\\#,+()$~%.'":*?<>{}]/g, '')
    .trim()
    .replace(/[ ]/g, '_')
    .replace(/-$/, '');
}

/**
 * Retrieve every subprocess and subprocesses of call activities for the
 * selected processes and their meta information, bpmn and user tasks
 *
 * @param {Object[]} allProcesses all known processes to search for referenced call activities
 * @param {Object[]} selectedProcesses - all processes that need to be exported
 * @param {String} selectedOption - export format
 */
export async function prepareProcesses(allProcesses, selectedProcesses, selectedOption) {
  for (let index = 0; index < selectedProcesses.length; index++) {
    const process = selectedProcesses[index];

    process.bpmn = await getXmlByProcess(process);
    process.userTasks = await getUserTasksByProcess(process);

    // collect subprocess data
    if (selectedOption.additionalParam.includeSubprocess) {
      // get all callactivities
      let callActivities = await getAllSubprocesses(allProcesses, process, [process]);

      // get all subprocesses which are no call activities
      const subprocesses = callActivities
        .filter((process) => !!process.subprocesses)
        .flatMap((process) => flattenSubprocesses(process.subprocesses))
        .filter((subprocess) => !subprocess.isCallActivity);

      for (let index = 0; index < subprocesses.length; index++) {
        const subprocess = subprocesses[index];
        subprocess.id = subprocess.elementId;
        // search process which contains the subprocess
        let pro = callActivities.find((process) => process.bpmn.includes(subprocess.elementId));
        subprocess.bpmn = await getSubprocessFromXML(pro.bpmn, subprocess.elementId);
      }

      // remove root process
      callActivities = callActivities.filter((callActivity) => process.id !== callActivity.id);

      process.imported = [...subprocesses, ...callActivities];
      process.callActivities = callActivities;
    }
  }

  return selectedProcesses;
}

/**
 * add call activities to zip file
 * add user tasks of call activities to zip file (optional)
 * @param {Object} process
 * @param {JSZip} zipObject
 * @param {Boolean} includeUserTasks includer user tasks of call activities or not (optional)
 */
export async function getImported(process, zipObject, includeUserTasks) {
  const processFileName = getCleanedUpName(process.name);
  const processFolder = zipObject.folder(processFileName);
  processFolder.file(`${processFileName}.bpmn`, process.bpmn);

  // add call activities to /imported folder
  if (process.callActivities.length > 0) {
    const subprocessFolder = processFolder.folder('imported');

    for (const callActivity of process.callActivities) {
      const filename = getCleanedUpName(callActivity.name);
      const bpmn = await getXmlByProcess(callActivity);

      subprocessFolder.file(`${filename}.bpmn`, bpmn);
    }

    // add user tasks of call activites
    if (includeUserTasks) {
      process.callActivities.forEach((pro) => {
        const { userTasks } = pro;
        const userTaskIds = Object.keys(userTasks);
        //Combining Process with its supporting files
        if (userTaskIds.length > 0) {
          const userTaskFolder = subprocessFolder.folder('User-Tasks');

          //Combining Process with its supporting files
          for (const userTaskId of userTaskIds) {
            const taskHTML = userTasks[userTaskId];
            userTaskFolder.file(`${userTaskId}.html`, taskHTML);
          }
        }
      });
    }
  }
  return zipObject;
}

/**
 * recursive method to get all call activities and their subprocesses
 *
 * @param {Object[]} allProcesses all known processes to search for referenced call activities
 * @param {Object} currentProcess current selected process of the hierarchy
 * @param {Object[]} includedProcesses container for the result
 */
export async function getAllSubprocesses(allProcesses, currentProcess, includedProcesses) {
  if (!Array.isArray(currentProcess.subprocesses) || currentProcess.subprocesses.length === 0) {
    return includedProcesses;
  }
  const flatSubprocesses = flattenSubprocesses(currentProcess);

  // map through all subprocesses of a process/call actvitiy
  for (let index = 0; index < flatSubprocesses.length; index++) {
    const subprocess = flatSubprocesses[index];
    if (typeof subprocess !== 'object' || !subprocess.isCallActivity) {
      continue;
    }

    const importElements = await getImports(currentProcess.bpmn);

    for (const importElement of importElements) {
      let callActivityProcess = allProcesses.find((pro) => pro.id === importElement.location);
      if (callActivityProcess) {
        // to prevent updating the original process object
        callActivityProcess = { ...callActivityProcess };

        callActivityProcess.bpmn = await getXmlByProcess(callActivityProcess);
        callActivityProcess.userTasks = await getUserTasksByProcess(callActivityProcess);
        // check if process was in recursive loop before
        if (!includedProcesses.some((process) => process.id === callActivityProcess.id)) {
          includedProcesses.push(callActivityProcess);
          includedProcesses = await getAllSubprocesses(
            allProcesses,
            callActivityProcess,
            includedProcesses
          );
        }
      }
    }
  }

  return includedProcesses;
}
