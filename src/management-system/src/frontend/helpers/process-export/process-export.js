import jsPDF from 'jspdf';
import JSZip from 'jszip';
import Viewer from 'bpmn-js/lib/Viewer';
import Modeler from 'bpmn-js/dist/bpmn-modeler.production.min';

import { getExporterName, getExporterVersion } from '@proceed/bpmn-helper';
import {
  getXmlByProcess,
  getCleanedUpName,
  prepareProcesses,
  getImported,
} from '@/helpers/process-export/export-preparation.js';
import { flattenSubprocesses } from '@/helpers/process-hierarchy.js';

let pdf;

//Initialize Pdf Object
function initPdf() {
  pdf = new jsPDF({
    orientation: 'l',
    unit: 'mm',
    format: 'a4',
    compressPdf: true,
  });

  //Delete Default Page from Pdf
  pdf.deletePage(1);

  return pdf;
}

/**
 *  Export selected processes to the selected format.
 *
 * @param {Object[]} allProcesses all known processes to search for referenced call activities
 * @param {Object[]} selectedProcesses - all processes that need to be exported
 * @param {String} selectedOption - export format
 */
export async function exportSelectedProcesses(allProcesses, selectedProcesses, selectedOption) {
  // deep copy to prevent updating the original process objects
  selectedProcesses = JSON.parse(JSON.stringify(selectedProcesses));
  selectedProcesses = await prepareProcesses(allProcesses, selectedProcesses, selectedOption);
  const zip = new JSZip();
  let zipName = '';

  const processesLength = selectedProcesses.length;

  const isMultiDownload = processesLength > 1;
  const { format, includeSubprocess } = selectedOption.additionalParam;
  if (isMultiDownload) {
    zipName = `PROCEED-Multiple-Processes_${selectedOption.format}`;
  }
  if (selectedOption.format == 'bpmn') {
    let downloadItem;
    for (const process of selectedProcesses) {
      const { bpmn, name, id } = process;
      const processFileName = getCleanedUpName(name);

      const includeUserTasks = format === 'withUserTasks';
      // get BPMN with assets or just BPMN file
      if (includeSubprocess) {
        downloadItem = await getImported(process, zip, includeUserTasks);
      }
      if (includeUserTasks) {
        downloadItem = getBPMNWithAssets(process, zip);
      }
      if (!includeSubprocess && !includeUserTasks) {
        //handle single process vs multi process download format
        if (!isMultiDownload) {
          downloadItem = new Blob([bpmn], {
            type: 'application/bpmn+xml',
          });
          zipName = `${processFileName}.${selectedOption.format}`;
        } else {
          zip.file(`${processFileName}.${selectedOption.format}`, bpmn);
        }
      }
      if (!isMultiDownload && (includeUserTasks || includeSubprocess)) {
        zipName = `PROCEED_${processFileName}_bpmn`;
      }
    }

    // download zip / file
    if (isMultiDownload || format == 'withUserTasks' || includeSubprocess === true) {
      downloadItem = await zip.generateAsync({
        type: 'blob',
      });
    }
    anchorElement(zipName, downloadItem);
  } else if (
    selectedOption.format == 'svg' ||
    selectedOption.format == 'png' ||
    selectedOption.format == 'pdf'
  ) {
    // get image as SVG | PNG | PDF

    let downloadItem;

    for (const process of selectedProcesses) {
      const processFolderName = getCleanedUpName(process.name);
      let processFolder = null;

      //If Selected Option is Pdf, don't create jsZip folders
      if (selectedOption.format !== 'pdf') {
        processFolder = zip.folder(processFolderName);
        if (!isMultiDownload) {
          zipName = `PROCEED_${processFolderName}_${selectedOption.format}`;
        }
      } else {
        //If Selected Option is Pdf, initialize Pdf Object
        pdf = initPdf();
      }

      let currentProcesses;
      if (selectedOption.additionalParam.includeSubprocess) {
        //Get all Subprocesses and filter all distinct subprocesses
        //currentProcesses = [process].concat(process.imported);
        currentProcesses = [process].concat(flattenSubprocesses(process)).concat(process.imported);
        currentProcesses = currentProcesses.filter(
          (process, index) =>
            currentProcesses.indexOf(process) == index &&
            (process.subprocesses || process.elementId)
        );
      } else {
        currentProcesses = [process];
      }

      for (const currentProcess of currentProcesses) {
        if (!!currentProcess) {
          let { bpmn, name, id } = currentProcess;

          const processFileName = getCleanedUpName(name);
          //Creating temporary element for BPMN Viewer
          const viewerElement = document.createElement('div');

          //Assiging process id to temp element and append to DOM
          viewerElement.id = 'canvas_' + id;
          document.body.appendChild(viewerElement);

          //Initiate BPMN Viewer and import XML
          let viewer = new Viewer({ container: '#' + viewerElement.id });

          //If current process is a subprocess, create viewer as BPMN Modeler
          if (currentProcess.elementId) {
            viewer = new Modeler({ container: '#' + viewerElement.id });
          }

          //Check if process contains bpmn, if-not get from store.
          await viewer.importXML(!bpmn ? await getXmlByProcess(currentProcess) : bpmn);

          // get image to download
          downloadItem = await getImage(
            viewer,
            currentProcess,
            selectedOption,
            isMultiDownload || includeSubprocess
          );

          //remove temporary viewer element from DOM
          document.body.removeChild(viewerElement);

          if (isMultiDownload || (includeSubprocess && selectedOption.format != 'pdf')) {
            if (selectedOption.format == 'png') {
              processFolder.file(`${processFileName}.${selectedOption.format}`, downloadItem, {
                base64: true,
              });
            } else if (selectedOption.format == 'svg') {
              processFolder.file(`${processFileName}.${selectedOption.format}`, downloadItem);
            }
          } else {
            zipName = `${processFileName}.${selectedOption.format}`;

            //If Selected Option is Pdf and is for single export
            if (selectedOption.format == 'pdf') {
              if (!isMultiDownload) {
                const pdfName = getCleanedUpName(selectedProcesses[0].name);
                zipName = `${pdfName}.${selectedOption.format}`;
              }
              downloadItem = pdf.output('blob');
            }
          }
        }
      }

      //If Selected Option is Pdf and it is for multi-download, then add it to jsZip Folder
      if (selectedOption.format == 'pdf' && isMultiDownload) {
        zip.file(`${processFolderName}.${selectedOption.format}`, pdf.output('blob'));
      }
    }

    // download zip / file
    if (isMultiDownload || (includeSubprocess && selectedOption.format != 'pdf')) {
      downloadItem = await zip.generateAsync({ type: 'blob' });
    }

    //Download single file
    anchorElement(zipName, downloadItem);
  } else {
    throw new Error('Tried to export in an unsupported format.');
  }
}

/**
 *
 * @param {String} fileName name used to download file
 * @param {String|Object} objectContent content, either as Blob or objectURL DOMString
 */
export async function anchorElement(fileName, objectContent) {
  // check if objectContent is an objectURL
  // otherwise create a blob, for the following reason: https://stackoverflow.com/questions/16761927/aw-snap-when-data-uri-is-too-large

  const objectURL =
    typeof objectContent === 'string'
      ? URL.createObjectURL(await fetch(objectContent).then((res) => res.blob()))
      : URL.createObjectURL(objectContent);

  // Creating Anchor Element to trigger download feature
  const aLink = document.createElement('a');

  // Setting anchor tag properties
  aLink.style.display = 'none';
  aLink.download = fileName;
  aLink.href = objectURL;

  // Setting anchor tag to DOM
  document.body.appendChild(aLink);
  aLink.click();
  document.body.removeChild(aLink);

  // Release Object URL, so browser dont keep reference
  URL.revokeObjectURL(objectURL);
}

/**
 * create PDF with Properties and Values
 */
export function setPdfPropsAndValues(imageURI, process, isHeadingRequested, svgWidth, svgHeight) {
  const { name, description = '', departments = [] } = process;
  let keywords = 'BPMN';

  // adding a new page
  pdf.addPage([svgWidth, svgHeight], svgHeight > svgWidth ? 'p' : 'l');

  //Getting PDF Documents width and height
  const pdfWidth = pdf.internal.pageSize.getWidth() - 10;
  const pdfHeight = pdf.internal.pageSize.getHeight() - 10;

  //Setting pdf font size
  pdf.setFontSize(20);

  //Adding Header to the Pdf
  if (isHeadingRequested) {
    pdf.text(10, 10, `Process: ${name} \n`);
  }

  if (Array.isArray(departments)) {
    const departmentsString = departments.map((d) => d.name).join(', ');
    keywords =
      `${departmentsString != '' ? departmentsString : departments.join(', ')}, ` + keywords;
  }

  //Adding Meta Information to the PDF
  pdf.setProperties({
    title: `${name}`,
    subject: `${description}`,
    keywords: keywords,
    creator: `${getExporterName()} v${getExporterVersion()}`,
  });

  /**
   * Adding image to pdf document
   * If isHeadingRequested is true then assign value to 'y-axis'
   */
  pdf.addImage(
    imageURI,
    'PNG',
    5,
    isHeadingRequested ? 10 : 0,
    pdfWidth,
    pdfHeight,
    undefined,
    'FAST'
  );
}

/**
 * Get PDF file
 */
export async function getPDF(modeler, process, isHeadingRequested, resolution) {
  let uri = await getPNG(modeler, false, resolution);

  let svg = await getSVG(modeler, true);
  let width = svg.split('width="')[1].split('"')[0];
  let height = String(20 + parseFloat(svg.split('height="')[1].split('"')[0]));
  // relevant for the server: if pdf.addImage throws an error, try again with minimum resolution
  try {
    setPdfPropsAndValues(uri, process, isHeadingRequested, width, height);
  } catch (err) {
    if (resolution !== 1) {
      const pageCount = pdf.internal.getNumberOfPages();
      pdf.deletePage(pageCount);
      await getPDF(modeler, process, isHeadingRequested, (resolution -= 1));
    }
  }
}

/**
 * Get SVG file
 */
export async function getSVG(modeler, isMultiDownload) {
  let svgBlob;
  let svgFile;
  try {
    const result = await modeler.saveSVG();
    const { svg } = result;
    //Combining SVG and its content type
    svgBlob = new Blob([svg], {
      type: 'image/svg+xml',
    });
    svgFile = svg;
  } catch (err) {
    console.log(err);
  }

  return isMultiDownload ? svgFile : svgBlob;
}

/**
 * Get PNG file
 */
export function getPNG(modeler, isMultiDownload, resolution) {
  let uri;
  const DATA_URL_REGEX = /^data:((?:\w+\/(?:(?!;).)+)?)((?:;[\w\W]*?[^;])*),(.+)$/;
  let headerlessImage;
  return new Promise(async (resolve, reject) => {
    let svg = await getSVG(modeler, true);

    //Getting width and height from BPMN SVG
    let width = svg.split('width="')[1].split('"')[0];
    let height = svg.split('height="')[1].split('"')[0];

    //Initiating Image Element
    let image = new Image(width, height);

    //Combining SVG and its content type
    let svgString = new Blob([svg], { type: 'image/svg+xml;charset=utf-8' });

    //Creating Canvas out of SVG
    let canvas = document.createElement('canvas');

    image.onload = () => {
      for (let scale = resolution; scale >= 1; scale -= 1) {
        try {
          canvas.width = scale * image.width;
          canvas.height = scale * image.height;
          //Creating 2D Canvas
          let ctx = canvas.getContext('2d');
          //prevent from bluring the pixels
          ctx.imageSmoothingEnabled = false;

          ctx.scale(scale, scale);

          //Drawing Image on Canvas
          ctx.drawImage(image, 0, 0, width, height);

          //Getting URI for Image in PNG **Default = PNG
          uri = canvas.toDataURL('image/png');
          if (DATA_URL_REGEX.test(uri)) {
            headerlessImage = uri.replace('data:image/png;base64,', '');
            resolve(isMultiDownload ? headerlessImage : uri);
            break;
          }

          //Release Object URL, so browser dont keep reference
          URL.revokeObjectURL(uri);
        } catch (err) {}
      }
    };
    //Takes BLOB, File and Media Source and returns object url
    image.src = URL.createObjectURL(svgString);
  });
}

/**
 * Get BPMN with Assets
 *
 * @param {Object} process
 * @param {JSZip} zipObject
 */
export function getBPMNWithAssets(process, zipObject) {
  const { id, name, bpmn, userTasks } = process;

  //Processing Folder Name
  const processFolderName = getCleanedUpName(name);
  //If its for multi download, then make folder for every bpmn process with its task id & name
  const processFolder = zipObject.folder(processFolderName);
  processFolder.file(`${processFolderName}.bpmn`, bpmn);

  const userTaskIds = Object.keys(userTasks);
  //Combining Process with its supporting files
  if (userTaskIds.length > 0) {
    //If its multi download then attach files to its specific folder else direct to zipObject
    const userTaskFolder = processFolder.folder('User-Tasks');

    //Combining Process with its supporting files
    for (const userTaskId of userTaskIds) {
      userTaskFolder.file(`${userTaskId}.html`, userTasks[userTaskId]);
    }
  }
  return zipObject;
}

/**
 *
 * @param {*} viewer
 * @param {*} process selected process to be exported
 * @param {*} selectedOption selected export format
 * @param {*} isMultiDownload check if export is single or multi process download
 */
export async function getImage(viewer, process, selectedOption, isMultiDownload) {
  let downloadItem;
  switch (selectedOption.format) {
    case 'svg':
      downloadItem = getSVG(viewer, isMultiDownload);
      break;
    case 'png':
      downloadItem = await getPNG(
        viewer,
        isMultiDownload,
        selectedOption.additionalParam.resolution
      );
      break;
    default:
      const isHeadingRequested = selectedOption.additionalParam.format ? true : false;
      await getPDF(viewer, process, isHeadingRequested, selectedOption.additionalParam.resolution);
      break;
  }
  return downloadItem;
}
