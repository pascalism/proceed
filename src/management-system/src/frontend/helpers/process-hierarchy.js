const { toObject, getDefinitionsIdForCallActivityByObject } = require('@proceed/bpmn-helper');

/**
 * Computes a recursiv list of subprocesses (only global) for the given bpm file
 *
 * @param {string|Object} definitions either the xml describing the process or the object parsed from a process xml
 *
 * @returns {Array} hierarchy list of subprocesses
 */
async function getProcessHierarchy(definitions) {
  let xmlObj;
  if (typeof definitions === 'object') {
    xmlObj = definitions;
  } else {
    xmlObj = await toObject(definitions);
  }

  const process = xmlObj.rootElements.find((element) => element.$type === 'bpmn:Process');

  const findSubprocesses = (flowElements) => {
    if (!Array.isArray(flowElements)) return [];

    return (
      flowElements
        .filter((element) => element.$type === 'bpmn:CallActivity')
        .map((element) => {
          return {
            id: getDefinitionsIdForCallActivityByObject(xmlObj, element.id),
            name: element.name,
            isCallActivity: true,
          };
        })
        // if the id of the element is set (referenced call activity), look if its the first within the array
        // => to remove multiple call activities referencing the same process on the same level
        .filter(
          (element, index, array) =>
            !element.id ||
            array.findIndex((innerElement) => element.id === innerElement.id) === index
        )
    );
  };

  const hierarchy = findSubprocesses(process.flowElements);

  return hierarchy;
}

/**
 * This method is a recursive method to get all n-level subprocesses within a process
 *
 * @param {Object} process complete process with all supporting elements
 * @returns Returns an array of all subprocesses with a process
 */
function flattenSubprocesses(process) {
  const flatten = (a) => {
    if (Array.isArray(a.subprocesses)) {
      return [].concat(...a.subprocesses, ...a.subprocesses.map(flatten));
    } else {
      return a;
    }
  };

  const isUnique = (element, index, array) => array.indexOf(element) === index;

  return flatten(process).filter(isUnique);
}

module.exports = { getProcessHierarchy, flattenSubprocesses };
