import JSZip from 'jszip';

/**
 * Read File (Blob) with FileReader and resolve the returned promises once the file content is loaded
 *
 * @param {Blob} file - bpmn file
 */
export function readFileAsync(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();

    reader.onload = (event) => {
      resolve(event.target.result);
    };

    reader.onerror = reject;

    reader.readAsText(file);
  });
}

/**
 * @summary Read zip files and searches for bpmn files and user tasks for each bpmn file
 *
 * @param {Blob} zipBlob - zip file containing several bpmn files
 */
export async function readZipAsync(zipBlob) {
  const zip = await JSZip.loadAsync(zipBlob);

  // list all bpmn files inside this zip regardless of the folder depth
  const bpmnFiles = Object.values(zip.files).filter(
    (file) => file.name.endsWith('.bpmn') && !file.dir
  );

  const contentContainer = [];

  for (const bpmnFile of bpmnFiles) {
    // get the actual file name for nested files like 'Delivery-Proces-604fdef1/Delivery-Proces-604fdef1.bpmn'
    const bpmnFileName = bpmnFile.name.split('/').pop();

    // generate folder name like 'Delivery-Proces-604fdef1/User-Tasks/'
    const userTaskDir = bpmnFile.name.replace(bpmnFileName, 'User-Tasks/');

    // list all html files inside the User-Tasks folder which is in the same folder like the current bpmn file
    const userTaskFiles = Object.values(zip.files).filter(
      (file) => file.name.startsWith(userTaskDir) && file.name.endsWith('.html')
    );

    const userTasks = [];
    for (const userTaskFile of userTaskFiles) {
      const name = userTaskFile.name.split('/').pop().replace('.html', '');
      // resolves the content of the file in a string
      const htmlFileContent = await userTaskFile.async('string');

      userTasks.push({
        taskId: name,
        html: htmlFileContent,
      });
    }

    // resolves the content of the file in a string
    const bpmnFileContent = await bpmnFile.async('string');

    contentContainer.push({
      bpmnFile: {
        fileName: bpmnFileName,
        fileContent: bpmnFileContent,
      },
      userTasks: userTasks,
    });
  }

  return contentContainer;
}
