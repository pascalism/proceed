import parser from 'fast-xml-parser';
import { j2xParser } from 'fast-xml-parser';
import customSchema from '@proceed/bpmn-helper/customSchema.json';

export function getOriginalDefinitions(xml) {
  const options = {
    attributeNamePrefix: '@_',
    ignoreAttributes: false,
    ignoreNameSpace: false,
  };
  let jsObj = parser.parse(xml, options);
  const [definitionsName] = Object.keys(jsObj);
  let xmlObj = jsObj[definitionsName];
  const definitions = {
    id: xmlObj['@_id'],
    exporter: xmlObj['@_exporter'],
    exporterVersion: xmlObj['@_exporterVersion'],
    targetNamespace: xmlObj['@_targetNamespace'],
  };
  return definitions;
}

export function correctXmlDefinitions(xml, id) {
  const options = {
    attributeNamePrefix: '@_',
    ignoreAttributes: false,
    ignoreNameSpace: false,
  };
  let jsObj = parser.parse(xml, options);
  const [definitionsName] = Object.keys(jsObj);
  let definitions = jsObj[definitionsName];
  const oldProceedXSIs = [
    'http://docs.snet.tu-berlin.de/proceed/xsd/XSD-PROCEED.xsd',
    'http://docs.snet.tu-berlin.de/proceed',
  ];
  if (
    definitions['@_xmlns:proceed'] !== customSchema.uri ||
    oldProceedXSIs.some((xsi) => definitions['@_xsi:schemaLocation'].includes(xsi))
  ) {
    definitions['@_xmlns:proceed'] = customSchema.uri;
    definitions['@_targetNamespace'] = `https://docs.proceed-labs.org/${id}`;
    const proceedXSIs = [
      'https://docs.proceed-labs.org/BPMN https://docs.proceed-labs.org/xsd/XSD-PROCEED.xsd',
      'http://www.omg.org/spec/BPMN/20100524/MODEL https://www.omg.org/spec/BPMN/20100501/BPMN20.xsd',
    ];
    if (!definitions['@_xsi:schemaLocation']) {
      definitions['@_xsi:schemaLocation'] = '';
    }
    for (const xsi of proceedXSIs) {
      if (!definitions['@_xsi:schemaLocation'].includes(xsi)) {
        definitions['@_xsi:schemaLocation'] += ' ' + xsi;
      }
    }

    for (const oldXsi of oldProceedXSIs) {
      definitions['@_xsi:schemaLocation'] = definitions['@_xsi:schemaLocation'].replace(oldXsi, '');
    }

    definitions['@_xsi:schemaLocation'] = definitions['@_xsi:schemaLocation'].trim();

    const jsonOptions = {
      attributeNamePrefix: '@_',
      ignoreAttributes: false,
      ignoreNameSpace: false,
      format: true,
      supressEmptyNode: true,
    };

    const parserJson = new j2xParser(jsonOptions);
    const newXml = parserJson.parse(jsObj, jsonOptions);
    return newXml;
  }
  return null;
}
