import Vue from 'vue';
import axios from 'axios';
import App from './App.vue';
import Loading from './views/LoadingScreen.vue';
import router from './router.js';
import store from './store.js';
import vuetify from './plugins/vuetify.js';
import 'roboto-fontface/css/roboto/roboto-fontface.css';
import '@mdi/font/css/materialdesignicons.css';
import { engineInterface, eventHandler } from './backend-api/index.js';
// TODO: decide how logging in frontend is supposed to work? can engine module be imported if it doesn't need native part?
//import { logging } from '@proceed/machine';
const logging = {
  getLogger: () => ({
    log: console.log,
    debug: console.log,
    trace: console.log,
    error: console.error,
    info: console.log,
  }),
};

Vue.config.devtools = process.env.NODE_ENV === 'development';
Vue.http = axios;
Vue.prototype.$http = axios;
Vue.config.productionTip = false;

// export empty store object and wait for initialization of the engine to fill it
const dataStore = {};

// Displays loading screen and waits for engine to load before mounting the actual app
async function loadApp() {
  // display loading screen while engine is loading
  const loadingScreen = new Vue({
    render: (h) => h(Loading),
  }).$mount('#loading');

  await engineInterface.engineStarted;
  const publishing = await engineInterface.isEnginePublished();

  // make engine accessible in all components and views via this.$engine
  const engineWrapper = {
    publishing,
    changingState: false,
    async toggleSilentMode() {
      if (!this.changingState) {
        if (this.publishing) {
          await engineInterface.activateSilentMode();
        } else {
          await engineInterface.deactivateSilentMode();
        }
      }
    },
  };

  eventHandler.on('engineChangingState', () => {
    Vue.set(engineWrapper, 'changingState', true);
  });
  eventHandler.on('enginePublished', () => {
    Vue.set(engineWrapper, 'changingState', false);
    Vue.set(engineWrapper, 'publishing', true);
  });
  eventHandler.on('engineUnpublished', () => {
    Vue.set(engineWrapper, 'changingState', false);
    Vue.set(engineWrapper, 'publishing', false);
  });
  eventHandler.on('toggleSilentMode', () => {
    engineWrapper.toggleSilentMode();
  });

  // configStore needs a module of the engine so the engine has to be started
  // before we can initialize the store
  const VuexStore = await store();

  await initStores(VuexStore);

  // make store accessible for all modules that import dataStore
  Object.keys(VuexStore).forEach((key) => {
    dataStore[key] = VuexStore[key];
  });

  const configObject = {
    moduleName: 'MS',
    consoleOnly: true,
  };
  // make logger accessible in all components and views via this.$logger
  const logger = await logging.getLogger(configObject);
  Vue.prototype.$logger = logger;

  setTimeout(() => {
    // remove loading screen
    const { body } = document;
    const loadingScreenContainer = document.getElementById('loading-screen-container');
    body.removeChild(loadingScreenContainer);
    loadingScreen.$destroy();

    // display main app
    new Vue({
      router,
      store: VuexStore,
      vuetify,
      render: (h) => h(App),
      data: {
        engine: engineWrapper,
      },
    }).$mount('#app');
  }, 500);
}

// Triggers all stores to load their data and waits for them to finish
async function initStores(store) {
  await Promise.all([
    store.dispatch('capabilityStore/loadCapabilities'),
    store.dispatch('machineStore/loadMachines'),
    store.dispatch('configStore/loadConfig'),
    store.dispatch('environmentStore/loadEnvProfiles'),
    store.dispatch('processStore/loadProcesses'),
    store.dispatch('processEditorStore/init'),
    store.dispatch('userPreferencesStore/loadUserPreferences'),
  ]);
}

loadApp();

export default dataStore;
