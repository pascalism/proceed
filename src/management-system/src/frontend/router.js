import Vue from 'vue';
import Router from 'vue-router';

import Machines from './views/Machines.vue';
import Deployments from './views/Deployments.vue';
import DeploymentMonitoring from './views/DeploymentMonitoring.vue';
import Process from './views/Process.vue';
import EditorExplorer from './views/EditorExplorer.vue';
import Capabilities from './views/Capabilities.vue';
import EnvironmentProfile from './views/EnvironmentProfile.vue';
import Settings from './views/Settings.vue';
import UserSettings from './views/UserSettings.vue';
import InstanceViewer from './views/InstanceViewer.vue';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      redirect: '/process',
    },
    {
      path: '/process',
      name: 'process',
      component: Process,
    },
    {
      path: '/process/bpmn/:id',
      name: 'edit-process-bpmn',
      component: EditorExplorer,
    },
    {
      path: '/machines',
      name: 'machines',
      component: Machines,
    },
    {
      path: '/deployments',
      name: 'deployments',
      component: Deployments,
    },
    {
      path: '/deployments/:id',
      name: 'deployment-overview',
      component: DeploymentMonitoring,
    },
    {
      path: '/deployments/:id/instance/:instanceId',
      name: 'instance-view',
      component: InstanceViewer,
      props: ({ params }) => ({
        processDefinitionsId: params.id,
        instanceId: params.instanceId,
      }),
    },
    {
      path: '/capabilities',
      name: 'capabilities',
      component: Capabilities,
    },
    {
      path: '/environment-profile',
      name: 'environment-profile',
      component: EnvironmentProfile,
    },
    {
      path: '/user-settings',
      name: 'user-settings',
      component: UserSettings,
    },
    {
      path: '/settings',
      name: 'settings',
      component: Settings,
    },
  ],
});
