import Vue from 'vue';
import Vuex from 'vuex';
import createMachineStore from './stores/machine.js';
import createProcessEditorStore from './stores/process-editor.js';
import createWarningStore from './stores/show-warning.js';
import createProcessStore from './stores/process.js';
import createCapabilityStore from './stores/capability.js';
import createEnvironmentProfileStore from './stores/environment.js';
import createConfigStore from './stores/config.js';
// import createServiceStore from './stores/service';
import createDepartmentStore from './stores/department.js';
import createUserPreferencesStore from './stores/user-preferences.js';

Vue.use(Vuex);

export default async function () {
  const machineStore = createMachineStore();
  const processStore = createProcessStore();
  const processEditorStore = createProcessEditorStore();
  const capabilityStore = createCapabilityStore();
  const environmentStore = createEnvironmentProfileStore();
  const configStore = await createConfigStore();
  const warningStore = createWarningStore();
  const departmentStore = createDepartmentStore();
  const userPreferencesStore = createUserPreferencesStore();

  return new Vuex.Store({
    modules: {
      machineStore,
      processStore,
      processEditorStore,
      capabilityStore,
      environmentStore,
      configStore,
      departmentStore,
      userPreferencesStore,
      warningStore,
    },
    strict: false,
  });
}
