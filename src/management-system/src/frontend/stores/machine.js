import * as R from 'ramda';
import * as d3 from 'd3';
import { eventHandler, dataInterface } from '@/backend-api/index.js';

export default function createMachineStore() {
  const colorScale = d3.scaleOrdinal(d3.schemeCategory10);

  const initialState = {
    machines: [],
    deployments: [],
  };

  /**
   * Creates object with all deployments from per machine deployments
   */
  function mapMachinesToDeployments(machines) {
    const deployedProcesses = [];

    // create single deployment information object from information in all machines
    for (const machine of machines) {
      if (!machine.deployedProcesses) continue;

      // move deployment information from one machine into object
      for (const definitionId in machine.deployedProcesses) {
        const deployment = machine.deployedProcesses[definitionId];

        const previousDeployment = deployedProcesses.find(
          (deployment) => deployment.definitionId === definitionId
        );
        // deployment found in a previous machine
        if (previousDeployment) {
          previousDeployment.machineIds.push(machine.id);
          // merge instance information from different machines
          deployment.instances.forEach((instance) => {
            const knownInstance = previousDeployment.instances.find(
              (storedInstance) => storedInstance.id === instance.id
            );
            if (!knownInstance) {
              // instance was not already seen on another machine
              previousDeployment.instances.push({
                ...instance,
                instanceState: [...instance.instanceState],
                machineIds: [machine.id],
              });
            } else {
              instance.instanceState.forEach((state) => {
                if (!knownInstance.instanceState.includes(state)) {
                  knownInstance.instanceState.push(state);
                }
              });
              knownInstance.machineIds.push(machine.id);
            }
          });
        } else {
          deployedProcesses.push({
            ...deployment,
            machineIds: [machine.id],
            instances: deployment.instances.map((instance) => ({
              ...instance,
              machineIds: [machine.id],
            })),
          });
        }
      }
    }

    return deployedProcesses;
  }

  const getters = {
    machines(state) {
      return state.machines;
    },
    savedMachines(state) {
      return state.machines.filter((machine) => !!machine.saved);
    },
    discoveredMachines(state) {
      return state.machines.filter((machine) => !machine.saved);
    },
    machineById(state) {
      return (id) =>
        R.find(R.propEq('id', id), state.machines)
          ? R.find(R.propEq('id', id), state.machines)
          : R.find(R.propEq('internalId', id), state.machines);
    },
    machineByHost(state) {
      return (host) => R.find(R.propEq('host', host), state.machines);
    },
    color() {
      return (id) => colorScale(id);
    },
    deployments(state) {
      return state.deployments;
    },
  };

  const actions = {
    async loadMachines({ state, commit, dispatch }) {
      const machines = await dataInterface.getMachines();

      eventHandler.on('machinesChanged', ({ machines }) => {
        commit('setMachines', [...machines]);
        dispatch('calculateDeployments');
      });

      eventHandler.on('machineAdded', ({ machine }) => {
        commit('add', machine);
        dispatch('calculateDeployments');
      });

      eventHandler.on('machineRemoved', ({ machineId }) => {
        commit('remove', machineId);
        dispatch('calculateDeployments');
      });

      eventHandler.on('machineUpdated', ({ oldId, updatedInfo }) => {
        commit('update', { machineId: oldId, machine: updatedInfo });
        dispatch('calculateDeployments');
      });

      commit('setMachines', machines);
      dispatch('calculateDeployments');
    },
    async add(_, { machine }) {
      // machines are handled in the backend and then pushed to the frontend
      await dataInterface.addMachine(machine);
    },
    async remove(_, { id }) {
      // machines are handled in the backend and then pushed to the frontend
      await dataInterface.removeMachine(id);
    },
    async update(_, { machine }) {
      // machines are handled in the backend and then pushed to the frontend
      await dataInterface.updateMachine(machine.id, machine);
    },
    calculateDeployments({ commit, state }) {
      const deployments = mapMachinesToDeployments(state.machines);
      commit('setDeployments', deployments);
    },
  };

  const mutations = {
    setDeployments(state, deployments) {
      state.deployments = deployments;
    },
    setMachines(state, machines) {
      state.machines = machines;
    },
    add(state, machine) {
      if (state.machines.every((m) => m.id !== machine.id)) {
        state.machines = [...state.machines, machine];
      }
    },
    remove(state, id) {
      state.machines = state.machines.filter((machine) => machine.id !== id);
    },
    update(state, { machine, machineId }) {
      const foundMachineIdx = state.machines.findIndex((machine) => machine.id === machineId);
      if (foundMachineIdx > -1) {
        state.machines = [
          ...R.take(foundMachineIdx, state.machines),
          machine,
          ...R.drop(foundMachineIdx + 1, state.machines),
        ];
      }
    },
  };

  return {
    namespaced: true,
    state: initialState,
    getters,
    actions,
    mutations,
  };
}
