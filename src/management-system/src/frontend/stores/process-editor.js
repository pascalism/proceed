import * as R from 'ramda';
import { processInterface, eventHandler } from '@/backend-api/index.js';
import { getName } from '@proceed/bpmn-helper';
import { initXml } from '@/../shared-frontend-backend/helpers/processHelpers.js';

export default function createProcessEditorStore() {
  const initialState = {
    xml: initXml(),
    processName: null,
    selectedElements: [],
    elementCapabilityMapping: {},
    taskHtmlMapping: {},
    library: {
      data: '',
      capabilities: [],
    },
    processDefinitionsId: null,
  };

  const getters = {
    selectedElements(state) {
      return state.selectedElements;
    },
    xml(state) {
      return state.xml;
    },
    id(state) {
      return state.processDefinitionsId;
    },
    processName(state) {
      return state.processName;
    },
    elementCapabilityMapping(state) {
      return state.elementCapabilityMapping;
    },
    capabilitiesOfElement(state) {
      return (id) => {
        if (state.elementCapabilityMapping[id] === undefined) {
          return [];
        }
        return state.elementCapabilityMapping[id];
      };
    },
    taskHtmlOfElement(state) {
      return (id) => state.taskHtmlMapping[id] || '';
    },
    taskHtmlMapping(state) {
      return state.taskHtmlMapping;
    },
    library(state) {
      return state.library;
    },
  };

  const actions = {
    init({ commit, state }) {
      eventHandler.on('processTaskHtmlChanged', ({ processDefinitionsId, taskId, newHtml }) => {
        if (processDefinitionsId === state.processDefinitionsId) {
          // if newHtml is null the task was deleted
          if (newHtml) {
            commit('setTaskHtmlMapping', { elementID: taskId, html: newHtml });
          } else {
            commit('removeTaskFromHtmlMapping', { elementID: taskId });
          }
        }
      });
    },

    async loadProcessFromStore({ commit, dispatch }, { process }) {
      commit('setProcessDefinitionsId', process.id);
      const taskHtmlMapping = await processInterface.getUserTasksHTML(process.id);
      commit('setTaskHtmlMapping', { taskHtmlMapping });

      const xml = (await processInterface.getProcess(process.id)).bpmn;
      await dispatch('setXml', { xml });
    },
    async setXml({ commit, dispatch }, { xml }) {
      commit('setXml', { xml });
      const name = await getName(xml);
      dispatch('setProcessName', { name });
    },
    setProcessName({ commit }, { name }) {
      commit('setName', name);
    },
    selectElement({ commit }, { element }) {
      commit('selectElement', { element });
    },

    unselectElement({ commit }, { id }) {
      commit('unselectElement', { id });
    },
    selectElements({ commit }, { elements }) {
      commit('selectElements', { elements });
    },
    unselectElements({ commit }, { ids }) {
      commit('unselectElements', { ids });
    },

    clearSelection({ commit }) {
      commit('clearSelection');
    },

    setElementCapabilityMapping({ commit }, { elementId, capabilities, elementCapabilityMapping }) {
      commit('setElementCapabilityMapping', { elementId, capabilities, elementCapabilityMapping });
    },
    setTaskHtmlMapping({ commit }, { elementID, html, taskHtmlMapping }) {
      commit('setTaskHtmlMapping', { elementID, html, taskHtmlMapping });
    },
    setScriptOfElement({ state }, { script, elId, elType, change }) {
      processInterface.broadcastScriptChangeEvent(
        state.processDefinitionsId,
        elId,
        elType,
        script,
        change
      );
    },
    setLibrary({ commit }, { library }) {
      commit('setLibrary', { library });
    },
  };

  const mutations = {
    setProcessDefinitionsId(state, processDefinitionsId) {
      state.processDefinitionsId = processDefinitionsId;
    },
    setName(state, name) {
      state.processName = name;
    },
    setXml(state, { xml }) {
      state.xml = xml;
    },

    selectElement(state, { element }) {
      state.selectedElements = R.append(element, state.selectedElements);
    },

    unselectElement(state, { id }) {
      state.selectedElements = R.reject(R.propEq('id', id), state.selectedElements);
    },

    selectElements(state, { elements }) {
      state.selectedElements = [...state.selectedElements, ...elements];
    },

    unselectElements(_, { ids }) {
      ids.forEach((id) => this.commit('unselectElement', { id }));
    },

    clearSelection(state) {
      state.selectedElements = [];
    },

    setElementCapabilityMapping(state, { elementId, capabilities, elementCapabilityMapping }) {
      if (elementCapabilityMapping) {
        state.elementCapabilityMapping = elementCapabilityMapping;
      } else if (elementId && capabilities) {
        state.elementCapabilityMapping = R.assoc(
          elementId,
          capabilities,
          state.elementCapabilityMapping
        );
      } else {
        state.elementCapabilityMapping = {};
      }
    },
    setTaskHtmlMapping(state, { elementID, html, taskHtmlMapping }) {
      if (taskHtmlMapping) {
        state.taskHtmlMapping = taskHtmlMapping;
      } else if (elementID && html) {
        state.taskHtmlMapping = R.assoc(elementID, html, state.taskHtmlMapping);
      } else {
        state.taskHtmlMapping = {};
      }
    },
    removeTaskFromHtmlMapping(state, { elementID }) {
      if (state.taskHtmlMapping[elementID]) {
        delete state.taskHtmlMapping[elementID];
      }
    },
    reset(state) {
      state.xml = initXml();
      state.selectedElements = [];
    },
    setLibrary(state, { library }) {
      state.library = library || {
        data: '',
        capabilities: [],
      };
    },
  };

  return {
    namespaced: true,
    state: initialState,
    getters,
    actions,
    mutations,
  };
}
