import * as R from 'ramda';
import { processInterface, eventHandler } from '@/backend-api/index.js';
import {
  createProcess,
  getProcessInfo,
} from '../../shared-frontend-backend/helpers/processHelpers.js';

export default function createProcessStore() {
  const initialState = {
    processes: [],
  };

  const getters = {
    processes(state) {
      return state.processes;
    },
    processById(state) {
      return (id) => R.find(R.propEq('id', id))(state.processes);
    },
    xmlById(state, getters) {
      return async (id) => (await processInterface.getProcess(id)).bpmn;
    },
  };

  const actions = {
    async loadProcesses({ commit, state }) {
      const processes = await processInterface.getProcesses();
      commit('setProcesses', processes);

      eventHandler.on('processAdded', ({ process }) => {
        if (!state.processes.some((p) => p.id === process.id)) {
          commit('add', { process });
        }
      });

      eventHandler.on('processRemoved', ({ processDefinitionsId }) => {
        commit('remove', { processDefinitionsId });
      });

      eventHandler.on('processUpdated', ({ oldId, updatedInfo }) => {
        const index = state.processes.findIndex((p) => p.id === oldId);

        if (index > -1) {
          commit('update', { foundProcessIdx: index, process: updatedInfo });
        }
      });
    },
    async add({ state, commit }, { process, bpmn }) {
      if (!R.find(R.propEq('id', process.id), state.processes)) {
        try {
          process = await createProcess({ ...process, bpmn });
          bpmn = process.bpmn;
          delete process.bpmn;
        } catch (error) {
          console.error(error);
        }

        commit('add', { process });
        await processInterface.addProcess({ ...process, bpmn }, process.local);
      }
    },
    async remove({ state, commit }, { id }) {
      const foundProcess = R.find(R.propEq('id', id), state.processes);

      if (foundProcess) {
        const newProcessList = R.reject(R.propEq('id', id), state.processes);

        commit('setProcesses', newProcessList);
        await processInterface.removeProcess(id);
      }
    },
    async update({ state, commit }, { process }) {
      const foundProcessIdx = R.findIndex(R.propEq('id', process.id), state.processes);

      if (foundProcessIdx >= 0) {
        const currentDate = new Date().toUTCString();
        process.createdOn = process.createdOn ? process.createdOn : currentDate; // available for importing deprecated processes
        process.lastEdited = currentDate;

        const foundProcess = state.processes[foundProcessIdx];

        // if the process is supposed to be moved into the backend
        if (!process.local && foundProcess.local) {
          // get current bpmn
          const { bpmn } = await processInterface.getProcess(process.id);
          // get data for all user tasks
          const userTasksMapping = await processInterface.getUserTasksHTML(process.id);
          // try adding process to backend and remove it from browserStorage if it worked
          delete process.local;
          await processInterface.addProcess({ bpmn, id: process.id });
          await processInterface.removeProcess(process.id);
          // send user task data to backend after process was added
          Object.entries(userTasksMapping).forEach(([fileName, html]) => {
            processInterface.saveUserTaskHTML(process.id, fileName, html);
          });
        } else if (process.local && !foundProcess.local) {
          throw new Error('Moving from backend to local storage currently not allowed!');
        } else {
          // used so we are able to update information not in the bpmn (like departments, variables) in the backend
          await processInterface.updateProcessMetaData(process.id, process, foundProcess.local);
        }

        commit('update', { foundProcessIdx, process });
      }
    },
    async updateBpmn({ state, dispatch }, { id, bpmn }) {
      const foundProcess = R.find(R.propEq('id', id), state.processes);

      if (foundProcess) {
        // get new info from bpmn
        const newData = await getProcessInfo(bpmn);

        if (process.env.IS_ELECTRON || foundProcess.local) {
          processInterface.updateProcess(id, { bpmn });
          await dispatch('update', {
            process: {
              ...foundProcess,
              ...newData,
            },
          });
        }
        return bpmn;
      }
    },
    updateWholeXml({ state }, { id, bpmn }) {
      const foundProcess = R.find(R.propEq('id', id), state.processes);
      if (foundProcess) {
        processInterface.updateWholeXml(id, { bpmn });
      }
    },
    async updateProcessName({ commit, state }, { id, name }) {
      const foundProcess = R.find(R.propEq('id', id), state.processes);
      const foundProcessIdx = R.findIndex(R.propEq('id', id), state.processes);
      if (foundProcess) {
        await processInterface.updateProcessName(id, name);
        const process = {
          ...foundProcess,
          lastEdited: new Date().toUTCString(),
          name,
        };
        commit('update', { foundProcessIdx, process });
      }
    },

    async updateProcessDescription({ dispatch, state }, { id, description }) {
      const foundProcess = R.find(R.propEq('id', id), state.processes);
      if (foundProcess) {
        await processInterface.updateProcessDescription(
          id,
          foundProcess.processIds[0],
          description
        );
        foundProcess.description = description;
        dispatch('update', { process: foundProcess });
      }
    },

    updateSubprocessInfo({ state, dispatch }, { processDefinitionsId, subprocesses }) {
      const foundProcess = R.find(R.propEq('id', processDefinitionsId), state.processes);
      if (foundProcess) {
        foundProcess.subprocesses = subprocesses;
        dispatch('update', { process: foundProcess });
      }
    },
    async updateConstraints({ state }, { processDefinitionsId, elementId, constraints }) {
      // only send constraint updates in the server version
      if (!process.env.IS_ELECTRON) {
        const foundProcess = R.find(R.propEq('id', processDefinitionsId), state.processes);

        // only send process updates for processes that are stored on the server
        if (foundProcess && !foundProcess.local) {
          processInterface.updateConstraints(processDefinitionsId, elementId, constraints);
        }
      }
    },
    updateVariables({ state, dispatch }, { id, variables }) {
      const foundProcess = R.find(R.propEq('id', id), state.processes);
      if (foundProcess && variables) {
        foundProcess.variables = variables;
        dispatch('update', { process: foundProcess });
      }
    },
    saveUserTask({ state }, { processDefinitionsId, taskFileName, html }) {
      const foundProcess = R.find(R.propEq('id', processDefinitionsId), state.processes);

      if (foundProcess) {
        processInterface.saveUserTaskHTML(processDefinitionsId, taskFileName, html);
      }
    },
    deleteUserTask({ state }, { processDefinitionsId, taskId }) {
      const foundProcess = R.find(R.propEq('id', processDefinitionsId), state.processes);
      if (foundProcess) {
        processInterface.deleteUserTaskHTML(processDefinitionsId, taskId);
      }
    },
    startObservingEditing({ state }, { id }) {
      const foundProcess = R.find(R.propEq('id', id), state.processes);
      if (foundProcess && !foundProcess.local) {
        processInterface.observeProcessEditing(id);
      }
    },
    stopObservingEditing({ state }, { id }) {
      const foundProcess = R.find(R.propEq('id', id), state.processes);
      if (foundProcess && !foundProcess.local) {
        processInterface.stopObservingProcessEditing(id);
      }
    },
    startEditing({ state }, { id }) {
      const foundProcess = R.find(R.propEq('id', id), state.processes);
      if (foundProcess && !foundProcess.local) {
        processInterface.blockProcess(id);
      }
    },
    stopEditing({ state }, { id }) {
      const foundProcess = R.find(R.propEq('id', id), state.processes);
      if (foundProcess && !foundProcess.local) {
        processInterface.unblockProcess(id);
      }
    },
    startEditingTask({ state }, { processDefinitionsId, taskId }) {
      const foundProcess = R.find(R.propEq('id', processDefinitionsId), state.processes);
      if (foundProcess && !foundProcess.local && taskId) {
        processInterface.blockTask(processDefinitionsId, taskId);
      }
    },

    stopEditingTask({ state }, { processDefinitionsId, taskId }) {
      const foundProcess = R.find(R.propEq('id', processDefinitionsId), state.processes);
      if (foundProcess && !foundProcess.local && taskId) {
        processInterface.unblockTask(processDefinitionsId, taskId);
      }
    },
  };

  const mutations = {
    setProcesses(state, processes) {
      state.processes = processes;
    },

    add(state, { process }) {
      state.processes = [...state.processes, process];
    },

    remove(state, { processDefinitionsId }) {
      state.processes = state.processes.filter((p) => p.id !== processDefinitionsId);
    },

    update(state, { process, foundProcessIdx }) {
      state.processes = [
        ...R.take(foundProcessIdx, state.processes),
        process,
        ...R.drop(foundProcessIdx + 1, state.processes),
      ];
    },
  };

  return {
    namespaced: true,
    state: initialState,
    getters,
    actions,
    mutations,
  };
}
