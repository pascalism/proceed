const uuid = require('uuid');
const IdsModule = require('ids');
const Ids = IdsModule.default || IdsModule;
const {
  toObject,
  toXML,
  getElementsByTagName,
  setStandardDefinitionsOnDefinitionsObject,
  addDocumentationToProcessObject,
  getProcessDescriptionFromObject,
} = require('@proceed/bpmn-helper');

const { getProcessHierarchy } = require('../../frontend/helpers/process-hierarchy.js');

const idGenerator = new Ids([32, 36, 1]);

function initXml(processId = 'Process_1', startEventId = 'StartEvent_1') {
  const bpmn = `
  <?xml version="1.0" encoding="UTF-8"?>
  <definitions
      xmlns="http://www.omg.org/spec/BPMN/20100524/MODEL"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI"
      xmlns:dc="http://www.omg.org/spec/DD/20100524/DC"
      xmlns:di="http://www.omg.org/spec/DD/20100524/DI"
  >
    <Process id="${processId}" name="PROCEED Main Process" processType="Private" isExecutable="true">
      <startEvent id="${startEventId}"/>
    </Process>
    <bpmndi:BPMNDiagram id="BPMNDiagram_1">
      <bpmndi:BPMNPlane id="BPMNPlane_1" bpmnElement="${processId}">
        <bpmndi:BPMNShape id="${startEventId}_di" bpmnElement="${startEventId}">
          <dc:Bounds height="36.0" width="36.0" x="350.0" y="200.0"/>
        </bpmndi:BPMNShape>
      </bpmndi:BPMNPlane>
    </bpmndi:BPMNDiagram>
  </definitions>
`;
  return bpmn;
}

async function createProcess(processInfo) {
  const metaInfo = { ...processInfo };

  const id = processInfo.id || `_${uuid.v4()}`;
  metaInfo.id = id;

  let definitions;

  // if user doesn't provide a bpmn create with base bpmn
  if (!processInfo.bpmn) {
    const xmlObj = await toObject(
      initXml(`Process_${idGenerator.next()}`, `StartEvent_${idGenerator.next()}`)
    );

    [definitions] = getElementsByTagName(xmlObj, 'bpmn:Definitions');
  } else {
    try {
      const xmlObj = await toObject(processInfo.bpmn);

      [definitions] = getElementsByTagName(xmlObj, 'bpmn:Definitions');
    } catch (err) {
      throw new Error(`Invalid bpmn: ${err}`);
    }

    if (!processInfo.name) {
      // try to parse name from bpmn
      metaInfo.name = definitions.name;
    }
  }

  setStandardDefinitionsOnDefinitionsObject(definitions);

  if (!metaInfo.name) {
    throw new Error(
      'No name provided (name can be provided in the general information or in the definitions of the given bpmn)'
    );
  }

  if (definitions.id && definitions.id !== id) {
    definitions.originalId = definitions.id;
  }

  definitions.id = id;
  definitions.name = metaInfo.name;

  const targetNamespace = `https://docs.proceed-labs.org/${id}`;

  if (definitions.targetNamespace && definitions.targetNamespace !== targetNamespace) {
    definitions.originalTargetNamespace = definitions.targetNamespace;
  }

  definitions.targetNamespace = targetNamespace;

  const processes = getElementsByTagName(definitions, 'bpmn:Process');

  // make sure every process has an id
  processes.forEach((p) => {
    if (!p.id) {
      p.id = `Process_${idGenerator.next()}`;
    }
  });

  metaInfo.processIds = processes.map((p) => p.id);

  const [process] = processes;

  // if the user gave a process description make sure to write it into bpmn
  if (process && processInfo.description) {
    addDocumentationToProcessObject(process, processInfo.description);
  }

  metaInfo.description = getProcessDescriptionFromObject(process);

  metaInfo.bpmn = await toXML(definitions);

  const currentDate = new Date().toUTCString();
  metaInfo.createdOn = metaInfo.createdOn || currentDate;
  metaInfo.lastEdited = currentDate;

  return metaInfo;
}

/**
 * Parses all necessary information from a process description
 *
 * @param {String} bpmn the xml process description
 */
async function getProcessInfo(bpmn) {
  if (!bpmn || typeof bpmn !== 'string') {
    throw new Error(`Expected given bpmn to be of type string but got ${typeof bpmn} instead!`);
  }

  let definitions;
  try {
    definitions = await toObject(bpmn);
  } catch (err) {
    throw new Error(`Given process description is invalid. Reason:\n${err}`);
  }

  const metadata = {};

  const { id, name } = definitions;

  if (!id) {
    throw new Error('Process definitions do not contain an id.');
  }

  metadata.id = id;

  if (!name) {
    throw new Error('Process definitions do not contain a name.');
  }

  metadata.name = name;

  const processes = getElementsByTagName(definitions, 'bpmn:Process');

  metadata.processIds = processes.map((p) => p.id);

  metadata.description = getProcessDescriptionFromObject(processes[0]);

  metadata.subprocesses = await getProcessHierarchy(definitions);

  return metadata;
}

module.exports = { initXml, createProcess, getProcessInfo };
