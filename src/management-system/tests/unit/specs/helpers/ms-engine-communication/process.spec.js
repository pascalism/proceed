import mockNetworkFactory from '../../../../mocks/network.js';

const mockNetwork = mockNetworkFactory(jest);

let mockResponse;

mockNetwork.sendRequest.mockImplementation(() =>
  Promise.resolve({ body: JSON.stringify(mockResponse) })
);

mockNetwork.sendData.mockImplementation(() =>
  Promise.resolve({ body: JSON.stringify(mockResponse) })
);

let RequestFuncs;
describe('Communication with /process/** endpoints', () => {
  beforeAll(async () => {
    RequestFuncs = await import(
      '@/../backend/shared-electron-server/network/ms-engine-communication/process.js'
    );
  });

  let mockMachine;
  beforeEach(async () => {
    mockMachine = { host: '192.168.1.1', port: 33029 };
    mockResponse = { msg: 'Mock Response' };
  });

  describe('getDeployedProcesses', () => {
    it('returns information about all processes deployed on this engine', async () => {
      mockResponse = [
        { definitionId: 'process1', deploymentDate: 1 },
        { definitionId: 'process2', deploymentDate: 2 },
      ];
      expect(await RequestFuncs.getDeployedProcesses(mockMachine)).toEqual(mockResponse);
      expect(mockNetwork.sendRequest).toHaveBeenCalledWith(
        mockMachine.host,
        mockMachine.port,
        '/process'
      );
    });
  });

  describe('getDeploymentBPMN', () => {
    it('returns the bpmn of a process with the given id from the given machine', async () => {
      mockResponse = { bpmn: 'Test BPMN' };
      expect(await RequestFuncs.getDeploymentBPMN(mockMachine, 'processId')).toEqual(
        mockResponse.bpmn
      );
      expect(mockNetwork.sendRequest).toHaveBeenCalledWith(
        mockMachine.host,
        mockMachine.port,
        '/process/processId'
      );
    });
  });

  describe('deployProcess', () => {
    it('deploys process with bpmn', async () => {
      await RequestFuncs.deployProcess(mockMachine, 'processId', 'processBPMN');
      expect(mockNetwork.sendData).toHaveBeenCalledWith(
        mockMachine.host,
        mockMachine.port,
        '/process/processId',
        'PUT',
        'application/json',
        { bpmn: 'processBPMN' }
      );
    });
  });

  describe('deleteDeployment', () => {
    it('removes deployment information for process with given id from engine', async () => {
      await RequestFuncs.removeDeployment(mockMachine, 'processId');
      expect(mockNetwork.sendRequest).toHaveBeenCalledWith(
        mockMachine.host,
        mockMachine.port,
        '/process/processId/',
        { method: 'DELETE' }
      );
    });
  });

  describe('sendUserTaskHtml', () => {
    it('sends the HTML of a User Task', async () => {
      await RequestFuncs.sendUserTaskHTML(mockMachine, 'processId', 'userTaskId', 'HTML');
      expect(mockNetwork.sendData).toHaveBeenCalledWith(
        mockMachine.host,
        mockMachine.port,
        '/process/processId/user-tasks/userTaskId',
        'PUT',
        'application/json',
        { html: 'HTML' }
      );
    });
  });

  describe('getProcessInstances', () => {
    it('returns list with instance information objects for all instances of a process', async () => {
      mockResponse = [
        { id: '1', status: 'Finished' },
        { id: '2', status: 'Running' },
      ];

      expect(await RequestFuncs.getProcessInstances(mockMachine, 'processId')).toEqual(
        mockResponse
      );
      expect(mockNetwork.sendRequest).toHaveBeenCalledWith(
        mockMachine.host,
        mockMachine.port,
        '/process/processId/instance'
      );
    });
  });

  describe('startProcessInstance', () => {
    it('sends request to start an instance and returns the repsonded instance id', async () => {
      mockResponse = {
        instanceId: 'instanceId',
      };

      expect(await RequestFuncs.startProcessInstance(mockMachine, 'processId', {})).toEqual(
        mockResponse.instanceId
      );
      expect(mockNetwork.sendData).toHaveBeenCalledWith(
        mockMachine.host,
        mockMachine.port,
        '/process/processId/instance',
        'POST',
        'application/json',
        { variables: {} }
      );
    });
  });

  describe('getInstanceInformation', () => {
    it('requests information for a certain instance (status, tokens, ...)', async () => {
      mockResponse = {
        status: 'Running',
        tokens: ['Token1', 'Token2'],
      };

      expect(
        await RequestFuncs.getInstanceInformation(mockMachine, 'processId', 'instanceId')
      ).toEqual(mockResponse);
      expect(mockNetwork.sendRequest).toHaveBeenCalledWith(
        mockMachine.host,
        mockMachine.port,
        '/process/processId/instance/instanceId'
      );
    });
  });

  describe('stopProcessInstance', () => {
    it('sends request to stop an instance', async () => {
      await RequestFuncs.stopProcessInstance(mockMachine, 'processId', 'instanceId');
      expect(mockNetwork.sendData).toHaveBeenCalledWith(
        mockMachine.host,
        mockMachine.port,
        '/process/processId/instance/instanceId/instanceState',
        'PUT',
        'application/json',
        { instanceState: 'stopped' }
      );
    });
  });
});
