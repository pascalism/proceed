import * as processHelpers from '@/../shared-frontend-backend/helpers/processHelpers.js';
import { getBpmn } from '../../../helpers/processHelpers.js';

jest.mock('uuid', () => ({
  calls: 0,
  v4: jest.fn().mockImplementation(function () {
    return `uniqueId${++this.calls}`;
  }),
}));

let mockCalls = 0;
jest.mock('ids', () => {
  return jest.fn().mockImplementation(() => ({
    next: jest.fn().mockImplementation(function () {
      return mockCalls++;
    }),
  }));
});

import uuid from 'uuid';

describe('Tests for process helpers', () => {
  describe('createProcess', () => {
    let exampleProcess;

    beforeEach(async () => {
      jest.clearAllMocks();
      uuid.calls = 0;
      mockCalls = 0;
      exampleProcess = await processHelpers.createProcess({ name: 'example' });
    });

    it('creates a unique process id', () => {
      expect(exampleProcess).toHaveProperty('id', '_uniqueId1');
    });

    it('keeps user defined name', () => {
      expect(exampleProcess).toHaveProperty('name', 'example');
    });

    it('throws when no name and no bpmn is given', async () => {
      await expect(processHelpers.createProcess({})).rejects.toThrow('No name provided');
    });

    it('creates a base bpmn with the given name the created id and a unique id for the contained process, processId is added to metaInfo', async () => {
      expect(exampleProcess).toHaveProperty(
        'bpmn',
        await getBpmn({
          id: '_uniqueId1',
          name: 'example',
          processId: 'Process_0',
          standardDefinitions: true,
        })
      );
      expect(exampleProcess).toHaveProperty('processIds', ['Process_0']);
    });

    it('accepts an already existing bpmn and adds created id, and created processId', async () => {
      uuid.v4.mockReturnValueOnce('definitionsId');
      const result = await processHelpers.createProcess({
        bpmn: await getBpmn({ name: 'someName' }),
      });
      expect(result.bpmn).toEqual(
        await getBpmn({
          id: '_definitionsId',
          name: 'someName',
          processId: 'Process_1',
          standardDefinitions: true,
        })
      );
    });

    it("doesn't create new processId if one already exists", async () => {
      uuid.v4.mockReturnValueOnce('definitionsId');
      const result = await processHelpers.createProcess({
        bpmn: await getBpmn({ name: 'someName', processId: 'someUserDefinedId' }),
      });
      expect(result.bpmn).toEqual(
        await getBpmn({
          id: '_definitionsId',
          name: 'someName',
          processId: 'someUserDefinedId',
          standardDefinitions: true,
        })
      );
    });

    it('parses name from given bpmn', async () => {
      const result = await processHelpers.createProcess({
        bpmn: await getBpmn({ id: 'someId', name: 'Test123' }),
      });

      expect(result).toHaveProperty('name', 'Test123');
    });

    it('throws if no name is provided and none is found in the given bpmn', async () => {
      await expect(
        processHelpers.createProcess({ bpmn: await getBpmn({ id: 'someId' }) })
      ).rejects.toThrow(expect.any(Error));
    });

    it('throws on invalid bpmn', async () => {
      await expect(
        processHelpers.createProcess({ bpmn: 'something thats not a correct bpmn' })
      ).rejects.toThrow(expect.any(Error));
    });

    it('creates entries for creation date and lastEdit date which should be the same', async () => {
      expect(exampleProcess).toHaveProperty('createdOn');
      expect(exampleProcess).toHaveProperty('lastEdited');
      expect(exampleProcess.createdOn).toEqual(exampleProcess.lastEdited);
    });

    it('adds given description to process', async () => {
      uuid.v4.mockReturnValueOnce('definitionsId');
      const result = await processHelpers.createProcess({
        name: 'example',
        description: 'description',
      });
      expect(result.bpmn).toEqual(
        await getBpmn({
          id: '_definitionsId',
          name: 'example',
          processId: 'Process_2',
          startEventId: 'StartEvent_3',
          processDescription: 'description',
          standardDefinitions: true,
        })
      );
    });

    it('parses an existing description from the bpmn if one exists', async () => {
      const result = await processHelpers.createProcess({
        bpmn: await getBpmn({ name: 'test', processDescription: 'someDescription' }),
      });
      expect(result).toHaveProperty('description', 'someDescription');
    });
  });

  describe('getProcessInfo', () => {
    let result;
    beforeEach(async () => {
      result = await processHelpers.getProcessInfo(
        await getBpmn({
          id: 'testId',
          name: 'testName',
          processId: 'testProcess',
          processDescription: 'testDescription',
        })
      );
    });

    it('throws when given no input or non string input', async () => {
      await expect(processHelpers.getProcessInfo()).rejects.toThrow(
        'Expected given bpmn to be of type string but got undefined instead!'
      );
      await expect(processHelpers.getProcessInfo({})).rejects.toThrow(
        'Expected given bpmn to be of type string but got object instead!'
      );
    });

    it('parses the definitions id and name from a given process description', () => {
      expect(result).toHaveProperty('id', 'testId');
      expect(result).toHaveProperty('name', 'testName');
    });

    it("throws if process definitions don't contain an id", async () => {
      await expect(processHelpers.getProcessInfo(await getBpmn({}))).rejects.toThrow(
        'Process definitions do not contain an id.'
      );
    });

    it("throws if process definitions don't contain a name", async () => {
      await expect(processHelpers.getProcessInfo(await getBpmn({ id: 'testId' }))).rejects.toThrow(
        'Process definitions do not contain a name.'
      );
    });

    it('parses the id of the contained process', () => {
      expect(result).toHaveProperty('processIds', ['testProcess']);
    });

    it('parses the description of the contained process', async () => {
      expect(result).toHaveProperty('description', 'testDescription');
    });

    it('parses called processes', async () => {
      const bpmn = `
<definitions
    xmlns="http://www.omg.org/spec/BPMN/20100524/MODEL"
    xmlns:p33c24="https://docs.proceed-labs.org/_17c6fed0-8a8c-4722-a62f-86ebf1324c33"
    id="_e292e6c4-4d7f-4aff-b91f-c102d5ea4ae8"
    name="Global Task Process"
    targetNamespace="https://docs.proceed-labs.org/_e292e6c4-4d7f-4aff-b91f-c102d5ea4ae8"
    >
    <import namespace="https://docs.proceed-labs.org/_17c6fed0-8a8c-4722-a62f-86ebf1324c33" location="Other-Process-17c6fed0" importType="http://www.omg.org/spec/BPMN/20100524/MODEL"/>
    <process id="Process_1">
        <callActivity id="Task_0ul33bj" name="Call the global process" calledElement="p33c24:_e069937f-27b6-464b-b397-b88a2599f1b9">
        </callActivity>
    </process>
</definitions>`;
      const { subprocesses } = await processHelpers.getProcessInfo(bpmn);
      expect(subprocesses).toStrictEqual([
        {
          id: '_17c6fed0-8a8c-4722-a62f-86ebf1324c33',
          isCallActivity: true,
          name: 'Call the global process',
        },
      ]);
    });
  });
});
