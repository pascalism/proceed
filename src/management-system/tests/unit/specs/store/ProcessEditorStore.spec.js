import Vue from 'vue';
import Vuex from 'vuex';

import createProcessEditorStore from '@/stores/process-editor.js';
import { initXml } from '@/../shared-frontend-backend/helpers/processHelpers.js';

Vue.use(Vuex);

/** importing vuetify/dist/vuetify.min.css causes a problem */
let store;
describe('Process Editor Store', () => {
  test('Bla', () => {});
  beforeEach(() => {
    store = new Vuex.Store(createProcessEditorStore());
  });

  describe('init state', () => {
    test('should have empty selectedMachines', () => {
      expect(store.getters.selectedElements).toHaveLength(0);
    });

    test('should have init xml', () => {
      expect(store.getters.xml).toEqual(initXml());
    });
  });

  describe('actions', () => {
    describe('element selection', () => {
      test('it should set xml', () => {
        store.dispatch('setXml', { xml: 'my-custom-xml' });
        expect(store.getters.xml).toEqual('my-custom-xml');
      });

      test('it should select an element', () => {
        store.dispatch('selectElement', { element: { id: '1', name: 'my-element' } });
        expect(store.getters.selectedElements).toHaveLength(1);
        expect(store.getters.selectedElements).toContainEqual({ id: '1', name: 'my-element' });
      });

      test('it should unselect an element', () => {
        store.dispatch('selectElement', { element: { id: '1', name: 'my-element-1' } });
        store.dispatch('selectElement', { element: { id: '2', name: 'my-element-2' } });
        store.dispatch('unselectElement', { id: '1' });
        expect(store.getters.selectedElements).toHaveLength(1);
        expect(store.getters.selectedElements).toContainEqual({ id: '2', name: 'my-element-2' });
      });

      test('it should add multiple selected elements', () => {
        store.dispatch('selectElements', {
          elements: [
            { id: '1', name: 'my-element-1' },
            { id: '2', name: 'my-element-2' },
          ],
        });
        expect(store.getters.selectedElements).toHaveLength(2);
        expect(store.getters.selectedElements).toContainEqual({ id: '1', name: 'my-element-1' });
        expect(store.getters.selectedElements).toContainEqual({ id: '2', name: 'my-element-2' });
      });

      test('it should remove multiple selected elements', () => {
        store.dispatch('selectElements', {
          elements: [
            { id: '1', name: 'my-element-1' },
            { id: '2', name: 'my-element-2' },
            { id: '3', name: 'my-element-3' },
          ],
        });
        store.dispatch('unselectElements', { ids: ['1', '3'] });
        expect(store.getters.selectedElements).toHaveLength(1);
        expect(store.getters.selectedElements).toContainEqual({ id: '2', name: 'my-element-2' });
      });

      test('it should clear all selection', () => {
        store.dispatch('selectElements', {
          elements: [
            { id: '1', name: 'my-element-1' },
            { id: '2', name: 'my-element-2' },
            { id: '3', name: 'my-element-3' },
          ],
        });
        store.commit('clearSelection');
        expect(store.getters.selectedElements).toHaveLength(0);
      });
    });

    describe('element capability mapping', () => {
      test('it should set element capability mapping', () => {
        store.commit('setElementCapabilityMapping', {
          elementCapabilityMapping: { 1: ['do-that', 'do-this'], 2: ['fly'] },
        });
        expect(store.getters.elementCapabilityMapping).toEqual({
          1: ['do-that', 'do-this'],
          2: ['fly'],
        });
      });

      test('it should add capabilities to element', () => {
        store.commit('setElementCapabilityMapping', {
          elementId: 'xyz',
          capabilities: ['take-photo', 'do-something'],
        });
        expect(store.getters.elementCapabilityMapping).toEqual({
          xyz: ['take-photo', 'do-something'],
        });
      });

      test('it should overwrite capabilities of element', () => {
        store.commit('setElementCapabilityMapping', {
          elementId: '1',
          capabilities: ['take-photo'],
        });
        store.commit('setElementCapabilityMapping', {
          elementId: '2',
          capabilities: ['transport-parcel'],
        });
        store.commit('setElementCapabilityMapping', {
          elementId: '1',
          capabilities: ['do-that', 'do-this'],
        });
        expect(store.getters.elementCapabilityMapping).toEqual({
          1: ['do-that', 'do-this'],
          2: ['transport-parcel'],
        });
      });
    });

    describe('reset', () => {
      test('should have empty selectedMachines', () => {
        store.commit('setXml', { xml: 'my-custom-xml' });
        store.commit('selectElements', {
          elements: [
            { id: '1', name: 'my-element-1' },
            { id: '2', name: 'my-element-2' },
            { id: '3', name: 'my-element-3' },
          ],
        });
        store.commit('reset');
        expect(store.getters.selectedElements).toHaveLength(0);
      });

      test('it should have init xml', () => {
        store.commit('setXml', { xml: 'my-custom-xml' });
        store.commit('selectElements', {
          elements: [
            { id: '1', name: 'my-element-1' },
            { id: '2', name: 'my-element-2' },
            { id: '3', name: 'my-element-3' },
          ],
        });
        store.commit('reset');
        expect(store.getters.xml).toEqual(initXml());
      });
    });
  });
});
