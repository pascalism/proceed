import uuid from 'uuid';
import Vue from 'vue';
import Vuex from 'vuex';
import ProcessInterface from '../../../mocks/ProcessInterface.js';
import { initXml, createProcess } from '@/../shared-frontend-backend/helpers/processHelpers.js';

Vue.use(Vuex);

let createProcessStore;
describe('Process store', () => {
  beforeAll(async () => {
    createProcessStore = (await import('@/stores/process.js')).default;
  });

  const createMockProcess = () => ({
    id: uuid.v4(),
    name: 'this-is-a-random-name',
    createdOn: new Date().toUTCString(),
    lastEdited: new Date().toUTCString(),
    variables: [],
    description: '',
    processIds: ['Process_1'],
  });

  const p1 = createMockProcess();
  const p2 = createMockProcess();
  const p3 = createMockProcess();

  let processStore;
  let mockProcess;
  beforeEach(async () => {
    jest.clearAllMocks();
    ProcessInterface.prototype.getProcesses.mockReturnValue([p1, p2, p3]);
    processStore = new Vuex.Store(createProcessStore());
    mockProcess = createMockProcess();
    await processStore.dispatch('loadProcesses');
  });

  describe('init state', () => {
    test('should initilize with processes from provided repository', () => {
      expect(processStore.getters.processes).toHaveLength(3);
      expect(processStore.getters.processes).toContainEqual(p1);
      expect(processStore.getters.processes).toContainEqual(p2);
      expect(processStore.getters.processes).toContainEqual(p3);
    });
  });

  describe('getters', () => {
    test('it should get process by id when exists', () => {
      expect(processStore.getters.processById(p1.id)).toEqual(p1);
    });

    test('it should return null when process by id does not exist', () => {
      expect(processStore.getters.processById(uuid.v4())).toBeUndefined();
    });
  });

  describe('actions', () => {
    describe('add process when list is empty', () => {
      beforeEach(async () => {
        ProcessInterface.prototype.getProcesses.mockReturnValueOnce([]);
        processStore = new Vuex.Store(createProcessStore());
        await processStore.dispatch('loadProcesses');
      });
      test('should have added the process', async () => {
        await processStore.dispatch('add', { process: mockProcess, bpmn: initXml() });
        expect(processStore.getters.processes).toHaveLength(1);
        expect(processStore.getters.processes).toContainEqual(mockProcess);
      });

      test('should have called the process controller with new process list', async () => {
        await processStore.dispatch('add', { process: mockProcess, bpmn: initXml() });
        expect(ProcessInterface.prototype.addProcess).toHaveBeenCalledWith(
          await createProcess({ ...mockProcess, bpmn: initXml() }),
          undefined
        );
      });
    });

    describe('add process with id conflict', () => {
      test('should not have added the process', async () => {
        await processStore.dispatch('add', { process: p1, bpmn: initXml() });
        expect(processStore.getters.processes).toHaveLength(3);
      });

      test('should not have called db store', async () => {
        await processStore.dispatch('add', { process: p1, bpmn: initXml() });
        expect(ProcessInterface.prototype.addProcess).not.toHaveBeenCalled();
      });
    });

    describe('add process to end of list', () => {
      test('should have added the process', async () => {
        await processStore.dispatch('add', { process: mockProcess, bpmn: initXml() });
        expect(processStore.getters.processes).toHaveLength(4);
        expect(processStore.getters.processes[3]).toEqual(mockProcess);
      });

      test('should have called the process controller with new process', async () => {
        await processStore.dispatch('add', { process: mockProcess, bpmn: initXml() });
        expect(ProcessInterface.prototype.addProcess).toHaveBeenCalledWith(
          await createProcess({ ...mockProcess, bpmn: initXml() }),
          undefined
        );
      });
    });

    describe('remove non-existent process', () => {
      test('should not have removed anything', async () => {
        await processStore.dispatch('remove', { id: uuid.v4() });
        expect(processStore.getters.processes).toHaveLength(3);
      });

      test('should not have called db store', async () => {
        await processStore.dispatch('remove', { id: uuid.v4() });
        expect(processStore.getters.processes).toHaveLength(3);
        expect(ProcessInterface.prototype.removeProcess).not.toHaveBeenCalled();
      });
    });

    describe('remove process with existing id', () => {
      test('should have removed the process', async () => {
        await processStore.dispatch('remove', { id: p2.id });
        expect(processStore.getters.processes).toHaveLength(2);
        expect(processStore.getters.processes).not.toContainEqual(p2);
      });

      test('should have called db to remove process with given id', async () => {
        await processStore.dispatch('remove', { id: p2.id });
        expect(ProcessInterface.prototype.removeProcess).toHaveBeenCalledWith(p2.id);
      });
    });

    describe('update process when not exists', () => {
      test('should not have removed anything', async () => {
        await processStore.dispatch('update', { process: mockProcess });
        expect(processStore.getters.processes).toHaveLength(3);
        expect(processStore.getters.processes).not.toContainEqual(mockProcess);
      });

      test('should not have called db store', async () => {
        await processStore.dispatch('update', { process: mockProcess });
        expect(ProcessInterface.prototype.updateProcessMetaData).not.toHaveBeenCalled();
      });
    });

    describe('update process if exists', () => {
      test('should have updated the process', async () => {
        mockProcess.id = p2.id;
        mockProcess.name = 'changed-name';
        mockProcess.bpmn =
          '<?xml version="1.0" encoding="UTF-8"?><bpmn2:definitions></bpmn2:definitions>`,';
        mockProcess.variables.push({ name: 'amount', type: 'number' });
        await processStore.dispatch('update', { process: mockProcess });
        expect(processStore.getters.processes).toHaveLength(3);
        expect(processStore.getters.processes).not.toContainEqual(p2);
        expect(processStore.getters.processes).toContainEqual(mockProcess);
      });

      test('should have called the db store with process update', async () => {
        mockProcess.id = p2.id;
        mockProcess.name = 'changed-name';
        mockProcess.bpmn =
          '<?xml version="1.0" encoding="UTF-8"?><bpmn2:definitions></bpmn2:definitions>`,';
        mockProcess.variables.push({ name: 'amount', type: 'number' });
        await processStore.dispatch('update', { process: mockProcess });
        expect(ProcessInterface.prototype.updateProcessMetaData).toHaveBeenCalledWith(
          mockProcess.id,
          mockProcess,
          undefined
        );
      });
    });

    describe('update process bpmn with uuid when not exists', () => {
      test('should not have updated anything', async () => {
        await processStore.dispatch('updateBpmn', {
          id: uuid.v4(),
          bpmn: '<custom-xml></custom-xml>',
        });
        expect(processStore.getters.processes).toHaveLength(3);
        expect(processStore.getters.processes).toContainEqual(p1);
        expect(processStore.getters.processes).toContainEqual(p2);
        expect(processStore.getters.processes).toContainEqual(p3);
      });

      test('should not have called the db store', async () => {
        await processStore.dispatch('updateBpmn', {
          id: uuid.v4(),
          bpmn: '<custom-xml></custom-xml>',
        });
        expect(ProcessInterface.prototype.updateProcess).not.toHaveBeenCalled();
      });
    });

    describe('update process bpmn with uuid', () => {
      test('should have updated the process', async () => {
        // set to true to test functionality for local processes and electron
        p2.local = true;
        const newBPMN = (await createProcess({ name: 'testName', bpmn: initXml() })).bpmn;
        await processStore.dispatch('updateBpmn', {
          id: p2.id,
          name: 'this-is-a-random-name',
          bpmn: newBPMN,
        });
        expect(processStore.getters.processes).toHaveLength(3);
        expect(ProcessInterface.prototype.updateProcess).toHaveBeenCalledWith(p2.id, {
          bpmn: expect.any(String),
        });
      });
    });
  });
});
