import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import fs from 'fs';
import path from 'path';
import createMachineStore from '@/stores/machine.js';
import DataInterface from '../../../mocks/DataInterface.js';

const baseBPMN = fs.readFileSync(
  path.resolve(__dirname, '../../../../data/bpmn/baseBPMN.xml'),
  'utf8'
);

jest.mock('@/main.js', () => ({
  store: {},
}));

jest.mock('@proceed/machine', () => ({
  logging: {
    getLogger: () => ({}),
  },
}));

const localVue = createLocalVue();
localVue.use(Vuex);

const mockMachine = {
  id: 'abc123',
  host: '192.168.1.1',
  port: 33029,
  status: 'CONNECTED',
  deployedProcesses: {
    xyz789: {
      definitionId: 'xyz789',
      instances: [
        {
          definitionId: 'xyz789',
          processInstanceId: 'xyz789-1',
          globalStartTime: 0,
          instanceState: ['ENDED'],
        },
      ],
    },
  },
};

describe('Deployments', () => {
  let Deployments;
  let store;
  let wrapper;
  let mockProcesses = [];
  const mockUpdateBpmn = jest.fn();

  beforeEach(async (done) => {
    Deployments = (await import('@/views/Deployments.vue')).default;
    mockUpdateBpmn.mockClear();
    mockProcesses = [{ id: 'xyz789', name: 'Test' }];

    store = new Vuex.Store({
      modules: {
        processStore: {
          getters: {
            processes: () => mockProcesses,
          },
          actions: {
            updateWholeXml: mockUpdateBpmn,
          },
          namespaced: true,
        },
        machineStore: createMachineStore(),
      },
    });
    DataInterface.prototype.get.mockReturnValue([]);
    DataInterface.prototype.set.mockReset();

    await store.commit('machineStore/add', mockMachine);
    await store.dispatch('machineStore/calculateDeployments');

    wrapper = shallowMount(Deployments, { store, localVue });

    done();
  });

  describe('computed', () => {
    it('returns processes in vuex store', () => {
      expect(wrapper.vm.processes).toEqual(mockProcesses);
    });
    it('returns object with information about to be deployed process', () => {
      wrapper.vm.deployDefinitionId = 'xyz789';
      const deployProcess = wrapper.vm.processes.find(
        (process) => process.id === wrapper.vm.deployDefinitionId
      );
      expect(deployProcess).toEqual(mockProcesses[0]);
    });
    it('returns an object containing sorted deployments of local and external processes', async (done) => {
      const newMachine = {
        id: 'def456',
        host: '192.168.1.2',
        port: 33029,
        status: 'CONNECTED',
        deployedProcesses: {
          xyz789: {
            definitionId: 'xyz789',
            instances: [
              {
                definitionId: 'xyz789',
                processInstanceId: 'xyz789-1',
                globalStartTime: 0,
                instanceState: ['RUNNING'],
              },
            ],
          },
          fgh345: {
            definitionId: 'fgh345',
            instances: [
              {
                definitionId: 'fgh345',
                processInstanceId: 'fgh345-1',
                globalStartTime: 10,
                instanceState: ['ENDED'],
              },
            ],
          },
        },
      };

      await store.commit('machineStore/add', newMachine);
      await store.dispatch('machineStore/calculateDeployments');

      await wrapper.vm.$nextTick();

      expect(wrapper.vm.sortedDeployments).toStrictEqual({
        local: [
          {
            definitionId: 'xyz789',
            instances: [
              {
                definitionId: 'xyz789',
                processInstanceId: 'xyz789-1',
                instanceState: ['ENDED', 'RUNNING'],
                globalStartTime: 0,
                machineIds: ['abc123', 'def456'],
              },
            ],
            machineIds: ['abc123', 'def456'],
          },
        ],
        external: [
          {
            definitionId: 'fgh345',
            instances: [
              {
                definitionId: 'fgh345',
                processInstanceId: 'fgh345-1',
                instanceState: ['ENDED'],
                globalStartTime: 10,
                machineIds: ['def456'],
              },
            ],
            machineIds: ['def456'],
          },
        ],
      });

      done();
    });
    it('returns an object containing information about all process deployments on all machines', async (done) => {
      await store.commit('machineStore/add', {
        id: 'def456',
        host: '192.168.1.2',
        port: 33029,
        status: 'CONNECTED',
        deployedProcesses: {
          xyz789: {
            definitionId: 'xyz789',
            instances: [
              {
                definitionId: 'xyz789',
                processInstanceId: 'xyz789-1',
                instanceState: ['RUNNING'],
                globalStartTime: 0,
              },
            ],
          },
          fgh345: {
            definitionId: 'fgh345',
            instances: [
              {
                definitionId: 'fgh345',
                processInstanceId: 'fgh345-1',
                instanceState: ['ENDED'],
                globalStartTime: 10,
              },
            ],
          },
        },
      });
      await store.dispatch('machineStore/calculateDeployments');
      wrapper.vm.$nextTick(() => {
        expect(wrapper.vm.deployedProcesses).toStrictEqual([
          {
            definitionId: 'xyz789',
            instances: [
              {
                definitionId: 'xyz789',
                processInstanceId: 'xyz789-1',
                instanceState: ['ENDED', 'RUNNING'],
                globalStartTime: 0,
                machineIds: ['abc123', 'def456'],
              },
            ],
            machineIds: ['abc123', 'def456'],
          },
          {
            definitionId: 'fgh345',
            instances: [
              {
                definitionId: 'fgh345',
                processInstanceId: 'fgh345-1',
                instanceState: ['ENDED'],
                globalStartTime: 10,
                machineIds: ['def456'],
              },
            ],
            machineIds: ['def456'],
          },
        ]);
        done();
      });
    });
  });
  describe('methods', () => {
    describe('saveMachineMapping', () => {
      it('write the given machineMapping into the process bpmn', async () => {
        wrapper.vm.deployProcessXml = baseBPMN;
        wrapper.vm.deployDefinitionId = mockProcesses[0].id;
        await wrapper.vm.saveMachineMapping({ StartEvent_1: { machineId: 'abc123' } });

        expect(mockUpdateBpmn).toHaveBeenCalledTimes(1);
        expect(mockUpdateBpmn.mock.calls[0][1].bpmn.includes('machineId="abc123"')).toBe(true);
      });
    });
  });
});
