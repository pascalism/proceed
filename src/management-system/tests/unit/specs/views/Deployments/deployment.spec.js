import fs from 'fs';
import path from 'path';

import mockNetworkFactory from '../../../../mocks/network.js';

jest.mock('@proceed/machine', () => ({
  logging: {
    getLogger: () => ({
      debug: jest.fn(),
      info: jest.fn(),
    }),
  },
}));

jest.mock('@/../backend/shared-electron-server/logging.js', () => ({
  log: jest.fn(),
  debug: jest.fn(),
}));

jest.mock('@/../backend/shared-electron-server/network/machines/machineInfo.js', () => ({
  requestDeploymentInformation: jest.fn(),
}));

let mockMachines = [];
jest.doMock('@/../backend/shared-electron-server/data/machines.js', () => ({
  getMachines: () => mockMachines,
}));

let mockProcesses = [];
let mockBPMN;
let mockHTMLMapping = {};
jest.doMock('@/../backend/shared-electron-server/data/process.js', () => ({
  getProcesses: () => mockProcesses,
  getProcessBpmn: () => mockBPMN,
  updateProcess: jest.fn(),
  getProcessUserTasksHtml: () => mockHTMLMapping,
}));

jest.doMock('@/../backend/shared-electron-server/data/fileHandling.js', () => ({
  getProcessFolder: (id, name) => {
    return `${id}`;
  },
  saveProcess: () => {},
}));

const mockFindOptimalExternalMachine = jest.fn();
jest.doMock('@proceed/decider', () => ({
  findOptimalExternalMachine: mockFindOptimalExternalMachine,
}));

const staticDeployIdXml = fs.readFileSync(
  path.resolve(__dirname, '../../../../data/bpmn/staticDeployWithIdBPMN.xml'),
  'utf8'
);
const staticDeployAddXml = fs.readFileSync(
  path.resolve(__dirname, '../../../../data/bpmn/staticDeployWithAddBPMN.xml'),
  'utf8'
);
const machineMappingXml = fs.readFileSync(
  path.resolve(__dirname, '../../../../data/bpmn/machineMappingBPMN.xml'),
  'utf8'
);

const mockNetwork = mockNetworkFactory(jest);
mockNetwork.sendRequest.mockImplementation(() => Promise.resolve({ body: '{}' }));

mockNetwork.sendData.mockImplementation(() => Promise.resolve({ body: '{}' }));

let backend;
describe('Backend deployment functions', () => {
  beforeAll(async () => {
    backend = await import('@/../backend/shared-electron-server/network/deployment.js');
  });

  beforeEach(() => {
    jest.clearAllMocks();
    mockMachines = [
      {
        id: 'abc123',
        host: '192.168.1.1',
        port: '33029',
        status: 'CONNECTED',
        deployedProcesses: {
          process1: {
            definitionId: 'process1',
            instances: [
              { id: '1', status: 'Running' },
              { id: '2', status: 'Running' },
            ],
          },
        },
      },
    ];
  });

  describe('removeDeployment', () => {
    it('sends request to remove deployment of a process from all machines the process is deployed on', async () => {
      mockMachines = mockMachines.concat([
        {
          id: 'def456',
          host: '192.168.1.2',
          port: '33021',
          status: 'CONNECTED',
          deployedProcesses: {
            process1: {
              definitionId: 'process1',
              instances: [
                { id: '1', status: 'Running' },
                { id: '2', status: 'Running' },
              ],
            },
          },
        },
        {
          id: 'ghi789',
          host: '192.168.1.3',
          port: '33022',
          status: 'CONNECTED',
        },
      ]);

      await backend.removeDeployment('process1');

      expect(mockNetwork.sendRequest).toHaveBeenCalledTimes(2);
      expect(mockNetwork.sendRequest).toHaveBeenCalledWith(
        mockMachines[0].host,
        mockMachines[0].port,
        '/process/process1/',
        { method: 'DELETE' }
      );
      expect(mockNetwork.sendRequest).toHaveBeenCalledWith(
        mockMachines[1].host,
        mockMachines[1].port,
        '/process/process1/',
        { method: 'DELETE' }
      );
    });
  });

  describe('startInstance', () => {
    beforeEach(async () => {
      mockMachines.push({
        id: 'def456',
        host: '192.168.2.1',
        port: '33029',
        status: 'CONNECTED',
      });

      mockMachines[0].deployedProcesses.process1.deploymentMethod = 'static';
      mockMachines[0].deployedProcesses.process1.bpmn = staticDeployIdXml;
    });
    it('starts an instance on the device mapped to the startProcess machineId in case of static deployment', async () => {
      await backend.startInstance('process1');

      expect(mockNetwork.sendData).toHaveBeenCalledWith(
        mockMachines[0].host,
        mockMachines[0].port,
        '/process/process1/instance',
        'POST',
        'application/json',
        { variables: {} }
      );
    });
    it('starts an instance on the device mapped to the startProcess machineAddress in case of static deployment', async () => {
      mockMachines[0].deployedProcesses.process1.bpmn = staticDeployAddXml;
      await backend.startInstance('process1');

      expect(mockNetwork.sendData).toHaveBeenCalledWith(
        '192.168.1.1',
        '44444',
        '/process/process1/instance',
        'POST',
        'application/json',
        { variables: {} }
      );
    });
    it('throws when sending fails', async () => {
      mockNetwork.sendData.mockRejectedValueOnce('TEST ERROR');

      await expect(backend.startInstance('process1')).rejects.toBe('TEST ERROR');
    });
  });
  describe('deployProcess', () => {
    beforeEach(() => {
      mockMachines.push({
        id: 'def456',
        host: '192.168.2.1',
        port: '33029',
        status: 'CONNECTED',
      });

      mockProcesses = [
        {
          id: 'xyz789',
          name: 'Test',
          processConstraints: {
            hardConstraints: [],
            softConstraints: [],
          },
          taskConstraintMapping: {},
        },
      ];
    });
    describe('static', () => {
      beforeAll(() => {
        mockBPMN = machineMappingXml;
        mockHTMLMapping = { Task1: 'HTML' };
      });
      it('statically deploys process to all machines that were mapped to FlowNodes', async () => {
        await backend.deployProcess('xyz789', false);
        expect(mockNetwork.sendData).toHaveBeenCalledWith(
          mockMachines[0].host,
          mockMachines[0].port,
          '/process/xyz789',
          'PUT',
          'application/json',
          { bpmn: expect.stringContaining('deploymentMethod="static"') }
        );
        expect(mockNetwork.sendData).toHaveBeenCalledWith(
          '123.456.7.8',
          12345,
          '/process/xyz789',
          'PUT',
          'application/json',
          { bpmn: expect.stringContaining('proceed:deploymentMethod="static"') }
        );
      });
      it('reverts all succesful deployments if deploying to one machine fails and throws', async () => {
        mockNetwork.sendData.mockResolvedValueOnce('');
        mockNetwork.sendData.mockRejectedValueOnce('');

        await expect(backend.deployProcess('xyz789', false)).rejects.toEqual(
          new Error('check if all machines are available!')
        );

        expect(mockNetwork.sendRequest).toHaveBeenCalledTimes(1);
        expect(mockNetwork.sendRequest).toHaveBeenCalledWith(
          mockMachines[0].host,
          mockMachines[0].port,
          '/process/xyz789/',
          { method: 'DELETE' }
        );
      });
      it('sends user task to machine where user task is mapped to', async () => {
        mockHTMLMapping = {
          Task1: 'TEST HTML',
        };

        await backend.deployProcess('xyz789');

        expect(mockNetwork.sendData).toHaveBeenCalledWith(
          mockMachines[0].host,
          mockMachines[0].port,
          '/process/xyz789/user-tasks/Task1',
          'PUT',
          'application/json',
          { html: 'TEST HTML' }
        );
      });
    });

    describe('dynamic', () => {
      beforeEach(() => {
        mockFindOptimalExternalMachine.mockResolvedValueOnce({ engineList: [mockMachines[0].id] });
      });
      it('finds ids for all startEvents in process to deploy', async () => {
        await backend.deployProcess('xyz789', true);

        expect(mockFindOptimalExternalMachine).toHaveBeenCalledWith(
          {
            id: mockProcesses[0].id,
            name: mockProcesses[0].name,
            nextFlowNode: 'StartEvent_1',
          },
          { hardConstraints: [], softConstraints: [] },
          mockProcesses[0].processConstraints
        );
        expect(mockNetwork.sendData).toHaveBeenCalledWith(
          mockMachines[0].host,
          mockMachines[0].port,
          '/process/xyz789',
          'PUT',
          'application/json',
          { bpmn: expect.stringContaining('deploymentMethod="dynamic"') }
        );
      });
      it('sends user task to startMachine', async () => {
        mockHTMLMapping = {
          Task1: 'TEST HTML',
        };

        await backend.deployProcess('xyz789', true);

        expect(mockNetwork.sendData).toHaveBeenCalledWith(
          mockMachines[0].host,
          mockMachines[0].port,
          '/process/xyz789/user-tasks/Task1',
          'PUT',
          'application/json',
          { html: 'TEST HTML' }
        );
      });
    });
  });
  // describe('stopInstance', () => {
  //   it('sends request to stop an instance with a specific id', async () => {
  //     const { id } = mockMachines[0];

  //     await backend.stopInstance('process1', '1');

  //     expect(mockNetwork.sendData).toHaveBeenCalledWith(
  //       mockMachines[0].host,
  //       mockMachines[0].port,
  //       '/process/process1/instance/1/status',
  //       'PUT',
  //       'application/json',
  //       { status: 'Ended' }
  //     );
  //   });
  //});
});
